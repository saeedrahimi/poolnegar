package ir.saeedrahimi.poolnegar;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.viewpagerindicator.TitlePageIndicator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import ir.saeedrahimi.poolnegar.adapters.TransactionListAdapter;
import ir.saeedrahimi.poolnegar.application.helper.KeyHelper;
import ir.saeedrahimi.poolnegar.customs.TypeFaceProvider;
import ir.saeedrahimi.poolnegar.database.DAO.TransactionDAO;
import ir.saeedrahimi.poolnegar.database.model.Account;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
import ir.saeedrahimi.poolnegar.databinding.ActivityReportBillBinding;
import ir.saeedrahimi.poolnegar.dialogs.DialogTransactionDetails;
import ir.saeedrahimi.poolnegar.dialogs.ProgressLoading;
import ir.saeedrahimi.poolnegar.models.TransactionFilter;
import ir.saeedrahimi.poolnegar.ui.activities.cu.ActivityCU_Base;
import ir.saeedrahimi.poolnegar.ui.activities.cu.Activity_Transaction;

public class ReportBillActivity extends BasePremiumActivity<ActivityReportBillBinding> implements AdapterView.OnItemClickListener, DialogTransactionDetails.OnDialogClickListener, DialogTransactionDetails.OnTransactionDelete, DialogTransactionDetails.OnTransactionEdit, AbsListView.OnScrollListener, ViewPager.OnPageChangeListener {

    public LinearLayout mHeader;
    public View mPlaceHolderView;
    public int mHeaderHeight;
    public int mMinHeaderTranslation;
    public boolean mFirstAsync = true;
    public AccelerateDecelerateInterpolator mSmoothInterpolator;
    public LinearLayout inEmptyview;
    public LinearLayout outEmptyview;
    public ListView lvIn;
    public ListView lvOut;
    public LinearLayout inListLayout;
    cAdapter mAdapter;
    ViewPager viewPager;
    int[] accountIds;
    String selectedAccountId;
    String title;
    double beginBalance;
    ArrayAdapter<TransactionItem> inAdapter, outAdapter;
    List<TransactionItem> listInTrans;
    List<TransactionItem> listOutTrans;
    TransactionFilter filter;
    Account account;
    AsyncLoadDB async;
    private int visibleThreshold = 10;
    private int currentInPage = 0;
    private int currentOutPage = 0;
    private boolean loading = true;
    private int lastInListSize = 1;
    private int lastOutListSize = 1;
    private Integer loadType = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_report_bill);
        super.onCreate(savedInstanceState);


        mAdapter = new cAdapter(new String[]{"پرداخت ها", "دریافت ها"});
        viewPager = findViewById(R.id.bill_pager);
        viewPager.setAdapter(mAdapter);
        TitlePageIndicator mIndicator = findViewById(R.id.bill_pager_titles);
        mIndicator.setViewPager(viewPager);
        mIndicator.setCurrentItem(mAdapter.getCount() - 1);
        mIndicator.setOnPageChangeListener(this);
        Typeface typeface = TypeFaceProvider.get(this, "vazir.ttf");
        mIndicator.setTypeface(typeface);
        mIndicator.setSelectedColor(getResources().getColor(
                R.color.Artem));

        mSmoothInterpolator = new AccelerateDecelerateInterpolator();
        mHeaderHeight = getResources().getDimensionPixelSize(R.dimen.project_header_height);
        mMinHeaderTranslation = -mHeaderHeight + 70;
        mHeader = findViewById(R.id.llai1);

        Bundle localBundle = getIntent().getExtras();
        if (localBundle != null) {
            this.accountIds = localBundle.getIntArray("account");
            this.title = localBundle.getString("name");
        }
        TextView begbalance = findViewById(R.id.begbalance);

        begbalance.setText(new DecimalFormat(" ###,###.## ").format(this.beginBalance * -1));
        setActionBarTitle("گزارش حساب" + " - " + this.title);


        this.listInTrans = new ArrayList<TransactionItem>();
        this.listOutTrans = new ArrayList<TransactionItem>();

        this.inAdapter = new TransactionListAdapter(this, listInTrans, 0);
        this.outAdapter = new TransactionListAdapter(this, listOutTrans, 0);


        RefreshAll();
    }

    @Override
    protected void onActivityFirstInit(Bundle savedInstanceState) {
        initComponents();
    }

    private void initComponents() {

    }

    @Override
    protected ActivityReportBillBinding getViewBinding() {
        return ActivityReportBillBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void onActionBarRightToolClicked() {

    }

    @Override
    protected void onActionBarLeftToolClicked() {

    }

    @Override
    protected String getActionBarTitle() {
        return null;
    }

    @Override
    public void onItemClick(AdapterView<?> adp, View arg1, int position, long arg3) {
        if (position == 0)
            return;
        if (adp.equals(ReportBillActivity.this.lvIn))
            new DialogTransactionDetails(this, this, this, this, this, this.listInTrans.get(position - 1));
        else
            new DialogTransactionDetails(this, this, this, this, this, this.listOutTrans.get(position - 1));

    }

    @Override
    public void onDialogDeleteClick() {
        RefreshAll();
    }

    @Override
    public void onDialogDeleteTransaction() {

    }

    @Override
    public void onDialogEditTransaction(TransactionItem transaction) {
        Intent intent = new Intent(this, Activity_Transaction.class);
        intent.putExtra(KeyHelper.KEY_ENTITY, transaction);
        intent.putExtra(KeyHelper.KEY_ACTIVITY_CU_STATE, ActivityCU_Base.ACTIVITY_STATE_EDIT_MODE);
        startActivityForResult(intent, KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION) {
            if (resultCode == RESULT_OK) {
                RefreshAll();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void RefreshAll() {

        if (async != null && !async.isCancelled())
            async.cancel(true);
        async = new AsyncLoadDB();
        this.loadType = 2;
        this.lastInListSize = 1;
        this.lastOutListSize = 1;
        this.listInTrans.clear();
        this.listOutTrans.clear();
        async.execute(new Integer[]{this.currentInPage, this.currentOutPage, this.loadType});
    }

    public void LoadMore() {
        if (async != null && !async.isCancelled())
            async.cancel(true);
        async = new AsyncLoadDB();
        async.execute(new Integer[]{this.currentInPage, this.currentOutPage, this.loadType});
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        int scrollY = getScrollY((ListView) view);
        mHeader.setTranslationY(Math.max(-scrollY, mMinHeaderTranslation));
        if (((String) view.getTag()).contains("loadMore")) {
            int l = visibleItemCount + firstVisibleItem;
            if (l >= totalItemCount && !loading) {
                if (this.viewPager.getCurrentItem() == 1) {
                    this.loadType = 0;
                    this.currentInPage++;
                }
                if (this.viewPager.getCurrentItem() == 0) {
                    this.loadType = 1;
                    this.currentOutPage++;
                }
                loading = true;
                LoadMore();
            }
        }

    }

    public int getScrollY(ListView lv) {
        View c = lv.getChildAt(0);
        if (c == null) {
            return 0;
        }

        int firstVisiblePosition = lv.getFirstVisiblePosition();
        int top = c.getTop();

        int headerHeight = 0;
        if (firstVisiblePosition >= 1) {
            headerHeight = mPlaceHolderView.getHeight();
        }

        return -top + firstVisiblePosition * c.getHeight() + headerHeight;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        int scrollHeight = (int) (mHeader.getHeight() + mHeader.getTranslationY());


        if (position == 1) {
            if (scrollHeight == 0 && lvIn.getFirstVisiblePosition() >= 1) {
                return;
            }
            lvIn.setSelectionFromTop(1, scrollHeight);
        }
        if (position == 0) {
            if (scrollHeight == 0 && lvOut.getFirstVisiblePosition() >= 1) {
                return;
            }
            lvOut.setSelectionFromTop(1, scrollHeight);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    class AsyncLoadDB extends AsyncTask<Integer, List<TransactionItem>, List<TransactionItem>> {
        ProgressLoading p;

        public void execute(int[] parentId) {
        }

        @Override
        protected void onPreExecute() {
            if (ReportBillActivity.this.lastInListSize > 0 && ReportBillActivity.this.viewPager.getCurrentItem() == 1) {
                this.p = new ProgressLoading(ReportBillActivity.this, "");
            } else if (ReportBillActivity.this.lastOutListSize > 0 && ReportBillActivity.this.viewPager.getCurrentItem() == 0) {
                this.p = new ProgressLoading(ReportBillActivity.this, "");
            }
        }

        @Override
        protected List<TransactionItem> doInBackground(Integer... values) {

            int inPage = values[0] * ReportBillActivity.this.visibleThreshold;
            int outPage = values[1] * ReportBillActivity.this.visibleThreshold;
            int loadType = values[2];
            List<TransactionItem> temp_inlist = new ArrayList<TransactionItem>();
            List<TransactionItem> temp_outlist = new ArrayList<TransactionItem>();
            Log.i("inPage: ", inPage + "");
            Log.i("outPage: ", outPage + "");
            // check if all items loaded and just load needed page
            if (ReportBillActivity.this.lastInListSize > 0 && (loadType == 0 || loadType == 2)) {
                temp_inlist = TransactionDAO.getAccountReceives(selectedAccountId);
                ReportBillActivity.this.listInTrans.addAll(temp_inlist);
                ReportBillActivity.this.lastInListSize = temp_inlist.size();
            }
            if (ReportBillActivity.this.lastOutListSize > 0 && (loadType == 1 || loadType == 2)) {
                temp_outlist = TransactionDAO.getAccountPayments(selectedAccountId);
                ReportBillActivity.this.listOutTrans.addAll(temp_outlist);
                ReportBillActivity.this.lastOutListSize = temp_outlist.size();
            }
            if (ReportBillActivity.this.mFirstAsync == true) {
                ReportBillActivity.this.account = new Account();
                ReportBillActivity.this.account.setTotalIncome(TransactionDAO.getAccountBalance(selectedAccountId)); //TODO: was >  _db.getAccountsActivityBalance(ReportBillActivity.this.accountIds, 0, "9999/99/99","")
                ReportBillActivity.this.account.setTotalOutcome(1000); // TODO: Was > _db.getAccountsActivityBalance(ReportBillActivity.this.accountIds, 1, "9999/99/99", "")
            }
            Log.i("temp_inlist size: ", temp_inlist.size() + "");
            Log.i("temp_outlist size: ", temp_outlist.size() + "");

            return null;
        }

        @Override
        protected void onPostExecute(List<TransactionItem> result) {
            ReportBillActivity.this.mPlaceHolderView = getLayoutInflater().inflate(R.layout.project_header_placeholder, lvOut, false);
            if (ReportBillActivity.this.mFirstAsync == true) {
                ReportBillActivity.this.account = new Account();
                ReportBillActivity.this.account.setTotalIncome(1000);//TODO: was >  _db.getAccountsActivityBalance(ReportBillActivity.this.accountIds, 0, "9999/99/99","")
                ReportBillActivity.this.account.setTotalOutcome(1000);// TODO: Was > _db.getAccountsActivityBalance(ReportBillActivity.this.accountIds, 1, "9999/99/99", "")
                ReportBillActivity.this.lvOut.addHeaderView(mPlaceHolderView);
                ReportBillActivity.this.lvIn.addHeaderView(mPlaceHolderView);
                ReportBillActivity.this.lvIn.setAdapter(ReportBillActivity.this.inAdapter);
                ReportBillActivity.this.lvOut.setAdapter(ReportBillActivity.this.outAdapter);
                ReportBillActivity.this.mFirstAsync = false;
            }
            if (ReportBillActivity.this.lastInListSize > 0 && (ReportBillActivity.this.loadType == 0 || ReportBillActivity.this.loadType == 2)) {
                ReportBillActivity.this.inAdapter.notifyDataSetChanged();
            }
            if (ReportBillActivity.this.lastOutListSize > 0 && (ReportBillActivity.this.loadType == 1 || ReportBillActivity.this.loadType == 2)) {
                ReportBillActivity.this.outAdapter.notifyDataSetChanged();
            }
            if (ReportBillActivity.this.listInTrans == null || ReportBillActivity.this.listInTrans.size() <= 0) {
                View txtNoItem = getLayoutInflater().inflate(R.layout.blank_textview, null);
                ((TextView) txtNoItem.findViewById(R.id.blank_textview)).setText("موردی یافت نشد!");
                ReportBillActivity.this.inEmptyview.addView(txtNoItem);
            }
            if (ReportBillActivity.this.listOutTrans == null || ReportBillActivity.this.listOutTrans.size() <= 0) {
                View txtNoItem = getLayoutInflater().inflate(R.layout.blank_textview, null);
                ((TextView) txtNoItem.findViewById(R.id.blank_textview)).setText("موردی یافت نشد!");
                ReportBillActivity.this.outEmptyview.addView(txtNoItem);
            }

            TextView inbalance = findViewById(R.id.inbalance);
            TextView expbalance = findViewById(R.id.expbalance);
            TextView endbalance = findViewById(R.id.endbalance);
            double endBalance = (ReportBillActivity.this.beginBalance + ReportBillActivity.this.account.getTotalOutcome()) * -1;
            inbalance.setText(new DecimalFormat(" ###,###.## ").format(ReportBillActivity.this.account.getTotalIncome()));
            expbalance.setText(new DecimalFormat(" ###,###.## ").format(ReportBillActivity.this.account.getTotalOutcome()));
            endbalance.setText(new DecimalFormat(" ###,###.## ").format(endBalance));

            ImageView endimg = findViewById(R.id.endingbalancecolor);


            if (ReportBillActivity.this.lastInListSize > 0) {
                ReportBillActivity.this.lvIn.setTag("loadMore");
            } else if (loadType == 0 && !ReportBillActivity.this.lvIn.getTag().toString().contains("ended")) {
                if (listInTrans.size() > 0)
                    Crouton.makeText(ReportBillActivity.this,
                            "به انتهای لیست دریافتی ها رسیدید", Style.INFO).show();
                ReportBillActivity.this.lvIn.setTag("ended");
            }
            if (ReportBillActivity.this.lastOutListSize > 0) {
                ReportBillActivity.this.lvOut.setTag("loadMore");
            } else if (loadType == 1 && !ReportBillActivity.this.lvOut.getTag().toString().contains("ended")) {
                if (listOutTrans.size() > 0)
                    Crouton.makeText(ReportBillActivity.this,
                            "به انتهای لیست پرداختی ها رسیدید", Style.INFO).show();
                ReportBillActivity.this.lvOut.setTag("ended");
            }

            ReportBillActivity.this.loading = false;

            if (this.p != null)
                this.p.done();
            super.onPostExecute(result);

            ReportBillActivity.this.lvOut.setOnScrollListener(ReportBillActivity.this);
            ReportBillActivity.this.lvIn.setOnScrollListener(ReportBillActivity.this);
        }
    }

    class cAdapter extends PagerAdapter {

        private final String[] TITLES;

        public cAdapter(String[] paramArrayOfString) {
            this.TITLES = paramArrayOfString;
        }

        @Override
        public int getCount() {
            return this.TITLES.length;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return this.TITLES[position];
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater localLayoutInflater = getLayoutInflater();
            switch (position) {

                case 0:
                    View outLayout = localLayoutInflater.inflate(R.layout.fragment_transactions_listview, null);
                    ReportBillActivity.this.outEmptyview = outLayout.findViewById(R.id.emptyview);
                    ReportBillActivity.this.lvOut = outLayout.findViewById(R.id.listviewtransactions);
                    ReportBillActivity.this.lvOut.setTag("");
                    ReportBillActivity.this.lvOut.setOnItemClickListener(ReportBillActivity.this);
                    container.addView(outLayout, 0);
                    return outLayout;
                case 1:
                    View listLayoutOut = localLayoutInflater.inflate(R.layout.fragment_transactions_listview, null);
                    ReportBillActivity.this.inEmptyview = listLayoutOut.findViewById(R.id.emptyview);
                    ReportBillActivity.this.lvIn = listLayoutOut.findViewById(R.id.listviewtransactions);
                    ReportBillActivity.this.lvIn.setTag("");
                    ReportBillActivity.this.lvIn.setOnItemClickListener(ReportBillActivity.this);

                    container.addView(listLayoutOut, 0);
                    return listLayoutOut;
                default:
                    return null;
            }

        }


    }
}
