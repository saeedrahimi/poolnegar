package ir.saeedrahimi.poolnegar.application;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Handler;

import androidx.multidex.MultiDexApplication;
import co.ronash.pushe.Pushe;
import ir.saeedrahimi.poolnegar.Helper.TypefaceUtil;
import ir.saeedrahimi.poolnegar.database.AppDatabaseManager;

import java.util.Locale;

public class MainApplication extends MultiDexApplication {


    private static Context context;

    private static MainApplication singletonInstance;


    public MainApplication() {


    }

    public static MainApplication getInstance() {
        return singletonInstance;
    }

    public static Context getAppContext() {

        Context ctx = MainApplication.context;
        if (ctx == null) {
            ctx = GlobalParams.getApplicationActivityContext();
        }

        return ctx;
    }

    public static Handler getAppHandler() {

        Handler handler = new Handler(MainApplication.getAppContext().getMainLooper());
        if (handler == null) {
            handler = GlobalParams.getApplicationActivityHandler();
        }

        return handler;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        MainApplication.context = getApplicationContext();

        singletonInstance = this;

        AppDatabaseManager.initializeInstance();

        setDefaultLocale();

        setDefaultFont();

        Pushe.initialize(MainApplication.context, false);

    }

    private void setDefaultFont() {
        TypefaceUtil.overrideFont(MainApplication.context, "DEFAULT", "fonts/vazir.ttf");
        TypefaceUtil.overrideFont(MainApplication.context, "SANS_SERIF", "fonts/vazir.ttf");
        TypefaceUtil.overrideFont(MainApplication.context, "DEFAULT_BOLD", "fonts/vazir.ttf");
        TypefaceUtil.overrideFont(MainApplication.context, "SERIF", "fonts/vazir.ttf");
        TypefaceUtil.overrideFont(MainApplication.context, "MONOSPACE", "fonts/vazir.ttf");
        TypefaceUtil.overrideFont(MainApplication.context, "sDefaults", "fonts/vazir.ttf");
    }

    private void setDefaultLocale() {
        String languageToLoad = "en_US";
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, null);
    }

}
