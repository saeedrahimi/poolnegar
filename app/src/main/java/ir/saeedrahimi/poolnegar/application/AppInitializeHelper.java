package ir.saeedrahimi.poolnegar.application;


import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.database.model.Account;

public class AppInitializeHelper {
	
	
	public synchronized static Boolean initialApp() {
		
		initialBasicData();
		
		return true;
	}
	
	private synchronized static void initialBasicData() {
		
		if (GlobalParams.isInitialDataInserted() == false)
			initBasicData();
		
		
	}
	
	private static void initBasicData() {
		GlobalParams.setIsInitialDataInserted(true);
		initAllGlobalParamsMethods();
		//initOtherData
		seedAccounts();


	}

	private static void initAllGlobalParamsMethods() {
		GlobalParams.isInitialDataInserted();
		GlobalParams.getApplicationActivityContext();
	}

	private static void seedAccounts() {
		Account acc = new Account("هزینه‌ها","",0,"",false, true);
		acc.setAccID(Constants.ACCOUNTS.EXPENSE);
		AccountDAO.insert(acc, false);

		acc = new Account("درآمد","",0,"",true, false);
		acc.setAccID(Constants.ACCOUNTS.INCOME);
		AccountDAO.insert(acc, false);

		acc = new Account("بانک‌ها","",0,"",true, true);
		acc.setAccID(Constants.ACCOUNTS.Banks);
		AccountDAO.insert(acc, false);

		acc = new Account("طلب‌ها","",0,"",true, true);
		acc.setAccID(Constants.ACCOUNTS.BILLS);
		AccountDAO.insert(acc, false);

		acc = new Account("بدهی‌ها","",0,"",true, true);
		acc.setAccID(Constants.ACCOUNTS.DEBT);
		AccountDAO.insert(acc, false);

		acc = new Account("پروژه‌ها","",0,"",true, true);
		acc.setAccID(Constants.ACCOUNTS.PROJECTS);
		AccountDAO.insert(acc, false);

		acc = new Account("اشخاص","",0,"",true, true);
		acc.setAccID(Constants.ACCOUNTS.PERSONS);
		AccountDAO.insert(acc, false);

	}
	
	
}
