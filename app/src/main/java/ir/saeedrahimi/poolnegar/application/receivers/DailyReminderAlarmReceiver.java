package ir.saeedrahimi.poolnegar.application.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ir.saeedrahimi.poolnegar.application.helper.NotificationHelper;

public class DailyReminderAlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        NotificationHelper.checkAndCreateDailySubmitTransaction(context);

        //NotificationHelper.createDailySubmitVendorTeransactionReminderNotification(context, "tmb");
    }
}
