package ir.saeedrahimi.poolnegar.models;

import java.io.Serializable;

public class CheqItem
implements Serializable
{
private String date;
private int transId;

public String getDate()
{
  return this.date;
}

public void setTransId(int paramInt)
{
  this.transId = paramInt;
}

public void setDate(String date)
{
  this.date = date;
}

public int getTransId()
{
  return this.transId;
}
}