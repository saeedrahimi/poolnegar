package ir.saeedrahimi.poolnegar.models;

import java.util.ArrayList;

import ir.saeedrahimi.poolnegar.database.model.TransactionItem;

public class BillItem
  extends TransactionItem
{
	private int billCount;
	private double billAmount;
	private int payedCount;
	private double payedAmount;
	private String title;
	private double remainAmount;
	private ArrayList<Integer> projectsIds;
	public int getBillCount() {
		return billCount;
	}

	public void setBillCount(int billCount) {
		this.billCount = billCount;
	}

	public double getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(double billAmount) {
		this.billAmount = billAmount;
	}

	public int getPayedCount() {
		return payedCount;
	}

	public void setPayedCount(int payedCount) {
		this.payedCount = payedCount;
	}

	public double getPayedAmount() {
		return payedAmount;
	}

	public void setPayedAmount(double payedAmount) {
		this.payedAmount = payedAmount;
	}

	public String getTitle() {
		String dirTo = getTargetAccountTitle();
		
		
		return dirTo;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getRemainAmount() {
		return billAmount - payedAmount;
	}

	public void setRemainAmount(double remainAmount) {
		this.remainAmount = remainAmount;
	}

	public ArrayList<Integer> getProjectsIds() {
		return projectsIds;
	}

	public void setProjectsIds(ArrayList<Integer> projectsIds) {
		this.projectsIds = projectsIds;
	}
}
