package ir.saeedrahimi.poolnegar.models;

public class ProjectItem {
	public ProjectItem(String _id,String title,String desc,String salary,String dy,String dm,String dd) {
		super();
		this.setTitle(title);
		this.setSalary(salary);
		this.setDesc(desc);
		this.setDy(dy);
		this.setDm(dm);
		this.setDd(dd);
		this.set_id(_id);
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getSalary() {
		return salary;
	}
	public void setSalary(String salary) {
		this.salary = salary;
	}
	public String getDy() {
		return dy;
	}
	public void setDy(String dy) {
		this.dy = dy;
	}
	public String getDm() {
		return dm;
	}
	public void setDm(String dm) {
		this.dm = dm;
	}
	public String getDd() {
		return dd;
	}
	public void setDd(String dd) {
		this.dd = dd;
	}

	private String title;
	private String salary;
	private String desc;
	private String dy;
	private String dm;
	private String dd;
	private String _id;
	}
