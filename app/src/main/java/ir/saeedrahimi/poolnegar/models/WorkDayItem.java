package ir.saeedrahimi.poolnegar.models;

import ir.saeedrahimi.poolnegar.database.model.BaseModel;

public class WorkDayItem extends BaseModel {
    private String id;
    private String acID;
    private String transactionID;

    private String pID;
    private String projectID;
    private ProjectItem project;

    private double lackHour;
    private double overtime;
    private String workFaDate;
    private int workGeDate;
    private String regDate;
    private String regTime;
    private String info;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }


    public String getWorkFaDate() {
        return workFaDate;
    }

    public void setWorkFaDate(String workFaDate) {
        this.workFaDate = workFaDate;
    }

    public String getDateYear() {
        int i1 = workFaDate.indexOf("/");
        int i2 = workFaDate.lastIndexOf("/");
        return workFaDate.substring(0, i1);
    }

    public String getDateMonth() {
        int i1 = workFaDate.indexOf("/");
        int i2 = workFaDate.lastIndexOf("/");
        return workFaDate.substring(i1 + 1, i2);
    }

    public String getDateDay() {
        int i1 = workFaDate.indexOf("/");
        int i2 = workFaDate.lastIndexOf("/");
        return workFaDate.substring(i2 + 1, workFaDate.length());
    }


    public double getLackHour() {
        return lackHour;
    }

    public void setLackHour(double lackHour) {
        this.lackHour = lackHour;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getpID() {
        return pID;
    }

    public void setPersonId(String pID) {
        this.pID = pID;
    }

    public String getAcID() {
        return acID;
    }

    public void setAcID(String acID) {
        this.acID = acID;
    }


    public double getOvertime() {
        return overtime;
    }

    public void setOvertime(double overtime) {
        this.overtime = overtime;
    }
    public void incOvertime() {
        this.overtime = overtime+1;
    }
    public void decOvertime() {
        this.overtime = overtime-1;
        if(this.overtime<0)
            this.overtime=0;
    }




    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getRegTime() {
        return regTime;
    }

    public void setRegTime(String regTime) {
        this.regTime = regTime;
    }


    public String getProjectID() {
        return projectID;
    }

    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public ProjectItem getProject() {
        return project;
    }

    public void setProject(ProjectItem project) {
        this.project = project;
    }

    public int getWorkGeDate() {
        return workGeDate;
    }

    public void setWorkGeDate(int workGeDate) {
        this.workGeDate = workGeDate;
    }
}
