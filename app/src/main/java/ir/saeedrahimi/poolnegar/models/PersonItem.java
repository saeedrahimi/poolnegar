package ir.saeedrahimi.poolnegar.models;

import java.util.List;

import ir.saeedrahimi.poolnegar.database.model.BaseModel;

public class PersonItem extends BaseModel
{
	private String id;
	private String acID;
	private String billAcId;
	private double balance;
	private double hour;
	private double fee;
	private double overtime;
	private String date;
	private String closed;
	private String name;
	private String info;
	private String mobile;
	private String phone;
	private String email;
	private String accNumber;
	private String cardNumber;
	private boolean isWorker;
	private int order;
	private byte[] image;

	private boolean isPresent;
	private List<WorkDayItem> workDays;
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}


	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDateYear() {
		int i1 = date.indexOf("/");
		int i2 = date.lastIndexOf("/");
		return date.substring(0, i1);
	}
	public String getDateMonth() {
		int i1 = date.indexOf("/");
		int i2 = date.lastIndexOf("/");
		return date.substring(i1 + 1, i2);
	}
	public String getDateDay() {
		int i1 = date.indexOf("/");
		int i2 = date.lastIndexOf("/");
		return date.substring(i2 + 1, date.length());
	}

	public boolean isClosed() {
		if(this.closed != null)
			return closed.equals("Y");
		return false;
	}
	public void setClosed(String closed) {
		this.closed = closed;
	}

	public double getHour() {
		return hour;
	}

	public void setHour(double hour) {
		this.hour = hour;
	}


	public String getId() {
		return id;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public double getOvertime() {
		return overtime;
	}

	public void setOvertime(double overtime) {
		this.overtime = overtime;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAcID() {
		return acID;
	}

	public void setAcID(String acID) {
		this.acID = acID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<WorkDayItem> getWorkDays() {
		return workDays;
	}

	public void setWorkDays(List<WorkDayItem> workDays) {
		this.workDays = workDays;
	}
	public void addWorkDay(WorkDayItem workDay) {
		this.workDays.add(workDay);
	}


	public boolean isPresent() {
		return isPresent;
	}

	public void setPresent(boolean present) {
		isPresent = present;
	}

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccNumber() {
        return accNumber;
    }

    public void setAccNumber(String accNumber) {
        this.accNumber = accNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

	public boolean isWorker() {
		return isWorker;
	}

	public void setWorker(boolean worker) {
		isWorker = worker;
	}

	public String getBillAcId() {
		return billAcId;
	}

	public void setBillAcId(String billAcId) {
		this.billAcId = billAcId;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public void setImage(byte[] blob) {
		this.image = blob;

	}
	public byte[] getImage(){
		return this.image;
	}
}
