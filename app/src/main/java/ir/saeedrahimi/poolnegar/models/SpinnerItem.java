package ir.saeedrahimi.poolnegar.models;


public class SpinnerItem {

	String name;
	int id;
	
	

	public SpinnerItem(int id , String name) {
		super();
		this.name = name;
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getID() {
		return id;
	}
	public void setID(int id) {
		this.id = id;
	}
	
	
	
}
