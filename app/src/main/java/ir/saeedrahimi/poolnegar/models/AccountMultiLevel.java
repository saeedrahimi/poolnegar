package ir.saeedrahimi.poolnegar.models;


import com.multilevelview.models.RecyclerViewItem;

import java.util.ArrayList;
import java.util.List;

import ir.saeedrahimi.poolnegar.database.model.Account;

public class AccountMultiLevel extends RecyclerViewItem {

    public AccountMultiLevel(int level) {
        super(level);
    }

    public Account associatedObject;

    public AccountMultiLevel(int level, Account associatedObject) {
        this(level);
        this.associatedObject = associatedObject;


    }
    public void  addChild(AccountMultiLevel item){
        List<AccountMultiLevel> list = new ArrayList<>();
        list.add(item);
        this.addChildren(list);
    }

    public static List<AccountMultiLevel> PopulateChildren(int level, AccountMultiLevel root, List<Account> actualObjects)
    {
        List<AccountMultiLevel> itemList = new ArrayList<>();
        List<AccountMultiLevel> childList = new ArrayList<>();
        for (Account object : actualObjects) {
            AccountMultiLevel ourNode = new AccountMultiLevel(level+1, object);
            if( root != null && root.associatedObject != null && object.getParentId().equals(root.associatedObject.getAccID())){
                List<AccountMultiLevel> children = PopulateChildren(level+1,ourNode,actualObjects);
                childList.addAll(children);

            }

        }
        root.addChildren(childList);
        itemList.add(root);
        return itemList;

    }

}
