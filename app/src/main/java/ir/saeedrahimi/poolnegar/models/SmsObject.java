package ir.saeedrahimi.poolnegar.models;


import java.io.Serializable;

public class SmsObject implements Serializable {
    private String AccountNumber;

    private String Amount;

    private String BankName;

    private String Content;

    private String Date;

    private String Description;

    private int ID;

    private String Time;

    private int Type;

    private int transactionId;

    public String getAccountNumber() { return this.AccountNumber; }

    public String getAmount() { return this.Amount; }

    public String getBankName() { return this.BankName; }

    public String getContent() { return this.Content; }

    public String getDate() { return this.Date; }

    public String getDescription() { return this.Description; }

    public int getID() { return this.ID; }

    public String getTime() { return this.Time; }

    public int getTransactionId() { return this.transactionId; }

    public int getType() { return this.Type; }

    public void setAccountNumber(String paramString) { this.AccountNumber = paramString; }

    public void setAmount(String paramString) { this.Amount = paramString; }

    public void setBankName(String paramString) { this.BankName = paramString; }

    public void setContent(String paramString) { this.Content = paramString; }

    public void setDate(String paramString) { this.Date = paramString; }

    public void setDescription(String paramString) { this.Description = paramString; }

    public void setID(int paramInt) { this.ID = paramInt; }

    public void setTime(String paramString) { this.Time = paramString; }

    public void setTransactionId(int paramInt) { this.transactionId = paramInt; }

    public void setType(int paramInt) { this.Type = paramInt; }
}
