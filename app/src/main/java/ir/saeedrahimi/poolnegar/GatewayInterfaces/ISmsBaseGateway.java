package ir.saeedrahimi.poolnegar.GatewayInterfaces;

import ir.saeedrahimi.poolnegar.models.SmsObject;

public interface ISmsBaseGateway extends IBaseGateWay<SmsObject> {
    void clearType(int paramInt);

    int numberOFNotSubmittedSMS();

    boolean updateType(int paramInt1, int paramInt2);
}
