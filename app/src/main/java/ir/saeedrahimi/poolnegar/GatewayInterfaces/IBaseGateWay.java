package ir.saeedrahimi.poolnegar.GatewayInterfaces;

import java.util.List;

/**
 * Created by Saeed on 3/19/2017.
 */

public interface IBaseGateWay<T>
{
    void create();

    void deleteById(int paramInt);

    List<T> getAll();

    List<Integer> getAllDistinctIconTemplates();

    T getByID(int paramInt);

    List<T> getObjectsWithWhereClause(String paramString);

    String getTableName();

    T insert(T paramT)
            throws IllegalArgumentException, IllegalAccessException;

    boolean update(T paramT)
            throws IllegalArgumentException, IllegalAccessException;
}
