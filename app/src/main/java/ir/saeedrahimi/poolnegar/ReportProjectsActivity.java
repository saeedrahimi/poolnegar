package ir.saeedrahimi.poolnegar;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.languages.ArabicLigaturizer;
import com.itextpdf.text.pdf.languages.LanguageProcessor;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import androidx.appcompat.widget.Toolbar;
import ir.saeedrahimi.poolnegar.adapters.ReportProjectsListAdapter;
import ir.saeedrahimi.poolnegar.customs.HtmlUtils;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.database.model.Account;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
import ir.saeedrahimi.poolnegar.dialogs.ProgressLoading;

public class ReportProjectsActivity extends BaseFragmentActivity implements OnItemClickListener, View.OnClickListener {

	ReportProjectsListAdapter mAdapter;
	List<Account> mProjects;
	public LinearLayout mEmptyview;
	public ListView mListView;
	String parentId = Constants.ACCOUNTS.PROJECTS;
	private boolean hideClosed;
	ArrayList<Account> listAcc;
	LinkedHashMap<Account,ArrayList<Account>> hashAcc;
	Button btnDone,btnCheck;
	private LanguageProcessor mPersian;
	
	public void cancel(View paramView) {
		dispatchKeyEvent(new KeyEvent(0, 4));
		dispatchKeyEvent(new KeyEvent(1, 4));
	}

	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setContentView(R.layout.report_projects);

		mToolBar = (Toolbar) findViewById(R.id.tool_bar);
		prepareToolbar(mToolBar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		this.mProjects = new ArrayList<Account>();
		mListView = (ListView) findViewById(R.id.listviewtransactions);
		mListView.setTag("");
		mListView.setOnItemClickListener(this);

		mPersian = new ArabicLigaturizer();
		btnDone= (Button) findViewById(R.id.btnDone);
		btnCheck = (Button) findViewById(R.id.btnCheck);
		btnDone.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				createHTML();
			}
		});
		btnCheck.setOnClickListener(this);
		//asyncLoadData();

		fillProjectsList();
	}
	private void fillProjectsList() {
		mProjects.addAll(AccountDAO.getProjects(false));
		this.mAdapter = new ReportProjectsListAdapter(this,mProjects,0);
		mListView.setAdapter(mAdapter);
		mAdapter.notifyDataSetChanged();
	}

	public void test() {
		String state = Environment.getExternalStorageState();

		if (!Environment.MEDIA_MOUNTED.equals(state)) {
			return;
		}
		File outFile = new File(Environment.getExternalStorageDirectory(), "/PoolNegarData/report/Projects_Report.pdf");

		File exportdir = new File(Environment.getExternalStorageDirectory() + "/PoolNegarData/report");
		if (!exportdir.exists()) {
			exportdir.mkdirs();
		}
		createPDF(outFile);


	}
	private String createHTML (){

		StringBuilder buf = new StringBuilder();

		DecimalFormat df = new DecimalFormat(" ###,###.## ");

		try {
			buf.append("<table class='table'>" +
					"<tr>" +
					"<th>تاریخ</th>" +
					"<th>عنوان</th>" +
					"<th>بودجه</th>" +
					"<th>دریافتی</th>" +
					"<th>هزینه</th>" +
					"<th>سود</th>" +
					"</tr>");

			double totalSud = 0;

			this.hashAcc = new LinkedHashMap<Account,ArrayList<Account>>();
			String isClosed = hideClosed ? "N" : "Y";


			// TODO: Was:
//			this.hashAcc = this._db.getChildBalances(parentId, "9999/99/99", "", 1,isClosed);
			//this.hashAcc = TransactionDAO.getAccountBalance(parentId);
			this.listAcc = new ArrayList<>();

			int i=-1;
			for (Iterator<Account> iterator = hashAcc.keySet().iterator(); iterator.hasNext();) {
				Account acc = iterator.next();
				// TODO: Fix these values
//				acc.setTotalIncome(_db.getProjectIncomeBalance(acc.getAccID(), "", "9999/99/99"));
//				acc.setTotalOutcome(_db.getProjectOutcomeBalance(acc.getAccID(), "", "9999/99/99"));
				i++;
				if(mAdapter.mCheckStates.get(i)!=true) continue;



				this.listAcc.add(acc);
				double endBalance= (acc.getBalance()+acc.getTotalOutcome())*-1;
				totalSud += endBalance;
				buf.append("<tr><td>")
						.append(acc.getProjectDate())
						.append("</td><td>")
						.append(acc.getTitle())
						.append("</td><td>")
						.append(df.format(acc.getBalance()*-1))
						.append("</td><td>")
						.append(df.format(acc.getTotalIncome()))
						.append("</td><td>")
						.append(df.format(acc.getTotalOutcome()))
						.append("</td><td>")
						.append(df.format(endBalance))
						.append("</td></tr>");

			}
			buf.append("<tr><td>")
					.append("")
					.append("</td><td>")
					.append("")
					.append("</td><td>")
					.append("")
					.append("</td><td>")
					.append("")
					.append("</td><td>")
					.append("جمع سود")
					.append("</td><td>")
					.append( df.format(totalSud))
					.append("</td></tr>");
			buf.append("</table>" +
					"<p class='copyright'> نرم افزار حسابداری همراه پـولنـگار - "+ getResources().getString(ir.saeedrahimi.poolnegar.R.string.app_version)+
							"</p>");
			String table = buf.toString();
			String body = HtmlUtils.createElement("body",null,table);
			String html = HtmlUtils.getHtml("report.css",body);
			Intent intent = new Intent(this, ReportViewActivity.class);
			intent.putExtra("html", html);
			intent.putExtra("chart", new int[]{1,2});
			startActivity(intent);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			return buf.toString();
		}
	}
	private void createPDF (File pdfFile){

		Document doc = new Document();
		PdfWriter docWriter = null;

		DecimalFormat df = new DecimalFormat(" ###,###.## ");

		try {

			BaseFont urName = BaseFont.createFont("assets/fonts/vazir.ttf", BaseFont.IDENTITY_H,BaseFont.EMBEDDED);
			Font fHeader = new Font(urName, 18, Font.BOLD, new BaseColor(0, 0, 0));
			Font bYekanBold = new Font(urName, 12, Font.BOLD, new BaseColor(0, 0, 0));
			Font bYekanReg = new Font(urName, 10);
			Font bf12 = new Font(FontFamily.COURIER, 12);

			docWriter = PdfWriter.getInstance(doc , new FileOutputStream(pdfFile));

			//document header attributes
			doc.addAuthor("PoolNegar");
			doc.addCreationDate();
			doc.addProducer();
			doc.addCreator("Saeedr.com");
			doc.addTitle("گزارش تمامی پروژه ها");
			doc.setPageSize(PageSize.LETTER);

			//open document
			doc.open();

			//create a paragraph
			Paragraph paragraph = new Paragraph(mPersian.process("کزارش تمامی پروژه ها"),fHeader);

			paragraph.setAlignment(Paragraph.ALIGN_CENTER);
			//specify column widths
			float[] columnWidths = {2.5f, 2.5f, 2f, 2f, 2.8f, 2f};
			//create PDF table with the given widths
			PdfPTable table = new PdfPTable(columnWidths);
			// set table width a percentage of the page width
			table.setWidthPercentage(95f);

			//insert column headings
			insertCell(table, "سود", Element.ALIGN_CENTER, 1, bYekanBold);
			insertCell(table, "هزینه", Element.ALIGN_CENTER, 1, bYekanBold);
			insertCell(table, "دریافتی", Element.ALIGN_CENTER, 1, bYekanBold);
			insertCell(table, "بودجه", Element.ALIGN_CENTER, 1, bYekanBold);
			insertCell(table, "عنوان", Element.ALIGN_CENTER, 1, bYekanBold);
			insertCell(table, "تاریخ" , Element.ALIGN_CENTER, 1, bYekanBold);
			table.setHeaderRows(1);

			double totalSud = 0;
			
		    this.hashAcc = new LinkedHashMap<Account,ArrayList<Account>>();
		    String isClosed = hideClosed ? "N" : "Y";
		    
		    
		    // TODO: Fix This
		   // this.hashAcc = this._db.getChildBalances(parentId, "9999/99/99", "", 1,isClosed);
		    this.listAcc = new ArrayList<>();
		    
		    for (Iterator<Account> iterator = hashAcc.keySet().iterator(); iterator.hasNext();) {
				Account acc =  iterator.next();
				// TODO: Fix This
//		    	acc.setTotalIncome(_db.getProjectIncomeBalance(acc.getAccID(), "", "9999/99/99"));
//		    	acc.setTotalOutcome(_db.getProjectOutcomeBalance(acc.getAccID(), "", "9999/99/99"));
//		    	Log.d("FragmentProject: '"+ acc.getTitle() +"-"+acc.getAccID()+"' income balance",acc.getTotalIncome()+"");
//		    	Log.d("FragmentProject: '"+ acc.getTitle() +"-"+acc.getAccID()+"' outcome balance",acc.getTotalOutcome()+"");

		    	this.listAcc.add(acc);
		    	
		    	double endBalance= (acc.getBalance()+acc.getTotalOutcome())*-1;
		    	totalSud += endBalance;
		    	insertCell(table, df.format(endBalance) , Element.ALIGN_LEFT, 1, bYekanReg);
		    	insertCell(table, df.format(acc.getTotalOutcome()) , Element.ALIGN_LEFT, 1, bYekanReg);
		    	insertCell(table, df.format(acc.getTotalIncome()) , Element.ALIGN_LEFT, 1, bYekanReg);
				insertCell(table, df.format(acc.getBalance()*-1) , Element.ALIGN_LEFT, 1, bYekanReg);
				insertCell(table, acc.getTitle(), Element.ALIGN_RIGHT, 1, bYekanReg);
				insertCell(table, acc.getProjectDate(), Element.ALIGN_LEFT, 1, bYekanReg);
				
			}

			insertCell(table, df.format(totalSud), Element.ALIGN_LEFT, 2, bYekanReg);
			insertCell(table, "جمع سود", Element.ALIGN_RIGHT, 4, bYekanBold);


			paragraph.add(table);
			doc.add(paragraph);
			
			Paragraph p2 = new Paragraph(mPersian.process("نرم افزار حسابداری همراه پـول نـگار - "+ ir.saeedrahimi.poolnegar.R.string.app_version ),bYekanReg);
			
			doc.add(p2);

		}
		catch (DocumentException dex)
		{
			dex.printStackTrace();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		finally
		{
			if (doc != null){
				//close the document
				doc.close();
			}
			if (docWriter != null){
				//close the writer
				docWriter.close();
			}
		}
	}
	private void insertCell(PdfPTable table, String text, int align, int colspan, Font font){
		
		PdfPCell cell = new PdfPCell(new Phrase(mPersian.process(text.trim()), font));
		cell.setHorizontalAlignment(align);
		cell.setColspan(colspan);
		if(text.trim().equalsIgnoreCase("")){
			cell.setMinimumHeight(10f);
		}
		cell.setMinimumHeight(22f);
		table.addCell(cell);

	}
	public void asyncLoadData(){
		ReportProjectsActivity.this.setToolBarTitle("پروژه ها", "");
		AsyncPrintData async=new AsyncPrintData();
    	async.execute(new String[]{parentId , "", "" });
	}
	boolean checked =true;
	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.btnCheck:
				int itt =mProjects.size();
				for(int i=0; i<itt; i++){
					View item = mListView.getChildAt(i);
					mAdapter.toggle(i);

					if(item==null) continue;
					CheckBox chk = ((CheckBox)item.findViewById(R.id.checkBox));
					chk.setChecked(mAdapter.isChecked(i));
				}
				break;
		}
		return;
	}

	class AsyncPrintData extends AsyncTask<String, List<TransactionItem>, List<TransactionItem>> {
		ProgressLoading p;

		public void execute(String parentId) {
		}

		@Override
		protected void onPreExecute() {
			this.p = new ProgressLoading(ReportProjectsActivity.this,"");
	
		}

		@Override
		protected List<TransactionItem> doInBackground(String... values) {

			String loadType = values[2];
			
			String state = Environment.getExternalStorageState();

			if (!Environment.MEDIA_MOUNTED.equals(state)) {
				return null;
			}
			File outFile = new File(Environment.getExternalStorageDirectory(), "/PoolNegarData/report/Projects_Report.pdf");

			File exportdir = new File(Environment.getExternalStorageDirectory() + "/PoolNegarData/report");
			if (!exportdir.exists()) {
				exportdir.mkdirs();
			}
			ReportProjectsActivity.this.createPDF(outFile);
			Intent target = new Intent(Intent.ACTION_VIEW);
			target.setDataAndType(Uri.fromFile(outFile),"application/pdf");
			target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			Intent intent = Intent.createChooser(target, "Open File");
			try {
			    startActivity(intent);
			} catch (ActivityNotFoundException e) {
			    // Instruct the user to install a PDF reader here, or something
			} 
			return null;
		}

		@Override
		protected void onPostExecute(List<TransactionItem> result) {	
			
			if(this.p != null){
				this.p.done();
				ReportProjectsActivity.this.finish();
			}
			super.onPostExecute(result);
		}
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//super.onCreateOptionsMenu(menu);
		//getMenuInflater().inflate(R.menu.main_checkcancel, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		if (item != null && item.getItemId() == android.R.id.home) {
			finish();
		}
		if (item != null && item.getItemId() == R.id.maincancel) {
			finish();
		}
		if (item != null && item.getItemId() == R.id.mainaccept) {
			
		}

		return false;
	}

}
