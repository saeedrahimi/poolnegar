package ir.saeedrahimi.poolnegar.Gateways;

/**
 * Created by Saeed on 3/19/2017.
 */

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ir.saeedrahimi.poolnegar.GatewayInterfaces.ISmsBaseGateway;
import ir.saeedrahimi.poolnegar.models.SmsObject;

public class SmsGateway
        implements ISmsBaseGateway {


    private ContentValues toContentValues(SmsObject item) {
        ContentValues values = new ContentValues();
        try {
            values.put("ID", item.getID());
            values.put("Content", item.getContent());
            values.put("Type", item.getType());
            values.put("Date", item.getDate());
            values.put("Time", item.getTime());
            values.put("AccountNumber", item.getAccountNumber());
            values.put("Description", item.getDescription());
            values.put("Amount", item.getAmount());
            values.put("BankName", item.getBankName());
            values.put("TransactionId", item.getTransactionId());
            return values;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return values;
    }

    public void create() {
        //this._connection.execQuery("CREATE TABLE if not exists " + getTableName() + " (ID INTEGER PRIMARY KEY AUTOINCREMENT,Content TEXT,Type INTEGER,Date TEXT,Time TEXT,AccountNumber TEXT,Description TEXT,Amount TEXT,BankName TEXT, TransactionId INTEGER )");
    }

    public void deleteById(int id) {
        String str = "DELETE FROM " + getTableName() + " WHERE ID=?";
        String[] params = new String[1];
        params[0] = Integer.toString(id);
        // TODO: Fix Here
        //db.execSQL(str, params);
    }

    public List<SmsObject> getAll() {
        ArrayList list = new ArrayList();

        SQLiteDatabase sqlDb = null;
        try {
            Cursor cursor = sqlDb.rawQuery("SELECT * FROM " + getTableName() + ";", new String[0]);
            if (cursor.moveToFirst()) {
                do {
                    SmsObject item = new SmsObject();
                    item.setID(cursor.getInt(cursor.getColumnIndex("ID")));
                    item.setContent(cursor.getString(cursor.getColumnIndex("Content")));
                    item.setType(cursor.getInt(cursor.getColumnIndex("Type")));
                    item.setDate(cursor.getString(cursor.getColumnIndex("Date")));
                    item.setTime(cursor.getString(cursor.getColumnIndex("Time")));
                    item.setAccountNumber(cursor.getString(cursor.getColumnIndex("AccountNumber")));
                    item.setDescription(cursor.getString(cursor.getColumnIndex("Description")));
                    item.setAmount(cursor.getString(cursor.getColumnIndex("Amount")));
                    item.setBankName(cursor.getString(cursor.getColumnIndex("BankName")));
                    item.setTransactionId(cursor.getInt(cursor.getColumnIndex("TransactionId")));

                    list.add(item);
                } while (cursor.moveToNext());
            }
            sqlDb.close();
        } catch (Exception localException) {
            if (sqlDb != null && sqlDb.isOpen())
                sqlDb.close();
        }
        return list;
    }

    @Override
    public List<Integer> getAllDistinctIconTemplates() {
        return null;
    }

    public SmsObject getByID(int id) {
        List<SmsObject> list = getObjectsWithWhereClause("ID = " + id);
        if (list.size() > 0)
            return list.get(0);
        return null;
    }

    @Override
    public List<SmsObject> getObjectsWithWhereClause(String where) {
        ArrayList list = new ArrayList();
        SQLiteDatabase sqlDb = null;
        try {
            Cursor cursor = sqlDb.rawQuery("SELECT * FROM " + getTableName() + " WHERE " + where, null);
            if (cursor.moveToFirst()) {
                do {
                    SmsObject item = new SmsObject();
                    item.setID(cursor.getInt(cursor.getColumnIndex("ID")));
                    item.setContent(cursor.getString(cursor.getColumnIndex("Content")));
                    item.setType(cursor.getInt(cursor.getColumnIndex("Type")));
                    item.setDate(cursor.getString(cursor.getColumnIndex("Date")));
                    item.setTime(cursor.getString(cursor.getColumnIndex("Time")));
                    item.setAccountNumber(cursor.getString(cursor.getColumnIndex("AccountNumber")));
                    item.setDescription(cursor.getString(cursor.getColumnIndex("Description")));
                    item.setAmount(cursor.getString(cursor.getColumnIndex("Amount")));
                    item.setBankName(cursor.getString(cursor.getColumnIndex("BankName")));
                    item.setTransactionId(cursor.getInt(cursor.getColumnIndex("TransactionId")));
                    list.add(item);
                } while (cursor.moveToNext());
            }
            sqlDb.close();
        } catch (Exception localException) {
            if (sqlDb != null && sqlDb.isOpen())
                sqlDb.close();
        }
        return list;
    }


    public List<SmsObject> getByDate(String date) {
        List<SmsObject> list = getObjectsWithWhereClause("date = '" + date + "'");
        return list;
    }

    public String getTableName() {
        return "Sms";
    }

    @Override
    public SmsObject insert(SmsObject sms) {

        //sms.setID((int) this._connection.insert(toContentValues(sms), getTableName()));
        return sms;
    }

    @Override
    public boolean update(SmsObject sms) {
        String table = getTableName();
        ContentValues values = toContentValues(sms);
        String[] params = new String[1];
        params[0] = (sms.getID() + "");
        //db.update(table, values, "ID=?", params);
        return true;
    }

    @Override
    public void clearType(int paramInt) {
        StringBuilder query = new StringBuilder();
        query.append("DELETE FROM ");
        query.append(getTableName());
        query.append(" WHERE Type = ?");
        // TODO: Fix Here
//        db.execSQL(query.toString(), new String[]{Integer.toString(paramInt)});

    }

    @Override
    public int numberOFNotSubmittedSMS() {
        StringBuilder query = new StringBuilder();
        query.append("SELECT count(*) from ");
        query.append(getTableName());
        query.append(" where Type = 0 OR Type = 1");
        // TODO: Fix Here
//        Cursor cursor = db.rawQuery(query.toString(), null);
//        cursor.moveToFirst();
//        return cursor.getInt(0);
        return 0;
    }

    @Override
    public boolean updateType(int smsId, int newType) {

        ContentValues contentValues = new ContentValues();
        contentValues.put("Type", Integer.valueOf(newType));
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(smsId);
        //db.update(getTableName(), contentValues, "id=?", new String[]{stringBuilder.toString()});
        return true;
    }

    public boolean isUnique(String paramString) {
        //DbHelper db = this._connection;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT * FROM ");
        stringBuilder.append(getTableName());
        stringBuilder.append(" WHERE Content = '");
        stringBuilder.append(paramString);
        stringBuilder.append("'");
        // TODO: Fix Here
//        return db.rawQuery(stringBuilder.toString(), new String[0]).moveToFirst() ^ true;
        return false;
    }
}