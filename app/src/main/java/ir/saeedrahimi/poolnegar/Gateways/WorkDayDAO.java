package ir.saeedrahimi.poolnegar.Gateways;

/**
 * Created by Saeed on 3/19/2017.
 */

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.MainApplication;
import ir.saeedrahimi.poolnegar.application.helper.L;
import ir.saeedrahimi.poolnegar.application.persian.calendar.util.PersianCalendarUtils;
import ir.saeedrahimi.poolnegar.database.AppDatabaseManager;
import ir.saeedrahimi.poolnegar.models.PersonItem;
import ir.saeedrahimi.poolnegar.models.WorkDayItem;

public class WorkDayDAO {

    public static String getCreateTable() {
        return MainApplication.getAppContext().getString(R.string.sql_PersonWorkDay_CreateTable);
    }

    public static String getDropTable() {
        return MainApplication.getAppContext().getString(R.string.sql_PersonWorkDay_DropTable);
    }
    public static void create() {
        AppDatabaseManager.getInstance().executeQuery(database -> {
            try {

                String createCommand = MainApplication.getAppContext().getString(R.string.sql_PersonWorkDay_CreateTable);
                database.execSQL(createCommand);
                return true;
            } catch (Exception e) {

                String msg = "WorkDayDAO - create : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });

    }

    public static void deleteById(String id) {
        AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        id
                };

                String deleteCommand = MainApplication.getAppContext().getString(R.string.sql_PersonWorkDay_DeleteById);
                database.execSQL(deleteCommand, bindArgs);
                return true;
            } catch (Exception e) {

                String msg = "WorkDayDAO - deleteById : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
    }

    public static List<WorkDayItem> getAll() {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String query = MainApplication.getAppContext().getString(R.string.sql_Person_SelectAll);

                Cursor cursor = database.rawQuery(query, null);

                List<WorkDayItem> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "WorkDayDAO - selectAll : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return (List<WorkDayItem>) result;
    }
    public static List<WorkDayItem> getAllWhere(String where) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String query = MainApplication.getAppContext().getString(R.string.sql_PersonWorkDay_SelectAll);
                String orderPart = MainApplication.getAppContext().getString(R.string.sql_PersonWorkDay_Order_Part);
                query += where + orderPart;
                Cursor cursor = database.rawQuery(query, null);

                List<WorkDayItem> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "WorkDayDAO - getAllWhere : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return (List<WorkDayItem>) result;
    }

    public static WorkDayItem getById(String id) {
        List<WorkDayItem> list = getAllWhere("PersonId = " + id);
        if (list.size() > 0)
            return list.get(0);
        return null;
    }

    public static List<WorkDayItem> getAllProjects(String personId, int page) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                ArrayList<WorkDayItem> dataList = new ArrayList<>();
                String[] bindArgs = {
                        String.valueOf(personId),
                        String.valueOf(page),
                };

                String query = MainApplication.getAppContext().getString(R.string.sql_PersonWorkDay_Projects);
                Cursor cursor = database.rawQuery(query, bindArgs);

                if (cursor != null) {
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        WorkDayItem item = new WorkDayItem();
                        item.setProjectID(cursor.getString(cursor.getColumnIndex("ProjectID")));
                        item.setPersonId(cursor.getString(cursor.getColumnIndex("PersonID")));
                        item.setInfo(cursor.getString(cursor.getColumnIndex("ProjectTitle")));
                        item.setOvertime(cursor.getDouble(cursor.getColumnIndex("Amount")));
                        item.setLackHour(cursor.getDouble(cursor.getColumnIndex("Projects")));
                        dataList.add(item);
                        cursor.moveToNext();
                    }
                }
                closeCursor(cursor);
                return dataList;
            } catch (Exception e) {

                String msg = "WorkDayDAO - getAllProjects : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });
        return (List<WorkDayItem>) result;

    }

    public static List<WorkDayItem> getAllDays(String id) {
        return getAllWhere("PersonID = " + id);
    }

    public static List<WorkDayItem> getAllDays(String id, int page) {
        return getAllWhere("PersonID = " + id + "  ORDER BY WorkFaDate DESC LIMIT 10 OFFSET " + page);
    }

    public static int getWorkerDaysCount(String id) {
        List<WorkDayItem> list = getAllWhere("PersonID = " + id);
        if (list != null && list.size() > 0)
            return list.size();
        else
            return 0;
    }

    public static long getWorkerOvertimeHours(String accountId) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        String.valueOf(accountId)
                };

                String query = MainApplication.getAppContext().getString(R.string.sql_PersonWorkDay_Total_Overtime);
                Cursor cursor = database.rawQuery(query, bindArgs);

                long total = cursor.getLong(0);

                closeCursor(cursor);

                return total;
            } catch (Exception e) {

                String msg = "WorkDayDAO - getWorkerOvertimeHours : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });
        return (result == null) ? 0 : (long) result;

    }

    public static String getWorkerOvertimeHours(String id, boolean format) {
        long hours = getWorkerOvertimeHours(id);

        if (hours == 0)
            return "ندارد";
        if (hours < 0)
            return Math.abs(hours) + "-" + " ساعت";
        return hours + " ساعت";
    }

    public static List<WorkDayItem> getByDate(String date) {
        List<WorkDayItem> list = getAllWhere("WorkFaDate = '" + date + "'");
        return list;
    }

    public static WorkDayItem getByDate(String id, String date) {
        List<WorkDayItem> list = getAllWhere("PersonID = " + id + " AND WorkFaDate = '" + date + "'");
        if (list.size() > 0)
            return list.get(0);
        return null;
    }


    public static WorkDayItem insert(final WorkDayItem dayItem) {
        WorkDayItem result;
        result = (WorkDayItem) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {

                String[] bindArgs = {
                        String.valueOf(dayItem.getId()),
                        String.valueOf(dayItem.getpID()),
                        String.valueOf(dayItem.getTransactionID()),
                        String.valueOf(dayItem.getProjectID()),
                        String.valueOf(dayItem.getOvertime()),
                        String.valueOf(dayItem.getLackHour()),
                        String.valueOf(dayItem.getWorkFaDate()),
                        String.valueOf(dayItem.getWorkGeDate()),
                        String.valueOf(dayItem.getInfo()),
                        String.valueOf(dayItem.getCreatedAt()),
                        String.valueOf(dayItem.getUpdatedAt()),
                        String.valueOf(dayItem.getEditor()),
                        String.valueOf(dayItem.getEditorDeviceId()),
                        String.valueOf(dayItem.getTag()),
                        String.valueOf("")
                };
                database.execSQL(MainApplication.getAppContext().getString(R.string.sql_PersonWorkDay_Insert), bindArgs);
                return dayItem;
            } catch (Exception e) {

                String msg = "WorkDayDAO - insert : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return result;
    }

    public static boolean update(final WorkDayItem dayItem) {

        Boolean result;
        result = (Boolean) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        String.valueOf(dayItem.getpID()),
                        String.valueOf(dayItem.getTransactionID()),
                        String.valueOf(dayItem.getProjectID()),
                        String.valueOf(dayItem.getOvertime()),
                        String.valueOf(dayItem.getLackHour()),
                        String.valueOf(dayItem.getWorkFaDate()),
                        String.valueOf(dayItem.getWorkGeDate()),
                        String.valueOf(dayItem.getInfo()),
                        String.valueOf(dayItem.getCreatedAt()),
                        String.valueOf(dayItem.getUpdatedAt()),
                        String.valueOf(dayItem.getEditor()),
                        String.valueOf(dayItem.getEditorDeviceId()),
                        String.valueOf(dayItem.getTag()),
                        String.valueOf(""),
                        String.valueOf(dayItem.getId())
                };
                database.execSQL(MainApplication.getAppContext().getString(R.string.sql_PersonWorkDay_Update), bindArgs);
                return true;
            } catch (Exception e) {

                String msg = "WorkDayDAO - update : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return result;
    }


    public static boolean isPresent(PersonItem worker, String date) {
        List<WorkDayItem> days = getAllDays(worker.getId());
        for (WorkDayItem wd : days) {
            if ( PersianCalendarUtils.IsEqualPersianDates(date, wd.getWorkFaDate()))
                return true;
        }

        return false;
    }


    private static void closeCursor(Cursor cursor) {
        try {

            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {

            String msg = "WorkDayDAO - closeCursor : " + e.getMessage();
            L.e(msg);
            throw new RuntimeException(msg);
        }
    }

    private static WorkDayItem cursorToData(Cursor cursor) {
        try {

            int idIndex = cursor.getColumnIndex(Table.COLUMN_WORKDAYID);
            int personIdIndex = cursor.getColumnIndex(Table.COLUMN_PERSONID);
            int transactionIdIndex = cursor.getColumnIndex(Table.COLUMN_TRANSACTIONID);
            int projectIdIndex = cursor.getColumnIndex(Table.COLUMN_PROJECTID);
            int overtimeIndex = cursor.getColumnIndex(Table.COLUMN_OVERTIME);
            int lackhourIndex = cursor.getColumnIndex(Table.COLUMN_LACKHOUR);
            int workFaDateIndex = cursor.getColumnIndex(Table.COLUMN_WORKFADATE);
            int workGeDateIndex = cursor.getColumnIndex(Table.COLUMN_WORKGEDATE);
            int noteIndex = cursor.getColumnIndex(Table.COLUMN_NOTE);
            int editorIndex = cursor.getColumnIndex(Table.COLUMN_EDITOR);
            int tagIndex = cursor.getColumnIndex(Table.COLUMN_TAG);
            int editorDeviceIndex = cursor.getColumnIndex(Table.COLUMN_EDITORDEVICEID);
            int createdIndex = cursor.getColumnIndex(Table.COLUMN_CREATEDAT);
            int updatedIndex = cursor.getColumnIndex(Table.COLUMN_UPDATEDAT);


            WorkDayItem item = new WorkDayItem();
            item.setId(cursor.getString(idIndex));
            item.setPersonId(cursor.getString(personIdIndex));
            item.setTransactionID(cursor.getString(transactionIdIndex));
            item.setProjectID(cursor.getString(projectIdIndex));
            item.setOvertime(cursor.getDouble(overtimeIndex));
            item.setLackHour(cursor.getDouble(lackhourIndex));
            item.setWorkFaDate(cursor.getString(workFaDateIndex));
            item.setWorkGeDate(cursor.getInt(workGeDateIndex));
            item.setInfo(cursor.getString(noteIndex));
            item.setEditor(cursor.getString(editorIndex));
            item.setEditorDeviceId(cursor.getString(editorDeviceIndex));
            item.setTag(cursor.getString(tagIndex));
            item.setCreatedAt(cursor.getLong(createdIndex));
            item.setUpdatedAt(cursor.getLong(updatedIndex));


            return item;
        } catch (Exception e) {

            String msg = "WorkDayDAO - cursorToMasterData : " + e.getMessage();
            L.e(msg);
            throw new RuntimeException(msg);
        }
    }

    private static List<WorkDayItem> manageCursor(Cursor cursor) {
        try {
            List<WorkDayItem> dataList = new ArrayList<>();

            if (cursor != null) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    WorkDayItem account = cursorToData(cursor);
                    dataList.add(account);
                    cursor.moveToNext();
                }
            }
            return dataList;
        } catch (Exception e) {

            String msg = "WorkDayDAO - manageCursor : " + e.getMessage();
            L.e(msg);
            throw new RuntimeException(msg);
        }
    }


    public interface Table {

        String COLUMN_WORKDAYID = "WorkDayId";
        String COLUMN_PERSONID = "PersonId";
        String COLUMN_TRANSACTIONID = "TransactionId";
        String COLUMN_PROJECTID = "ProjectId";
        String COLUMN_OVERTIME = "Overtime";
        String COLUMN_LACKHOUR = "LackHour";
        String COLUMN_WORKFADATE = "WorkFaDate";
        String COLUMN_WORKGEDATE = "WorkGeDate";
        String COLUMN_NOTE = "Note";
        String COLUMN_EDITOR = "Editor";
        String COLUMN_EDITORDEVICEID = "EditorDeviceId";
        String COLUMN_TAG = "Tag";

        String COLUMN_CREATEDAT = "CreatedAt";
        String COLUMN_UPDATEDAT = "UpdatedAt";


    }

}