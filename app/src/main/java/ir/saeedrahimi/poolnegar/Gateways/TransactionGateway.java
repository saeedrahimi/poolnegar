//package ir.saeedrahimi.poolnegar.Gateways;
//
///**
// * Created by Saeed on 3/19/2017.
// */
//
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import ir.saeedrahimi.poolnegar.GatewayInterfaces.IBaseGateWay;
//import ir.saeedrahimi.poolnegar.R;
//import ir.saeedrahimi.poolnegar.application.MainApplication;
//import ir.saeedrahimi.poolnegar.application.helper.L;
//import ir.saeedrahimi.poolnegar.database.AppDatabaseManager;
//import ir.saeedrahimi.poolnegar.database.IQueryExecutor;
//import ir.saeedrahimi.poolnegar.models.PersonItem;
//import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
//
//public class TransactionGateway
//        implements IBaseGateWay<TransactionItem> {
//    private DbHelper _connection;
//    private Context mContext;
//
//    public TransactionGateway(Context context) {
//        this._connection = new DbHelper(context);
//        this.mContext = context;
//    }
//
//    public static List<TransactionItem> selectAll() {
//        Object result;
//        result = AppDatabaseManager.getInstance().executeQuery(new IQueryExecutor() {
//            @Override
//            public List<TransactionItem> run(SQLiteDatabase database) {
//                try {
//                    String query = MainApplication.getAppContext().getString(R.string.sql_App_Variable_SelectAll);
//
//                    Cursor cursor = database.rawQuery(query, null);
//
//                    List<TransactionItem> dataList = manageCursor(cursor);
//
//                    closeCursor(cursor);
//
//                    return dataList;
//
//                } catch (Exception e) {
//
//                    String msg = "TransactionItemDAO - selectAll : " + e.getMessage().toString();
//                    L.e(msg);
//                    throw new RuntimeException(msg);
//                }
//            }
//        });
//
//        return (List<TransactionItem>) result;
//    }
//
//    private ContentValues toContentValues(TransactionItem item) {
//        ContentValues values = new ContentValues();
//        /*try {
//            values.put("pAcID", item.getAcID());
//            values.put("pName", item.getName());
//            values.put("pFee", Double.valueOf(item.getFee()));
//            values.put("pOvertime", Double.valueOf(item.getOvertime()));
//            values.put("pMobile", item.getMobile());
//            values.put("pPhone", item.getPhone());
//            values.put("pEmail", item.getEmail());
//            values.put("pAccNumber", item.getAccNumber());
//            values.put("pCardNumber", item.getCardNumber());
//            values.put("pIsWorker", item.isWorker()?1:0);
//            return values;
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }*/
//        return values;
//    }
//
//    public void create() {
//         this._connection.execQuery(MainApplication.getAppContext().getString(R.string.sql_Transaction_CreateTable));
//    }
//
//    public void deleteById(int id) {
//        DatabaseHelper db = this._connection;
//        String str = "DELETE FROM " + getTableName() + " WHERE pID=?";
//        String[] params = new String[1];
//        params[0] = Integer.toString(id);
//        db.execSQL(str, params);
//    }
//
//    public List<TransactionItem> getAll() {
//        ArrayList list = new ArrayList();
//        Cursor cursor = this._connection.rawQuery("SELECT Person.pID, Person.pName,Person.pIsWorker, Person.pAcID as Ac_id, Person.pFee,Person.pOvertime\n" +
//                "FROM Person\n", new String[0]);
//        if (cursor.moveToFirst()) {
//            do {
//                PersonItem item = new PersonItem();
//                item.setId(cursor.getInt(cursor.getColumnIndex("pID")));
//                item.setAcID(cursor.getInt(cursor.getColumnIndex("Ac_id")));
//                item.setName(cursor.getString(cursor.getColumnIndex("pName")));
//                item.setFee(cursor.getDouble(cursor.getColumnIndex("pFee")));
//                item.setOvertime(cursor.getDouble(cursor.getColumnIndex("pOvertime")));
//                item.setWorker(cursor.getInt(cursor.getColumnIndex("pIsWorker")) > 0);
//                //WorkDayGateway workDayGateway = new WorkDayGateway(mContext);
//                //item.setWorkDays(workDayGateway.getByWorkerID(item.getId()));
//                list.add(item);
//            } while (cursor.moveToNext());
//        }
//        return list;
//    }
//
//    @Override
//    public List<Integer> getAllDistinctIconTemplates() {
//        return null;
//    }
//
//    public TransactionItem getByID(int id) {
//        List<TransactionItem> list = getObjectsWithWhereClause("Act_id = " + id);
//        if (list.size() > 0)
//            return list.get(0);
//        return null;
//    }
//
//    public List<TransactionItem> getObjectsWithWhereClause(String where) {
//        ArrayList list = new ArrayList();
//        /*Cursor cursor = this._connection.rawQuery("SELECT * FROM "+getTableName()+" WHERE " + where, null);
//        if (cursor.moveToFirst()) {
//            do {
//                TransactionItem item = new TransactionItem();
//                item.setTransactionId(cursor.getInt(cursor.getColumnIndex("Act_id")));
//                item.setMethod(cursor.getInt(cursor.getColumnIndex("Act_set")));
//                item.setProjectId(cursor.getInt(cursor.getColumnIndex("Act_accProject_id")));
//                item.setProjectId(cursor.getInt(cursor.getColumnIndex("Act_accProject_id")));
//                item.setProjectId(cursor.getInt(cursor.getColumnIndex("Act_accProject_id")));
//                item.setProjectId(cursor.getInt(cursor.getColumnIndex("Act_accProject_id")));
//                item.setProjectId(cursor.getInt(cursor.getColumnIndex("Act_accProject_id")));
//                list.add(item);
//            } while (cursor.moveToNext());
//        }*/
//        return list;
//    }
//
//    public String getTableName() {
//        return "Activity";
//    }
//
//    public TransactionItem insert(TransactionItem transaction)
//            throws IllegalArgumentException, IllegalAccessException {
//        transaction.setTransactionId((int) this._connection.insert(toContentValues(transaction), getTableName()));
//        return transaction;
//    }
//
//    public boolean update(TransactionItem transaction)
//            throws IllegalArgumentException, IllegalAccessException {
//        DbHelper db = this._connection;
//        String table = getTableName();
//        ContentValues values = toContentValues(transaction);
//        String[] params = new String[1];
//        params[0] = (transaction.getTransactionId() + "");
//        db.update(table, values, "pID=?", params);
//        return true;
//    }
//
//
//    protected static void closeCursor(Cursor cursor) {
//        try {
//
//
//            if (cursor != null) {
//                cursor.close();
//            }
//        } catch (Exception e) {
//
//            String msg = "AppVariableDAO - closeCursor : " + e.getMessage().toString();
//            L.e(msg);
//            throw new RuntimeException(msg);
//        }
//    }
//
//    protected static TransactionItem cursorToData(Cursor cursor) {
///*        try {
//            int idIndex = cursor.getColumnIndex(Table.COLUMN_ID);
//            int setIndex = cursor.getColumnIndex(Table.COLUMN_METHOD);
//            int projectIndex = cursor.getColumnIndex(Table.COLUMN_PROJECT_ID);
//            int infoIndex = cursor.getColumnIndex(Table.COLUMN_NOTE);
//            int amountIndex = cursor.getColumnIndex(Table.COLUMN_AMOUNT);
//            int payIdIndex = cursor.getColumnIndex(Table.COLUMN_TARGET_ACCOUNT_ID);
//            int subPayIdIndex = cursor.getColumnIndex(Table.COLUMN_TARGET_SUB_ACCOUNT_ID);
//            int rootPayIdIndex = cursor.getColumnIndex(Table.COLUMN_TARGET_ROOT_ACCOUNT_ID);
//
//
//            TransactionItem item = new TransactionItem();
//            item.setTransactionId(cursor.getInt(idIndex));
//            item.setMethod(cursor.getInt(setIndex));
//            item.setProjectId(cursor.getInt(projectIndex));
//            item.setProjectTitle(getAccountTitle(cursor.getInt(projectIndex)));
//            item.setInfo(cursor.getString(infoIndex));
//            item.setAmount(cursor.getDouble(amountIndex));
//            item.setTargetLeafAccountId(cursor.getInt(payIdIndex));
//            item.setTargetSubAccountId(cursor.getInt(subPayIdIndex));
//            item.setTargetRootAccountId(cursor.getInt(rootPayIdIndex));
//            payIds[0] = cursor.getInt(payIdIndex);
//            payIds[1] = cursor.getInt(subPayIdIndex);
//            payIds[2] = cursor.getInt(rootPayIdIndex);
//            String[] payTitles = getAccountsTitle(payIds);
//            item.setTargetLeafAccountTitle(payTitles[0]);
//            item.setTargetSubAccountTitle(payTitles[1]);
//            item.setTargetRootAccountTitle(payTitles[2]);
//
//            item.setAccRecId(cursor.getInt(8));
//            item.setSubAccRecId(cursor.getInt(9));
//            item.setRootRecId(cursor.getInt(10));
//            recIds[0] = cursor.getInt(8);
//            recIds[1] = cursor.getInt(9);
//            recIds[2] = cursor.getInt(10);
//            String[] recTitles = getAccountsTitle(recIds);
//            item.setAccRecTitle(recTitles[0]);
//            item.setSubAccRecTitle(recTitles[1]);
//            item.setRootRecTitle(recTitles[2]);
//            item.setRegDateFa(cursor.getString(11));
//            item.setSerial(cursor.getInt(12));
//            item.setIsCash(cursor.getInt(13));
//            item.setCheqId(cursor.getInt(14));
//            item.setIsCheqPassed(cursor.getInt(15));
//            item.setTAG(cursor.getString(cursor.getColumnIndex("Act_tags")));
//
//            return AppVariable;
//        } catch (Exception e) {
//
//            String msg = "TransactionItemDAO - cursorToMasterData : " + e.getMessage().toString();
//            L.e(msg);
//            throw new RuntimeException(msg);
//        }*/
//        return new TransactionItem();
//    }
//
//    protected static List<TransactionItem> manageCursor(Cursor cursor) {
//        try {
//            List<TransactionItem> dataList = new ArrayList<TransactionItem>();
//
//            if (cursor != null) {
//                cursor.moveToFirst();
//                while (!cursor.isAfterLast()) {
//                    TransactionItem account = cursorToData(cursor);
//                    dataList.add(account);
//                    cursor.moveToNext();
//                }
//            }
//            return dataList;
//        } catch (Exception e) {
//
//            String msg = "TransactionItemDAO - manageCursor : " + e.getMessage().toString();
//            L.e(msg);
//            throw new RuntimeException(msg);
//        }
//    }
//    public interface Table {
//        String COLUMN_ID = "Act_id";
//        String COLUMN_SET = "Act_set";
//        String COLUMN_PROJECT_ID = "Act_accProject_id";
//        String COLUMN_INFO = "Act_info";
//        String COLUMN_AMOUNT = "Act_amount";
//        String COLUMN_ACC_PAY_ID = "Act_accPay_id";
//        String COLUMN_PAY_SUB_ACC_ID = "Act_pay_Subacc_id";
//        String COLUMN_PAY_ROOT_ID = "Act_pay_root_id";
//
//        String COLUMN_ACC_RECIVE_ID = "Act_accRecive_id";
//        String COLUMN_RECIVE_SUB_ACC_ID = "Act_rec_Subacc_id";
//        String COLUMN_RECIVE_ROOT_ID = "Act_rec_Root_id";
//
//        String COLUMN_DATE = "Act_date";
//        String COLUMN_TIME = "Act_time";
//        String COLUMN_SERIAL = "Act_ser";
//        String COLUMN_ISCASH = "Act_iscash";
//        String COLUMN_CHEQUE_ID = "Act_cheq_id";
//        String COLUMN_ISCHEQUE_PASSED = "Act_ischeq_passed";
//        String COLUMN_TAGS = "Act_tags";
//
//
//        String TABLE_NAME = "TransactionItem";
//
//    }
//
//}