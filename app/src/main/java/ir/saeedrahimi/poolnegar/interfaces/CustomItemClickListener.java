package ir.saeedrahimi.poolnegar.interfaces;

import android.view.View;

/**
 * Created by Saeed on 3/20/2017.
 */

public interface CustomItemClickListener {
    public void onItemClick(View v, int position);
}
