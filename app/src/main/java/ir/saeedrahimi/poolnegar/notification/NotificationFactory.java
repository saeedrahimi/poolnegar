package ir.saeedrahimi.poolnegar.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.io.Serializable;

import androidx.core.app.NotificationCompat;
import ir.saeedrahimi.poolnegar.application.helper.KeyHelper;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
import ir.saeedrahimi.poolnegar.models.SmsObject;
import ir.saeedrahimi.poolnegar.ui.activities.cu.Activity_Transaction;

public class NotificationFactory {
    private static final int DEFAULT_NOTIFICATION_ID = -1;

    private static String NOTIFICATION_CHANNEL = "All Messages";

    static {

    }

    private final Context mContext;

    public NotificationFactory(Context paramContext) {
        this.mContext = paramContext;
    }

    private void configNotificationChannel() {
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL, "POOLNEGAR_NOTIFICATION", android.app.NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(-16776961);
            notificationChannel.setVibrationPattern(new long[]{0L, 1000L, 500L, 1000L});
            notificationChannel.enableVibration(true);
            notificationChannel.setShowBadge(true);
            getNotificationManager().createNotificationChannel(notificationChannel);
        }
    }

    private NotificationCompat.Builder getNotificationBuilder() {
        configNotificationChannel();
        return (Build.VERSION.SDK_INT >= 26) ? new NotificationCompat.Builder(this.mContext, NOTIFICATION_CHANNEL) : new NotificationCompat.Builder(this.mContext);
    }

    private NotificationManager getNotificationManager() {
        return (NotificationManager) this.mContext.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public void sendDailyReminderNotification() {
        Intent paymentIntent = new Intent(this.mContext, Activity_Transaction.class);
        PendingIntent pendingPaymentIntent = PendingIntent.getActivity(this.mContext, KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION, paymentIntent, Intent.FILL_IN_DATA);
        paymentIntent.putExtra("fromDailyReminder", true);
        paymentIntent.putExtra(KeyHelper.KEY_TRANSACTION_METHOD, TransactionItem.TRANSACTION_METHOD_PAYMENT);

        Intent incomeIntent = new Intent(this.mContext, Activity_Transaction.class);
        incomeIntent.putExtra("fromDailyReminder", true);
        incomeIntent.putExtra(KeyHelper.KEY_TRANSACTION_METHOD, TransactionItem.TRANSACTION_METHOD_INCOME);

        PendingIntent pendingIncomeIntent = PendingIntent.getActivity(this.mContext,  KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION, incomeIntent, Intent.FILL_IN_DATA);
        NotificationCompat.Builder builder = getNotificationBuilder().setSmallIcon(2131165751)
                .setContentTitle("حساب کتاب یادت نره!")
                .setStyle((NotificationCompat.Style) (new NotificationCompat.BigTextStyle())
                        .bigText("تراکنشهای امروزت رو ثبت کن"))
                .setContentText("تراکنشهای امروزت رو ثبت کن")
                .setAutoCancel(true)
                .addAction(2131165585, "دریافت", pendingIncomeIntent)
                .addAction(2131165585, "پرداخت", pendingPaymentIntent).setAutoCancel(true);
        builder.setContentIntent(pendingPaymentIntent);
        getNotificationManager().notify(-1, builder.build());
    }


    public void sendSMSNotification(Context context, SmsObject smsObject) {
        PendingIntent pendingIntent;
        Intent transactionIntent = new Intent(context, Activity_Transaction.class);
        transactionIntent.putExtra("sms", (Serializable) smsObject);

        if (smsObject.getType() == 0) {
            transactionIntent.putExtra(KeyHelper.KEY_TRANSACTION_METHOD, TransactionItem.TRANSACTION_METHOD_PAYMENT);
            pendingIntent = PendingIntent.getActivity(context, smsObject.getID(), transactionIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        } else if (smsObject.getType() == 1) {
            transactionIntent.putExtra(KeyHelper.KEY_TRANSACTION_METHOD, TransactionItem.TRANSACTION_METHOD_INCOME);
            pendingIntent = PendingIntent.getActivity(context, smsObject.getID(), transactionIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            pendingIntent = null;
        }
        NotificationCompat.Builder builder2 = getNotificationBuilder().setSmallIcon(2131165751).setContentTitle("پیامک از بانک:").setStyle((NotificationCompat.Style) new NotificationCompat.BigTextStyle());
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("پیامک: ");
        stringBuilder.append(smsObject.getBankName());
        NotificationCompat.Builder builder1 = builder2.setContentText(stringBuilder.toString()).setAutoCancel(true).setAutoCancel(true);
        builder1.setContentIntent(pendingIntent);
        getNotificationManager().notify(smsObject.getID(), builder1.build());
    }
}
