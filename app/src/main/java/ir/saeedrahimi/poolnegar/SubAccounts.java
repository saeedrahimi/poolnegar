package ir.saeedrahimi.poolnegar;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.databinding.FragmentAccountsBinding;
import ir.saeedrahimi.poolnegar.models.AccountMultiLevel;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_GeneralBase;
import ir.saeedrahimi.poolnegar.ui.activities.AddPersonActivity;
import ir.saeedrahimi.poolnegar.ui.activities.cu.ActivityCU_Account;
import ir.saeedrahimi.poolnegar.ui.adapters.AccountSelectListAdapter;
import ir.saeedrahimi.poolnegar.ui.helper.SnackBarHelper;


public class SubAccounts  extends Activity_GeneralBase<FragmentAccountsBinding>
{
  public static int RESULT_EDIT = 2;
  public static int RESULT_EDIT_LEAF = 3;
  public String parentId;
  public String parentTitle;
  public List<AccountMultiLevel> list;

  private void fillAccounts()
  {
    list = AccountDAO.getRecursiveByParentId(parentId);
    AccountSelectListAdapter myAdapter = new AccountSelectListAdapter(this, list, binding.rcvData);
    binding.rcvData.setAdapter(myAdapter);

  }



  @Override
  protected void onActivityFirstInit(Bundle savedInstanceState) {
    initComponents();
  }

  private void initComponents() {


    binding.rcvData.setLayoutManager(new LinearLayoutManager(this));
    this.parentTitle = getIntent().getStringExtra("SubName");
    this.parentId = getIntent().getStringExtra("ParentId");
    fillAccounts();
  }

  @Override
  protected FragmentAccountsBinding getViewBinding() {
    return FragmentAccountsBinding.inflate(getLayoutInflater());
  }

  @Override
  protected void onActionBarRightToolClicked() {
    addSubAccount();
  }

  @Override
  protected void onActionBarLeftToolClicked() {
    finish();
  }

  @Override
  protected String getActionBarTitle() {
    return null;
  }

  @Override
  protected void onPostCreate(Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);

    setActionBarTitle("حساب " + parentTitle);
  }


  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 4) {
      Intent backIntent = new Intent();
      setResult(RESULT_OK, backIntent);
      finish();
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }


  public void addSubAccount()
  {
    if(parentId.equals(Constants.ACCOUNTS.PERSONS)){
      Intent intent = new Intent(this, AddPersonActivity.class);
      startActivityForResult(intent, Constants.RESULTS.ADD_ITEM);
    }else{

      Intent localIntent = new Intent(this, ActivityCU_Account.class);
      localIntent.putExtra("ParentId", parentId);
      this.startActivityForResult(localIntent, Constants.RESULTS.ADD_ITEM);
    }
  }
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode,resultCode,data);
    if (requestCode == Constants.RESULTS.ADD_ITEM) {
      if(resultCode == RESULT_OK){
        fillAccounts();
        // TODO: FIX HERE
//        this.adapter = new SubAccountAdapter(this, this, this.list, this.hashItems, this.expandList);
//        this.expandList.setAdapter(this.adapter);
//        this.adapter.notifyDataSetChanged();
        SnackBarHelper.showSnack(this, SnackBarHelper.SnackState.Success ,"حساب ایجاد شد");

      }
      if(resultCode == RESULT_CANCELED)
      {

      }
    }
    if (requestCode == RESULT_EDIT || requestCode == RESULT_EDIT_LEAF) {
      if(resultCode == RESULT_OK){
        fillAccounts();
        // TODO: FIX HERE
//        this.adapter = new SubAccountAdapter(this, this, this.list, this.hashItems, this.expandList);
//        this.expandList.setAdapter(this.adapter);
        SnackBarHelper.showSnack(this, SnackBarHelper.SnackState.Success ,"تغیرات انجام شد");

      }
      if(resultCode == RESULT_CANCELED)
      {

      }
    }

  }
}
