package ir.saeedrahimi.poolnegar;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import ir.saeedrahimi.poolnegar.databinding.ActivityReportsBinding;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_AccountSelection;


public class ReportsActivity extends BasePremiumActivity<ActivityReportsBinding> implements View.OnClickListener {


    private String selectedAccount;


    @Override
    protected void onActivityFirstInit(Bundle savedInstanceState) {
        initComponents();
    }

    private void initComponents() {

        findViewById(R.id.btn_bill_accounts).setOnClickListener(this);
        findViewById(R.id.btn_bill_projects).setOnClickListener(this);
    }

    @Override
    protected ActivityReportsBinding getViewBinding() {
        return ActivityReportsBinding.inflate(getLayoutInflater());
    }


    @Override
    protected void onActionBarRightToolClicked() {

    }

    @Override
    protected void onActionBarLeftToolClicked() {

    }

    @Override
    protected String getActionBarTitle() {
        return "گزارشات";
    }


    @Override
    public void onClick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.btn_bill_projects:
                intent = new Intent(this, ReportProjectsActivity.class);
                break;
            case R.id.btn_bill_accounts:
                accSelector();
            default:

        }

        if(intent!=null){
            Bundle bundle = ActivityOptions.makeScaleUpAnimation(view, 0, 0, view.getWidth(), view.getHeight()).toBundle();
            startActivity(intent,bundle);
        }

    }
    public void accSelector() {
        Intent dlg = new Intent(this, Activity_AccountSelection.class);
        dlg.putExtra("accType", "acc");
        dlg.putExtra("title", "گزارش");
        dlg.putExtra("hasRadio", true);
        startActivityForResult(dlg, Constants.RESULTS.ACC);
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == Constants.RESULTS.ACC)) {
            if (resultCode == RESULT_OK) {
                this.selectedAccount = data.getStringExtra("SelectedAccounts");
                Intent intent = new Intent(this, ReportBillActivity.class);
                intent.putExtra("account", selectedAccount);
                startActivity(intent);
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
