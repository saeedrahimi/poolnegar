package ir.saeedrahimi.poolnegar;

/**
 * Created by Saeed on 3/20/2017.
 */

public  class Constants {
    public static final String currentDBPath= "//data//ir.saeedrahimi.poolnegar//databases//poolnegar.db";
    public static final String backupDBPath="/PoolNegarData/db";
    public static final String backupFileExt=".rtm";
    public static final String paySKU="hp_59353a4596fa2878607475";

    public interface RESULTS {
        int RESULT_OK = -1;
        int RESULT_CANCEL = 0;
        int ACC_PAYER = 102;
        int ACC_RECEIVER = 103;
        int ADD_ITEM = 104;
        int CALCULATOR = 105;
        int ACC_PROJECT = 106;
        int CONTACT = 107;
        int ACC = 108;
        int PAY = 109;
        int SORT = 110;
        int EDIT = 187;
        int REQUEST_CODE_DISABLE = 400;

    }
    public interface PERMISSIONS {
        int REQUEST_WRITE_EXTERNAL_STORAGE = 2;
        int READ_CONTACTS = 3;
    }
    public interface ACCOUNTS {
        String EXPENSE  = "c2973919-cb20-4223-a167-f866d489ca80"; //1
        String INCOME   = "c3373b04-5721-4725-bab7-0fd3f0c0b616"; //2
        String DEMAND   = "4e7b905e-d852-4418-8f7c-a917f1f2af81"; //6
        String DEBT   = "bd904844-f109-482d-bc1b-85c82a193035"; //8


        String PERSONS  = "ca144c2e-b3fb-4139-b297-9b4228cc79fa"; //13
        int PERSONS_INT = 13;
        String PROJECTS  = "3d3d06a7-cd3e-42be-aef0-3c0b678cb57d"; //12
        int PROJECTS_INT = 12;
        String BILLS  = "bc301906-3474-4de0-a10d-1699f8aa8064"; //8
        int BILLS_INT = 8;
        String WorkerBill  = "13cc6497-68f3-4826-a592-63ab5f446ddf"; //18
        int WorkerBill_INT = 18;
        String Banks  = "d6010e4b-d7ea-4659-9c75-39d14246be48"; //4


    }
    public interface DB_TAGS {
        String WorkDay ="wd" ;
        String PayBill ="pb" ;
    }

    public interface SQL_VARIABLE {
        String WhereClause ="%WHERECLAUSE%" ;
    }
    public interface SETTINGS {
        String WORKER_PARENT = "workerParent";
    }
}

