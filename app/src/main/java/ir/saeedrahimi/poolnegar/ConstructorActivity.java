package ir.saeedrahimi.poolnegar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ir.github.fafaldo.fabtoolbar.widget.FABToolbarLayout;
import ir.saeedrahimi.poolnegar.adapters.WorkersListAdapter;
import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar;
import ir.saeedrahimi.poolnegar.customs.OnSwipeTouchListener;
import ir.saeedrahimi.poolnegar.customs.TypeFaceProvider;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.database.DAO.PersonDAO;
import ir.saeedrahimi.poolnegar.databinding.ActivityContractorBinding;
import ir.saeedrahimi.poolnegar.dialogs.ProgressLoading;
import ir.saeedrahimi.poolnegar.models.PersonItem;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_AccountSelection;
import ir.saeedrahimi.poolnegar.ui.helper.SnackBarHelper;

public class ConstructorActivity extends BasePremiumActivity<ActivityContractorBinding> implements WorkersListAdapter.onClick, View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private static final int RESULT_DATE = 0;


    private PersianCalendar mCalendar;
    private int visibleThreshold = 10;
    private int currentPage = 0;
    private int m_nLastIndex = 0;
    private boolean loading = true;
    WorkersListAdapter adapter;
    public boolean mFirstAsync = false;

    RecyclerView recList;
    TextView txtDate;
    ImageButton btnPrevDay, btnNextDay;
    FABToolbarLayout mToolbarMenu;
    PreferencesManager mPrefManager;

    private boolean hideClosed;
    private int lastListSize;
    private List<PersonItem> listWorkers;
    private ConstructorActivity.AsyncLoadDB async;
    private int m_TotalPages;

    OnSwipeTouchListener onSwipeTouchListener;

    private static boolean devMode = true;
    private int mCurrentPos;
    private PersonItem mCurrentWorker;

    public void RefreshAll() {
        this.lastListSize = 10;
        this.currentPage = 0;
        this.listWorkers.clear();
        if (async != null && async.p != null)
            async.p.done();
        if (async != null && !async.isCancelled())
            async.cancel(true);

        m_TotalPages = AccountDAO.getChildrenCount(Constants.ACCOUNTS.PROJECTS) / visibleThreshold;
        async = new ConstructorActivity.AsyncLoadDB();

        async.execute(new Integer[]{this.currentPage});
    }
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev){
        onSwipeTouchListener.getGestureDetector().onTouchEvent(ev);
        return super.dispatchTouchEvent(ev);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.activity_contractor);
        super.onCreate(savedInstanceState);

        txtDate = findViewById(R.id.txt_date);
        btnPrevDay = findViewById(R.id.imageButtonDown);
        btnNextDay = findViewById(R.id.imageButtonUp);
        btnPrevDay.setOnClickListener(this);
        btnNextDay.setOnClickListener(this);
        txtDate.setOnClickListener(this);

        this.mCalendar = new PersianCalendar();
        updateDate();

        this.listWorkers = new ArrayList<>();
        this.recList = findViewById(R.id.rep_balance_list);//c
        listWorkers = PersonDAO.getAll();
        this.adapter = new WorkersListAdapter(this, listWorkers, this, mCalendar.getPersianShortDate());
        this.recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        this.recList.setAdapter(this.adapter);
        this.mFirstAsync = true;
        setActionBarTitle("پیمانکار" + " - "+ "حضورغیاب");

        //this.recList.setEmptyView(view.findViewById(R.id.emptylist));

        mPrefManager = new PreferencesManager(this.getApplicationContext());

        this.hideClosed = mPrefManager.getHideClosedProjects();

        final FloatingActionButton fab = findViewById(R.id.fabtoolbar_fab);
        mToolbarMenu = findViewById(R.id.fabtoolbar);
        final View fabLayout = findViewById(R.id.fabtoolbar_toolbar);
        findViewById(R.id.worker_add).setOnClickListener(this);
        findViewById(R.id.worker_pays).setOnClickListener(this);
        findViewById(R.id.worker_report).setOnClickListener(this);
        fab.setOnClickListener(v -> mToolbarMenu.show());
        recList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    if (fabLayout.getVisibility() == View.VISIBLE) {
                        mToolbarMenu.hide();
                    } else {
                        fab.hide();
                    }
                } else if (dy < 0) {
                    //layout.show();
                    fab.show();
                }
            }
        });
       /* int parent = mPrefManager.settings.getInt(Constants.SETTINGS.WORKER_PARENT, 0);
        if (parent == 0) {
            final Dialog questionDialog = new Dialog(this);
            questionDialog.requestWindowFeature(1);
            questionDialog.setContentView(R.layout.dialog_message);
            TextView message_title = (TextView) questionDialog.findViewById(R.id.headermessage1);
            TextView message1 = (TextView) questionDialog.findViewById(R.id.messagetext1);
            TextView message2 = (TextView) questionDialog.findViewById(R.id.messagetext2);
            Button ok_btn = (Button) questionDialog.findViewById(R.id.messageok);
            message_title.setText(" پیمانکار");
            message1.setText("حساب والد برای کارگران انتخاب نشده است.");
            message2.setText("برای ادامه یک حساب را انتخاب کنید.");
            ok_btn.setText("انتخاب");
            ok_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    Intent dlg = new Intent(ConstructorActivity.this, DialogParentAccountSelector.class);
                    dlg.putExtra("accType", "payer");
                    dlg.putExtra("title", "والد کارگران");
                    startActivityForResult(dlg, Constants.RESULTS.ACC_PAYER);
                    questionDialog.dismiss();
                    return;
                }
            });
            questionDialog.show();
            questionDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    finish();
                }
            });
        }*/
        onSwipeTouchListener = new OnSwipeTouchListener(ConstructorActivity.this){
            public void onSwipeRight() {
                mCalendar.add(Calendar.DATE, 1);
                YoYo.with(Techniques.ZoomOut).duration(300).playOn(mToolbarMenu);
                YoYo.with(Techniques.ZoomIn).delay(100).duration(300).playOn(mToolbarMenu);
                YoYo.with(Techniques.SlideInLeft).delay(100).duration(300).playOn(mToolbarMenu);
                YoYo.with(Techniques.Shake).delay(100).duration(500).playOn(findViewById(R.id.currentDate));
                YoYo.with(Techniques.Shake).delay(100).duration(500).playOn(findViewById(R.id.imageButtonUp));

                updateDate();
            }
            public void onSwipeLeft() {
                mCalendar.add(Calendar.DATE, -1);

                YoYo.with(Techniques.ZoomOut).duration(300).playOn(mToolbarMenu);
                YoYo.with(Techniques.ZoomIn).delay(100).duration(300).playOn(mToolbarMenu);
                YoYo.with(Techniques.SlideInRight).delay(100).duration(300).playOn(mToolbarMenu);
                YoYo.with(Techniques.Shake).delay(100).duration(500).playOn(findViewById(R.id.currentDate));
                YoYo.with(Techniques.Shake).delay(100).duration(500).playOn(findViewById(R.id.imageButtonDown));
                updateDate();
            }
        };

        mToolbarMenu.setOnTouchListener(onSwipeTouchListener);
        RefreshAll();
    }

    @Override
    protected void onActivityFirstInit(Bundle savedInstanceState) {

    }

    @Override
    protected ActivityContractorBinding getViewBinding() {
        return ActivityContractorBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void onActionBarRightToolClicked() {

    }

    @Override
    protected void onActionBarLeftToolClicked() {

    }

    @Override
    protected String getActionBarTitle() {
        return null;
    }

    @Override
    public void onBackPressed() {
        if (mToolbarMenu.isToolbar()) {
            mToolbarMenu.hide();
            return;
        }
        super.onBackPressed();
        return;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_constructor, menu);
        return true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item != null && item.getItemId() == R.id.mnu_sort) {
            Intent intent = new Intent(this,PersonOrderActivity.class);
            startActivityForResult(intent,Constants.RESULTS.SORT);
        }

        return false;
    }

    public void updateDate() {

        txtDate.setText( mCalendar.getPersianDayNameOfWeek() + " " + mCalendar.getPersianShortDate());
        if(mCalendar.getPersianShortDate().equals(new PersianCalendar().getPersianShortDate()))
            txtDate.setTextColor(ContextCompat.getColor(this, R.color.Teal));
        else
            txtDate.setTextColor(ContextCompat.getColor(this, R.color.ColorPrimaryDark));

        if(adapter!=null){
            adapter.setDate(mCalendar.getPersianShortDate());
            adapter.notifyDataSetChanged();
            recList.invalidate();
        }
    }

    public void datePickerDialog(View paramView) {
        PersianCalendar persianCalendar = new PersianCalendar();
        persianCalendar.setPersianDate(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DATE));
        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                ConstructorActivity.this,
                persianCalendar.getPersianYear(),
                persianCalendar.getPersianMonth() - 1 ,
                persianCalendar.getPersianDay(),
                TypeFaceProvider.get(this, "vazir.ttf")
        );
        datePickerDialog.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.worker_add:
                mToolbarMenu.hide();
                Intent intent = new Intent(ConstructorActivity.this, SubAccounts.class);
                intent.putExtra("SubName","اشخاص");
                intent.putExtra("ParentId",Constants.ACCOUNTS.PERSONS_INT);
                startActivityForResult(intent, Constants.RESULTS.ADD_ITEM);
                break;
            case R.id.txt_date:
                datePickerDialog(txtDate);
                break;
            case R.id.imageButtonDown:
                mCalendar.add(Calendar.DATE, 1);
                updateDate();
                break;
            case R.id.imageButtonUp:
                mCalendar.add(Calendar.DATE, -1);
                updateDate();
                break;
        }
    }

    @Override
    public void onWorkerClick(View view, PersonItem acc) {
		/*Fragment frag = new FragmentProjectDetail();
		Bundle args = new Bundle();
		args.putInt("id", acc.getId());
		args.putDouble("fee", acc.getFee());
		args.putDouble("overtime", acc.getOvertime());
		frag.setArguments(args);
		FragmentTransaction fgt = getSupportFragmentManager().beginTransaction();
		fgt.replace(R.id.content_frame, frag, "projectTransDetail");
		fgt.addToBackStack("projectTransDetail");
		fgt.commit();*/
    }

    @Override
    public void onCommandClick(View view, PersonItem worker, int commandType, int position) {


        switch (commandType) {
            case WorkersListAdapter.Commands.PRESENT:
                PersonDAO.setWorkerPresent(worker, mCalendar.getPersianShortDate());
                adapter.notifyItemChanged(position);
                break;
            case WorkersListAdapter.Commands.ABSENT:
                PersonDAO.setWorkerAbsent(worker, mCalendar.getPersianShortDate());
                adapter.notifyItemChanged(position);
                break;
            case WorkersListAdapter.Commands.INCOVERTIME:
                if(PersonDAO.increaseOverTime(worker, mCalendar.getPersianShortDate()))
                    adapter.notifyItemChanged(position);
                break;
            case WorkersListAdapter.Commands.DECOVERTIME:
                if(PersonDAO.decreaseOverTime(worker, mCalendar.getPersianShortDate()))
                    adapter.notifyItemChanged(position);
                break;
            case WorkersListAdapter.Commands.PROJECT:
                projectSelector(worker,position);
                break;
            case WorkersListAdapter.Commands.CARD:
                Intent detail = new Intent(this,WorkerDetailActivity.class);
                detail.putExtra("worker",worker);
                this.startActivity(detail);
                break;
            default:
                return;
        }
    }
    public void projectSelector(PersonItem wi, int pos) {
        Intent dlg = new Intent(this, Activity_AccountSelection.class);
        this.mCurrentPos=pos;
        this.mCurrentWorker = wi;
        dlg.putExtra("accType", "project");
        dlg.putExtra("title", "پروژه");
        this.startActivityForResult(dlg, Constants.RESULTS.ACC_PROJECT);
    }

    @Override
    protected void onResume() {
       //RefreshAll();
        super.onResume();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        mCalendar.setPersianDate(year, monthOfYear + 1 , dayOfMonth);
        updateDate();

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == Constants.RESULTS.ACC_PAYER) {
            if (resultCode == Constants.RESULTS.RESULT_OK) {
                String title = data.getStringExtra("title");
                int[] selectedIds = data.getIntArrayExtra("selectedIds");
                if(selectedIds[1]==0 || selectedIds[1] == -1)
                    mPrefManager.editor.putInt (Constants.SETTINGS.WORKER_PARENT, selectedIds[0]);
                else
                    mPrefManager.editor.putInt (Constants.SETTINGS.WORKER_PARENT, selectedIds[1]);
                mPrefManager.editor.commit();
                SnackBarHelper.showSnack(this, SnackBarHelper.SnackState.Success,"تغیرات انجام شد");
            }
            if (resultCode == Constants.RESULTS.RESULT_CANCEL) {

            }
        }else if ((requestCode == Constants.RESULTS.ACC_PROJECT)) {
            if (resultCode == RESULT_OK) {
                PersonDAO.setProject(mCurrentWorker, mCalendar.getPersianShortDate(), data.getStringExtra("SelectedAccounts"));
                adapter.notifyItemChanged(this.mCurrentPos);

            }
        }else if ((requestCode == Constants.RESULTS.ADD_ITEM)) {
            if (resultCode == RESULT_OK) {
               RefreshAll();

            }
        }else if ((requestCode == Constants.RESULTS.SORT)) {
                RefreshAll();
        }

    }
    class AsyncLoadDB extends AsyncTask<Integer, List<PersonItem>, List<PersonItem>> {
        ProgressLoading p;

        @Override
        protected void onPreExecute() {
            if (ConstructorActivity.this.lastListSize == 10) {
                if (this.p != null)
                    this.p.done();
                this.p = new ProgressLoading(ConstructorActivity.this, "");
            }
        }

        @SuppressLint("LongLogTag")
        @Override
        protected List<PersonItem> doInBackground(Integer... values) {
            List<PersonItem> temp_list = new ArrayList<>();
            //if (ConstructorActivity.this.lastListSize == 10) {

                long startTime = System.currentTimeMillis();
                temp_list = PersonDAO.getAllWhere("IsWorker = 1");

            //}

            return temp_list;
        }

        @Override
        protected void onPostExecute(List<PersonItem> result) {
            if (this.p != null)
                this.p.done();
            ConstructorActivity.this.adapter.setDate( ConstructorActivity.this.mCalendar.getPersianShortDate());
            ConstructorActivity.this.listWorkers.addAll(result);
            if (ConstructorActivity.this.lastListSize > 0) {
                ConstructorActivity.this.adapter.notifyDataSetChanged();
            }


            if (ConstructorActivity.this.lastListSize == 10) {
                ConstructorActivity.this.recList.setTag("loadMore");
            } else if (!ConstructorActivity.this.recList.getTag().toString().contains("ended")) {
                if (listWorkers.size() > 0){

                    SnackBarHelper.showSnack(ConstructorActivity.this, SnackBarHelper.SnackState.Info,"به انتهای لیست دریافتی ها رسیدید");
                }
                ConstructorActivity.this.recList.setTag("ended");
            }
            ConstructorActivity.this.loading = false;

            if (this.p != null)
                this.p.done();
            // FragmentContractor.this.recList.setOnScrollListener(FragmentContractor.this);


            super.onPostExecute(result);
        }
    }

}
