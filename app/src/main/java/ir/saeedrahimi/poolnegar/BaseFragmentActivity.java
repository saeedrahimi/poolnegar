package ir.saeedrahimi.poolnegar;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ncapdevi.fragnav.FragNavController;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import java.lang.reflect.Field;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import ir.saeedrahimi.poolnegar.customs.TypeFaceProvider;
import ir.saeedrahimi.poolnegar.ui.fragments.BaseFragment;
import ir.saeedrahimi.poolnegar.ui.fragments.FragmentHome;
import ir.saeedrahimi.poolnegar.ui.fragments.FragmentRPA;
import ir.saeedrahimi.poolnegar.ui.fragments.FragmentSettings;
import ir.saeedrahimi.poolnegar.ui.fragments.FragmentTransactionsKt;


public class BaseFragmentActivity extends AppCompatActivity implements FragNavController.TransactionListener, FragNavController.RootFragmentListener {

    //Better convention to properly name the indices what they are in your app
    private final int INDEX_DASHBOARD = FragNavController.TAB1;
    private final int INDEX_TRANSACTIONS = FragNavController.TAB2;
    private final int INDEX_REPORTS = FragNavController.TAB3;
    private final int INDEX_SETTINGS = FragNavController.TAB4;
    private BottomBar mBottomBar;

    private boolean doubleBackToExitPressedOnce = false;
    protected FragNavController mNavController;

    //private ActionBarRtlizer rtlizer;
    public Toolbar mToolBar;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
		/*rtlizer = new ActionBarRtlizer(this);
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1){
			rtlizer.rtlize();
		}*/
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
		/*if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1){
			if (rtlizer != null){
			    rtlizer.rtlize();
			    this.invalidateOptionsMenu();
			    }
		}*/

        super.onResume();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            getWindow().getDecorView()
                    .setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBottomBar = findViewById(R.id.bottomBar);
        mBottomBar.selectTabAtPosition(INDEX_DASHBOARD);
        mNavController = new FragNavController(getSupportFragmentManager(), R.id.content);
//
        mNavController.setTransactionListener(this);
        mNavController.setRootFragmentListener(this);
        mNavController.initialize(FragNavController.TAB1, savedInstanceState);


        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabtoolbar_fab);


        mBottomBar.setOnTabSelectListener(new

                                                  OnTabSelectListener() {
                                                      @Override
                                                      public void onTabSelected(@IdRes int tabId) {
                                                          switch (tabId) {
                                                              case R.id.bb_menu_dashboard:
                                                                  mNavController.switchTab(INDEX_DASHBOARD);
                                                                  break;
                                                              case R.id.bb_menu_transactions:
                                                                  mNavController.switchTab(INDEX_TRANSACTIONS);
                                                                  break;
                                                              case R.id.bb_menu_reports:
                                                                  mNavController.switchTab(INDEX_REPORTS);
                                                                  break;
                                                              case R.id.bb_menu_settings:
                                                                  mNavController.switchTab(INDEX_SETTINGS);
                                                                  break;
                                                          }
                                                      }
                                                  });

        mBottomBar.setOnTabReselectListener(new

                                                    OnTabReselectListener() {
                                                        @Override
                                                        public void onTabReSelected(@IdRes int tabId) {
                                                            mNavController.clearStack();
                                                        }
                                                    });


        fab.bringToFront();
        fab.getParent().requestLayout();
        ((View)fab.getParent()).invalidate();

    }

    public void prepareToolbar(Toolbar toolbar) {
        mToolBar = toolbar;
        mToolBar.setTitle(R.string.app_name);
        TextView mAppName = getToolBarTextView();
        Typeface face = TypeFaceProvider.get(this, "vazir.ttf");
        mAppName.setTextColor(getResources().getColor(R.color.white));
        mAppName.setTypeface(face, Typeface.BOLD);
        mAppName.setTextSize(19f);
        mAppName.getWidth();
        mAppName.setY(10f);
        setSupportActionBar(mToolBar);
    }

    public void setToolBarTitle(String title, String subtitle) {
        setTitle(title);
        mToolBar.setTitle(title);
        if (subtitle != null && subtitle.length() > 0)
            mToolBar.setSubtitle(subtitle);
        else
            mToolBar.setSubtitle(null);
    }

    public void setToolBarTitle(CharSequence title, CharSequence subtitle) {
        setTitle(title);
        mToolBar.setTitle(title);
        if (subtitle != null && subtitle.length() > 0)
            mToolBar.setSubtitle(subtitle);
        else
            mToolBar.setSubtitle(null);
    }

    private TextView getToolBarTextView() {
        TextView titleTextView = null;

        try {
            Field f = mToolBar.getClass().getDeclaredField("mTitleTextView");
            f.setAccessible(true);
            titleTextView = (TextView) f.get(mToolBar);
        } catch (NoSuchFieldException e) {
        } catch (IllegalAccessException e) {
        }
        return titleTextView;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish();
            super.onBackPressed();
            return;
        } else if ((mNavController.getCurrentStackIndex() == INDEX_DASHBOARD) || !mNavController.popFragment()) {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "برای خروج دوباره دکمه بازگشت را بزنید", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mNavController != null) {
            mNavController.onSaveInstanceState(outState);
        }
    }

    public void pushFragment(Fragment fragment) {
        if (mNavController != null) {
            mNavController.pushFragment(fragment);
        }
    }

    @Override
    public void onTabTransaction(Fragment fragment, int index) {
        // If we have a backstack, show the back button
        if (getSupportActionBar() != null && mNavController != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(!mNavController.isRootFragment());
        }
    }


    @Override
    public void onFragmentTransaction(Fragment fragment, FragNavController.TransactionType
            transactionType) {
        //do fragmentty stuff. Maybe change title, I'm not going to tell you how to live your life
        // If we have a backstack, show the back button
        if (getSupportActionBar() != null && mNavController != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(!mNavController.isRootFragment());
        }
    }

    @Override
    public Fragment getRootFragment(int index) {
        switch (index) {
            case INDEX_DASHBOARD:
                return FragmentHome.newInstance(0);
            case INDEX_TRANSACTIONS:
                return FragmentTransactionsKt.Companion.newInstance(0);
            case INDEX_REPORTS:
                return FragmentRPA.newInstance(0);
            case INDEX_SETTINGS:
                return FragmentSettings.newInstance(0);
        }
        throw new IllegalStateException("Need to send an index that we know");
    }


    @Override
    public int getNumberOfRootFragments() {
        return 4;
    }
}
