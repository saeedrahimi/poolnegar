//package ir.saeedrahimi.poolnegar.Helper;
//
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.sqlite.SQLiteDatabase;
//import android.os.Environment;
//import android.util.Log;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileOutputStream;
//import java.nio.channels.FileChannel;
//
//import ir.saeedrahimi.poolnegar.Constants;
//import ir.saeedrahimi.poolnegar.database.DAO.PersonDAO;
//import ir.saeedrahimi.poolnegar.Gateways.SmsGateway;
//import ir.saeedrahimi.poolnegar.Gateways.WorkDayGateway;
//import ir.saeedrahimi.poolnegar.R;
//import ir.saeedrahimi.poolnegar.dialogs.MessageDialog;
//
///**
// * Created by Saeed on 3/19/2017.
// */
//
//public class DbHelper
//        extends DatabaseHelper {
//    private final Context _context;
//    public int offset;
//
//    public DbHelper(Context paramContext) {
//        super(paramContext);
//        this._context = paramContext;
//    }
//
//    public void repairDatabase() {
//        PersonGateway pg = new PersonGateway(_context);
//        pg.create();
//        WorkDayGateway wdg = new WorkDayGateway(_context);
//        wdg.create();
//
//
//        SmsGateway smsGateway = new SmsGateway(_context);
//        smsGateway.create();
//
//        execQuery("ALTER TABLE Activity ADD COLUMN Act_tags text default ''");
//
//        if (!AccountExists(Constants.ACCOUNTS.WorkerBill_INT))
//        {
//            try {
//                ContentValues values = new ContentValues();
//                values.put("Ac_title", "نیروی کار");
//                values.put("Ac_info", "");
//                values.put("Ac_balance", 0);
//                values.put("Ac_recivable", 1);
//                values.put("Ac_payable", 1);
//                values.put("Ac_parent_id", Constants.ACCOUNTS.BILLS_INT);
//                values.put("Ac_isSystematic", 1);
//                values.put("Ac_isLeaf", 0);
//                values.put("Ac_id", Constants.ACCOUNTS.WorkerBill_INT);
//                insert(values, ACCOUNTS);
//            } catch (Exception localException) {
//            }
//        }
//        if (!AccountExists(0, "اشخاص")) {
//            try {
//                ContentValues values = new ContentValues();
//                values.put("Ac_title", "اشخاص");
//                values.put("Ac_info", "");
//                values.put("Ac_balance", 0);
//                values.put("Ac_recivable", 1);
//                values.put("Ac_payable", 1);
//                values.put("Ac_parent_id", 0);
//                values.put("Ac_isSystematic", 1);
//                values.put("Ac_isLeaf", 0);
//                values.put("Ac_id", Constants.ACCOUNTS.PERSONS_INT);
//                insert(values, ACCOUNTS);
//            } catch (Exception localException) {
//            }
//        }
//
//    }
//
//    public long insert(ContentValues values, String tableName) {
//        return getWritableDatabase().insert(tableName, null, values);
//    }
//
//    public void execQuery(String query) {
//        SQLiteDatabase sqlDb = null;
//        try {
//            sqlDb = getWritableDatabase();
//            sqlDb.execSQL(query);
//            sqlDb.close();
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//
//    }
//
//    public void dataBackUp(Context context, String filename) {
//
//        File sd;
//        try {
//
//            sd = Environment.getExternalStorageDirectory();
//            if (sd.canWrite()) {
//                String backupDBFile = Constants.backupDBPath + "/" + filename + Constants.backupFileExt;
//                File data = Environment.getDataDirectory();
//                File curFile = new File(data, Constants.currentDBPath);
//                File outFile = new File(sd, backupDBFile);
//                FileChannel inChannel = new FileInputStream(curFile).getChannel();
//                FileChannel outChannel = new FileOutputStream(outFile).getChannel();
//                outChannel.transferFrom(inChannel, 0L, inChannel.size());
//                inChannel.close();
//                outChannel.close();
//                new MessageDialog(context, null, context.getResources().getString(R.string.success), context.getResources().getString(R.string.exportcomplete), context.getResources().getString(R.string.exportcomplete2), "exportcomplete", true);
//                return;
//            }
//        } catch (Exception e) {
//            new MessageDialog(context, null, context.getResources().getString(R.string.failed), context.getResources().getString(R.string.exportfailed), context.getResources().getString(R.string.exportfailed2), "exportfailed", true);
//            Log.d("Export Error: ", e.getLocalizedMessage() + "");
//        }
//    }
//
//    public int update(String tableName, ContentValues values, String where, String[] whereValues) {
//        return getWritableDatabase().update(tableName, values, where, whereValues);
//    }
//}
