//package ir.saeedrahimi.poolnegar.Helper;
//
//import ir.saeedrahimi.poolnegar.CalendarTool;
//import ir.saeedrahimi.poolnegar.CheqReminderManager;
//import ir.saeedrahimi.poolnegar.Constants;
//import ir.saeedrahimi.poolnegar.database.model.Account;
//import ir.saeedrahimi.poolnegar.models.BillItem;
//import ir.saeedrahimi.poolnegar.models.BillProjectItem;
//import ir.saeedrahimi.poolnegar.models.CheqItem;
//import ir.saeedrahimi.poolnegar.models.CheqReminderItem;
//import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.UUID;
//
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//import android.util.Log;
//
//import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
//
//public class DatabaseHelper extends SQLiteAssetHelper {
//
//    /*
//     * Database stores an ID, name and content for each ASCII image Content is
//     * text characters Name is reference to date and time created or modified
//     */
//    // db version
//    static String DATABASE_NAME = "artem.db";
//    static String ACCOUNTS = "Accounts";
//    static String BANK_ACCOUNTS = "BankAccounts";
//    static String BANK = "Bank";
//    static String ACTIVITY = "Activity";
//    static String DEFAULTTRANSACTIONS = "DefaultTransactions";
//    static String CHEQ = "cheq";
//    static String USERS = "Users";
//    static String CHEQ_REMINDER = "CheqReminder";
//    static String TICKET = "Ticket";
//    static final int DATABASE_VERSION = 4;
//    public static final int BILL_PARENT = 8;
//    Context context;
//
//    public DatabaseHelper(Context context) {
//        super(context, DATABASE_NAME, null, DATABASE_VERSION);
//        this.context = context;
//    }
//
//    public List<String[]> Accounts() {
//        ArrayList<String[]> listAcc = new ArrayList<String[]>();
//        String[] params = new String[1];
//        params[0] = Integer.toString(0);
//        SQLiteDatabase _db = getReadableDatabase();
//        Cursor localCursor = _db.rawQuery("SELECT  Ac_title , Ac_id FROM "
//                + ACCOUNTS + " WHERE Ac_parent_id = ? ", params);
//        String[] idLists = new String[localCursor.getCount()];
//        String[] titleLists = new String[localCursor.getCount()];
//        if (localCursor.moveToFirst()) {
//            int m = 0;
//            do {
//                Log.d("account names ", localCursor.getString(0));
//                idLists[m] = Integer.toString(localCursor.getInt(1));
//                titleLists[m] = localCursor.getString(0);
//                m++;
//            } while (localCursor.moveToNext());
//        }
//        listAcc.add(titleLists);
//        listAcc.add(idLists);
//        localCursor.close();
//        //_db.close();
//        return listAcc;
//    }
//
//    public int ProjectsCount() {
//        int retval = 0;
//        String[] params = new String[1];
//        params[0] = Integer.toString(Constants.ACCOUNTS.PROJECTS_INT);
//        SQLiteDatabase _db = getReadableDatabase();
//        Cursor localCursor = _db.rawQuery("SELECT  Count(Ac_id) FROM "
//                + ACCOUNTS + " WHERE Ac_parent_id = ? ", params);
//        String[] idLists = new String[localCursor.getCount()];
//        String[] titleLists = new String[localCursor.getCount()];
//        if (localCursor.moveToFirst()) {
//            retval = localCursor.getInt(0);
//        }
//        localCursor.close();
//        _db.close();
//        return retval;
//    }
//
//    public List<String[]> Accounts(int acType) {
//        ArrayList<String[]> accList = new ArrayList<String[]>();
//        String[] arrayOfString1 = new String[2];
//        arrayOfString1[0] = Integer.toString(0);
//        arrayOfString1[1] = Integer.toString(9);
//        SQLiteDatabase _db = getReadableDatabase();
//        Cursor localCursor = null;
//        if (acType == 1) {
//            localCursor = _db
//                    .rawQuery(
//                            "SELECT Ac_title , Ac_id FROM "
//                                    + ACCOUNTS
//                                    + " as DB1 WHERE Ac_parent_id = 0 AND Ac_id in ("
//                                    + " SELECT Ac_parent_id FROM "
//                                    + ACCOUNTS
//                                    + " as DB2 WHERE DB2.Ac_parent_id = DB1.Ac_id AND Ac_recivable = 1  ) AND Ac_id != 9 ",
//                            null);
//        } else if (acType == 2) {
//            localCursor = _db
//                    .rawQuery(
//                            "SELECT Ac_title , Ac_id FROM "
//                                    + ACCOUNTS
//                                    + " as DB1 WHERE Ac_parent_id = 0 AND Ac_id in ("
//                                    + " SELECT Ac_parent_id FROM "
//                                    + ACCOUNTS
//                                    + " as DB2 WHERE DB2.Ac_parent_id = DB1.Ac_id AND Ac_payable = 1  ) AND Ac_id != 9 ",
//                            null);
//        } else {
//            localCursor = _db.rawQuery("SELECT  Ac_title , Ac_id FROM "
//                            + ACCOUNTS + " WHERE Ac_parent_id = ? AND Ac_id != ? ",
//                    arrayOfString1);
//        }
//
//        String[] accIds = new String[localCursor.getCount()];
//        String[] accTitles = new String[localCursor.getCount()];
//        if (localCursor.moveToFirst()) {
//            int m = 0;
//            do {
//                Log.d("account names ", localCursor.getString(0));
//                accIds[m] = Integer.toString(localCursor.getInt(1));
//                accTitles[m] = localCursor.getString(0);
//                m++;
//            } while (localCursor.moveToNext());
//        }
//        accList.add(accTitles);
//        accList.add(accIds);
//        localCursor.close();
//        ////_db.close();
//        return accList;
//    }
//
//    public LinkedHashMap<Account, ArrayList<Account>> getSubAccounts(String parentId) {
//
//        LinkedHashMap<Account, ArrayList<Account>> hashItmes = new LinkedHashMap<Account, ArrayList<Account>>();
//        SQLiteDatabase _db = getReadableDatabase();
//        Cursor cursor1 = _db.rawQuery("SELECT Ac_title , Ac_id, Ac_balance FROM " + ACCOUNTS + " WHERE Ac_parent_id = ?  ORDER BY Ac_regDate DESC",
//                new String[]{parentId});
//        Cursor cursor2 = null;
//        if (cursor1.moveToFirst()) {
//            do {
//                Log.d("account names ", cursor1.getString(0));
//                Account key = new Account();
//                key.setAccID(cursor1.getString(cursor1.getColumnIndex("Ac_id")));
//                key.setTitle(cursor1.getString(0));
//                key.setAccGroup(new String[]{parentId,key.getAccID()});
//                key.setBalance(cursor1.getDouble(2));
//                String[] arrayOfInt = new String[3];
//                arrayOfInt[0] = parentId;
//                arrayOfInt[1] = cursor1.getString(1);
//                arrayOfInt[2] = "";
//
//                double temp = 0;
//                if (parentId != Constants.ACCOUNTS.PROJECTS) {
//                    temp = getAccountsActivityBalance(arrayOfInt, 2, "9999/99/99", "");
//                    key.setTotalBalance(cursor1.getDouble(2) + temp);
//                }
//                if (parentId == Constants.ACCOUNTS.PROJECTS)
//                    key.setTotalBalance(cursor1.getDouble(2) - temp);
//                String str2 = "SELECT  Ac_title, Ac_id, Ac_balance FROM " + ACCOUNTS + " WHERE Ac_parent_id = ? ORDER BY Ac_regDate DESC";
//                _db = getReadableDatabase();
//                cursor2 = _db.rawQuery(str2, new String[]{Integer.toString(cursor1.getInt(1))});
//                ArrayList<Account> childs = new ArrayList<Account>();
//                if (cursor2.moveToFirst()) {
//                    do {
//                        Account child = new Account();
//                        child.setAccID(cursor2.getInt(1));
//                        child.setTitle(cursor2.getString(0));
//                        child.setBalance(cursor2.getDouble(2));
//
//                        child.setAccGroup(new int[]{parentId,key.getAccID(),cursor2.getInt(1)});
//                        key.setTotalBalance(key.getTotalBalance() + child.getBalance());
//                        arrayOfInt[2] = cursor2.getInt(1);
//                        if (parentId != Constants.ACCOUNTS.PROJECTS_INT) {
//                            temp = getAccountsActivityBalance(arrayOfInt, 2, "9999/99/99", "");
//                            child.setTotalBalance(cursor2.getDouble(2) + temp);
//                        }
//                        Log.d("child is ", cursor2.getString(0));
//                        childs.add(child);
//                    } while (cursor2.moveToNext());
//                }
//                hashItmes.put(key, childs);
//                if (cursor2 != null && !cursor2.isClosed())
//                    cursor2.close();
//            } while (cursor1.moveToNext());
//        }
//        if (cursor1 != null && !cursor1.isClosed())
//            cursor1.close();
//        ////_db.close();
//        return hashItmes;
//    }
//
//    public LinkedHashMap getSubAccounts(int parentId, int payRecType, boolean hideClosed) {
//        ArrayList list1 = new ArrayList();
//        LinkedHashMap hashMap = new LinkedHashMap();
//        SQLiteDatabase _db = getReadableDatabase();
//        Cursor cursor1 = null;
//        String closedWhere = hideClosed ? " AND Ac_isClosed != 'Y' " : "";
//        //just Receivables
//        if (payRecType == 0) {
//            String query1 = "SELECT Ac_title , Ac_id FROM " + ACCOUNTS
//                    + " WHERE Ac_parent_id = ? AND Ac_recivable = ?"
//                    + closedWhere + " ORDER BY Ac_id";
//            cursor1 = _db.rawQuery(query1,
//                    new String[]{Integer.toString(parentId), "1"});
//        }
//        //just Payable
//        else if (payRecType == 1) {
//            String query = "SELECT Ac_title , Ac_id FROM " + ACCOUNTS
//                    + " WHERE Ac_parent_id = ? AND Ac_payable = ?"
//                    + closedWhere + " ORDER BY Ac_id";
//
//            cursor1 = _db.rawQuery(query,
//                    new String[]{Integer.toString(parentId), "1"});
//        }
//        //Both Payable and Receivables
//        else if (payRecType == 2) {
//            String query = "SELECT Ac_title , Ac_id FROM " + ACCOUNTS
//                    + " WHERE Ac_parent_id = ?"
//                    + closedWhere + " ORDER BY Ac_id";
//
//            cursor1 = _db.rawQuery(query,
//                    new String[]{Integer.toString(parentId)});
//        }
//        ArrayList list2 = null;
//        Cursor cursor2 = null;
//        if (cursor1.moveToFirst()) {
//            do {
//                list2 = new ArrayList();
//                int m = cursor1.getInt(1);
//                Log.d("account names ", cursor1.getString(0));
//                list1.add(cursor1.getString(0));
//                cursor2 = null;
//                //just Receivables
//                if (payRecType == 0) {
//                    String str2 = "SELECT  Ac_title FROM " + ACCOUNTS
//                            + " WHERE Ac_parent_id = ? AND Ac_recivable = ? "
//                            + closedWhere;
//                    String[] arrayOfString1 = new String[2];
//                    arrayOfString1[0] = Integer.toString(m);
//                    arrayOfString1[1] = Integer.toString(1);
//                    cursor2 = _db.rawQuery(str2, arrayOfString1);
//                }
//                //just Payable
//                else if (payRecType == 1) {
//                    String str3 = "SELECT  Ac_title FROM " + ACCOUNTS
//                            + " WHERE Ac_parent_id = ? AND Ac_payable = ? "
//                            + closedWhere;
//                    String[] arrayOfString2 = new String[2];
//                    arrayOfString2[0] = Integer.toString(m);
//                    arrayOfString2[1] = Integer.toString(1);
//                    cursor2 = _db.rawQuery(str3, arrayOfString2);
//                }
//                //Both Payable and Receivables
//                else if (payRecType == 2) {
//                    String str3 = "SELECT  Ac_title FROM " + ACCOUNTS
//                            + " WHERE Ac_parent_id = ? "
//                            + closedWhere;
//                    String[] arrayOfString2 = new String[1];
//                    arrayOfString2[0] = Integer.toString(m);
//                    cursor2 = _db.rawQuery(str3, arrayOfString2);
//                }
//                label313:
//                {
//                    if (!cursor2.moveToFirst()) {
//                        Log.d("in empty cursor", "ok ");
//                        break label313;
//                    }
//                    do {
//                        list2.add(cursor2.getString(0));
//                        Log.d("child is ", cursor2.getString(0));
//                    } while (cursor2.moveToNext());
//                    if (cursor2 != null && !cursor2.isClosed())
//                        cursor2.close();
//                }
//                hashMap.put(cursor1.getString(0), list2);
//            } while (cursor1.moveToNext());
//        }
//
//        if (cursor2 != null && !cursor2.isClosed())
//            cursor2.close();
//        if (cursor1 != null && !cursor1.isClosed())
//            cursor1.close();
//        ////_db.close();
//        return hashMap;
//    }
//    public LinkedHashMap getSubAccountsWithoutLeaf(int parentId, int payRecType) {
//        ArrayList list1 = new ArrayList();
//        LinkedHashMap hashMap = new LinkedHashMap();
//        SQLiteDatabase _db = getReadableDatabase();
//        Cursor cursor1 = null;
//        //just Receivables
//        if (payRecType == 0) {
//            String query1 = "SELECT Ac_title , Ac_id FROM " + ACCOUNTS
//                    + " WHERE Ac_parent_id = ? AND Ac_recivable = ?  ORDER BY Ac_id";
//            cursor1 = _db.rawQuery(query1,
//                    new String[]{Integer.toString(parentId), "1"});
//        }
//        //just Payable
//        else if (payRecType == 1) {
//            String query = "SELECT Ac_title , Ac_id FROM " + ACCOUNTS
//                    + " WHERE Ac_parent_id = ? AND Ac_payable = ?  ORDER BY Ac_id";
//
//            cursor1 = _db.rawQuery(query,
//                    new String[]{Integer.toString(parentId), "1"});
//        }
//        //Both Payable and Receivables
//        else if (payRecType == 2) {
//            String query = "SELECT Ac_title , Ac_id FROM " + ACCOUNTS
//                    + " WHERE Ac_parent_id = ?  ORDER BY Ac_id";
//
//            cursor1 = _db.rawQuery(query,
//                    new String[]{Integer.toString(parentId)});
//        }
//        ArrayList list2 = null;
//        Cursor cursor2 = null;
//        if (cursor1.moveToFirst()) {
//            do {
//                list2 = new ArrayList();
//                list1.add(cursor1.getString(0));
//
//                hashMap.put(cursor1.getString(0), list2);
//            } while (cursor1.moveToNext());
//        }
//
//        if (cursor2 != null && !cursor2.isClosed())
//            cursor2.close();
//        if (cursor1 != null && !cursor1.isClosed())
//            cursor1.close();
//        ////_db.close();
//        return hashMap;
//    }
//    public HashMap AccountGetSubaccount(String acTitle, int acParentId) {
//        Log.d("SubAccount name in data base", acTitle);
//        HashMap hash = new HashMap();
//        SQLiteDatabase _db = getReadableDatabase();
//        String str = "SELECT  Ac_balance , Ac_info , Ac_recivable , Ac_payable , Ac_id , Ac_isLeaf , Ac_regDate, Ac_introPercent, Ac_workPercent FROM "
//                + ACCOUNTS + " WHERE Ac_title = ? AND Ac_parent_id = ? ";
//        String[] arrayOfString = new String[2];
//        arrayOfString[0] = acTitle;
//        arrayOfString[1] = Integer.toString(acParentId);
//        Cursor cursor = _db.rawQuery(str, arrayOfString);
//        if (cursor.moveToFirst()) {
//            hash.put("Ac_title", acTitle);
//            if (cursor.getString(0) == null) {
//                hash.put("Ac_balance", "0");
//            } else {
//                hash.put("Ac_balance", cursor.getDouble(0));
//            }
//            if (cursor.getString(1) == null) {
//                hash.put("Ac_info", "");
//            } else {
//                hash.put("Ac_info", cursor.getString(1));
//            }
//
//            hash.put("Ac_payable", cursor.getString(3));
//            hash.put("Ac_recivable", cursor.getString(2));
//            hash.put("Ac_id", cursor.getString(4));
//            if (cursor.getString(5) == null) {
//                hash.put("Ac_isLeaf", "0");
//            } else {
//                hash.put("Ac_isLeaf", cursor.getString(5));
//            }
//            hash.put("Ac_regDate", cursor.getString(6));
//            hash.put("Ac_introPercent", cursor.getString(7));
//            hash.put("Ac_workPercent", cursor.getString(8));
//        }
//        if (cursor != null && !cursor.isClosed())
//            cursor.close();
//        ////_db.close();
//        return hash;
//    }
//
//    public String getAccountTitle(int paramInt) {
//        SQLiteDatabase _db = getReadableDatabase();
//        String str1 = "SELECT Ac_title FROM " + ACCOUNTS + " WHERE Ac_id = ? ";
//        String[] params = new String[1];
//        params[0] = Integer.toString(paramInt);
//        Cursor cursor = _db.rawQuery(str1, params);
//        String title = "";
//        if (cursor.moveToFirst()) {
//            title = cursor.getString(0);
//
//        }
//        if (cursor != null && !cursor.isClosed())
//            cursor.close();
//        ////_db.close();
//        return title;
//    }
//
//    private String[] getAccountsTitle(int[] arrIds) {
//        SQLiteDatabase _db = getReadableDatabase();
//        String[] arrTitles = {"", "", ""};
//        String query = "SELECT Ac_title FROM " + ACCOUNTS + " WHERE Ac_id = ? ";
//        Cursor cursor = _db.rawQuery(query,
//                new String[]{Integer.toString(arrIds[0])});
//
//        if (cursor.moveToFirst()) {
//            arrTitles[0] = cursor.getString(0);
//        }
//        cursor.close();
//        query = "SELECT Ac_title FROM " + ACCOUNTS + " WHERE Ac_id = ? ";
//        cursor = _db.rawQuery(query,
//                new String[]{Integer.toString(arrIds[1])});
//        if (cursor.moveToFirst()) {
//            arrTitles[1] = cursor.getString(0);
//        }
//        cursor.close();
//        query = "SELECT Ac_title FROM " + ACCOUNTS + " WHERE Ac_id = ? ";
//        cursor = _db.rawQuery(query,
//                new String[]{Integer.toString(arrIds[2])});
//        if (cursor.moveToFirst()) {
//            arrTitles[2] = cursor.getString(0);
//        }
//        if (cursor != null && !cursor.isClosed())
//            cursor.close();
//        ////_db.close();
//        return arrTitles;
//    }
//
//    public int getAccountId(String title, int parentId) {
//        SQLiteDatabase _db = getReadableDatabase();
//        String str = "SELECT  AC_id FROM " + ACCOUNTS
//                + " WHERE Ac_title = ? AND Ac_parent_id = ?  ";
//        String[] params = new String[2];
//        params[0] = title;
//        params[1] = Integer.toString(parentId);
//        Cursor localCursor = _db.rawQuery(str, params);
//        int m = -1;
//        if (localCursor.moveToFirst()) {
//            m = localCursor.getInt(0);
//        }
//        localCursor.close();
//        //_db.close();
//        return m;
//    }
//
//    public List<String> getUser() {
//        SQLiteDatabase _db = getReadableDatabase();
//        ArrayList<String> userList = new ArrayList();
//        String str = "SELECT Usr_name , Usr_email , Usr_number , Usr_temp1 FROM "
//                + USERS + " WHERE Usr_id = ? ";
//        String[] params = new String[1];
//        params[0] = Integer.toString(1);
//        Cursor cursor = _db.rawQuery(str, params);
//        if (cursor.moveToFirst()) {
//            {
//                userList.add(cursor.getString(0));
//                userList.add(cursor.getString(1));
//                userList.add(cursor.getString(2));
//                userList.add(cursor.getString(3));
//            }
//            while (cursor.moveToNext())
//                ;
//        }
//        cursor.close();
//        //_db.close();
//        return userList;
//    }
//
//    public long AddSubAccount(String title, String info, double balance,
//                              int payable, int recivable, int parentId, int paramInt4, String date, Integer introPercent, Integer workPercent) {
//        SQLiteDatabase _db = getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put("Ac_title", title);
//        values.put("Ac_info", info);
//        values.put("Ac_balance", Double.valueOf(balance));
//        values.put("Ac_recivable", Integer.valueOf(recivable));
//        values.put("Ac_payable", Integer.valueOf(payable));
//        values.put("Ac_parent_id", Integer.valueOf(parentId));
//        values.put("Ac_regDate", date);
//        values.put("Ac_introPercent", introPercent);
//        values.put("Ac_workPercent", workPercent);
//        values.put("Ac_isClosed", "N");
//        if (paramInt4 == 0) {
//            values.put("Ac_isSystematic", Integer.valueOf(0));
//            values.put("Ac_isLeaf", Integer.valueOf(0));
//            values.put("Ac_bank_ac_id", Integer.valueOf(-1));
//            if (parentId == Constants.ACCOUNTS.PROJECTS_INT)
//                values.put("Ac_isProject", Integer.valueOf(1));
//            else
//                values.put("Ac_isProject", Integer.valueOf(0));
//        } else if (paramInt4 == 1) {
//            values.put("Ac_isSystematic", Integer.valueOf(0));
//            values.put("Ac_isLeaf", Integer.valueOf(1));
//            values.put("Ac_bank_ac_id", Integer.valueOf(-1));
//            if (parentId == Constants.ACCOUNTS.PROJECTS_INT)
//                values.put("Ac_isProject", Integer.valueOf(1));
//            else
//                values.put("Ac_isProject", Integer.valueOf(0));
//        } else if (paramInt4 == 2) {
//            values.put("Ac_isSystematic", Integer.valueOf(0));
//            values.put("Ac_isLeaf", Integer.valueOf(1));
//            values.put("Ac_bank_ac_id", Integer.valueOf(-1));
//            values.put("Ac_isProject", Integer.valueOf(1));
//        }
//
//        long l1 = _db.insert(ACCOUNTS, null, values);
//        if (_db != null && _db.isOpen())
//            _db.close();
//        return l1;
//
//    }
//
//    public boolean AccountSubHasBalance(int acID) {
//        SQLiteDatabase _db = getReadableDatabase();
//        String str1 = "select ac_balance from  " + ACCOUNTS + " where ac_id = ?  ";
//        String[] arrayOfString1 = new String[1];
//        arrayOfString1[0] = Integer.toString(acID);
//        Cursor cursor1 = _db.rawQuery(str1, arrayOfString1);
//        if ((cursor1.moveToFirst()) && (cursor1.getFloat(0) != 0.0F)) {
//
//            cursor1.close();
//            //_db.close();
//            return true;
//        }
//        String str2 = "select act_id from " + ACTIVITY + " where (act_pay_subacc_id = ? AND Act_accPay_id = -1) OR (act_rec_subacc_id = ? AND Act_accRecive_id = -1) ";
//        String[] arrayOfString2 = new String[2];
//        arrayOfString2[0] = Integer.toString(acID);
//        arrayOfString2[1] = Integer.toString(acID);
//        Cursor cursor2 = _db.rawQuery(str2, arrayOfString2);
//        if (cursor2.moveToFirst()) {
//            cursor2.close();
//            //_db.close();
//
//            return true;
//        }
//        cursor2.close();
//        //_db.close();
//        return false;
//    }
//
//    public boolean AccountLeafHasTransaction(int acID) {
//        SQLiteDatabase _db = getReadableDatabase();
//        String str = "select act_id from "
//                + ACTIVITY
//                + " where (act_pay_subacc_id = ? ) OR (act_rec_subacc_id = ?) OR (act_accpay_id = ?) OR (act_accrecive_id = ?) ";
//        String[] params = new String[4];
//        params[0] = Integer.toString(acID);
//        params[1] = Integer.toString(acID);
//        params[2] = Integer.toString(acID);
//        params[3] = Integer.toString(acID);
//        Cursor cursor = _db.rawQuery(str, params);
//        if (cursor.moveToFirst()) {
//            cursor.close();
//            //_db.close();
//            return true;
//        }
//        return false;
//    }
//
//    public boolean AccountExists(int acID) {
//        SQLiteDatabase sqlDb = getReadableDatabase();
//        try {
//            String str = "SELECT  Ac_title FROM " + ACCOUNTS
//                    + " WHERE Ac_id = ?";
//            String[] params = new String[1];
//            params[0] = Integer.toString(acID);
//            Cursor localCursor = sqlDb.rawQuery(str, params);
//            boolean find = localCursor.moveToFirst();
//
//            sqlDb.close();
//            if (find) {
//                return find;
//            }
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//
//        return false;
//    }
//    public boolean AccountExists(int parentID, String acTitle) {
//        SQLiteDatabase sqlDb = getReadableDatabase();
//        try {
//            String str = "SELECT  Ac_title FROM " + ACCOUNTS
//                    + " WHERE Ac_title = ? AND Ac_parent_id = ? ";
//            String[] params = new String[2];
//            params[0] = acTitle;
//            params[1] = Integer.toString(parentID);
//            Cursor localCursor = sqlDb.rawQuery(str, params);
//            boolean find = localCursor.moveToFirst();
//
//            sqlDb.close();
//            if (find) {
//                return find;
//            }
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//
//        return false;
//    }
//
//    public boolean AccountIsSystematic(String title, int parentId) {
//        SQLiteDatabase localSQLiteDatabase = getReadableDatabase();
//        String str = "SELECT Ac_isSystematic FROM " + ACCOUNTS
//                + " WHERE Ac_title = ? AND Ac_parent_id = ? ";
//        String[] params = new String[2];
//        params[0] = title;
//        params[1] = Integer.toString(parentId);
//        Cursor localCursor = localSQLiteDatabase.rawQuery(str, params);
//        if ((localCursor.moveToFirst()) && (localCursor.getInt(0) == 1)) {
//            localCursor.close();
//            localSQLiteDatabase.close();
//            return true;
//        }
//        localCursor.close();
//        localSQLiteDatabase.close();
//        return false;
//    }
//
//    public boolean AccountHasChild(int parentId) {
//        SQLiteDatabase _db = getReadableDatabase();
//        String str = "SELECT Ac_id FROM " + ACCOUNTS
//                + " WHERE Ac_parent_id = ? ";
//        String[] params = new String[1];
//        params[0] = Integer.toString(parentId);
//        Cursor cursor = _db.rawQuery(str, params);
//        if (cursor.moveToFirst()) {
//            cursor.close();
//            //_db.close();
//            return true;
//        }
//        cursor.close();
//        //_db.close();
//        return false;
//    }
//
//    public void deleteAccount(int parentId, String title) {
//        int m = getAccountId(title, parentId);
//        SQLiteDatabase _db = getWritableDatabase();
//        try {
//            String[] arrayOfString1 = new String[2];
//            arrayOfString1[0] = title;
//            arrayOfString1[1] = Integer.toString(parentId);
//            _db.delete(ACCOUNTS, "Ac_title = ? AND Ac_parent_id = ? ",
//                    arrayOfString1);
//            String[] arrayOfString2 = new String[1];
//            arrayOfString2[0] = Integer.toString(m);
//            _db.delete(ACCOUNTS, "Ac_parent_id = ? ", arrayOfString2);
//
//        } finally {
//            //_db.close();
//        }
//        return;
//    }
//
//    public void deleteAccount(int acId) {
//        SQLiteDatabase sqlDb = getWritableDatabase();
//        try {
//            sqlDb = getWritableDatabase();
//            sqlDb.delete(ACCOUNTS, "Ac_id = ?",
//                    new String[]{Integer.toString(acId)});
//            sqlDb.delete(ACCOUNTS, "Ac_parent_id = ? ",
//                    new String[]{Integer.toString(acId)});
//            sqlDb.close();
//        } catch (Exception ex) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//    }
//
//    public void fullDeleteAccount(int acId) {
//        SQLiteDatabase _db = getWritableDatabase();
//        try {
//            _db.delete(ACCOUNTS, "Ac_id = ?",
//                    new String[]{Integer.toString(acId)});
//            _db.delete(ACCOUNTS, "Ac_parent_id = ? ",
//                    new String[]{Integer.toString(acId)});
//            _db.delete(ACTIVITY, "Act_accProject_id = ? ",
//                    new String[]{Integer.toString(acId)});
//        } finally {
//            //_db.close();
//        }
//        return;
//    }
//
//    public void AccountSetLeaf(int leaf, int accId) {
//        SQLiteDatabase _db = getWritableDatabase();
//        String str = "SELECT  Ac_id FROM " + ACCOUNTS
//                + " WHERE Ac_parent_id = ? ";
//        String[] params1 = new String[1];
//        params1[0] = Integer.toString(accId);
//        Cursor cursor = _db.rawQuery(str, params1);
//        int m;
//        if (cursor.moveToFirst()) {
//            if (leaf == 1) {
//                cursor.close();
//                //_db.close();
//                return;
//            }
//        }
//        String[] params2 = new String[2];
//        params2[0] = Integer.toString(leaf);
//        params2[1] = Integer.toString(accId);
//        _db.execSQL("update Accounts set Ac_isLeaf= ?  where Ac_id = ? ",
//                params2);
//        cursor.close();
//        //_db.close();
//        return;
//    }
//
//    public void AccountAddBalance(int accID, double amount, int print) {
//        SQLiteDatabase _db = getWritableDatabase();
//        if (print == 0) {
//            _db.execSQL("UPDATE " + ACCOUNTS
//                    + " SET Ac_balance = Ac_balance + " + amount
//                    + " WHERE Ac_id = " + accID + " ");
//        } else {
//            Log.d("Mydatabase update balance", " id : " + accID);
//            _db.execSQL("UPDATE " + ACCOUNTS
//                    + " SET Ac_balance = Ac_balance + " + amount
//                    + " WHERE Ac_id = " + accID + " ");
//        }
//        //_db.close();
//        return;
//    }
//
//    public int AccountUpdate(String newTitle, double newBalance,
//                             String newInfo, int newPayable, int newRecivable, int accID,
//                             String date, Integer introPercent, Integer workPercent) {
//        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put("Ac_title", newTitle);
//        values.put("Ac_balance", newBalance);
//        values.put("Ac_info", newInfo);
//        values.put("Ac_payable", Integer.valueOf(newPayable));
//        values.put("Ac_recivable", Integer.valueOf(newRecivable));
//        values.put("Ac_regDate", date);
//        values.put("Ac_introPercent", introPercent);
//        values.put("Ac_workPercent", workPercent);
//        String[] params = new String[1];
//        params[0] = String.valueOf(accID);
//        int m = localSQLiteDatabase.update(ACCOUNTS, values, "Ac_id = ?",
//                params);
//        localSQLiteDatabase.close();
//        return m;
//    }
//
//    public List<Account> Projects(boolean hideClosed) {
//        ArrayList<Account> localArrayList = new ArrayList<Account>();
//        String closedWhere = hideClosed ? " AND Ac_isClosed != 'Y'" : "";
//        SQLiteDatabase _db = getReadableDatabase();
//        String str = "Select Ac_id , Ac_title , Ac_recivable , Ac_payable , Ac_balance , Ac_info, Ac_regDate FROM "
//                + ACCOUNTS + " WHERE Ac_isProject = ? " + closedWhere + " ORDER BY Ac_regDate DESC";
//        String[] arrayOfString = new String[1];
//        arrayOfString[0] = Integer.toString(1);
//        Cursor localCursor = _db.rawQuery(str, arrayOfString);
//        if (localCursor.moveToFirst()) {
//            do {
//                Account pItem = new Account();
//                pItem.setTitle(localCursor.getString(1));
//                pItem.setAccID(localCursor.getInt(0));
//                pItem.setReciveable(localCursor.getInt(2));
//                pItem.setPayable(localCursor.getInt(3));
//                pItem.setBalance(localCursor.getDouble(4));
//                pItem.setInfo(localCursor.getString(5));
//                pItem.setDate(localCursor.getString(6));
//                localArrayList.add(pItem);
//            } while (localCursor.moveToNext());
//        }
//        localCursor.close();
//        //_db.close();
//        return localArrayList;
//    }
//
//    public void AddTransaction(int serial, int set, int accPay_id,
//                               int pay_Subacc_id, int pay_Root_id, int accRecive_id,
//                               int rec_Subacc_id, int rec_Root_id, int projectId, double amount,
//                               String date, String info, String image_path) {
//        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
//        ContentValues localContentValues = new ContentValues();
//        localContentValues.put("Act_info", info);
//        localContentValues.put("Act_accRecive_id",
//                Integer.valueOf(accRecive_id));
//        localContentValues.put("Act_rec_Root_id", Integer.valueOf(rec_Root_id));
//        localContentValues.put("Act_rec_Subacc_id",
//                Integer.valueOf(rec_Subacc_id));
//        Log.d("Act_accRecive_id", accRecive_id + "");
//        Log.d("Act_rec_Root_id", rec_Root_id + "");
//        Log.d("Act_rec_Subacc_id", rec_Subacc_id + "");
//        localContentValues.put("Act_accPay_id", Integer.valueOf(accPay_id));
//        localContentValues.put("Act_pay_Root_id", Integer.valueOf(pay_Root_id));
//        localContentValues.put("Act_pay_Subacc_id",
//                Integer.valueOf(pay_Subacc_id));
//        Log.d("Act_accPay_id", accPay_id + "");
//        Log.d("Act_pay_Root_id", pay_Root_id + "");
//        Log.d("Act_pay_Subacc_id", pay_Subacc_id + "");
//        localContentValues.put("Act_amount", Double.valueOf(amount));
//        localContentValues.put("Act_date", date);
//        localContentValues.put("Act_accProject_id", Integer.valueOf(projectId));
//        localContentValues.put("Act_ser", Integer.valueOf(serial));
//        localContentValues.put("Act_iscash", Integer.valueOf(1));
//        localContentValues.put("Act_set", Integer.valueOf(set));
//        localContentValues.put("Act_cheq_id", Integer.valueOf(-1));
//        localContentValues.put("image_path", image_path);
//        localSQLiteDatabase.insertOrThrow(ACTIVITY, null, localContentValues);
//        localSQLiteDatabase.close();
//
//    }
//
//    public void AddOutcomeTransaction(int serial, int set, int accPay_id,
//                                      int pay_Subacc_id, int pay_Root_id, int accRecive_id,
//                                      int rec_Subacc_id, int rec_Root_id, int projectId, double amount,
//                                      String date, String info, int isCash, String chSerial, String chDate, int bankId, int isCheqPassed) {
//        SQLiteDatabase sqlDb = null;
//        try {
//            sqlDb = getWritableDatabase();
//            ContentValues values = new ContentValues();
//            long newTransId = 0;
//            if (isCash == 1) {
//                values.put("Act_accPay1_id", Integer.valueOf(-1));
//                values.put("Act_info", info);
//                values.put("Act_accRecive_id", Integer.valueOf(accRecive_id));
//                values.put("Act_rec_Root_id", Integer.valueOf(rec_Root_id));
//                values.put("Act_rec_Subacc_id", Integer.valueOf(rec_Subacc_id));
//                values.put("Act_accPay_id", Integer.valueOf(accPay_id));
//                values.put("Act_pay_Root_id", Integer.valueOf(pay_Root_id));
//                values.put("Act_pay_Subacc_id", Integer.valueOf(pay_Subacc_id));
//                values.put("Act_amount", Double.valueOf(amount));
//                values.put("Act_date", date);
//                values.put("Act_accProject_id", Integer.valueOf(projectId));
//                values.put("Act_ser", Integer.valueOf(serial));
//                values.put("Act_iscash", Integer.valueOf(isCash));
//                values.put("Act_set", Integer.valueOf(set));
//                values.put("Act_cheq_id", Integer.valueOf(-1));
//                values.put("Act_isCheq_passed", Integer.valueOf(-1));
//
//            } else if (isCash == 0) {
//                values.put("Act_accPay1_id", Integer.valueOf(-1));
//                values.put("Act_info", info);
//                values.put("Act_accRecive_id", Integer.valueOf(accRecive_id));
//                values.put("Act_rec_Root_id", Integer.valueOf(rec_Root_id));
//                values.put("Act_rec_Subacc_id", Integer.valueOf(rec_Subacc_id));
//                values.put("Act_accPay_id", Integer.valueOf(accPay_id));
//                values.put("Act_pay_Root_id", Integer.valueOf(pay_Root_id));
//                values.put("Act_pay_Subacc_id", Integer.valueOf(pay_Subacc_id));
//                values.put("Act_amount", Double.valueOf(amount));
//                values.put("Act_isCheq_passed", Integer.valueOf(isCheqPassed));
//                values.put("Act_date", date);
//                values.put("Act_accProject_id", Integer.valueOf(projectId));
//                values.put("Act_ser", Integer.valueOf(serial));
//                values.put("Act_iscash", Integer.valueOf(isCash));
//                values.put("Act_set", Integer.valueOf(set));
//            }
//            Log.d("Act_accRecive_id", accRecive_id + "");
//            Log.d("Act_rec_Root_id", rec_Root_id + "");
//            Log.d("Act_rec_Subacc_id", rec_Subacc_id + "");
//            Log.d("-----------", "------------");
//            Log.d("Act_accPay_id", accPay_id + "");
//            Log.d("Act_pay_Root_id", pay_Root_id + "");
//            Log.d("Act_pay_Subacc_id", pay_Subacc_id + "");
//
//            newTransId = sqlDb.insert(ACTIVITY, null, values);
//            if (accPay_id != -1)
//                AddCheq((int) newTransId, chSerial, amount, chDate, bankId,
//                        pay_Subacc_id, set);
//            sqlDb.close();
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//
//        //_db.close();
//    }
//
//    public void AddCheq(int transId, String chSerial, double amount,
//                        String date, int bankId, int acc_id_received, int set) {
//        SQLiteDatabase _db = getWritableDatabase();
//        ContentValues values1 = new ContentValues();
//        values1.put("ch_activity_id", Integer.valueOf(transId));
//        values1.put("ch_serial", chSerial);
//        values1.put("ch_amount", Double.valueOf(amount));
//        values1.put("ch_date", date);
//        values1.put("ch_bankAccount_id", Integer.valueOf(bankId));
//        values1.put("ch_acc_id_received", Integer.valueOf(acc_id_received));
//        values1.put("ch_set", Integer.valueOf(set));
//        int m = (int) _db.insert(CHEQ, null, values1);
//        ContentValues values2 = new ContentValues();
//        values2.put("Act_cheq_id", Integer.valueOf(m));
//        _db.update(ACTIVITY, values2, "Act_id = ?",
//                new String[]{Integer.toString(transId)});
//
//        CalendarTool ct = new CalendarTool();
//
//        StringBuilder sb = new StringBuilder();
//        sb.append(ct.getIranianYear()).append("/");
//        sb.append(String.format("%02d", Integer.valueOf(ct.getIranianMonth())))
//                .append("/");
//        sb.append(String.format("%02d", Integer.valueOf(ct.getIranianDay())));
//
//        if ((set == 0) && (date.compareTo(sb.toString()) >= 0)) {
//            _db.execSQL("insert into " + CHEQ_REMINDER
//                    + "(Chr_uniqId , Chr_date , Chr_transactionId)"
//                    + " select '" + UUID.randomUUID().toString() + "' , '"
//                    + date + "' ," + transId + " WHERE NOT EXISTS "
//                    + " (select 1 from " + CHEQ_REMINDER
//                    + " WHERE Chr_transactionId =" + transId + " )");
//            new CheqReminderManager(this.context).setAlarm(date, transId);
//        }
//        //_db.close();
//    }
//
//    public void EditTransaction(int serial, int accPay_id, int pay_Subacc_id,
//                                int pay_Root_id, int accRecive_id, int rec_Subacc_id,
//                                int rec_Root_id, int projectId, double amount, String date,
//                                String info) {
//        SQLiteDatabase sqlDb = null;
//        try {
//            sqlDb = getWritableDatabase();
//            ContentValues values = new ContentValues();
//            values.put("Act_accRecive_id",
//                    Integer.valueOf(accRecive_id));
//            values.put("Act_rec_Root_id", Integer.valueOf(rec_Root_id));
//            values.put("Act_rec_Subacc_id",
//                    Integer.valueOf(rec_Subacc_id));
//            values.put("Act_accPay_id", Integer.valueOf(accPay_id));
//            values.put("Act_pay_Root_id", Integer.valueOf(pay_Root_id));
//            values.put("Act_pay_Subacc_id",
//                    Integer.valueOf(pay_Subacc_id));
//            values.put("Act_date", date);
//            values.put("Act_amount", Double.valueOf(amount));
//            values.put("Act_info", info);
//            values.put("Act_accProject_id", Integer.valueOf(projectId));
//            sqlDb.update(ACTIVITY, values,
//                    " Act_id = ? ", new String[]{Integer.toString(serial)});
//            sqlDb.close();
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//
//    }
//
//    public void UpdateTransaction(int serial,double amount, String date,
//                                String info) {
//        SQLiteDatabase sqlDb = null;
//        try {
//            sqlDb = getWritableDatabase();
//            ContentValues values = new ContentValues();
//            values.put("Act_date", date);
//            values.put("Act_amount", Double.valueOf(amount));
//            values.put("Act_info", info);
//            sqlDb.update(ACTIVITY, values,
//                    " Act_id = ? ", new String[]{Integer.toString(serial)});
//            sqlDb.close();
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//
//    }
//    public void UpdateTransactionProject(int serial,String projectID) {
//        SQLiteDatabase sqlDb = null;
//        try {
//            sqlDb = getWritableDatabase();
//            ContentValues values = new ContentValues();
//            values.put("Act_accProject_id", projectID);
//            sqlDb.update(ACTIVITY, values,
//                    " Act_id = ? ", new String[]{Integer.toString(serial)});
//            sqlDb.close();
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//
//    }
//    public void SetTransactionTag(int serial,String tags) {
//        SQLiteDatabase sqlDb = null;
//        try {
//            sqlDb = getWritableDatabase();
//            ContentValues values = new ContentValues();
//            values.put("Act_tags", tags);
//            sqlDb.update(ACTIVITY, values,
//                    " Act_id = ? ", new String[]{Integer.toString(serial)});
//            sqlDb.close();
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//
//    }
//    public void AddTransactionTag(int serial,String tags) {
//        SQLiteDatabase sqlDb = null;
//        try {
//            String oldTags=getTransactionTag(serial);
//            String newTags=tags;
//            if(oldTags.length()>0)
//                newTags = oldTags+","+tags;
//            sqlDb = getWritableDatabase();
//            ContentValues values = new ContentValues();
//            values.put("Act_tags", newTags);
//            sqlDb.update(ACTIVITY, values,
//                    " Act_id = ? ", new String[]{Integer.toString(serial)});
//            sqlDb.close();
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//
//    }
//    public String getTransactionTag(int serial) {
//        SQLiteDatabase _db = getReadableDatabase();
//        Cursor cursor = _db.rawQuery("SELECT Act_tags FROM " + ACTIVITY
//                + " WHERE Act_id = "+serial+"", null);
//        String tags = "";
//        if (cursor.moveToFirst()) {
//            tags = cursor.getString(0);
//        }
//        cursor.close();
//        //_db.close();
//        return tags;
//
//    }
//    public double getTransactioAmount(int serial) {
//        SQLiteDatabase sqlDb = null;
//        try {
//            sqlDb = getWritableDatabase();
//            Cursor cursor = sqlDb.rawQuery("SELECT Act_amount FROM " + ACTIVITY
//                    + " WHERE Act_id = "+serial, null);
//            if (cursor.moveToFirst()) {
//                return cursor.getDouble(0);
//            }
//            sqlDb.close();
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//        return 0;
//
//    }
//
//    public void EditOutcomeTransaction(int serial, int isCash, int accPay_id, int pay_Subacc_id,
//                                       int pay_Root_id, int accRecive_id, int rec_Subacc_id,
//                                       int rec_Root_id, int projectId, double amount, String date,
//                                       String info) {
//        if (isCash == 1) {
//
//            SQLiteDatabase _db = getWritableDatabase();
//            ContentValues param = new ContentValues();
//            param.put("Act_accRecive_id", Integer.valueOf(accRecive_id));
//            param.put("Act_rec_Root_id", Integer.valueOf(rec_Root_id));
//            param.put("Act_rec_Subacc_id", Integer.valueOf(rec_Subacc_id));
//            param.put("Act_accPay_id", Integer.valueOf(accPay_id));
//            param.put("Act_pay_Root_id", Integer.valueOf(pay_Root_id));
//            param.put("Act_pay_Subacc_id", Integer.valueOf(pay_Subacc_id));
//            param.put("Act_date", date);
//            param.put("Act_amount", Double.valueOf(amount));
//            param.put("Act_info", info);
//            param.put("Act_accProject_id", Integer.valueOf(projectId));
//            param.put("Act_cheq_id", Integer.valueOf(-1));
//            param.put("Act_isCheq_passed", Integer.valueOf(-1));
//            _db.update(ACTIVITY, param, " Act_id = ? ", new String[]{Integer.toString(serial)});
//            _db.delete(CHEQ, " Ch_activity_id = ? ", new String[]{Integer.toString(serial)});
//            //_db.close();
//        }
//    }
//
//    public int NewTransID() {
//        SQLiteDatabase _db = getReadableDatabase();
//        Cursor cursor = _db.rawQuery("SELECT Act_ser FROM " + ACTIVITY
//                + " WHERE Act_id = (SELECT MAX(Act_id) FROM Activity)", null);
//        int m = 1;
//        if (cursor.moveToFirst()) {
//            m = 1 + cursor.getInt(0);
//        }
//        cursor.close();
//        //_db.close();
//        return m;
//    }
//
//    public void AddPaybillTransaction(int serial, int AccPay1_id,
//                                      int accPay_id, int pay_Subacc_id, int pay_Root_id,
//                                      int accRecive_id, int rec_Subacc_id, int rec_Root_id,
//                                      double amount, String date, String info) {
//        SQLiteDatabase _db = getWritableDatabase();
//        ContentValues values = new ContentValues();
//
//        values.put("Act_info", info);
//        values.put("Act_accPay1_id", AccPay1_id);
//        values.put("Act_accRecive_id", Integer.valueOf(accRecive_id));
//        values.put("Act_rec_Root_id", Integer.valueOf(rec_Root_id));
//        values.put("Act_rec_Subacc_id", Integer.valueOf(rec_Subacc_id));
//        values.put("Act_accPay_id", Integer.valueOf(accPay_id));
//        values.put("Act_pay_Root_id", Integer.valueOf(pay_Root_id));
//        values.put("Act_pay_Subacc_id", Integer.valueOf(pay_Subacc_id));
//        values.put("Act_amount", Double.valueOf(amount));
//        values.put("Act_date", date);
//        values.put("Act_accProject_id", Integer.valueOf(-1));
//        values.put("Act_ser", Integer.valueOf(serial));
//        values.put("Act_iscash", Integer.valueOf(1));
//        values.put("Act_set", Integer.valueOf(1));
//        values.put("Act_cheq_id", Integer.valueOf(-1));
//        values.put("Act_isCheq_passed", Integer.valueOf(-1));
//
//        Log.d("Paybill:", "--");
//        Log.d("Act_accRecive_id", accRecive_id + "");
//        Log.d("Act_rec_Root_id", rec_Root_id + "");
//        Log.d("Act_rec_Subacc_id", rec_Subacc_id + "");
//        Log.d("-----------", "------------");
//        Log.d("Act_accPay_id", accPay_id + "");
//        Log.d("Act_pay_Root_id", pay_Root_id + "");
//        Log.d("Act_pay_Subacc_id", pay_Subacc_id + "");
//
//        _db.insert(ACTIVITY, null, values);
//        //_db.close();
//    }
//
//    public String getProjectTitle(int projectId) {
//        return getAccountTitle(projectId);
//    }
//
//    public List getSubAccountsList(int parentId, boolean hideClosed) {
//        SQLiteDatabase _db = getReadableDatabase();
//        ArrayList list = new ArrayList();
//        String closedWhere = hideClosed ? " AND Ac_isClosed != 'Y'" : "";
//        String str = "SELECT Ac_title , Ac_id , Ac_balance  FROM " + ACCOUNTS
//                + " WHERE Ac_parent_id = ? AND Ac_enable = ? " + closedWhere;
//
//        Cursor cursor = _db.rawQuery(str,
//                new String[]{Integer.toString(parentId), "1"});
//        if (cursor.moveToFirst()) {
//            do {
//                Account saItem = new Account();
//                saItem.setTitle(cursor.getString(0));
//                saItem.setAccID(cursor.getInt(1));
//                saItem.setBalance(cursor.getDouble(2));
//                list.add(saItem);
//            } while (cursor.moveToNext());
//        }
//        cursor.close();
//        //_db.close();
//        return list;
//    }
//
//    public CheqReminderItem getCheqReminder(int chActId) {
//        CheqReminderItem item = new CheqReminderItem();
//        SQLiteDatabase _db = getReadableDatabase();
//        String query = "SELECT ch_activity_id , ch_id , ch_serial , ch_date , ch_amount ,ch_bankAccount_id , ch_acc_id_received , ch_set FROM "
//                + CHEQ_REMINDER + " WHERE ch_activity_id = ? ";
//        Cursor localCursor = _db.rawQuery(query,
//                new String[]{Integer.toString(chActId)});
//        if ((localCursor != null) && (localCursor.moveToFirst())) {
//            item.transId = localCursor.getInt(0);
//            item.chId = localCursor.getInt(1);
//            item.serial = localCursor.getString(2);
//            item.date = localCursor.getString(3);
//            item.cheqAmount = localCursor.getDouble(4);
//            item.bankId = localCursor.getInt(5);
//            item.accId = localCursor.getInt(6);
//            int[] arrayOfInt = getAccountParents(localCursor.getInt(6));
//            item.reciversTitles = getAccountsTitle(arrayOfInt);
//            item.reciversIds = arrayOfInt;
//            item.isPassed = localCursor.getInt(7);
//        }
//        return item;
//    }
//
//    public List getCheqByDate(String date) {
//        SQLiteDatabase _db = getReadableDatabase();
//        ArrayList list = new ArrayList();
//        Cursor localCursor = _db.rawQuery(
//                "select Chr_transactionId , Chr_date from " + CHEQ
//                        + " Where chr_date >= '" + date + "' ", null);
//        if (localCursor.moveToFirst()) {
//            do {
//                CheqItem item = new CheqItem();
//                item.setTransId(localCursor.getInt(0));
//                item.setDate(localCursor.getString(1));
//                list.add(item);
//            } while (localCursor.moveToNext());
//        }
//        return list;
//    }
//
//    public int[] getAccountParents(int accId) {
//        int acBaseParent = -1;
//        int acParent= -1;
//        int[] path = new int[3];
//        path[0] = accId;
//        path[1] = acParent;
//        path[2] = acBaseParent;
//        SQLiteDatabase sqlDb = null;
//        String query = "SELECT Ac_parent_id FROM " + ACCOUNTS
//                + " WHERE Ac_id = ? ";
//        try {
//            sqlDb = getWritableDatabase();
//            Cursor cursor = sqlDb
//                    .rawQuery(query, new String[]{Integer.toString(accId)});
//            if (cursor.moveToFirst()) {
//                acParent= cursor.getInt(0);
//                String query2 = "SELECT Ac_parent_id FROM " + ACCOUNTS
//                        + " WHERE Ac_id = ? ";
//                String[] arrayOfString2 = new String[1];
//                arrayOfString2[0] = Integer.toString(acParent);
//                Cursor cursor2 = sqlDb.rawQuery(query2,
//                        new String[]{Integer.toString(acParent)});
//                if ((cursor2.moveToFirst()) && (cursor2.getInt(0) != 0)) {
//                    acBaseParent = cursor2.getInt(0);
//                    Log.d("in database for transId", cursor2.getInt(0) + "");
//                }
//                path[0] = accId;
//                path[1] = acParent;
//                path[2] = acBaseParent;
//                cursor2.close();
//
//            }
//
//
//            cursor.close();
//            sqlDb.close();
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//
//
//        return path;
//    }
//    public int getAccountParent(int accId) {
//        int acParent= -1;
//        SQLiteDatabase sqlDb = null;
//        String query = "SELECT Ac_parent_id FROM " + ACCOUNTS
//                + " WHERE Ac_id = ? ";
//        try {
//            sqlDb = getWritableDatabase();
//            Cursor cursor = sqlDb
//                    .rawQuery(query, new String[]{Integer.toString(accId)});
//            if (cursor.moveToFirst()) {
//                acParent= cursor.getInt(0);
//            }
//            cursor.close();
//            sqlDb.close();
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//
//
//        return acParent;
//    }
//    public void PassUnpassCheq(int cheqId, int transId, int accId,
//                               int isCheqPassed, String remindDate) {
//        SQLiteDatabase _db = getWritableDatabase();
//        CheqReminderManager remindItem = new CheqReminderManager(this.context);
//        ContentValues cheqValues = new ContentValues();
//        ContentValues actValues = new ContentValues();
//        cheqValues.put("ch_set", Integer.valueOf(isCheqPassed));
//
//        _db.update(CHEQ, cheqValues, " ch_id = ? ",
//                new String[]{Integer.toString(cheqId)});
//
//        if (isCheqPassed == 0) {
//            actValues.put("Act_isCheq_passed", Integer.valueOf(isCheqPassed));
//            actValues.put("Act_accRecive_id", Integer.valueOf(-1));
//            actValues.put("Act_rec_Root_id", Integer.valueOf(8));
//            actValues.put("Act_rec_Subacc_id", Integer.valueOf(11));
//            if (remindDate.compareTo(new CalendarTool().getIranianDate()) >= 0) {
//                _db.execSQL("insert into " + CHEQ_REMINDER
//                        + "(Chr_date,Chr_transactionId,Chr_uniqId)"
//                        + " select " + remindDate + " , '" + transId + "' ,'"
//                        + UUID.randomUUID().toString() + "' WHERE NOT EXISTS "
//                        + " (select 1 from " + CHEQ_REMINDER
//                        + " WHERE Chr_transactionId =" + transId + " )");
//                remindItem.setAlarm(remindDate, transId);
//            }
//        }
//        if (isCheqPassed == 1) {
//            actValues.put("Act_isCheq_passed", Integer.valueOf(isCheqPassed));
//            actValues.put("Act_accRecive_id", Integer.valueOf(-1));
//            actValues.put("Act_rec_Root_id", Integer.valueOf(4));
//            actValues.put("Act_rec_Subacc_id", Integer.valueOf(accId));
//            remindItem.cancelAlarm(transId);
//        }
//        _db.update(ACTIVITY, actValues, " Act_id = ? ",
//                new String[]{Integer.toString(transId)});
//        //_db.close();
//    }
//
//    public List<Account> getAccountsBalance(ArrayList<Integer> selected) {
//
//        SQLiteDatabase sqlDb = null;
//        Cursor cursor2 = null;
//        ArrayList<Account> accounts = new ArrayList<>();
//        try {
//            sqlDb = getWritableDatabase();
//            for (Integer selID : selected) {
//                double parentBalance = 0.0D;
//
//                Account acc = new Account();
//                String query = "SELECT SUM(Ac_balance) FROM " + ACCOUNTS
//                        + " WHERE Ac_parent_id = ? ";
//                cursor2 = sqlDb.rawQuery(query,
//                        new String[]{Integer.toString(selID)});
//
//                if (cursor2.moveToFirst()) {
//                    parentBalance = cursor2.getDouble(0);
//                }
//                acc.setTitle(getAccountTitle(selID));
//                acc.setAccID(selID);
//                int[] arrayOfInt = new int[3];
//                arrayOfInt[0] = selID;
//                arrayOfInt[1] = -1;
//                arrayOfInt[2] = -1;
//                double childActivityBalance = getAccountsActivityBalance(arrayOfInt, 2,
//                        "9999/99/99", "");
//                acc.setTotalBalance(parentBalance + childActivityBalance);
//                if (selID == Constants.ACCOUNTS.PROJECTS_INT)
//                    acc.setTotalBalance(parentBalance - childActivityBalance);
//                if (selID == Constants.ACCOUNTS.BILLS_INT)
//                    acc.setTotalBalance(acc.getTotalBalance()*-1);
//                accounts.add(acc);
//                if (cursor2 != null && !cursor2.isClosed()) {
//                    cursor2.close();
//                }
//            }
//            sqlDb.close();
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//        return accounts;
//    }
//
//    public double getAccountBegBalance(int accId) {
//
//        SQLiteDatabase sqlDb = null;
//        Cursor cursor2 = null;
//        double balance = 0.0D;
//        try {
//            sqlDb = getWritableDatabase();
//            String query = "SELECT Ac_balance FROM " + ACCOUNTS
//                    + " WHERE Ac_id = ? ";
//            cursor2 = sqlDb.rawQuery(query,
//                    new String[]{Integer.toString(accId)});
//            if (cursor2.moveToFirst()) {
//                balance = cursor2.getDouble(0);
//            }
//            sqlDb.close();
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//        return balance;
//    }
//
//    public LinkedHashMap<Account, ArrayList<Account>> getChildBalances(
//            int parentId, String endDate, String startDate, int includeDefBal, String isClosed) {
//        SQLiteDatabase _db = getReadableDatabase();
//        _db.beginTransaction();
//
//        LinkedHashMap<Account, ArrayList<Account>> hash = new LinkedHashMap<Account, ArrayList<Account>>();
//        try {
//            String query = "SELECT Ac_title , Ac_id , Ac_balance , Ac_regDate , Ac_isClosed, Ac_introPercent, Ac_workPercent FROM "
//                    + ACCOUNTS
//                    + " WHERE Ac_parent_id = ?  AND ( Ac_isClosed = 'N' OR Ac_isClosed = ?) ORDER BY Ac_regDate DESC";
//
//            Cursor cursor = _db.rawQuery(query,
//                    new String[]{Integer.toString(parentId), isClosed});
//            try {
//                if (cursor.moveToFirst()) {
//                    do {
//                        Account key = new Account();
//                        ArrayList<Account> childs = new ArrayList<Account>();
//                        int keyId = cursor.getInt(1);
//                        key.setTitle(cursor.getString(0));
//                        key.setAccID(keyId);
//                        key.setDate(cursor.getString(3));
//                        key.setClosed(cursor.getString(4));
//                        key.setIntroPercent(cursor.getInt(5));
//                        key.setWorkPercent(cursor.getInt(6));
//
//                        double balance = 0.0D;
//                        if (includeDefBal == 1) {
//                            balance = cursor.getDouble(2);
//                        }
//                        key.setBalance(balance);
//                        double temp = getAccountsActivityBalance(new int[]{parentId, keyId, -1}, 2, endDate, startDate);
//
//                        key.setTotalBalance(balance + temp);
//                        if (parentId == Constants.ACCOUNTS.PROJECTS_INT)
//                            key.setTotalBalance(balance - temp);
//
//                        if (parentId != Constants.ACCOUNTS.PROJECTS_INT) {
//                            Cursor cursor2 = null;
//                            try {
//                                query = "SELECT  Ac_title , Ac_id , ac_balance , Ac_regDate , Ac_isClosed FROM "
//                                        + ACCOUNTS + " WHERE Ac_parent_id = ?";
//                                if (!_db.isOpen())
//                                    _db = getReadableDatabase();
//                                cursor2 = _db.rawQuery(query,
//                                        new String[]{Integer.toString(keyId)});
//                                if (cursor2.moveToFirst()) {
//                                    do {
//                                        double childBalance = 0.0D;
//                                        Account accItem = new Account();
//                                        accItem.setTitle(cursor2.getString(0));
//                                        accItem.setAccID(cursor2.getInt(1));
//                                        accItem.setDate(cursor2.getString(3));
//                                        accItem.setClosed(cursor2.getString(4));
//                                        int[] accSubject = new int[3];
//                                        accSubject[0] = parentId;
//                                        accSubject[1] = keyId;
//                                        accSubject[2] = cursor2.getInt(1);
//                                        if (includeDefBal == 1) {
//                                            childBalance = cursor2.getDouble(2);
//                                        }
//                                        accItem.setBalance(balance);
//                                        Log.d("directory is : ",
//                                                "" + parentId + "-" + keyId + "-"
//                                                        + cursor2.getInt(1));
//                                        temp = getAccountsActivityBalance(
//                                                accSubject, 2, endDate, startDate);
//                                        Log.d("totalBalance is : ", "" + temp);
//                                        accItem.setTotalBalance(childBalance + temp);
//                                        childs.add(accItem);
//                                        Log.d("child is ", cursor2.getString(0));
//                                    } while (cursor2.moveToNext());
//                                } else {
//                                    //Log.d("in empty cursor", "ok ");
//                                }
//                                hash.put(key, childs);
//                            } finally {
//                                if (cursor2 != null && !cursor2.isClosed()) {
//                                    cursor2.close();
//                                }
//                            }
//                        } else {
//                            hash.put(key, new ArrayList<Account>());
//                        }
//
//                    } while (cursor.moveToNext());
//                }
//
//            } finally {
//                if (cursor != null && !cursor.isClosed()) {
//                    cursor.close();
//                }
//            }
//            _db.setTransactionSuccessful();
//        } finally {
//            _db.endTransaction();
//        }
//        //_db.close();
//        return hash;
//
//    }
//
//    public ArrayList<Account> getProjectsBalance(int page, String endDate, String startDate, int includeDefBal, String isClosed) {
//        SQLiteDatabase _db = getReadableDatabase();
//        _db.beginTransaction();
//
//        ArrayList<Account> retList = new ArrayList<>();
//        try {
//            String query = "SELECT Ac_title , Ac_id , Ac_balance , Ac_regDate , Ac_isClosed, Ac_introPercent, Ac_workPercent FROM "
//                    + ACCOUNTS
//                    + " WHERE Ac_parent_id = ?  AND ( Ac_isClosed = 'N' OR Ac_isClosed = ?) ORDER BY Ac_regDate DESC LIMIT 10 OFFSET ? ";
//
//            Cursor cursor = _db.rawQuery(query,
//                    new String[]{Integer.toString(Constants.ACCOUNTS.PROJECTS_INT), isClosed, Integer.toString(page)});
//            try {
//                if (cursor.moveToFirst()) {
//                    do {
//                        Account proj = new Account();
//                        int projectID = cursor.getInt(1);
//                        proj.setTitle(cursor.getString(0));
//                        proj.setAccID(projectID);
//                        proj.setDate(cursor.getString(3));
//                        proj.setClosed(cursor.getString(4));
//                        proj.setIntroPercent(cursor.getInt(5));
//                        proj.setWorkPercent(cursor.getInt(6));
//                        proj.setTotalIncome(getProjectIncomeBalance(projectID, "", "9999/99/99"));
//                        proj.setTotalOutcome(getProjectOutcomeBalance(projectID, "", "9999/99/99"));
//                        double balance = 0.0D;
//                        if (includeDefBal == 1) {
//                            balance = cursor.getDouble(2);
//                        }
//                        proj.setBalance(balance);
//                        //double temp =getAccountsActivityBalance(new int[] {Constants.ACCOUNTS.PROJECTS_INT, projectID, -1 }, 2, endDate, startDate);
//
//                        //roj.setTotalBalance(balance - temp);
//
//                        retList.add(proj);
//                    } while (cursor.moveToNext());
//                }
//
//            } finally {
//                if (cursor != null && !cursor.isClosed()) {
//                    cursor.close();
//                }
//            }
//            _db.setTransactionSuccessful();
//        } finally {
//            _db.endTransaction();
//        }
//        //_db.close();
//        return retList;
//
//    }
//    public int GetLevelOfAccount(int[] acc){
//        if (acc[1] == -1) {
//            return 1;
//        } else if (acc[2] == -1) {
//         return 2;
//        } else {
//            return 3;
//        }
//    }
//
//    public int GetLevelOfAccount(int acc){
//        int[] parents=getAccountParents(acc);
//        if(parents[1]==-1)
//            return 1;
//        if(parents[2]==-1)
//            return 2;
//        return 3;
//    }
//    public double getAccountsActivityBalance(String[] accSubject, int actSet, String dateEnd, String dateStart) {
//        SQLiteDatabase _db = getReadableDatabase();
//
//        double totalBalance = 0.0D;
//        double inBalance = 0.0D;
//        double outBalance = 0.0D;
//        if (dateStart.equals("")) {
//            dateStart = "0000/00/00";
//        }
//        String recWhere="";
//        String payWhere="";
//        String id="";
//        switch (GetLevelOfAccount(accSubject)){
//            case 1:
//                payWhere = "act_pay_root_id";
//                recWhere = "act_rec_root_id";
//                id=accSubject[0];
//                break;
//            case 2:
//                id=accSubject[1];
//                payWhere = "act_pay_subacc_id";
//                recWhere = "act_rec_subacc_id";
//                break;
//            case 3:
//                id=accSubject[2];
//                payWhere = "act_accpay_id";
//                recWhere = "act_accRecive_id";
//                break;
//        }
//
//
//        Cursor cursor = null;
//        String[] params;
//        String query, query1, query2;
//
//
//        try {
//
//            query = "SELECT (SELECT SUM(Act_amount) From "+ ACTIVITY + " WHERE "+recWhere+" = "+id+" ) as rec ,(SELECT SUM(Act_amount) From "+ ACTIVITY + " WHERE "+payWhere+" = "+id+" ) as pay";
//            cursor = _db.rawQuery(query, new String[0]);
//            if (cursor.moveToFirst()) {
//                outBalance = cursor.getDouble(1);
//                inBalance = cursor.getDouble(0);
//            }
//            cursor.close();
//            totalBalance=  outBalance + (0.0D - inBalance);
//            /*if (accSubject[1] == -1) {
//
//                query1 = "SELECT SUM(Act_amount) From " + ACTIVITY
//                        + " WHERE ( Act_set = ? ) AND ( Act_rec_root_id = ? )"
//                        + " AND ( Act_date >= '" + dateStart + "' AND Act_date <='" + dateEnd + "')";
//                query2 = "SELECT SUM(Act_amount) From " + ACTIVITY
//                        + " WHERE ( Act_set = ? ) AND ( Act_rec_root_id = ? )"
//                        + " AND ( Act_date >= '" + dateStart
//                        + "' AND Act_date <='" + dateEnd + "')";
//
//                query = "SELECT (" + query1 + ")+(" + query2 + ")";
//                params = new String[4];
//                params[0] = Integer.toString(0);
//                params[1] = Integer.toString(accSubject[0]);
//                params[2] = Integer.toString(1);
//                params[3] = Integer.toString(accSubject[0]);
//                cursor = _db.rawQuery(query, params);
//                if (cursor.moveToFirst()) {
//                    totalBalance -= cursor.getDouble(0);
//                    outBalance += cursor.getDouble(0);
//                }
//                cursor.close();
//
//                query1 = "SELECT SUM(Act_amount) From " + ACTIVITY
//                        + " WHERE ( Act_set = ? ) AND (Act_pay_root_id = ? )"
//                        + " AND ( Act_date >= '" + dateStart
//                        + "'  AND Act_date <='" + dateEnd + "')";
//                query2 = "SELECT SUM(Act_amount) From " + ACTIVITY
//                        + " WHERE ( Act_set = ? ) AND (Act_pay_root_id = ? )"
//                        + " AND ( Act_date >= '" + dateStart
//                        + "'  AND Act_date <='" + dateEnd + "')";
//                query = "SELECT (" + query1 + ")+(" + query2 + ")";
//                params = new String[4];
//                params[0] = Integer.toString(0);
//                params[1] = Integer.toString(accSubject[0]);
//                params[2] = Integer.toString(1);
//                params[3] = Integer.toString(accSubject[0]);
//                cursor = _db.rawQuery(query, params);
//                if (cursor.moveToFirst()) {
//                    totalBalance += cursor.getDouble(0);
//                    inBalance += cursor.getDouble(0);
//                }
//            } else if (accSubject[2] == -1) {
//                query1 = "SELECT SUM(Act_amount) From "
//                        + ACTIVITY
//                        + " WHERE ( Act_set = ? ) AND ( Act_rec_Subacc_id = ? AND Act_rec_root_id = ? )"
//                        + " AND ( Act_date >= '" + dateStart
//                        + "'  AND Act_date <='" + dateEnd + "')";
//                query2 = "SELECT SUM(Act_amount) From "
//                        + ACTIVITY
//                        + " WHERE ( Act_set = ? ) AND ( Act_rec_Subacc_id = ? AND Act_rec_root_id = ? )"
//                        + " AND ( Act_date >= '" + dateStart
//                        + "'  AND Act_date <='" + dateEnd + "')";
//
//                query = "SELECT (" + query1 + ")+(" + query2 + ")";
//                params = new String[6];
//                params[0] = Integer.toString(0);
//                params[1] = Integer.toString(accSubject[1]);
//                params[2] = Integer.toString(accSubject[0]);
//                params[3] = Integer.toString(1);
//                params[4] = Integer.toString(accSubject[1]);
//                params[5] = Integer.toString(accSubject[0]);
//                cursor = _db.rawQuery(query, params);
//                if (cursor.moveToFirst()) {
//                    totalBalance -= cursor.getDouble(0);
//                    outBalance += cursor.getDouble(0);
//                }
//                cursor.close();
//
//                query1 = "SELECT SUM(Act_amount) From "
//                        + ACTIVITY
//                        + " WHERE ( Act_set = ? ) AND (Act_pay_Subacc_id = ? AND Act_pay_root_id = ? )"
//                        + " AND ( Act_date >= '" + dateStart
//                        + "'  AND Act_date <='" + dateEnd + "')";
//                query2 = "SELECT SUM(Act_amount) From "
//                        + ACTIVITY
//                        + " WHERE ( Act_set = ? ) AND (Act_pay_Subacc_id = ? AND Act_pay_root_id = ? )"
//                        + " AND ( Act_date >= '" + dateStart
//                        + "'  AND Act_date <='" + dateEnd + "')";
//
//                query = "SELECT (" + query1 + ")+(" + query2 + ")";
//                params = new String[6];
//
//                params[0] = Integer.toString(0);
//                params[1] = Integer.toString(accSubject[1]);
//                params[2] = Integer.toString(accSubject[0]);
//                params[3] = Integer.toString(1);
//                params[4] = Integer.toString(accSubject[1]);
//                params[5] = Integer.toString(accSubject[0]);
//                cursor = _db.rawQuery(query, params);
//                if (cursor.moveToFirst()) {
//                    totalBalance += cursor.getDouble(0);
//                    inBalance += cursor.getDouble(0);
//                }
//                cursor.close();
//            } else {
//                if ((accSubject[1] == -1) || (accSubject[2] == -1)) {
//                    // break label1628;
//                }
//                query1 = "SELECT SUM(Act_amount) From "
//                        + ACTIVITY
//                        + " WHERE ( Act_set = ? ) AND ( Act_accRecive_id = ? AND Act_rec_Subacc_id = ? AND Act_rec_root_id = ? )"
//                        + " AND ( Act_date >= '" + dateStart
//                        + "'  AND Act_date <='" + dateEnd + "')";
//                query2 = "SELECT SUM(Act_amount) From "
//                        + ACTIVITY
//                        + " WHERE ( Act_set = ? ) AND ( Act_accRecive_id = ? AND Act_rec_Subacc_id = ? AND Act_rec_root_id = ? )"
//                        + " AND ( Act_date >= '" + dateStart
//                        + "'  AND Act_date <='" + dateEnd + "')";
//                query = "SELECT (" + query1 + ")+(" + query2 + ")";
//                params = new String[8];
//                params[0] = Integer.toString(0);
//                params[1] = Integer.toString(accSubject[2]);
//                params[2] = Integer.toString(accSubject[1]);
//                params[3] = Integer.toString(accSubject[0]);
//                params[4] = Integer.toString(1);
//                params[5] = Integer.toString(accSubject[2]);
//                params[6] = Integer.toString(accSubject[1]);
//                params[7] = Integer.toString(accSubject[0]);
//                cursor = _db.rawQuery(query, params);
//                if (cursor.moveToFirst()) {
//                    totalBalance -= cursor.getDouble(0);
//                    outBalance += cursor.getDouble(0);
//                }
//                cursor.close();
//
//                query1 = "SELECT SUM(Act_amount) From "
//                        + ACTIVITY
//                        + " WHERE ( Act_set = ? ) AND ( Act_accPay_id = ? AND Act_pay_Subacc_id = ? AND Act_pay_root_id = ? )"
//                        + " AND ( Act_date >= '" + dateStart
//                        + "'  AND Act_date <='" + dateEnd + "')";
//                query2 = "SELECT SUM(Act_amount) From "
//                        + ACTIVITY
//                        + " WHERE ( Act_set = ? ) AND ( Act_accPay_id = ? AND Act_pay_Subacc_id = ? AND Act_pay_root_id = ? )"
//                        + " AND ( Act_date >= '" + dateStart
//                        + "'  AND Act_date <='" + dateEnd + "')";
//
//                query = "SELECT (" + query1 + ")+(" + query2 + ")";
//                params = new String[8];
//                params[0] = Integer.toString(0);
//                params[1] = Integer.toString(accSubject[2]);
//                params[2] = Integer.toString(accSubject[1]);
//                params[3] = Integer.toString(accSubject[0]);
//                params[4] = Integer.toString(1);
//                params[5] = Integer.toString(accSubject[2]);
//                params[6] = Integer.toString(accSubject[1]);
//                params[7] = Integer.toString(accSubject[0]);
//                cursor = _db.rawQuery(query, params);
//                if (cursor.moveToFirst()) {
//                    totalBalance += cursor.getDouble(0);
//                    inBalance += cursor.getDouble(0);
//                }
//                cursor.close();
//            }*/
//        } finally {
//            if (cursor != null && !cursor.isClosed()) {
//                cursor.close();
//            }
//            //_db.close();
//        }
//
//        if (actSet == 0)
//            return inBalance;
//        else if (actSet == 1)
//            return outBalance;
//
//        return totalBalance;
//    }
//
//    public List<Account> getProjectBalanceReport(String endDate,
//                                                     String stratDate, int projectId) {
//        SQLiteDatabase _db = getReadableDatabase();
//        List<Account> accList = new ArrayList<Account>();
//        String str = "SELECT Ac_id , Ac_title FROM " + ACCOUNTS
//                + " WHERE Ac_parent_id = 0 AND Ac_id < 12";
//        Cursor cursor = _db.rawQuery(str, null);
//        if (cursor.moveToFirst()) {
//            do {
//                Account acc = new Account();
//                acc.setTitle(cursor.getString(1));
//                acc.setAccID(cursor.getInt(0));
//                int[] accTarget = new int[3];
//                accTarget[0] = cursor.getInt(0);
//                accTarget[1] = -1;
//                accTarget[2] = -1;
//                acc.setTotalBalance(getProjectBalance(accTarget, endDate,
//                        projectId, stratDate));
//                accList.add(acc);
//            } while (cursor.moveToNext());
//        }
//        cursor.close();
//        //_db.close();
//        return accList;
//    }
//
//    public double getProjectBalance(int[] accSubject, String dateEnd,
//                                    int projectID, String dateStart) {
//        SQLiteDatabase _db = getReadableDatabase();
//        double totalBalance = 0.0D;
//        if (dateStart.equals("")) {
//            dateStart = "0000/00/00";
//        }
//        Cursor cursor;
//        String[] params;
//        String query;
//        Object localObject = null;
//        if (accSubject[1] == -1) {
//            query = "SELECT SUM(Act_amount) From " + ACTIVITY
//                    + " WHERE ( Act_set = ? ) AND ( Act_rec_root_id = ? )"
//                    + " AND ( Act_date >= '" + dateStart + "' AND Act_date <='"
//                    + dateEnd + "') AND ( Act_accProject_id = ?) ";
//            params = new String[3];
//            params[0] = Integer.toString(0);
//            params[1] = Integer.toString(accSubject[0]);
//            params[2] = Integer.toString(projectID);
//            cursor = _db.rawQuery(query, params);
//            if (cursor.moveToFirst()) {
//                totalBalance -= cursor.getDouble(0);
//            }
//            query = "SELECT SUM(Act_amount) From " + ACTIVITY
//                    + " WHERE ( Act_set = ? ) AND (Act_pay_root_id = ? )"
//                    + " AND ( Act_date >= '" + dateStart
//                    + "'  AND Act_date <='" + dateEnd
//                    + "')  AND ( Act_accProject_id = ?)  ";
//            params = new String[3];
//            params[0] = Integer.toString(0);
//            params[1] = Integer.toString(accSubject[0]);
//            params[2] = Integer.toString(projectID);
//            cursor.close();
//            cursor = null;
//            cursor = _db.rawQuery(query, params);
//            if (cursor.moveToFirst()) {
//                totalBalance += cursor.getDouble(0);
//            }
//            query = "SELECT SUM(Act_amount) From " + ACTIVITY
//                    + " WHERE ( Act_set = ? ) AND ( Act_rec_root_id = ? )"
//                    + " AND ( Act_date >= '" + dateStart + "' AND Act_date <='"
//                    + dateEnd + "') AND ( Act_accProject_id = ?) ";
//            params = new String[3];
//            params[0] = Integer.toString(1);
//            params[1] = Integer.toString(accSubject[0]);
//            params[2] = Integer.toString(projectID);
//            cursor.close();
//            cursor = null;
//            cursor = _db.rawQuery(query, params);
//            if (cursor.moveToFirst()) {
//                totalBalance -= cursor.getDouble(0);
//            }
//            query = "SELECT SUM(Act_amount) From " + ACTIVITY
//                    + " WHERE ( Act_set = ? ) AND (Act_pay_root_id = ? )"
//                    + " AND ( Act_date >= '" + dateStart
//                    + "'  AND Act_date <='" + dateEnd
//                    + "')  AND ( Act_accProject_id = ?)  ";
//            params = new String[3];
//            params[0] = Integer.toString(1);
//            params[1] = Integer.toString(accSubject[0]);
//            params[2] = Integer.toString(projectID);
//            cursor.close();
//            cursor = _db.rawQuery(query, params);
//            if (cursor.moveToFirst()) {
//                totalBalance += cursor.getDouble(0);
//            }
//        } else if (accSubject[2] == -1) {
//            query = "SELECT SUM(Act_amount) From "
//                    + ACTIVITY
//                    + " WHERE ( Act_set = ? ) AND ( Act_rec_Subacc_id = ? AND Act_rec_root_id = ? )"
//                    + " AND ( Act_date >= '" + dateStart
//                    + "'  AND Act_date <='" + dateEnd
//                    + "')  AND ( Act_accProject_id = ?) ";
//            params = new String[4];
//            params[0] = Integer.toString(0);
//            params[1] = Integer.toString(accSubject[1]);
//            params[2] = Integer.toString(accSubject[0]);
//            params[3] = Integer.toString(projectID);
//            cursor = _db.rawQuery(query, params);
//            if (cursor.moveToFirst()) {
//                totalBalance -= cursor.getDouble(0);
//            }
//            query = "SELECT SUM(Act_amount) From "
//                    + ACTIVITY
//                    + " WHERE ( Act_set = ? ) AND (Act_pay_Subacc_id = ? AND Act_pay_root_id = ? )"
//                    + " AND ( Act_date >= '" + dateStart
//                    + "'  AND Act_date <='" + dateEnd
//                    + "')  AND ( Act_accProject_id = ?) ";
//            params = new String[4];
//            params[0] = Integer.toString(0);
//            params[1] = Integer.toString(accSubject[1]);
//            params[2] = Integer.toString(accSubject[0]);
//            params[3] = Integer.toString(projectID);
//            cursor.close();
//            cursor = _db.rawQuery(query, params);
//            if (cursor.moveToFirst()) {
//                totalBalance += cursor.getDouble(0);
//            }
//            query = "SELECT SUM(Act_amount) From "
//                    + ACTIVITY
//                    + " WHERE ( Act_set = ? ) AND ( Act_rec_Subacc_id = ? AND Act_rec_root_id = ? )"
//                    + " AND ( Act_date >= '" + dateStart
//                    + "'  AND Act_date <='" + dateEnd
//                    + "')  AND ( Act_accProject_id = ?) ";
//            params = new String[4];
//            params[0] = Integer.toString(1);
//            params[1] = Integer.toString(accSubject[1]);
//            params[2] = Integer.toString(accSubject[0]);
//            params[3] = Integer.toString(projectID);
//            cursor.close();
//            cursor = _db.rawQuery(query, params);
//            if (cursor.moveToFirst()) {
//                totalBalance -= cursor.getDouble(0);
//            }
//            query = "SELECT SUM(Act_amount) From "
//                    + ACTIVITY
//                    + " WHERE ( Act_set = ? ) AND (Act_pay_Subacc_id = ? AND Act_pay_root_id = ? )"
//                    + " AND ( Act_date >= '" + dateStart
//                    + "'  AND Act_date <='" + dateEnd
//                    + "')  AND ( Act_accProject_id = ?) ";
//            params = new String[4];
//            params[0] = Integer.toString(1);
//            params[1] = Integer.toString(accSubject[1]);
//            params[2] = Integer.toString(accSubject[0]);
//            params[3] = Integer.toString(projectID);
//            cursor.close();
//            cursor = _db.rawQuery(query, params);
//            if (cursor.moveToFirst()) {
//                totalBalance += cursor.getDouble(0);
//            }
//        } else {
//            if ((accSubject[1] == -1) || (accSubject[2] == -1)) {
//                // break label1628;
//            }
//            query = "SELECT SUM(Act_amount) From "
//                    + ACTIVITY
//                    + " WHERE ( Act_set = ? ) AND ( Act_accRecive_id = ? AND Act_rec_Subacc_id = ? AND Act_rec_root_id = ? )"
//                    + " AND ( Act_date >= '" + dateStart
//                    + "'  AND Act_date <='" + dateEnd
//                    + "')  AND ( Act_accProject_id = ?) ";
//            params = new String[5];
//            params[0] = Integer.toString(0);
//            params[1] = Integer.toString(accSubject[2]);
//            params[2] = Integer.toString(accSubject[1]);
//            params[3] = Integer.toString(accSubject[0]);
//            params[4] = Integer.toString(projectID);
//            cursor = _db.rawQuery(query, params);
//            if (cursor.moveToFirst()) {
//                totalBalance -= cursor.getDouble(0);
//            }
//            query = "SELECT SUM(Act_amount) From "
//                    + ACTIVITY
//                    + " WHERE ( Act_set = ? ) AND ( Act_accPay_id = ? AND Act_pay_Subacc_id = ? AND Act_pay_root_id = ? )"
//                    + " AND ( Act_date >= '" + dateStart
//                    + "'  AND Act_date <='" + dateEnd
//                    + "')  AND ( Act_accProject_id = ?) ";
//            params = new String[5];
//            params[0] = Integer.toString(0);
//            params[1] = Integer.toString(accSubject[2]);
//            params[2] = Integer.toString(accSubject[1]);
//            params[3] = Integer.toString(accSubject[0]);
//            params[4] = Integer.toString(projectID);
//            cursor.close();
//            cursor = _db.rawQuery(query, params);
//            if (cursor.moveToFirst()) {
//                totalBalance += cursor.getDouble(0);
//            }
//            query = "SELECT SUM(Act_amount) From "
//                    + ACTIVITY
//                    + " WHERE ( Act_set = ? ) AND ( Act_accRecive_id = ? AND Act_rec_Subacc_id = ? AND Act_rec_root_id = ? )"
//                    + " AND ( Act_date >= '" + dateStart
//                    + "'  AND Act_date <='" + dateEnd
//                    + "')  AND ( Act_accProject_id = ?) ";
//            params = new String[5];
//            params[0] = Integer.toString(1);
//            params[1] = Integer.toString(accSubject[2]);
//            params[2] = Integer.toString(accSubject[1]);
//            params[3] = Integer.toString(accSubject[0]);
//            params[4] = Integer.toString(projectID);
//            cursor.close();
//            cursor = _db.rawQuery(query, params);
//            if (cursor.moveToFirst()) {
//                totalBalance -= cursor.getDouble(0);
//            }
//            query = "SELECT SUM(Act_amount) From "
//                    + ACTIVITY
//                    + " WHERE ( Act_set = ? ) AND ( Act_accPay_id = ? AND Act_pay_Subacc_id = ? AND Act_pay_root_id = ? )"
//                    + " AND ( Act_date >= '" + dateStart
//                    + "'  AND Act_date <='" + dateEnd
//                    + "')  AND ( Act_accProject_id = ?) ";
//            params = new String[5];
//            params[0] = Integer.toString(1);
//            params[1] = Integer.toString(accSubject[2]);
//            params[2] = Integer.toString(accSubject[1]);
//            params[3] = Integer.toString(accSubject[0]);
//            params[4] = Integer.toString(projectID);
//            cursor.close();
//            cursor = _db.rawQuery(query, params);
//            if (cursor.moveToFirst()) {
//                totalBalance += cursor.getDouble(0);
//            }
//        }
//        if (cursor != null)
//            cursor.close();
//        //_db.close();
//        return totalBalance;
//    }
//    public List<TransactionItem> getAllTransactions(int set, String startDate, String endDate) {
//        return getAllTransactions(set, startDate, endDate, -1);
//    }
//    public List<TransactionItem> getAllTransactions(int set, String startDate, String endDate, int projectId) {
//        return getAllTransactions(set, startDate, endDate, projectId, true);
//    }
//    public List<TransactionItem> getAllTransactions(int set, String startDate, String endDate, int projectId, boolean hideWorkers) {
//
//        SQLiteDatabase sqlDb = null;
//        List<TransactionItem> listTrans = new ArrayList<TransactionItem>();
//        try {
//            sqlDb = getWritableDatabase();
//
//            Cursor cursor = null;
//            String projectWhere = "";
//            String dateWhere = "";
//            String workerWhere = "";
//            if (startDate.equals("")) {
//                startDate = "0000/00/00";
//            }
//            if (endDate.equals("")) {
//                endDate = "9999/99/99";
//            }
//            dateWhere = " AND ( Act_date >= '" + startDate + "' AND Act_date <='" + endDate + "')";
//
//
//            if (hideWorkers) {
//                workerWhere = " AND Act_tags != '" + Constants.DB_TAGS.WorkDay + "'";
//            }
//
//            if (startDate.equals(endDate))
//                dateWhere = " AND Act_date = '" + startDate + "'";
//
//            if (projectId != -1)
//                projectWhere = " Act_accProject_id = " + projectId + " AND ";
//            if (set == 0) {
//                String str3 = "SELECT Act_id , Act_set , Act_accProject_id , Act_info , Act_amount , Act_accPay_id , Act_pay_Subacc_id , Act_pay_root_id, Act_accRecive_id , Act_rec_Subacc_id , Act_rec_Root_id , Act_date , Act_ser , Act_iscash , Act_cheq_id , Act_ischeq_passed,Act_tags FROM "
//                        + ACTIVITY
//                        + " WHERE"
//                        + projectWhere
//                        + " Act_set = ?"
//                        + dateWhere
//                        + workerWhere
//                        + " ORDER BY Act_date DESC ,  Act_ser DESC";
//
//                cursor = sqlDb.rawQuery(str3,new String[]{"0"});
//
//            }
//            if (set == 1) {
//                String str2 = "SELECT Act_id , Act_set , Act_accProject_id , Act_info , Act_amount , Act_accPay_id , Act_pay_Subacc_id , Act_pay_root_id, Act_accRecive_id , Act_rec_Subacc_id , Act_rec_Root_id , Act_date , Act_ser , Act_iscash , Act_cheq_id , Act_ischeq_passed,Act_tags FROM "
//                        + ACTIVITY
//                        + " WHERE"
//                        + projectWhere
//                        + " Act_set = ?"
//                        + dateWhere
//                        + workerWhere
//                        + " ORDER BY Act_date DESC , Act_ser DESC";
//
//                cursor = sqlDb.rawQuery(str2,new String[]{"1"});
//            }
//            if (set == 2) {
//                String str2 = "SELECT Act_id , Act_set , Act_accProject_id , Act_info , Act_amount , Act_accPay_id , Act_pay_Subacc_id , Act_pay_root_id, Act_accRecive_id , Act_rec_Subacc_id , Act_rec_Root_id , Act_date , Act_ser , Act_iscash , Act_cheq_id , Act_ischeq_passed,Act_tags FROM "
//                        + ACTIVITY
//                        + " WHERE"
//                        + projectWhere
//                        + " 1 = 1"
//                        + dateWhere
//                        + workerWhere
//                        + " ORDER BY Act_date DESC , Act_ser DESC";
//
//
//                cursor = sqlDb.rawQuery(str2, new String[]{});
//            }
//            if ((cursor != null) && (cursor.moveToFirst())) {
//                do {
//                    TransactionItem item = new TransactionItem();
//                    int[] recIds = new int[3];
//                    int[] payIds = new int[3];
//                    item.setTransactionId(cursor.getInt(0));
//                    item.setMethod(cursor.getInt(1));
//                    item.setProjectId(cursor.getInt(2));
//                    item.setProjectTitle(getAccountTitle(cursor.getInt(2)));
//                    item.setInfo(cursor.getString(3));
//                    item.setAmount(cursor.getDouble(4));
//                    item.setTargetLeafAccountId(cursor.getInt(5));
//                    item.setTargetSubAccountId(cursor.getInt(6));
//                    item.setTargetRootAccountId(cursor.getInt(7));
//                    payIds[0] = cursor.getInt(5);
//                    payIds[1] = cursor.getInt(6);
//                    payIds[2] = cursor.getInt(7);
//                    String[] payTitles = getAccountsTitle(payIds);
//                    item.setTargetLeafAccountTitle(payTitles[0]);
//                    item.setTargetSubAccountTitle(payTitles[1]);
//                    item.setTargetRootAccountTitle(payTitles[2]);
//                    item.setAccRecId(cursor.getInt(8));
//                    item.setSubAccRecId(cursor.getInt(9));
//                    item.setRootRecId(cursor.getInt(10));
//                    recIds[0] = cursor.getInt(8);
//                    recIds[1] = cursor.getInt(9);
//                    recIds[2] = cursor.getInt(10);
//                    String[] recTitles = getAccountsTitle(recIds);
//                    item.setAccRecTitle(recTitles[0]);
//                    item.setSubAccRecTitle(recTitles[1]);
//                    item.setRootRecTitle(recTitles[2]);
//                    item.setRegDateFa(cursor.getString(11));
//                    item.setSerial(cursor.getInt(12));
//                    item.setIsCash(cursor.getInt(13));
//                    item.setCheqId(cursor.getInt(14));
//                    item.setIsCheqPassed(cursor.getInt(15));
//                    item.setTAG(cursor.getString(cursor.getColumnIndex("Act_tags")));
//                    if (item.getIsCash() == 0) {
//                        SQLiteDatabase sqlDb2 = null;
//                        try {
//                            sqlDb2 = getWritableDatabase();
//                            String str1 = "SELECT  Ch_serial , Ch_date , Ch_bankAccount_id FROM "
//                                    + CHEQ + " WHERE Ch_id = ? ";
//                            Cursor cursor2 = sqlDb2
//                                    .rawQuery(str1, new String[]{Integer
//                                            .toString(item.getCheqId())});
//                            if (cursor2.moveToFirst()) {
//                                item.setCheqSerial(cursor2.getString(0));
//                                item.setCheqDate(cursor2.getString(1));
//                                item.setChecqBankAccId(cursor2.getInt(2));
//                            }
//                            cursor2.close();
//                            sqlDb2.close();
//                        } catch (Exception localException) {
//                            if (sqlDb2 != null && sqlDb2.isOpen())
//                                sqlDb2.close();
//                        }
//                    }
//                    listTrans.add(item);
//                } while (cursor.moveToNext());
//            }
//            cursor.close();
//            sqlDb.close();
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//        return listTrans;
//    }
//
//    public List<TransactionItem> getAllTransactionsForAccount(int[] account, int set, int page) {
//        SQLiteDatabase sqlDb = null;
//        List<TransactionItem> listTrans = new ArrayList<>();
//        try {
//            sqlDb = getReadableDatabase();
//            Cursor cursor = null;
//            String[] params = new String[7];
//            params[0] = Integer.toString(account[2]);
//            params[1] = Integer.toString(account[1]);
//            params[2] = Integer.toString(account[0]);
//
//            params[3] = Integer.toString(account[2]);
//            params[4] = Integer.toString(account[1]);
//            params[5] = Integer.toString(account[0]);
//            params[6] = Integer.toString(page);
//            Log.d(" getAllTransactionsForAccount : accId", "[" + account[2] + "]-[" + account[1] + "]-[" + account[0] + "]");
//            if (set == 0) {
//                String str3 = "SELECT Act_id , Act_set , Act_accProject_id , Act_info , Act_amount , Act_accPay_id , Act_pay_Subacc_id , Act_pay_root_id, Act_accRecive_id , Act_rec_Subacc_id , Act_rec_Root_id , Act_date , Act_ser , Act_iscash , Act_cheq_id , Act_ischeq_passed,Act_tags FROM "
//                        + ACTIVITY
//                        + " WHERE ( Act_set = '0' AND Act_accRecive_id = ? AND Act_rec_Subacc_id = ? AND Act_rec_root_id = ? )"
//                        + " OR ( Act_set = '0' AND Act_accPay_id = ? AND Act_pay_Subacc_id = ? AND Act_pay_root_id = ? )"
//                        + " ORDER BY Act_date DESC ,  Act_ser DESC  LIMIT 10 OFFSET ? ";
//                cursor = sqlDb.rawQuery(str3, params);
//            }
//            if (set == 1) {
//                String str2 = "SELECT Act_id , Act_set , Act_accProject_id , Act_info , Act_amount , Act_accPay_id , Act_pay_Subacc_id , Act_pay_root_id, Act_accRecive_id , Act_rec_Subacc_id , Act_rec_Root_id , Act_date , Act_ser , Act_iscash , Act_cheq_id , Act_ischeq_passed,Act_tags FROM "
//                        + ACTIVITY
//                        + " WHERE ( Act_set = '1' AND Act_accPay_id = ? AND Act_pay_Subacc_id = ? AND Act_pay_root_id = ? )"
//                        + " OR ( Act_set = '1' AND Act_accRecive_id = ? AND Act_rec_Subacc_id = ? AND Act_rec_root_id = ? )"
//                        + " ORDER BY Act_date DESC , Act_ser DESC  LIMIT 10 OFFSET ? ";
//                cursor = sqlDb.rawQuery(str2, params);
//            }
//            if (set == 2) {
//                String str2 = "SELECT Act_id , Act_set , Act_accProject_id , Act_info , Act_amount , Act_accPay_id , Act_pay_Subacc_id , Act_pay_root_id, Act_accRecive_id , Act_rec_Subacc_id , Act_rec_Root_id , Act_date , Act_ser , Act_iscash , Act_cheq_id , Act_ischeq_passed,Act_tags FROM "
//                        + ACTIVITY
//                        + " WHERE ( Act_accPay_id = ? AND Act_pay_Subacc_id = ? AND Act_pay_root_id = ? ) OR ( Act_accRecive_id = ? AND Act_rec_Subacc_id = ? AND Act_rec_root_id = ? )"
//                        + " ORDER BY Act_date DESC , Act_ser DESC  LIMIT 10 OFFSET ? ";
//                cursor = sqlDb
//                        .rawQuery(str2, new String[]{params[0], params[1], params[2], params[0], params[1], params[2], Integer.toString(page)});
//            }
//            if ((cursor != null) && (cursor.moveToFirst())) {
//                do {
//                    TransactionItem item = new TransactionItem();
//                    int[] recIds = new int[3];
//                    int[] payIds = new int[3];
//                    item.setTransactionId(cursor.getInt(0));
//                    item.setMethod(cursor.getInt(1));
//                    item.setProjectId(cursor.getInt(2));
//                    item.setProjectTitle(getAccountTitle(cursor.getInt(2)));
//                    item.setInfo(cursor.getString(3));
//                    item.setAmount(cursor.getDouble(4));
//                    item.setTargetLeafAccountId(cursor.getInt(5));
//                    item.setTargetSubAccountId(cursor.getInt(6));
//                    item.setTargetRootAccountId(cursor.getInt(7));
//                    payIds[0] = cursor.getInt(5);
//                    payIds[1] = cursor.getInt(6);
//                    payIds[2] = cursor.getInt(7);
//                    Log.d("payId[0] is ", payIds[0] + "");
//                    Log.d("payId[1] is ", payIds[1] + "");
//                    Log.d("payId[2] is ", payIds[2] + "");
//                    String[] payTitles = getAccountsTitle(payIds);
//                    item.setTargetLeafAccountTitle(payTitles[0]);
//                    item.setTargetSubAccountTitle(payTitles[1]);
//                    item.setTargetRootAccountTitle(payTitles[2]);
//                    item.setAccRecId(cursor.getInt(8));
//                    item.setSubAccRecId(cursor.getInt(9));
//                    item.setRootRecId(cursor.getInt(10));
//                    recIds[0] = cursor.getInt(8);
//                    recIds[1] = cursor.getInt(9);
//                    recIds[2] = cursor.getInt(10);
//                    Log.d("recId[0] is ", recIds[0] + "");
//                    Log.d("recId[1] is ", recIds[1] + "");
//                    Log.d("recId[2] is ", recIds[2] + "");
//                    String[] recTitles = getAccountsTitle(recIds);
//                    item.setAccRecTitle(recTitles[0]);
//                    item.setSubAccRecTitle(recTitles[1]);
//                    item.setRootRecTitle(recTitles[2]);
//                    item.setRegDateFa(cursor.getString(11));
//                    item.setSerial(cursor.getInt(12));
//                    item.setIsCash(cursor.getInt(13));
//                    item.setCheqId(cursor.getInt(14));
//                    item.setIsCheqPassed(cursor.getInt(15));
//                    item.setTAG(cursor.getString(cursor.getColumnIndex("Act_tags")));
//                    if (item.getIsCash() == 0) {
//                        SQLiteDatabase sqlDb2 = null;
//                        try {
//                            sqlDb2 = getWritableDatabase();
//                            String str1 = "SELECT  Ch_serial , Ch_date , Ch_bankAccount_id FROM "
//                                    + CHEQ + " WHERE Ch_id = ? ";
//                            Cursor cursor2 = sqlDb2
//                                    .rawQuery(str1, new String[]{Integer
//                                            .toString(item.getCheqId())});
//                            if (cursor2.moveToFirst()) {
//                                item.setCheqSerial(cursor2.getString(0));
//                                item.setCheqDate(cursor2.getString(1));
//                                item.setChecqBankAccId(cursor2.getInt(2));
//                                Log.d(" It's in first record", " Done");
//                            }
//                            cursor2.close();
//                            sqlDb2.close();
//                        } catch (Exception localException) {
//                            if (sqlDb2 != null && sqlDb2.isOpen())
//                                sqlDb2.close();
//                        }
//                    }
//                    listTrans.add(item);
//                } while (cursor.moveToNext());
//            }
//            sqlDb.close();
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//        return listTrans;
//    }
//
//    public double getProjectOutcomeBalance(int projectId, String dateStart,
//                                           String dateEnd) {
//        SQLiteDatabase _db = getReadableDatabase();
//        List<TransactionItem> listTrans = new ArrayList<TransactionItem>();
//        Cursor cursor = null;
//        String projectWhere = "";
//        double totalBalance = 0.0D;
//        String query = "SELECT SUM(Act_amount) From " + ACTIVITY
//                + " WHERE ( Act_set = 1 )  AND ( Act_date >= '" + dateStart
//                + "'  AND Act_date <='" + dateEnd
//                + "')  AND ( Act_accProject_id = ?) ";
//        cursor = _db.rawQuery(query,
//                new String[]{Integer.toString(projectId)});
//        if (cursor.moveToFirst()) {
//            totalBalance += cursor.getDouble(0);
//        }
//        if (cursor != null && !cursor.isClosed()) {
//            cursor.close();
//        }
//        //_db.close();
//        return totalBalance;
//    }
//
//    public double getProjectIncomeBalance(int projectId, String dateStart,
//                                          String dateEnd) {
//        SQLiteDatabase _db = getReadableDatabase();
//        List<TransactionItem> listTrans = new ArrayList<TransactionItem>();
//        Cursor cursor = null;
//        String projectWhere = "";
//        double totalBalance = 0.0D;
//        String query = "SELECT SUM(Act_amount) From " + ACTIVITY
//                + " WHERE ( Act_set = 0 )  AND ( Act_date >= '" + dateStart
//                + "'  AND Act_date <='" + dateEnd
//                + "')  AND ( Act_accProject_id = ?) ";
//        cursor = _db.rawQuery(query,
//                new String[]{Integer.toString(projectId)});
//        if (cursor.moveToFirst()) {
//            totalBalance += cursor.getDouble(0);
//        }
//        if (cursor != null && !cursor.isClosed()) {
//            cursor.close();
//        }
//        //_db.close();
//        return totalBalance;
//    }
//
//    public int getProjectIntroPercent(int projectId) {
//        SQLiteDatabase _db = getReadableDatabase();
//        Cursor cursor = null;
//        int percent = 0;
//        String query = "SELECT Ac_introPercent From " + ACCOUNTS
//                + " WHERE Ac_id = ? ";
//        cursor = _db.rawQuery(query,
//                new String[]{Integer.toString(projectId)});
//        if (cursor.moveToFirst()) {
//            percent = cursor.getInt(0);
//        }
//        if (cursor != null && !cursor.isClosed()) {
//            cursor.close();
//        }
//        //_db.close();
//        return percent;
//    }
//
//    public int getProjectWorkPercent(int projectId) {
//        SQLiteDatabase _db = getReadableDatabase();
//        Cursor cursor = null;
//        int percent = 0;
//        String query = "SELECT Ac_workPercent From " + ACCOUNTS
//                + " WHERE Ac_id = ? ";
//        cursor = _db.rawQuery(query,
//                new String[]{Integer.toString(projectId)});
//        if (cursor.moveToFirst()) {
//            percent = cursor.getInt(0);
//        }
//        if (cursor != null && !cursor.isClosed()) {
//            cursor.close();
//        }
//        //_db.close();
//        return percent;
//    }
//
//    public void closeAccount(int accId) {
//        SQLiteDatabase _db = getReadableDatabase();
//        ContentValues cv = new ContentValues();
//        cv.put("Ac_isClosed", "Y");
//        _db.update(ACCOUNTS, cv, "Ac_id = ?",
//                new String[]{Integer.toString(accId)});
//        //_db.close();
//    }
//
//    public void openAccount(int accId) {
//        SQLiteDatabase _db = getReadableDatabase();
//        ContentValues cv = new ContentValues();
//        cv.put("Ac_isClosed", "N");
//        _db.update(ACCOUNTS, cv, "Ac_id = ?",
//                new String[]{Integer.toString(accId)});
//        //_db.close();
//    }
//
//    public List<BillItem> getBillAllTrans(boolean hideZero, String startDate, String endDate, String order) {
//        SQLiteDatabase _db = getReadableDatabase();
//        List<BillItem> billList = new ArrayList<>();
//        if (startDate.equals("")) {
//            startDate = "0000/00/00";
//        }
//        if (endDate.equals("")) {
//            endDate = "9999/99/99";
//        }
//        try {
//            String query = "SELECT SUM(Act_amount), Act_pay_root_id, Act_pay_subacc_id, Act_accPay_id, COUNT(Act_Amount), Act_accRecive_id , Act_rec_Subacc_id , Act_rec_Root_id From "
//                    + ACTIVITY
//                    + " WHERE ( Act_set > -1 )  AND ( act_rec_Root_id=?)AND ( Act_date >= '" + startDate + "' AND Act_date <='" + endDate + "') GROUP BY Act_pay_root_id,Act_pay_subacc_id,Act_accPay_id ORDER BY " + order;
//            Cursor cursor = _db.rawQuery(query,
//                    new String[]{Integer.toString(BILL_PARENT)});
//            try {
//                if (cursor.moveToFirst()) {
//                    do {
//                        if (hideZero && cursor.getDouble(0) == 0.0D)
//                            continue;
//                        BillItem key = new BillItem();
//                        ArrayList<Integer> projectsIds = new ArrayList<Integer>();
//                        int rootId = cursor.getInt(1);
//                        int keyId = cursor.getInt(2);
//                        int subkeyId = cursor.getInt(3);
//                        key.setTargetRootAccountId(rootId);
//                        key.setTargetRootAccountTitle(getAccountTitle(rootId));
//                        key.setTargetSubAccountId(keyId);
//                        key.setTargetSubAccountTitle(getAccountTitle(keyId));
//                        key.setTargetLeafAccountId(subkeyId);
//                        key.setTargetLeafAccountTitle(getAccountTitle(subkeyId));
//                        key.setTitle(key.getTargetSubAccountTitle() + " - "
//                                + key.getTargetLeafAccountTitle());
//                        key.setBillCount(cursor.getInt(4));// count of payed
//                        // bills
//                        key.setAccRecId(cursor.getInt(5));
//                        key.setSubAccRecId(cursor.getInt(6));
//                        key.setRootRecId(BILL_PARENT);
//                        double begBalance = 0.0D;
//                        if (key.getAccRecId() == -1)
//                            begBalance = getAccountBegBalance(key.getSubAccRecId());
//                        else
//                            begBalance = getAccountBegBalance(key.getAccRecId());
//
//                        double billBalance = 0.0D;
//                        billBalance = cursor.getDouble(0);
//                        Log.d("getAllBillBalances:1917", "Bill = " + (begBalance + billBalance));
//                        key.setBillAmount(begBalance + billBalance);
//                        int accBiller = subkeyId;
//                        if (accBiller == -1)
//                            accBiller = keyId;
//                        double temp = getPayedBillsAmount(
//                                new int[]{cursor.getInt(7), cursor.getInt(6),
//                                        cursor.getInt(5)}, accBiller);
//                        Log.d("GetPayedAmount:1925", "PayedAmount=" + temp);
//                        //if was zero
//                        if (((billBalance + begBalance) - temp == 0) && hideZero) continue;
//                        key.setPayedAmount(temp);
//                        Cursor cursor2 = null;
//                        try {
//                            _db = getReadableDatabase();
//                            query = "SELECT Act_accProject_id FROM "
//                                    + ACTIVITY
//                                    + " WHERE Act_pay_root_id = ? AND Act_pay_subacc_id = ? AND Act_accPay_id = ? ORDER BY Act_date DESC , Act_ser DESC ";
//                            cursor2 = _db.rawQuery(query, new String[]{Integer.toString(rootId),
//                                    Integer.toString(keyId),
//                                    Integer.toString(subkeyId)});
//                            if (cursor2.moveToFirst()) {
//                                do {
//                                    if (projectsIds.contains(cursor2.getInt(0))) continue;
//                                    projectsIds.add(cursor2.getInt(0));
//                                } while (cursor2.moveToNext());
//                            } else {
//                                Log.d("in empty cursor", "ok ");
//                            }
//                            key.setProjectsIds(projectsIds);
//                            billList.add(key);
//                        } finally {
//                            if (cursor2 != null && !cursor2.isClosed()) {
//                                // cursor2.close();
//                            }
//                        }
//
//                    } while (cursor.moveToNext());
//                }
//
//            } finally {
//                if (cursor != null && !cursor.isClosed()) {
//                    cursor.close();
//                }
//            }
//        } finally {
//
//        }
//        //_db.close();
//        return billList;
//
//    }
//
//    public ArrayList<BillProjectItem> getBillProjectsDetail(
//            List<Integer> projects, int[] account) {
//        SQLiteDatabase _db = getReadableDatabase();
//        ArrayList<BillProjectItem> listTrans = new ArrayList<BillProjectItem>();
//        for (int projectId : projects) {
//            Log.d("getBillProjectsDetail , projectId: ", projectId + "");
//            if (projectId == -1) continue;
//            String query = "SELECT SUM(Act_amount), COUNT(Act_Amount), MIN(Act_date), Max(Act_date) From "
//                    + ACTIVITY
//                    + " WHERE ( Act_set = 1 )  AND ( Act_iscash = 1 )  AND ( Act_accProject_id = ? ) AND ( Act_pay_root_id=?) AND ( Act_pay_subacc_id=?) AND ( Act_accPay_id=?)  GROUP BY Act_pay_root_id,Act_pay_subacc_id,Act_accPay_id,Act_accProject_id";
//            String[] params = new String[4];
//            params[0] = Integer.toString(projectId);
//            params[1] = Integer.toString(account[0]);
//            params[2] = Integer.toString(account[1]);
//            params[3] = Integer.toString(account[2]);
//            Cursor cursor = _db.rawQuery(query, params);
//            if (!cursor.moveToFirst()) continue;
//            if (cursor.getInt(1) == 0) continue;
//            try {
//
//                if (cursor.getInt(1) == 0)
//                    continue;
//
//                BillProjectItem key = new BillProjectItem();
//
//                key.setBillAmount(cursor.getDouble(0));
//                key.setBillCount(cursor.getInt(1));
//                key.setProjectTitle(getProjectTitle(projectId));
//                key.setMinDate(cursor.getString(2));
//                key.setMaxDate(cursor.getString(3));
//
//                listTrans.add(key);
//
//            } finally {
//                if (cursor != null && !cursor.isClosed()) {
//                    cursor.close();
//                }
//            }
//        }
//        //_db.close();
//        return listTrans;
//
//    }
//
//    public ArrayList<TransactionItem> getBilledBillsTrans(int[] accPayTo,
//                                                          int accBiller, int page, String startDate, String endDate, boolean showZeros) {
//        SQLiteDatabase _db = getReadableDatabase();
//
//        String dateWhere = "";
//        if (startDate.equals("")) {
//            startDate = "0000/00/00";
//        }
//        if (endDate.equals("")) {
//            endDate = "9999/99/99";
//        }
//        dateWhere = " AND ( Act_date >= '" + startDate + "' AND Act_date <='" + endDate + "')";
//
//        ArrayList<TransactionItem> transList = new ArrayList<TransactionItem>();
//        String query = "SELECT Act_id , Act_set , Act_accProject_id , Act_info , Act_amount , Act_accPay_id , Act_pay_Subacc_id , Act_pay_root_id, Act_accRecive_id , Act_rec_Subacc_id , Act_rec_Root_id , Act_date , Act_ser, Act_tags  FROM "
//                + ACTIVITY
//                + " WHERE Act_pay_root_id = ? AND Act_pay_subacc_id = ? AND Act_accPay_id = ? AND Act_rec_Root_id = ?"
//                //+ dateWhere
//                + " ORDER BY Act_date DESC , Act_ser DESC LIMIT 10 OFFSET ? ";
//        String[] params = new String[5];
//        params[0] = Integer.toString(accPayTo[0]);
//        params[1] = Integer.toString(accPayTo[1]);
//        params[2] = Integer.toString(accPayTo[2]);
//        params[3] = Integer.toString(BILL_PARENT);
//        params[4] = Integer.toString(page);
//        Cursor cursor = _db.rawQuery(query, params);
//        try {
//            if (cursor.moveToFirst()) {
//                do {
//                    if (!showZeros && cursor.getDouble(0) == 0.0D)
//                        continue;
//                    TransactionItem item = new TransactionItem();
//                    int[] recIds = new int[3];
//                    int[] payIds = new int[3];
//                    item.setTransactionId(cursor.getInt(0));
//                    item.setMethod(cursor.getInt(1));
//                    item.setProjectId(cursor.getInt(2));
//                    item.setProjectTitle(getAccountTitle(cursor.getInt(2)));
//                    item.setInfo(cursor.getString(3));
//                    item.setAmount(cursor.getDouble(4));
//                    item.setTargetLeafAccountId(cursor.getInt(5));
//                    item.setTargetSubAccountId(cursor.getInt(6));
//                    item.setTargetRootAccountId(cursor.getInt(7));
//                    payIds[0] = cursor.getInt(5);
//                    payIds[1] = cursor.getInt(6);
//                    payIds[2] = cursor.getInt(7);
//                    Log.d("payId[0] is ", payIds[0] + "");
//                    Log.d("payId[1] is ", payIds[1] + "");
//                    Log.d("payId[2] is ", payIds[2] + "");
//                    String[] payTitles = getAccountsTitle(payIds);
//                    item.setTargetLeafAccountTitle(payTitles[0]);
//                    item.setTargetSubAccountTitle(payTitles[1]);
//                    item.setTargetRootAccountTitle(payTitles[2]);
//                    item.setAccRecId(cursor.getInt(8));
//                    item.setSubAccRecId(cursor.getInt(9));
//                    item.setRootRecId(cursor.getInt(10));
//                    recIds[0] = cursor.getInt(8);
//                    recIds[1] = cursor.getInt(9);
//                    recIds[2] = cursor.getInt(10);
//                    Log.d("recId[0] is ", recIds[0] + "");
//                    Log.d("recId[1] is ", recIds[1] + "");
//                    Log.d("recId[2] is ", recIds[2] + "");
//                    String[] recTitles = getAccountsTitle(recIds);
//                    item.setAccRecTitle(recTitles[0]);
//                    item.setSubAccRecTitle(recTitles[1]);
//                    item.setRootRecTitle(recTitles[2]);
//                    item.setRegDateFa(cursor.getString(11));
//                    item.setSerial(cursor.getInt(12));
//                    item.setTAG(cursor.getString(13));
//                    transList.add(item);
//
//                } while (cursor.moveToNext());
//            }
//
//        } finally {
//            if (cursor != null && !cursor.isClosed()) {
//                cursor.close();
//            }
//        }
//        //_db.close();
//        return transList;
//    }
//
//    public ArrayList<TransactionItem> getPayedBillsTrans(int[] accPayTo, int accBiller, int page, String startDate, String endDate) {
//
//        String dateWhere = "";
//        if (startDate.equals("")) {
//            startDate = "0000/00/00";
//        }
//        if (endDate.equals("")) {
//            endDate = "9999/99/99";
//        }
//        dateWhere = " AND ( Act_date >= '" + startDate + "' AND Act_date <='" + endDate + "')";
//
//        SQLiteDatabase _db = getReadableDatabase();
//        ArrayList<TransactionItem> transList = new ArrayList<TransactionItem>();
//        String query = "SELECT Act_id, Act_amount, Act_accRecive_id , Act_rec_Subacc_id , Act_rec_Root_id, Act_date, Act_info , Act_accProject_Id, Act_tags From "
//                + ACTIVITY
//                + " WHERE ( Act_set = 1 ) AND ( Act_accPay_id=?) AND ( Act_pay_subacc_id=?)  AND ( Act_pay_root_id=?) AND ( Act_accPay1_id=?)"
//                //+ dateWhere
//                + " ORDER BY Act_date DESC , Act_ser DESC LIMIT 10 OFFSET ? ";
//
//        String[] params = new String[5];
//        params[0] = Integer.toString(accPayTo[0]);
//        params[1] = Integer.toString(accPayTo[1]);
//        params[2] = Integer.toString(accPayTo[2]);
//        params[3] = Integer.toString(accBiller);
//        params[4] = Integer.toString(page);
//        Cursor cursor = _db.rawQuery(query, params);
//        try {
//            if (cursor.moveToFirst()) {
//                do {
//                    if (cursor.getDouble(0) == 0.0D)
//                        continue;
//                    TransactionItem item = new TransactionItem();
//                    int[] recIds = new int[3];
//                    item.setTransactionId(cursor.getInt(0));
//                    item.setSerial(cursor.getInt(0));
//                    item.setAmount(cursor.getDouble(1));
//                    item.setInfo(cursor.getString(6));
//                    item.setRegDateFa(cursor.getString(5));
//                    item.setProjectId(cursor.getInt(7));
//                    item.setProjectTitle(getAccountTitle(cursor.getInt(7)));
//                    item.setAccRecId(cursor.getInt(2));
//                    item.setSubAccRecId(cursor.getInt(3));
//                    item.setRootRecId(cursor.getInt(4));
//                    recIds[0] = cursor.getInt(2);
//                    recIds[1] = cursor.getInt(3);
//                    recIds[2] = cursor.getInt(4);
//                    Log.d("recId[0] is ", recIds[0] + "");
//                    Log.d("recId[1] is ", recIds[1] + "");
//                    Log.d("recId[2] is ", recIds[2] + "");
//                    String[] recTitles = getAccountsTitle(recIds);
//                    item.setAccRecTitle(recTitles[0]);
//                    item.setSubAccRecTitle(recTitles[1]);
//                    item.setRootRecTitle(recTitles[2]);
//                    item.setMethod(1);
//                    item.setTargetLeafAccountId(accPayTo[0]);
//                    item.setTargetSubAccountId(accPayTo[1]);
//                    item.setTargetRootAccountId(accPayTo[2]);
//                    Log.d("payId[0] is ", accPayTo[0] + "");
//                    Log.d("payId[1] is ", accPayTo[1] + "");
//                    Log.d("payId[2] is ", accPayTo[2] + "");
//                    String[] payTitles = getAccountsTitle(accPayTo);
//                    item.setTargetLeafAccountTitle(payTitles[0]);
//                    item.setTargetSubAccountTitle(payTitles[1]);
//                    item.setTargetRootAccountTitle(payTitles[2]);
//                    item.setTAG(cursor.getString(cursor.getColumnIndex("Act_tags")));
//                    transList.add(item);
//
//                } while (cursor.moveToNext());
//            }
//
//        } finally {
//            if (cursor != null && !cursor.isClosed()) {
//                cursor.close();
//            }
//        }
//        //_db.close();
//        return transList;
//    }
//
//    public double getPayedBillsAmount(int[] accPayTo, int accBiller) {
//        SQLiteDatabase _db = getReadableDatabase();
//        double payed = 0.0D;
//        /*
//         * String query = "SELECT SUM(Act_amount)From " + ACTIVITY +
//		 * " WHERE ( Act_set = 1 )  AND ( Act_pay_root_id=?) AND ( Act_pay_subacc_id=?) AND ( Act_accPay_id=?)  AND ( Act_accPay1_id=?) GROUP BY Act_accPay1_id,Act_pay_root_id,Act_pay_subacc_id,Act_accPay_id"
//		 * ;
//		 */
//        String query = "SELECT SUM(Act_amount) From "
//                + ACTIVITY
//                + " WHERE ( Act_set = 1 )  AND ( Act_pay_root_id=?) AND ( Act_pay_subacc_id=?) AND ( Act_accPay_id=?)  AND ( Act_accPay1_id=?)";
//
//        String[] params = new String[4];
//        params[0] = Integer.toString(accPayTo[0]);
//        params[1] = Integer.toString(accPayTo[1]);
//        params[2] = Integer.toString(accPayTo[2]);
//        params[3] = Integer.toString(accBiller);
//        Log.d("recId[0] is ", accPayTo[0] + "");
//        Log.d("recId[1] is ", accPayTo[1] + "");
//        Log.d("recId[2] is ", accPayTo[2] + "");
//        Log.d("accBiller is ", accBiller + "");
//        Cursor cursor = _db.rawQuery(query, params);
//        try {
//            if (cursor.moveToFirst()) {
//                do {
//                    payed += cursor.getInt(0);
//
//                } while (cursor.moveToNext());
//            }
//
//        } finally {
//            if (cursor != null && !cursor.isClosed()) {
//                cursor.close();
//            }
//        }
//        return payed;
//    }
//
//    public void deleteTransaction(int actid) {
//        SQLiteDatabase sqlDb = null;
//        try {
//            sqlDb = getWritableDatabase();
//            sqlDb.delete(ACTIVITY, "Act_id = ? ",
//                    new String[]{Integer.toString(actid)});
//            sqlDb.close();
//        } catch (Exception localException) {
//            if (sqlDb != null && sqlDb.isOpen())
//                sqlDb.close();
//        }
//    }
//
//    public List<TransactionItem> getDefaultTransactions() {
//        SQLiteDatabase _db = getReadableDatabase();
//        List<TransactionItem> listTrans = new ArrayList<TransactionItem>();
//        Cursor cursor = null;
//        String str3 = "SELECT Act_id , Act_set , Act_info , Act_amount , Act_accPay_id , Act_pay_Subacc_id , Act_pay_root_id, Act_accRecive_id , Act_rec_Subacc_id , Act_rec_Root_id , Act_iscash , Title FROM "
//                + DEFAULTTRANSACTIONS
//                + " ORDER BY Sort ASC ";
//
//        cursor = _db.rawQuery(str3,
//                new String[]{});
//
//        if ((cursor != null) && (cursor.moveToFirst())) {
//            do {
//                TransactionItem item = new TransactionItem();
//                int[] recIds = new int[3];
//                int[] payIds = new int[3];
//                item.setTransactionId(cursor.getInt(0));
//                item.setMethod(cursor.getInt(1));
//                item.setInfo(cursor.getString(2));
//                item.setAmount(cursor.getDouble(3));
//                item.setTargetLeafAccountId(cursor.getInt(4));
//                item.setTargetSubAccountId(cursor.getInt(5));
//                item.setTargetRootAccountId(cursor.getInt(6));
//                payIds[0] = cursor.getInt(4);
//                payIds[1] = cursor.getInt(5);
//                payIds[2] = cursor.getInt(6);
//                String[] payTitles = getAccountsTitle(payIds);
//                item.setTargetLeafAccountTitle(payTitles[0]);
//                item.setTargetSubAccountTitle(payTitles[1]);
//                item.setTargetRootAccountTitle(payTitles[2]);
//                item.setAccRecId(cursor.getInt(7));
//                item.setSubAccRecId(cursor.getInt(8));
//                item.setRootRecId(cursor.getInt(9));
//                recIds[0] = cursor.getInt(7);
//                recIds[1] = cursor.getInt(8);
//                recIds[2] = cursor.getInt(9);
//                String[] recTitles = getAccountsTitle(recIds);
//                item.setAccRecTitle(recTitles[0]);
//                item.setSubAccRecTitle(recTitles[1]);
//                item.setRootRecTitle(recTitles[2]);
//                item.setIsCash(cursor.getInt(10));
//                item.setTitle(cursor.getString(11));
//                if (item.getIsCash() == 0) {
//                    SQLiteDatabase _db2 = getReadableDatabase();
//                    String str1 = "SELECT  Ch_serial , Ch_date , Ch_bankAccount_id FROM "
//                            + CHEQ + " WHERE Ch_id = ? ";
//
//                    Cursor cursor2 = _db2
//                            .rawQuery(str1, new String[]{Integer
//                                    .toString(item.getCheqId())});
//                    if (cursor2.moveToFirst()) {
//                        item.setCheqSerial(cursor2.getString(0));
//                        item.setCheqDate(cursor2.getString(1));
//                        item.setChecqBankAccId(cursor2.getInt(2));
//                    }
//                    cursor2.close();
//                    _db2.close();
//                }
//                listTrans.add(item);
//            } while (cursor.moveToNext());
//        }
//        cursor.close();
//        //_db.close();
//        return listTrans;
//    }
//
//    public void AddDefaultTransaction(String title, int set, int accPay_id,
//                                      int pay_Subacc_id, int pay_Root_id, int accRecive_id,
//                                      int rec_Subacc_id, int rec_Root_id, String info) {
//        SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
//        ContentValues localContentValues = new ContentValues();
//        localContentValues.put("Title", title);
//        localContentValues.put("Act_info", info);
//        localContentValues.put("Act_accRecive_id",
//                Integer.valueOf(accRecive_id));
//        localContentValues.put("Act_rec_Root_id", Integer.valueOf(rec_Root_id));
//        localContentValues.put("Act_rec_Subacc_id",
//                Integer.valueOf(rec_Subacc_id));
//        Log.d("Act_accRecive_id", accRecive_id + "");
//        Log.d("Act_rec_Root_id", rec_Root_id + "");
//        Log.d("Act_rec_Subacc_id", rec_Subacc_id + "");
//        localContentValues.put("Act_accPay_id", Integer.valueOf(accPay_id));
//        localContentValues.put("Act_pay_Root_id", Integer.valueOf(pay_Root_id));
//        localContentValues.put("Act_pay_Subacc_id",
//                Integer.valueOf(pay_Subacc_id));
//        Log.d("Act_accPay_id", accPay_id + "");
//        Log.d("Act_pay_Root_id", pay_Root_id + "");
//        Log.d("Act_pay_Subacc_id", pay_Subacc_id + "");
//        localContentValues.put("Act_accProject_id", Integer.valueOf(-1));
//        localContentValues.put("Act_iscash", Integer.valueOf(1));
//        localContentValues.put("Act_set", Integer.valueOf(set));
//        localContentValues.put("Act_cheq_id", Integer.valueOf(-1));
//        localSQLiteDatabase.insert(DEFAULTTRANSACTIONS, null, localContentValues);
//        localSQLiteDatabase.close();
//
//    }
//
//    public void sortDefaultTransaction(int accId, int sort) {
//        SQLiteDatabase _db = getReadableDatabase();
//        ContentValues cv = new ContentValues();
//        cv.put("Sort", sort);
//        _db.update(DEFAULTTRANSACTIONS, cv, "Act_id = ?",
//                new String[]{Integer.toString(accId)});
//        //_db.close();
//    }
//
//    public void deleteDefaultTransaction(int actid) {
//        SQLiteDatabase _db = getWritableDatabase();
//        try {
//            _db.delete(DEFAULTTRANSACTIONS, "Act_id = ? ",
//                    new String[]{Integer.toString(actid)});
//        } finally {
//            //_db.close();
//        }
//        return;
//    }
//
//    public Cursor rawQuery(String query, String[] params) {
//        return getReadableDatabase().rawQuery(query, params);
//    }
//
//    public int update(String tableName, ContentValues values, String where, String[] whereValues) {
//        return getWritableDatabase().update(tableName, values, where, whereValues);
//    }
//
//    public void execSQL(String query, String[] params) {
//        getWritableDatabase().execSQL(query, params);
//    }
//}
