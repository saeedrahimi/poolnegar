package ir.saeedrahimi.poolnegar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Calendar;

import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar;


public class CheqReminderManager
{
  SharedPreferences prefs;
  private Context context;
  
  public CheqReminderManager(Context paramContext)
  {
    this.context = paramContext;
  }
  
  private long getDateInSeconds(String date)
  {
    int daysBeforAlarm = this.prefs.getInt("startBeforeDate", 3);
    PersianCalendar pc = new PersianCalendar();
    String[] dateArray = date.split("/");
    pc.setPersianDate(Integer.parseInt(dateArray[0]), Integer.parseInt(dateArray[1]), Integer.parseInt(dateArray[2]));
    String[] sharedAlarmTime = this.prefs.getString("alarmTime", "18-00").split("-");
    Log.d("cheq reminder is set", " " + sharedAlarmTime[0] + " : " + sharedAlarmTime[1]);
    pc.addPersianDate(Calendar.DATE, -1);

    pc.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sharedAlarmTime[0]));
    pc.set(Calendar.MINUTE, Integer.parseInt(sharedAlarmTime[1]));
    return pc.getTimeInMillis();
  }
  
  public void a()
  {
//	  CalendarTool ct = new CalendarTool();
//	  StringBuilder sb = new StringBuilder();
//	  sb.append(ct.getIranianYear()).append("/");
//	  sb.append(String.format("%02d", Integer.valueOf(ct.getIranianMonth()))).append("/");
//	  sb.append(String.format("%02d", Integer.valueOf(ct.getIranianDay())));


//	  List todayCheqs = new DatabaseHelper(this.context).getCheqByDate(sb.toString());
//	  for (int i = 0;i<=todayCheqs.size(); i++)
//	  {
//		  setAlarm(((CheqItem)todayCheqs.get(i)).getDate(), ((CheqItem)todayCheqs.get(i)).getTransId());
//	  }
  }
  
  public void cancelAlarm(int paramInt)
  {
    Intent localIntent = new Intent(this.context, CheqBrodCastReciver.class);
    PendingIntent localPendingIntent = PendingIntent.getBroadcast(this.context, paramInt, localIntent, Intent.FILL_IN_DATA);
    ((AlarmManager)this.context.getSystemService(Context.ALARM_SERVICE)).cancel(localPendingIntent);
    localPendingIntent.cancel();
  }
  
  public void setAlarm(String date, int transId)
  {
    this.prefs = this.context.getSharedPreferences("parmisPreference", 0);
    if (this.prefs.getBoolean("cheqReminderState", true))
    {
      Intent localIntent = new Intent(this.context, CheqBrodCastReciver.class);
      localIntent.putExtra("cheqUniqueId", transId);
      localIntent.putExtra("cheqDate", date);
      Log.d("in set reminder", "cheqDate is " + date);
      Log.d("in set reminder", "cheqUniqueId is " + transId);
      PendingIntent localPendingIntent = PendingIntent.getBroadcast(this.context, transId, localIntent, Intent.FILL_IN_DATA);
      ((AlarmManager)this.context.getSystemService(Context.ALARM_SERVICE)).set(AlarmManager.RTC_WAKEUP, getDateInSeconds(date), localPendingIntent);
    }
  }
}

