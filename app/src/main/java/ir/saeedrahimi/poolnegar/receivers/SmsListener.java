package ir.saeedrahimi.poolnegar.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import ir.saeedrahimi.poolnegar.FileLog;
import ir.saeedrahimi.poolnegar.customs.Utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmsListener extends BroadcastReceiver {
    private DidReceivedSMS mDidReceivedSMS = null;
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            if (!Utils.isWaitingForSms()) {
                return;
            }
            Bundle bundle = intent.getExtras();
            SmsMessage[] msgs;
            if (bundle != null) {
                try {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    String wholeString = "";
                    for(int i = 0; i < msgs.length; i++){
                        msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                        wholeString += msgs[i].getMessageBody();
                    }

                    try {
                        Pattern pattern = Pattern.compile("[0-9]+");
                        Matcher matcher = pattern.matcher(wholeString);
                        if (matcher.find()) {
                            String str = matcher.group(0);
                            if (str.length() >= 3) {
                            	mDidReceivedSMS.gotSms(matcher.group(0)); 
                            }
                        }
                    } catch (Throwable e) {
                        FileLog.e("tmessages", e);
                    }

                } catch(Throwable e) {
                    FileLog.e("tmessages", e);
                }
            }
        }
    }
/*public void onReceive2(Context context, Intent intent) {
     Log.i(TAG, "Intent recieved: " + intent.getAction());

        if (intent.getAction() == SMS_RECEIVED) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object[] pdus = (Object[])bundle.get("pdus");
                final SmsMessage[] messages = new SmsMessage[pdus.length];
                String msg=null;
                String sender=null;
                for (int i = 0; i < pdus.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                    msg = messages[i].getMessageBody().toString();
                    sender =messages[i].getOriginatingAddress();       
                    if(sender.equals("+989354857744")){
                    	mSmsUpdater.gotSms(messages[i]);
                    }
                }
            }
        }
   }*/
public interface DidReceivedSMS 
{
    void gotSms(String string);
}


}
