package ir.saeedrahimi.poolnegar.customs;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import ir.saeedrahimi.poolnegar.R;

import java.text.NumberFormat;

public class ArtemCircleButton
extends LinearLayout implements View.OnClickListener
{
	static int BADGE_RADIUS = 150;
	static int BADGE_STROKE_WIDTH = 5;
	static int BADGE_OUTLINE_STROKE_WIDTH = 2;
	private int mBadgeRadius;
	private int mCurrentFillColor;
	private TextView mAverageValue;
	private TextView mCount;
	private NumberFormat mFloatFormatter;
	private int mFocusedOutlineColor;
	private float mOutlineStrokeWidth;
	private Paint mPaint;
	private int mPressedFillColor;
	private int mPressedOutlineColor;
	private float mWhiteOctagonRadius;
	private int mWhiteOctagonStrokeWidth;

	private boolean ripleEffect;
	private int circle_color, circle_hover_color, default_color, circle_border_color, circle_border_radius, cr_icon;

	public ArtemCircleButton(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		initialize(context,attrs);
	}
	public void initialize(Context context,AttributeSet attrs)
	{
		Resources res = getResources();
		this.mFloatFormatter = NumberFormat.getNumberInstance();
		this.mFloatFormatter.setMinimumFractionDigits(1);
		this.mFloatFormatter.setMaximumFractionDigits(1);
		this.mPaint = new Paint(1);
		setWillNotDraw(false);
		this.mWhiteOctagonStrokeWidth = BADGE_STROKE_WIDTH;
		this.mWhiteOctagonRadius = (this.mBadgeRadius - this.mWhiteOctagonStrokeWidth - this.mWhiteOctagonStrokeWidth / 2);
		//this.mCurrentFillColor = localResources.getColor(R.color.income);
		this.mPressedFillColor = res.getColor(R.color.Artem);
		this.mPressedOutlineColor = res.getColor(R.color.artem4);
		this.mFocusedOutlineColor = res.getColor(R.color.ArtemThird);
		this.mOutlineStrokeWidth = (0.5F * BADGE_OUTLINE_STROKE_WIDTH);
		
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.badge);
		this.mBadgeRadius = (int) a.getDimension(R.styleable.badge_badge_radius,res.getDimension(R.dimen.discovery_badge_radius));
	    int color = a.getColor(R.styleable.badge_backcolor, 0);
		
		this.mCurrentFillColor = color;
		a.recycle();
		final Typeface bold =  TypeFaceProvider.get(context, "vazir.ttf");
		int i = context.getSharedPreferences("saeedr_shared_prefs", 0).getInt("fontSize", -1);

	}
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		int i = getWidth() / 2;
	    int j = this.mBadgeRadius;
	    this.mPaint.setColor(this.mCurrentFillColor);
	    canvas.drawCircle(i, j, this.mBadgeRadius, this.mPaint);
	    if ((isPressed()) && ((isDuplicateParentStateEnabled()) || (isClickable())))
		{
	    	this.mPaint.setColor(this.mCurrentFillColor - 100);
			canvas.drawCircle(i, j, this.mBadgeRadius, this.mPaint);
		}
	}

	@Override
	protected void drawableStateChanged()
	  {
	    super.drawableStateChanged();
	    invalidate();
	  }
	@Override
	public void onClick(View arg0) {
	}
	public void setColorAnimation(int start, int end) {
		ValueAnimator animator = ValueAnimator.ofObject(new ArgbEvaluator(), start, end);
		animator.setDuration(500);
		animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				default_color = (int) animation.getAnimatedValue();
				invalidate();
			}
		});
		animator.setInterpolator(new AccelerateDecelerateInterpolator());
		animator.start();
	}

}
