package ir.saeedrahimi.poolnegar.customs;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ir.saeedrahimi.poolnegar.R;

import java.text.NumberFormat;

public class ArtemOctagonButton
extends LinearLayout implements View.OnClickListener
{
	static int BADGE_STROKE_WIDTH = 5;
	static int BADGE_OUTLINE_STROKE_WIDTH = 2;
	private int mBadgeRadius;
	private int mCurrentFillColor;
	private TextView mAverageValue;
	private TextView mCount;
	private NumberFormat mFloatFormatter;
	private int mFocusedOutlineColor;
	private Path mOctagonPath;
	private float mOutlineStrokeWidth;
	private Paint mPaint;
	private int mPressedFillColor;
	private int mPressedOutlineColor;
	private PointF[] mVertices;
	private float mWhiteOctagonRadius;
	private int mWhiteOctagonStrokeWidth;
	public ArtemOctagonButton(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		initialize(context,attrs);
	}
	public void initialize(Context context,AttributeSet attrs)
	{
		Resources res = getResources();
		this.mFloatFormatter = NumberFormat.getNumberInstance();
		this.mFloatFormatter.setMinimumFractionDigits(1);
		this.mFloatFormatter.setMaximumFractionDigits(1);
		this.mPaint = new Paint(1);
		this.mOctagonPath = new Path();
		this.mOctagonPath.setFillType(Path.FillType.EVEN_ODD);
		setWillNotDraw(false);
		this.mVertices = new PointF[8];
		for (int i = 0; i < 8; i++) {
			this.mVertices[i] = new PointF();
		}

		this.mPressedOutlineColor = res.getColor(R.color.artem4);
		this.mFocusedOutlineColor = res.getColor(R.color.ArtemThird);
		this.mOutlineStrokeWidth = (0.5F * BADGE_OUTLINE_STROKE_WIDTH);

		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.badge);

		this.mBadgeRadius =  a.getDimensionPixelSize(R.styleable.badge_badge_radius,res.getDimensionPixelSize(R.dimen.discovery_badge_radius));
	    int color = a.getColor(R.styleable.badge_backcolor, 0);
		this.mCurrentFillColor = color;
		a.recycle();


		this.mWhiteOctagonStrokeWidth = BADGE_STROKE_WIDTH;
		this.mWhiteOctagonRadius = (this.mBadgeRadius - this.mWhiteOctagonStrokeWidth - this.mWhiteOctagonStrokeWidth / 2);
		this.mPressedFillColor = this.mCurrentFillColor + 100;

	}
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		int i = getWidth() / 2;
		int j = this.mBadgeRadius;
		this.mPaint.setColor(this.mCurrentFillColor);
		this.mPaint.setStyle(Paint.Style.FILL);
		setOctagonPath(i, j, this.mBadgeRadius);
		canvas.drawPath(this.mOctagonPath, this.mPaint);
		this.mPaint.setStrokeWidth(this.mWhiteOctagonStrokeWidth);
		this.mPaint.setColor(-1);
		this.mPaint.setStyle(Paint.Style.STROKE);
		setOctagonPath(i, j, this.mWhiteOctagonRadius);
		canvas.drawPath(this.mOctagonPath, this.mPaint);
		if ((isPressed()) && ((isDuplicateParentStateEnabled()) || (isClickable())))
		{
			setOctagonPath(i, j, this.mBadgeRadius);
			this.mPaint.setColor(this.mPressedFillColor);
			this.mPaint.setStyle(Paint.Style.FILL);
			canvas.drawPath(this.mOctagonPath, this.mPaint);
			this.mPaint.setColor(this.mPressedOutlineColor);
			this.mPaint.setStyle(Paint.Style.STROKE);
			this.mPaint.setStrokeWidth(this.mOutlineStrokeWidth);
			canvas.drawPath(this.mOctagonPath, this.mPaint);
		}
		else if(isFocused()){
			this.mPaint.setColor(this.mFocusedOutlineColor);
			this.mPaint.setStyle(Paint.Style.STROKE);
			this.mPaint.setStrokeWidth(this.mOutlineStrokeWidth);
			setOctagonPath(i, j, this.mBadgeRadius);
			canvas.drawPath(this.mOctagonPath, this.mPaint);}
	}



	private void setOctagonPath(float paramFloat1, float paramFloat2, float paramFloat3)
	{
		updateOctagonCoordinates(paramFloat1, paramFloat2, paramFloat3, this.mVertices);
		this.mOctagonPath.reset();
		this.mOctagonPath.moveTo(this.mVertices[0].x, this.mVertices[0].y);
		for (int i = 1; i < this.mVertices.length; i++) {
			this.mOctagonPath.lineTo(this.mVertices[i].x, this.mVertices[i].y);
		}
		this.mOctagonPath.close();
	}

	private void updateOctagonCoordinates(float paramFloat1, float paramFloat2, float paramFloat3, PointF[] paramArrayOfPointF)
	{
		double d = Math.sin(0.7853981633974483D);
		this.mVertices[0].x = (-1.0F * paramFloat3);
		this.mVertices[0].y = 0.0F;
		this.mVertices[1].x = (-(int)(d * paramFloat3));
		this.mVertices[1].y = (-(int)(d * paramFloat3));
		this.mVertices[2].x = 0.0F;
		this.mVertices[2].y = (-paramFloat3);
		this.mVertices[3].x = ((int)(d * paramFloat3));
		this.mVertices[3].y = (-(int)(d * paramFloat3));
		this.mVertices[4].x = paramFloat3;
		this.mVertices[4].y = 0.0F;
		this.mVertices[5].x = ((int)(d * paramFloat3));
		this.mVertices[5].y = ((int)(d * paramFloat3));
		this.mVertices[6].x = 0.0F;
		this.mVertices[6].y = paramFloat3;
		this.mVertices[7].x = (-(int)(d * paramFloat3));
		this.mVertices[7].y = ((int)(d * paramFloat3));
		for (PointF localPointF : this.mVertices)
		{
			localPointF.x = (paramFloat1 + localPointF.x);
			localPointF.y = (paramFloat2 + localPointF.y);
		}
	}
	@Override
	protected void drawableStateChanged()
	  {
	    super.drawableStateChanged();
	    invalidate();
	  }
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
	}

}
