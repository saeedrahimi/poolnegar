package ir.saeedrahimi.poolnegar.customs;

/**
 * Created by Saeed on 6/9/2017.
 */

public class HtmlUtils {
    public static String createElement(String elemName, String className, String content)
    {
        String str = "";
        if ((className != null) && (className.length() > 0)) {
            str = "class=\"" + className + "\"";
        }
        return "<" + elemName + " " + str + ">" + content + "</" + elemName + ">";
    }
    public static String getHtml(String cssFilename , String content)
    {
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"><link href=\"templates/" + cssFilename + "\" type=\"text/css\" rel=\"stylesheet\"/></head>" + content + "</html>";
    }
}
