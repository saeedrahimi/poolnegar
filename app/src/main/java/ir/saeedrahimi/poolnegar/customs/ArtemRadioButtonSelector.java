package ir.saeedrahimi.poolnegar.customs;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.CompoundButton;

import ir.saeedrahimi.poolnegar.R;

public class ArtemRadioButtonSelector
        extends androidx.appcompat.widget.AppCompatRadioButton {
    Context context;

    private OnCheckedChangeListener onCheckedChangeListener;


    public ArtemRadioButtonSelector(Context context) {
        super(context);
    }

    public ArtemRadioButtonSelector(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ArtemRadioButtonSelector(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        setOwnOnCheckedChangeListener();
        setButtonDrawable(null);//lets remove the default drawable to create our own

    }



    public void setOwnOnCheckedChangeListener(OnCheckedChangeListener onCheckedChangeListener) {
        this.onCheckedChangeListener = onCheckedChangeListener;
    }

    private void setOwnOnCheckedChangeListener() {
        setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (onCheckedChangeListener != null) {
                    //this is called when we have set our listener
                    onCheckedChangeListener.onCheckedChanged(buttonView, isChecked);
                }
            }
        });
    }
}
