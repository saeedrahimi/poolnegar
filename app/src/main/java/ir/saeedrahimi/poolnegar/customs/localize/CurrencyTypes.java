package ir.saeedrahimi.poolnegar.customs.localize;


public enum CurrencyTypes {
    Rial(10, ""),
    Toman(20, "");

    int ID;
    String Title;

    CurrencyTypes(int id, String title) {
        this.ID = id;
        this.Title = title;
    }

    public int getID() { return this.ID; }

    public String getTitle() { return this.Title; }
    }

