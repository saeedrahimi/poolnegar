package ir.saeedrahimi.poolnegar.customs;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class OnSwipeTouchListener implements View.OnTouchListener {

    private final GestureDetector gestureDetector;
    public GestureDetector getGestureDetector(){
        return  gestureDetector;
    }
    public OnSwipeTouchListener(Context context) {
        gestureDetector = new GestureDetector(context, new GestureListener());
    }

    public void onSwipeLeft() {
    }

    public void onSwipeRight() {
    }

    public boolean onTouch(View v, MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

        private static final int SWIPE_MIN_DISTANCE = 120;
        private static final int SWIPE_MAX_OFF_PATH = 250;
        private static final int SWIPE_THRESHOLD_VELOCITY = 200;

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onFling(final MotionEvent e1, final MotionEvent e2, final float velocityX, final float velocityY) {
            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH) {  return false;  }

        /* positive value means right to left direction */
            final float distance = e1.getX() - e2.getX();
            final boolean enoughSpeed = Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY;
            if(distance > SWIPE_MIN_DISTANCE && enoughSpeed) {
                // right to left swipe
                onSwipeLeft();
                return true;
            }  else if (distance < -SWIPE_MIN_DISTANCE && enoughSpeed) {
                // left to right swipe
                onSwipeRight();
                return true;
            } else {
                // oooou, it didn't qualify; do nothing
                return false;
            }
        }
    }
}