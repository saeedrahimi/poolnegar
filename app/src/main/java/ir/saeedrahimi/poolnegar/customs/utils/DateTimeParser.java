package ir.saeedrahimi.poolnegar.customs.utils;

import java.util.StringTokenizer;

import ir.saeedrahimi.poolnegar.customs.DateTimeData;
import ir.saeedrahimi.poolnegar.customs.helper.JavaDateFormatter;

public class DateTimeParser {
    public String formatTime(String paramString) {
        String str1 = "";
        if (paramString.length() == 5)
            return paramString;
        String str2 = paramString.split(":")[0];
        try {
            paramString = paramString.split(":")[1];
        } catch (Exception exception) {
            str1 = "00";
        }
        String str3 = str2;
        if (str2.length() == 1) {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("0");
            stringBuilder1.append(str2);
            str3 = stringBuilder1.toString();
        }
        str2 = str1;
        if (str1.length() == 1) {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("0");
            stringBuilder1.append(str1);
            str2 = stringBuilder1.toString();
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str3);
        stringBuilder.append(":");
        stringBuilder.append(str2);
        return stringBuilder.toString();
    }

    public DateTimeData parse4DigitFormats(String paramString) {
        String str = paramString.substring(0, 2);
        int i = Integer.parseInt(paramString.substring(2, 4));
        int j = Integer.parseInt(str);
        return new DateTimeData((new JavaDateFormatter()).getIranianYear(), j, i);
    }

    public DateTimeData parse6DigitFormats(String paramString) {
        String str1 = "";
        String str2 = paramString.substring(0, 2);
        String str3 = paramString.substring(2, 4);
        String str4 = paramString.substring(4, 6);
        paramString = str2;
        if (str2.length() == 2) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("13");
            stringBuilder.append(str2);
            str1 = stringBuilder.toString();
        }
        int i = Integer.parseInt(str4);
        int j = Integer.parseInt(str3);
        return new DateTimeData(Integer.parseInt(str1), j, i);
    }

    public DateTimeData parseCommaFormats(String paramString) {
        String str1 = "";
        String str2 = paramString.split(",")[0];
        String str3 = paramString.split(",")[1];
        String str4 = paramString.split(",")[2];
        paramString = str2;
        if (str2.length() == 2) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("13");
            stringBuilder.append(str2);
            str1 = stringBuilder.toString();
        }
        int i = Integer.parseInt(str4);
        int j = Integer.parseInt(str3);
        return new DateTimeData(Integer.parseInt(str1), j, i);
    }

    public DateTimeData parseNonNumericalDates(String paramString) {
        StringTokenizer stringTokenizer = new StringTokenizer(paramString.replace(" \u0645\u0627\u0647", ""));
        String str2 = stringTokenizer.nextToken();
        paramString = stringTokenizer.nextToken();
        String str1 = stringTokenizer.nextToken();
        int i = Integer.parseInt(str2);
        int j = (new JavaDateFormatter()).containsMonthName(paramString);
        int k = j;
        if (j < 1)
            k = 1;
        return new DateTimeData(Integer.parseInt(str1), k, i);
    }

    public DateTimeData parseSlashFormats(String paramString) {
        String str1 = "";
        String str2 = paramString.split("/")[0];
        String str3 = paramString.split("/")[1];
        String str4 = paramString.split("/")[2];
        paramString = str2;
        if (str2.length() == 2) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("13");
            stringBuilder.append(str2);
            str1 = stringBuilder.toString();
        }
        int i = Integer.parseInt(str4);
        int j = Integer.parseInt(str3);
        return new DateTimeData(Integer.parseInt(str1), j, i);
    }

    public DateTimeData parseSlashMonthAndDayFormats(String paramString) {
        String str = paramString.split("/")[0];
        int i = Integer.parseInt(paramString.split("/")[1]);
        int j = Integer.parseInt(str);
        return new DateTimeData((new JavaDateFormatter()).getIranianYear(), j, i);
    }
}
