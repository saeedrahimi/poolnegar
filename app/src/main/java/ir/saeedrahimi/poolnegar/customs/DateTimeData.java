package ir.saeedrahimi.poolnegar.customs;


import ir.saeedrahimi.poolnegar.customs.helper.JavaDateFormatter;
import ir.saeedrahimi.poolnegar.customs.utils.DateTimeParser;

public class DateTimeData {
    private int day;

    private int month;

    private int year;

    public DateTimeData(int paramInt1, int paramInt2, int paramInt3) {
        this.year = paramInt1;
        this.month = paramInt2;
        this.day = paramInt3;
    }

    public static DateTimeData parse(String date) {
        DateTimeParser dateTimeParser = new DateTimeParser();
        return (date == null) ? (new JavaDateFormatter()).getIranianDateData() : ((date.contains("/") && date.length() <= 5) ? dateTimeParser.parseSlashMonthAndDayFormats(date) : (date.contains("/") ? dateTimeParser.parseSlashFormats(date) : (date.contains(",") ? dateTimeParser.parseCommaFormats(date) : ((date.contains("-") && date.length() == 5) ? dateTimeParser.parse4DigitFormats(date) : ((date.length() == 6) ? dateTimeParser.parse6DigitFormats(date) : (date.contains("\\u0645\\u0627\\u0647") ? dateTimeParser.parseNonNumericalDates(date) : (new JavaDateFormatter()).getIranianDateData()))))));
    }

    public int getDay() {
        return this.day;
    }

    public void setDay(int paramInt) {
        this.day = paramInt;
    }

    public int getMonth() {
        return this.month;
    }

    public void setMonth(int paramInt) {
        this.month = paramInt;
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int paramInt) {
        this.year = paramInt;
    }

    public void setDate(int paramInt1, int paramInt2, int paramInt3) {
        this.year = paramInt1;
        this.month = paramInt2;
        this.day = paramInt3;
    }

    public String toString() {
        StringBuilder resultBuilder = new StringBuilder();
        resultBuilder.append(this.year);
        resultBuilder.append("/");
        resultBuilder.append(String.format("%02d", new Object[]{Integer.valueOf(this.month)}));
        resultBuilder.append("/");
        resultBuilder.append(String.format("%02d", new Object[]{Integer.valueOf(this.day)}));
        return resultBuilder.toString();
    }
}
