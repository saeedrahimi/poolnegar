package ir.saeedrahimi.poolnegar.customs;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class ArtemEditText
  extends EditText
{
  public ArtemEditText(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public ArtemEditText(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  public ArtemEditText(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  public void init(Context paramContext)
  {
	  final Typeface bold = TypeFaceProvider.get(paramContext, "vazir.ttf");
    int i = paramContext.getSharedPreferences("saeedr_shared_prefs", 0).getInt("fontSize", -1);
    if (!isInEditMode())
    {
      setTypeface(bold);
      if (i != -1) {
        setTextSize(2, i);
      }
    }
  }
}
