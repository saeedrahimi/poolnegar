package ir.saeedrahimi.poolnegar.customs.localize;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.util.DisplayMetrics;

public class Localize {
    private static SharedPreferences _pref;


    private static int currencyID;


    private Context _context;

    public Localize(Context paramContext) {
        this._context = paramContext;
        _pref = paramContext.getSharedPreferences("artemPreference", 0);
        currencyID = _pref.getInt("currencyID", 10);
    }


    public static CurrencyTypes getCurrency() {
        currencyID = _pref.getInt("currencyID", 10);
        switch (currencyID){
            case 10:
                return CurrencyTypes.Rial;
            case 20:
                return CurrencyTypes.Toman;
        }
        return CurrencyTypes.Toman;
    }


    public void setCurrencyType(CurrencyTypes paramCurrencyTypes) { _pref.edit().putInt("currencyID", paramCurrencyTypes.getID()).commit(); }


}
