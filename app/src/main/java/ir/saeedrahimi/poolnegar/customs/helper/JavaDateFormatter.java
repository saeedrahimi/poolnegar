package ir.saeedrahimi.poolnegar.customs.helper;


import android.content.Context;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import ir.saeedrahimi.poolnegar.customs.DateTimeData;

public class JavaDateFormatter {
    String aban = "آبان";
    String azar = "آذر";
    String bahman = "بهمن";
    String dey = "دی";
    String esfand = "اسفند";
    String farvardin = "فروردین";
    String khordard = "خرداد";
    String mehr = "مهر";
    String mordad = "مرداد";
    String ordibehesht = "اردیبهشت";
    String shahrivar = "شهریور";
    String tir = "تیر";
    private int JDN;
    private int gDay;

    private int gMonth;

    private int gYear;

    private int irDay;

    private int irMonth;

    private int irYear;

    private int juDay;

    private int juMonth;

    private int juYear;

    private int leap;

    private int march;


    public JavaDateFormatter() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        setGregorianDate(gregorianCalendar.get(Calendar.YEAR), gregorianCalendar.get(Calendar.MONTH) + 1, gregorianCalendar.get(Calendar.DAY_OF_MONTH));
    }

    public JavaDateFormatter(int paramInt1, int paramInt2, int paramInt3) {
        setGregorianDate(paramInt1, paramInt2, paramInt3);
    }

    public static int DateCompare(String paramString1, String paramString2) {
        try {
            String[] arrayOfString1 = paramString1.split("/");
            String[] arrayOfString2 = paramString2.split("/");
            if (Integer.parseInt(arrayOfString1[0]) > Integer.parseInt(arrayOfString2[0]))
                return -1;
            if (Integer.parseInt(arrayOfString1[0]) == Integer.parseInt(arrayOfString2[0])) {
                if (Integer.parseInt(arrayOfString1[1]) > Integer.parseInt(arrayOfString2[1]))
                    return -1;
                if (Integer.parseInt(arrayOfString1[1]) == Integer.parseInt(arrayOfString2[1])) {
                    if (Integer.parseInt(arrayOfString1[2]) > Integer.parseInt(arrayOfString2[2]))
                        return -1;
                    if (Integer.parseInt(arrayOfString1[2]) == Integer.parseInt(arrayOfString2[2]))
                        return 0;
                    if (Integer.parseInt(arrayOfString1[2]) < Integer.parseInt(arrayOfString2[2]))
                        return 1;
                } else if (Integer.parseInt(arrayOfString1[1]) < Integer.parseInt(arrayOfString2[1])) {
                    return 1;
                }
            } else {
                int i = Integer.parseInt(arrayOfString1[0]);
                int j = Integer.parseInt(arrayOfString2[0]);
                if (i < j)
                    return 1;
            }
        } catch (Exception exception) {
        }
        return Integer.MAX_VALUE;
    }

    private void IranianCalendar() {
        int n;
        int m;
        int[] arrayOfInt = new int[20];
        arrayOfInt[0] = -61;
        arrayOfInt[1] = 9;
        arrayOfInt[2] = 38;
        arrayOfInt[3] = 199;
        arrayOfInt[4] = 426;
        arrayOfInt[5] = 686;
        arrayOfInt[6] = 756;
        arrayOfInt[7] = 818;
        arrayOfInt[8] = 1111;
        arrayOfInt[9] = 1181;
        arrayOfInt[10] = 1210;
        arrayOfInt[11] = 1635;
        arrayOfInt[12] = 2060;
        arrayOfInt[13] = 2097;
        arrayOfInt[14] = 2192;
        arrayOfInt[15] = 2262;
        arrayOfInt[16] = 2324;
        arrayOfInt[17] = 2394;
        arrayOfInt[18] = 2456;
        arrayOfInt[19] = 3178;
        this.gYear = this.irYear + 621;
        int i = arrayOfInt[0];
        int j = 1;
        int k = -14;
        int i1 = k;
        while (true) {
            int i2 = arrayOfInt[j];
            m = i2 - i;
            n = i;
            i1 = k;
            if (this.irYear >= i2) {
                i1 = k + m / 33 * 8 + m % 33 / 4;
                n = i2;
            }
            if (++j < 20) {
                i = n;
                k = i1;
                if (this.irYear < i2)
                    break;
                continue;
            }
            break;
        }
        j = this.irYear - n;
        k = i1 + j / 33 * 8 + (j % 33 + 3) / 4;
        if (m % 33 == 4) {
            i1 = k;
            if (m - j == 4)
                i1 = k + 1;
        }
        k = this.gYear;
        this.march = i1 + 20 - k / 4 - (k / 100 + 1) * 3 / 4 - 150;
        i1 = j;
        if (m - j < 6)
            i1 = j - m + (m + 4) / 33 * 33;
        this.leap = ((i1 + 1) % 33 - 1) % 4;
        if (this.leap == -1)
            this.leap = 4;
    }

    private int IranianDateToJDN() {
        IranianCalendar();
        int i = gregorianDateToJDN(this.gYear, 3, this.march);
        int j = this.irMonth;
        return i + (j - 1) * 31 - j / 7 * (j - 7) + this.irDay - 1;
    }

    private void JDNToGregorian() {
        int i = this.JDN;
        int j = i * 4 + 139361631 + (i * 4 + 183187720) / 146097 * 3 / 4 * 4 - 3908;
        i = j % 1461 / 4 * 5 + 308;
        this.gDay = i % 153 / 5 + 1;
        this.gMonth = i / 153 % 12 + 1;
        this.gYear = j / 1461 - 100100 + (8 - this.gMonth) / 6;
    }

    private void JDNToIranian() {
        JDNToGregorian();
        this.irYear = this.gYear - 621;
        IranianCalendar();
        int i = gregorianDateToJDN(this.gYear, 3, this.march);
        i = this.JDN - i;
        if (i >= 0) {
            if (i <= 185) {
                this.irMonth = i / 31 + 1;
                this.irDay = i % 31 + 1;
                return;
            }
            i -= 186;
        } else {
            this.irYear--;
            int j = i + 179;
            i = j;
            if (this.leap == 1)
                i = j + 1;
        }
        this.irMonth = i / 30 + 7;
        this.irDay = i % 30 + 1;
    }

    private void JDNToJulian() {
        int i = this.JDN * 4 + 139361631;
        int j = i % 1461 / 4 * 5 + 308;
        this.juDay = j % 153 / 5 + 1;
        this.juMonth = j / 153 % 12 + 1;
        this.juYear = i / 1461 - 100100 + (8 - this.juMonth) / 6;
    }

    private int gregorianDateToJDN(int paramInt1, int paramInt2, int paramInt3) {
        int i = (paramInt2 - 8) / 6;
        return (paramInt1 + i + 100100) * 1461 / 4 + ((paramInt2 + 9) % 12 * 153 + 2) / 5 + paramInt3 - 34840408 - (paramInt1 + 100100 + i) / 100 * 3 / 4 + 752;
    }

    private int julianDateToJDN(int paramInt1, int paramInt2, int paramInt3) {
        return (paramInt1 + (paramInt2 - 8) / 6 + 100100) * 1461 / 4 + ((paramInt2 + 9) % 12 * 153 + 2) / 5 + paramInt3 - 34840408;
    }

    private String replaceArabicLetters(String paramString) {
        return paramString.replace("\u0643", "\u06A9").replace("\u064A", "\u064A").replace("\u0625", "\u0627").replace("\u0623", "\u0627").replace("\u0626", "\u06CC").replace("\u0643", "\u06A9").replace("\u0629", "\u0647").replace("\u0649", "\u06CC").replace("\u0627", "\u0627").replace("\u0623", "\u0627").replace("\u0622", "\u0622").replace("\u0629", "\u0647").replace("\u0627", "\u0627").replace("\u0643", "\u06A9").replace("\u064A", "\u06CC").replace("\u0622", "\u0622").replace("\u0626", "\u06CC").replace("\u064A", "\u06CC").replace("\u0626", "\u06CC").replace("\u0625", "\u0627");
    }

    public int containsMonthName(String paramString) {
        paramString = replaceArabicLetters(paramString);
        return paramString.contains(this.farvardin) ? 1 : (paramString.contains(this.ordibehesht) ? 2 : (paramString.contains(this.khordard) ? 3 : (paramString.contains(this.tir) ? 4 : (paramString.contains(this.mordad) ? 5 : (paramString.contains(this.shahrivar) ? 6 : (paramString.contains(this.mehr) ? 7 : (paramString.contains(this.aban) ? 8 : (paramString.contains(this.azar) ? 9 : (paramString.contains(this.dey) ? 10 : (paramString.contains(this.bahman) ? 11 : (paramString.contains(this.esfand) ? 12 : -1)))))))))));
    }

    public long getCountDays(String paramString1, String paramString2) {
        long l;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        try {
            Date date = simpleDateFormat.parse(paramString1);
            l = (simpleDateFormat.parse(paramString2).getTime() - date.getTime()) / 86400000L;
        } catch (ParseException parseException) {
            l = -1L;
        }
        return l;
    }

    public String getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%02d", new Object[]{Integer.valueOf(calendar.get(Calendar.HOUR_OF_DAY))}));
        stringBuilder.append(":");
        stringBuilder.append(String.format("%02d", new Object[]{Integer.valueOf(calendar.get(Calendar.MINUTE))}));
        return stringBuilder.toString();
    }

    public int getDayOfWeek() {
        return this.JDN % 7;
    }

    public String getEndOfWeek(DateTimeData paramDateTimeData) {
        JavaDateFormatter javaDateFormatter = new JavaDateFormatter();
        javaDateFormatter.setIranianDate(paramDateTimeData.getYear(), paramDateTimeData.getMonth(), paramDateTimeData.getDay());
        javaDateFormatter.nextDay(7);
        return javaDateFormatter.getParmisDateFormat();
    }

    public String getGregorianDate() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.gYear);
        stringBuilder.append("/");
        stringBuilder.append(this.gMonth);
        stringBuilder.append("/");
        stringBuilder.append(this.gDay);
        return stringBuilder.toString();
    }

    public String getGregorianDateFormatted() {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("");
        stringBuilder1.append(this.gMonth);
        String str1 = stringBuilder1.toString();
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("");
        stringBuilder2.append(this.gDay);
        String str2 = stringBuilder2.toString();
        if (this.gMonth < 10) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("0");
            stringBuilder.append(this.gMonth);
            str1 = stringBuilder.toString();
        }
        if (this.gDay < 10) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("0");
            stringBuilder.append(this.gDay);
            str2 = stringBuilder.toString();
        }
        StringBuilder stringBuilder3 = new StringBuilder();
        stringBuilder3.append(this.gYear);
        stringBuilder3.append("/");
        stringBuilder3.append(str1);
        stringBuilder3.append("/");
        stringBuilder3.append(str2);
        return stringBuilder3.toString();
    }

    public int getGregorianDay() {
        return this.gDay;
    }

    public long getGregorianEpoch() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(this.gYear, this.gMonth - 1, this.gDay, 0, 0, 0);
        return calendar.getTimeInMillis();
    }

    public int getGregorianMonth() {
        return this.gMonth;
    }

    public int getGregorianYear() {
        return this.gYear;
    }

    public String getIranianDate() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.irYear);
        stringBuilder.append("/");
        stringBuilder.append(this.irMonth);
        stringBuilder.append("/");
        stringBuilder.append(this.irDay);
        return stringBuilder.toString();
    }

    public DateTimeData getIranianDateData() {
        return new DateTimeData(this.irYear, this.irMonth, this.irDay);
    }

    public String getIranianDateFormatted() {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("");
        stringBuilder1.append(this.irMonth);
        String str1 = stringBuilder1.toString();
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("");
        stringBuilder2.append(this.irDay);
        String str2 = stringBuilder2.toString();
        if (this.irMonth < 10) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("0");
            stringBuilder.append(this.irMonth);
            str1 = stringBuilder.toString();
        }
        if (this.irDay < 10) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("0");
            stringBuilder.append(this.irDay);
            str2 = stringBuilder.toString();
        }
        StringBuilder stringBuilder3 = new StringBuilder();
        stringBuilder3.append(this.irYear);
        stringBuilder3.append("/");
        stringBuilder3.append(str1);
        stringBuilder3.append("/");
        stringBuilder3.append(str2);
        return stringBuilder3.toString();
    }

    public int getIranianDay() {
        return this.irDay;
    }

    public String getIranianDayOFWeekName(Context context) {
        String str2 = "یکشنبه";
        String str3 = "دوشنبه";
        String str4 = "سه‌شنبه";
        String str5 = "چهارشنبه";
        String str6 = "پنج‌شنبه";
        String str7 = "جمعه";
        String str1 = "شنبه";
        int i = getIranianDayOfWeek();
        (new String[7])[0] = str2;
        (new String[7])[1] = str3;
        (new String[7])[2] = str4;
        (new String[7])[3] = str5;
        (new String[7])[4] = str6;
        (new String[7])[5] = str7;
        (new String[7])[6] = str1;
        return (new String[7])[i];
    }

    public int getIranianDayOfWeek() {
        int i = getDayOfWeek();
        if (i == 5) {
            i = 0;
        } else if (i == 6) {
            i = 1;
        } else {
            i += 2;
        }
        return i;
    }

    public int getIranianMonth() {
        return this.irMonth;
    }

    public int getIranianYear() {
        return this.irYear;
    }

    public String getJulianDate() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.juYear);
        stringBuilder.append("/");
        stringBuilder.append(this.juMonth);
        stringBuilder.append("/");
        stringBuilder.append(this.juDay);
        return stringBuilder.toString();
    }

    public int getJulianDay() {
        return this.juDay;
    }

    public int getJulianMonth() {
        return this.juMonth;
    }

    public int getJulianYear() {
        return this.juYear;
    }

    public int getMonthNumber(String paramString) {
        paramString = replaceArabicLetters(paramString);
        return paramString.equals(this.farvardin) ? 1 : (paramString.equals(this.ordibehesht) ? 2 : (paramString.equals(this.khordard) ? 3 : (paramString.equals(this.tir) ? 4 : (paramString.equals(this.mordad) ? 5 : (paramString.equals(this.shahrivar) ? 6 : (paramString.equals(this.mehr) ? 7 : (paramString.equals(this.aban) ? 8 : (paramString.equals(this.azar) ? 9 : (paramString.equals(this.dey) ? 10 : (paramString.equals(this.bahman) ? 11 : (paramString.equals(this.esfand) ? 12 : -1)))))))))));
    }

    public DateTimeData getNextDay(DateTimeData paramDateTimeData) {
        JavaDateFormatter javaDateFormatter = new JavaDateFormatter();
        javaDateFormatter.setIranianDate(paramDateTimeData.getYear(), paramDateTimeData.getMonth(), paramDateTimeData.getDay());
        javaDateFormatter.nextDay(1);
        return javaDateFormatter.getIranianDateData();
    }

    public String getParmisDateFormat() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("");
        stringBuilder.append(getIranianYear());
        stringBuilder.append("/");
        stringBuilder.append(String.format("%02d", new Object[]{Integer.valueOf(getIranianMonth())}));
        stringBuilder.append("/");
        stringBuilder.append(String.format("%02d", new Object[]{Integer.valueOf(getIranianDay())}));
        stringBuilder.append("");
        return stringBuilder.toString();
    }

    public DateTimeData getPreviousDay(DateTimeData paramDateTimeData) {
        JavaDateFormatter javaDateFormatter = new JavaDateFormatter();
        javaDateFormatter.setIranianDate(paramDateTimeData.getYear(), paramDateTimeData.getMonth(), paramDateTimeData.getDay());
        javaDateFormatter.previousDay(1);
        return javaDateFormatter.getIranianDateData();
    }

    public DateTimeData getPreviousDayForWeek(DateTimeData paramDateTimeData) {
        JavaDateFormatter javaDateFormatter = new JavaDateFormatter();
        javaDateFormatter.setIranianDate(paramDateTimeData.getYear(), paramDateTimeData.getMonth(), paramDateTimeData.getDay());
/*        if (Localize.getCalendar() == CalendarTypes.Gregorian) {
            javaDateFormatter.previousDay(3);
        } else {*/
        javaDateFormatter.previousDay(1);
        //      }
        return javaDateFormatter.getIranianDateData();
    }

    public String getStartOfWeek(DateTimeData paramDateTimeData) {
        JavaDateFormatter javaDateFormatter = new JavaDateFormatter();
        javaDateFormatter.setIranianDate(paramDateTimeData.getYear(), paramDateTimeData.getMonth(), paramDateTimeData.getDay());
        javaDateFormatter.previousDay(javaDateFormatter.getIranianDayOfWeek());
   /*     if (Localize.getCalendar() == CalendarTypes.Gregorian)
            javaDateFormatter.nextDay(2);*/
        return javaDateFormatter.getParmisDateFormat();
    }

    public String getWeekDayStr() {
        int i = getDayOfWeek();
        (new String[7])[0] = "Monday";
        (new String[7])[1] = "Tuesday";
        (new String[7])[2] = "Wednesday";
        (new String[7])[3] = "Thursday";
        (new String[7])[4] = "Friday";
        (new String[7])[5] = "Saturday";
        (new String[7])[6] = "Sunday";
        return (new String[7])[i];
    }

    public String getWeeksAgo(int paramInt1, int paramInt2, int paramInt3) {
        JavaDateFormatter javaDateFormatter = new JavaDateFormatter();
        javaDateFormatter.setIranianDate(paramInt1, paramInt2, paramInt3);
        javaDateFormatter.previousDay(7);
        return javaDateFormatter.getParmisDateFormat();
    }

    public String getyesterdayDate(int paramInt1, int paramInt2, int paramInt3) {
        int i = paramInt3 - 1;
        int j = paramInt1;
        int k = paramInt2;
        paramInt3 = i;
        if (i <= 0) {
            k = paramInt2 - 1;
            if (k <= 6) {
                paramInt3 = i + 31;
                j = paramInt1;
            } else if (k > 6 && k <= 11) {
                paramInt3 = i + 30;
                j = paramInt1;
            } else if (k == 12) {
                paramInt3 = i + 29;
                j = paramInt1;
            } else {
                j = paramInt1 - 1;
                paramInt3 = i + 29;
                k = 12;
            }
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(j);
        stringBuilder.append("/");
        stringBuilder.append(k);
        stringBuilder.append("/");
        stringBuilder.append(paramInt3);
        return stringBuilder.toString();
    }

    public String getyesterdayDate(String paramString1, String paramString2, String paramString3) {
        int i = Integer.parseInt(paramString2);
        int j = Integer.parseInt(paramString3);
        int k = Integer.parseInt(paramString1);
        int m = j - 1;
        int n = k;
        int i1 = i;
        j = m;
        if (m <= 0) {
            i1 = i - 1;
            if (i1 <= 6) {
                j = m + 31;
                n = k;
            } else if (i1 > 6 && i1 <= 11) {
                j = m + 30;
                n = k;
            } else if (i1 == 12) {
                j = m + 29;
                n = k;
            } else {
                n = k - 1;
                j = m + 29;
                i1 = 12;
            }
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(n);
        stringBuilder.append("/");
        stringBuilder.append(i1);
        stringBuilder.append("/");
        stringBuilder.append(j);
        return stringBuilder.toString();
    }

    public void nextDay() {
        this.JDN++;
        JDNToIranian();
        JDNToJulian();
        JDNToGregorian();
    }

    public void nextDay(int paramInt) {
        this.JDN += paramInt;
        JDNToIranian();
        JDNToJulian();
        JDNToGregorian();
    }

    public void nextMonth() {
        if (getIranianMonth() <= 6) {
            nextDay(31);
        } else if (getIranianMonth() > 6 && getIranianMonth() <= 11) {
            nextDay(30);
        } else if (getIranianMonth() == 12) {
            if (getIranianYear() % 4 == 3) {
                nextDay(30);
            } else {
                nextDay(29);
            }
        }
    }

    public void previousDay() {
        this.JDN--;
        JDNToIranian();
        JDNToJulian();
        JDNToGregorian();
    }

    public void previousDay(int paramInt) {
        this.JDN -= paramInt;
        JDNToIranian();
        JDNToJulian();
        JDNToGregorian();
    }

    public void setGregorianDate(int paramInt1, int paramInt2, int paramInt3) {
        this.gYear = paramInt1;
        this.gMonth = paramInt2;
        this.gDay = paramInt3;
        this.JDN = gregorianDateToJDN(paramInt1, paramInt2, paramInt3);
        JDNToIranian();
        JDNToJulian();
        JDNToGregorian();
    }

    public void setIranianDate(int paramInt1, int paramInt2, int paramInt3) {
        this.irYear = paramInt1;
        this.irMonth = paramInt2;
        this.irDay = paramInt3;
        this.JDN = IranianDateToJDN();
        JDNToIranian();
        JDNToJulian();
        JDNToGregorian();
    }

    public void setJulianDate(int paramInt1, int paramInt2, int paramInt3) {
        this.juYear = paramInt1;
        this.juMonth = paramInt2;
        this.juDay = paramInt3;
        this.JDN = julianDateToJDN(paramInt1, paramInt2, paramInt3);
        JDNToIranian();
        JDNToJulian();
        JDNToGregorian();
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getWeekDayStr());
        stringBuilder.append(", Gregorian:[");
        stringBuilder.append(getGregorianDate());
        stringBuilder.append("], Julian:[");
        stringBuilder.append(getJulianDate());
        stringBuilder.append("], Iranian:[");
        stringBuilder.append(getIranianDate());
        stringBuilder.append("]");
        return stringBuilder.toString();
    }
}
