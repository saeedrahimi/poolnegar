package ir.saeedrahimi.poolnegar.customs;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class ArtemButton
  extends androidx.appcompat.widget.AppCompatButton
{
  public ArtemButton(Context paramContext)
  {
    super(paramContext);
    a(paramContext);
  }

  public ArtemButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramContext);
  }

  public ArtemButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a(paramContext);
  }

  public void a(Context paramContext)
  {
	final Typeface bold =  TypeFaceProvider.get(paramContext, "vazir.ttf");
    int i = paramContext.getSharedPreferences("parmisPreference", 0).getInt("fontSize", -1);
    if (!isInEditMode())
    {
      setMinHeight(30);
      setTypeface(bold);
      if (i != -1) {
        setTextSize(2, i);
      }
    }
  }
}
