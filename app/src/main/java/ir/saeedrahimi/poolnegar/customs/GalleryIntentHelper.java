package ir.saeedrahimi.poolnegar.customs;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * Created by Saeed on 9/3/2015.
 */
public class GalleryIntentHelper {
    private static final int REQUEST_CODE_CHOOSE_PICTURE_FROM_GALLERY = 1000;
    private final static int REQUEST_CODE_CAPTURE_IMAGE = 101;

    public Activity mActivity;
    private final GalleryIntentHelperCallback mGalleryIntentHelperCallback;
    Intent mPhotoIntent;

    Bitmap bitmap;
    private String mFilename;
    String selectedImagePath;
    private String mFilePath;
    private String mTempDir = Environment.getExternalStorageDirectory()+ "/PoolNegarData/temp";
    public GalleryIntentHelper(Activity activity, String filename, GalleryIntentHelperCallback galleryIntentHelperCallback) {
        File tempdir = new File(mTempDir);
        if (!tempdir.exists()) {
            tempdir.mkdirs();
        }
        mActivity=activity;
        mFilename = filename;
        mFilePath = mTempDir + File.separator +"temp.jpg";
        mGalleryIntentHelperCallback = galleryIntentHelperCallback;
    }
    public void startIntent(String type){
        if(type.toLowerCase().trim() == "gallery")
        {
            mPhotoIntent = new Intent(
                    Intent.ACTION_PICK, null);
            mPhotoIntent.setType("image/*");
            mPhotoIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.name());
            mPhotoIntent.putExtra("return-data", true);
            mActivity.startActivityForResult(mPhotoIntent,
                    REQUEST_CODE_CHOOSE_PICTURE_FROM_GALLERY);

        }
        else if(type.toLowerCase().trim() == "camera"){
            mPhotoIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            mActivity.startActivityForResult(mPhotoIntent,
                    REQUEST_CODE_CAPTURE_IMAGE);
        }
/*        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        mFilePath = mTempDir + File.separator +"temp.jpg";
        Uri uriImagePath = Uri.fromFile(new File(mFilePath));
        photoPickerIntent.setType("image");
        photoPickerIntent.putExtra(MediaStore.EXTRA_OUTPUT,uriImagePath);
        photoPickerIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.name());
        photoPickerIntent.putExtra("return-data", true);
        mActivity.startActivityForResult(photoPickerIntent, REQUEST_CODE_CHOOSE_PICTURE_FROM_GALLERY);*/
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE_CHOOSE_PICTURE_FROM_GALLERY) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    BitmapDrawable bmpDrawable = null;

                    Uri selectedImage = data.getData();

                    Bitmap datifoto = null;
                    //temp.setImageBitmap(null);
                    Uri picUri = null;
                    picUri = data.getData();//<- get Uri here from data intent
                    if (picUri != null) {
                        try {
                            datifoto = MediaStore.Images.Media.getBitmap(mActivity.getContentResolver(), picUri);
                            //temp.setImageBitmap(datifoto);
                            Utils.copyFile(new File(picUri.toString()), new File(mFilePath));

                            mGalleryIntentHelperCallback.onPhotoUriFound(picUri);
                        } catch (FileNotFoundException e) {
                            throw new RuntimeException(e);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        } catch (OutOfMemoryError e) {
                            Toast.makeText(mActivity.getBaseContext(), "Image is too large. choose other", Toast.LENGTH_LONG).show();
                        }


                 /*   else {
                        File f = new File(mFilePath);
                        try {
                            f.createNewFile();
                            Utils.copyFile(new File( data.getData().getPath()), f);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        bmpDrawable = new BitmapDrawable(mActivity.getResources(), data
                                .getData().getPath());
                        //img_logo.setImageDrawable(bmpDrawable);
                    }*/

                    } else {
                        Toast.makeText(mActivity.getApplicationContext(), "Cancelled",
                                Toast.LENGTH_SHORT).show();
                    }
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    Toast.makeText(mActivity.getApplicationContext(), "Cancelled",
                            Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == REQUEST_CODE_CAPTURE_IMAGE) {
                if (resultCode == Activity.RESULT_OK) {
                    if (data.hasExtra("data")) {

                        // retrieve the bitmap from the intent
                        bitmap = (Bitmap) data.getExtras().get("data");


                        Cursor cursor = mActivity.getContentResolver()
                                .query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                        new String[]{
                                                MediaStore.Images.Media.DATA,
                                                MediaStore.Images.Media.DATE_ADDED,
                                                MediaStore.Images.ImageColumns.ORIENTATION},
                                        MediaStore.Images.Media.DATE_ADDED, null, "date_added ASC");
                        if (cursor != null && cursor.moveToFirst()) {
                            do {
                                Uri uri = Uri.parse(cursor.getString(cursor
                                        .getColumnIndex(MediaStore.Images.Media.DATA)));
                                selectedImagePath = uri.toString();
                            } while (cursor.moveToNext());
                            cursor.close();
                        }

                        Log.e("path of the image from camera ====> ", selectedImagePath + "");
                        File fileDir = new File(mFilePath);
                        FileOutputStream out;
                        try {
                            out = new FileOutputStream(fileDir);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
                            out.flush();
                            out.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                        bitmap = Bitmap.createScaledBitmap(bitmap, 100,
                                100, false);
                        // update the image view with the bitmap
                        //img_logo.setImageBitmap(bitmap);
                    } else if (data.getExtras() == null) {

                        Toast.makeText(mActivity.getApplicationContext(),
                                "No extras to retrieve!", Toast.LENGTH_SHORT)
                                .show();

                        BitmapDrawable thumbnail = new BitmapDrawable(
                                mActivity.getResources(), data.getData().getPath());

                        // update the image view with the newly created drawable
                        //img_logo.setImageDrawable(thumbnail);

                    }

                } else if (resultCode == Activity.RESULT_CANCELED) {
                    Toast.makeText(mActivity.getApplicationContext(), "Cancelled",
                            Toast.LENGTH_SHORT).show();
                }
            }

        }   }
    public interface GalleryIntentHelperCallback {
        void onPhotoUriFound(Uri photoUri);

        void deletePhotoWithUri(Uri photoUri);

        void onSdCardNotMounted();

        void onCanceled();

        void onPhotoUriNotFound();

        void logException(Exception e);
    }
}
