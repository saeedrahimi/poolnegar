package ir.saeedrahimi.poolnegar.customs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Locale;

import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.FileLog;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.MainApplication;

public class Utils {

	Context c;
	private static final Hashtable<String, Typeface> typefaceCache = new Hashtable<>();
	public static float density = 1;
	public static Point displaySize = new Point();
	public static DisplayMetrics displayMetrics = new DisplayMetrics();
	public static boolean usingHardwareInput;
	private static boolean waitingForSms = false;
    private static final Object smsLock = new Object();

	static{
		density = MainApplication.getAppContext().getResources().getDisplayMetrics().density;
		checkDisplaySize();
	}
	public Utils() {

	}

	public Utils(Context paramContext) {
		this.c = paramContext;
	}

	public static String CommaSeparate(String input, boolean clearBefore) {
		double dAmount = 0;
		String str = input;
		try {
			if (clearBefore)
				str = input.replaceAll("[^\\d]", "");
			dAmount = Double.parseDouble(str);
			NumberFormat nFormat = NumberFormat.getInstance(Locale.ENGLISH);
			((DecimalFormat) nFormat).applyPattern("###,###.###");
			return nFormat.format(dAmount);
		} catch (Exception e) {
			return input;
		}
	}
	public static int dp(float value) {
        if (value == 0) {
            return 0;
        }
        return (int)Math.ceil(density * value);
    }
	public static void clearCursorDrawable(EditText editText) {
        if (editText == null || Build.VERSION.SDK_INT < 12) {
            return;
        }
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.setInt(editText, 0);
        } catch (Exception e) {
            Log.d("tmessages", e.getMessage());
        }
    }
	public static Typeface getTypeface(String assetPath) {
        synchronized (typefaceCache) {
            if (!typefaceCache.containsKey(assetPath)) {
                try {
                    Typeface t = Typeface.createFromAsset(MainApplication.getAppContext().getAssets(), assetPath);
                    typefaceCache.put(assetPath, t);
                } catch (Exception e) {
                    FileLog.e("Typefaces", "Could not get typeface '" + assetPath + "' because " + e.getMessage());
                    return null;
                }
            }
            return typefaceCache.get(assetPath);
        }
    }
	public static boolean isWaitingForSms() {
        boolean value = false;
        synchronized (smsLock) {
            value = waitingForSms;
        }
        return value;
    }

    public static void setWaitingForSms(boolean value) {
        synchronized (smsLock) {
            waitingForSms = value;
        }
    }
	public static TextWatcher MoneyTextWatcher = new TextWatcher() {
		boolean isEdiging;

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
			if (isEdiging || s.length() < 1)
				return;

			isEdiging = true;
			String str = s.toString().replaceAll("[^\\d]", "");
			if (str.length() < 1) {
				isEdiging = false;
				s.clear();
				return;
			}
			double s1 = Double.parseDouble(str);

			NumberFormat nf2 = NumberFormat.getInstance(Locale.ENGLISH);
			((DecimalFormat) nf2).applyPattern("$ ###,###.###");
			s.replace(0, s.length(), nf2.format(s1));

			isEdiging = false;
		}
	};

	public static class exitHandler implements View.OnClickListener {
		private Dialog dlg;
		private Activity mainAct;

		public exitHandler(Activity paramMainActivity, Dialog paramDialog) {
			this.mainAct = paramMainActivity;
			this.dlg = paramDialog;

		}

		public void onClick(View paramView) {
			this.dlg.dismiss();
			this.mainAct.finish();
			this.mainAct.moveTaskToBack(true);
			System.exit(0);
		}
	}

	public static class dismissDialog implements View.OnClickListener {
		private Dialog dlg;

		public dismissDialog(Activity paramMainActivity, Dialog paramDialog) {
			this.dlg = paramDialog;

		}

		public void onClick(View paramView) {
			dlg.dismiss();
		}
	}
	public static void showKeyboard(View view) {
        if (view == null) {
            return;
        }
        InputMethodManager inputManager = (InputMethodManager)view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public static boolean isKeyboardShowed(View view) {
        if (view == null) {
            return false;
        }
        InputMethodManager inputManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        return inputManager.isActive(view);
    }

    public static void hideKeyboard(View view) {
        if (view == null) {
            return;
        }
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (!imm.isActive()) {
            return;
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
	public void RestartApp() {
		Intent inn = this.c.getPackageManager()
				.getLaunchIntentForPackage(this.c.getPackageName());
		inn.addFlags(67108864);
		this.c.startActivity(inn);
	}

	public static int GetAccIcon(String accountId) {

        switch (accountId) {
            case Constants.ACCOUNTS.EXPENSE:
                return R.mipmap.ic_coins;
            case Constants.ACCOUNTS.INCOME:
                return R.mipmap.deposit;
            case Constants.ACCOUNTS.Banks:
                return R.mipmap.bank;
            case Constants.ACCOUNTS.DEMAND:
                return R.mipmap.credit;
            case "دارایی ها":
                return R.mipmap.golds;
            case Constants.ACCOUNTS.DEBT:
                return R.mipmap.bill;
            case Constants.ACCOUNTS.PROJECTS:
                return R.mipmap.ic_project;
            case Constants.ACCOUNTS.PERSONS:
                return R.mipmap.users;
        }
		return R.drawable.ic_sign_account_not_selected;
	}
    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }

        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }


    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {

        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor =   context.getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
	public String getDeviceId(){

		TelephonyManager tm = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);

        String uuid;
        String androidID = Secure.getString(c.getContentResolver(), Secure.ANDROID_ID);
        String deviceID = tm.getDeviceId();
        String simID = tm.getSimSerialNumber();
        
        if ("9774d56d682e549c".equals(androidID) || androidID == null) {
            androidID = "";
        } 
        
        if (deviceID == null) {
            deviceID = "";
        }
        
        if (simID == null) { 
            simID = "";
        }   

        uuid = androidID + deviceID + simID;
        uuid = String.format("%32s", uuid).replace(' ', '0');
        uuid = uuid.substring(0,32);
		uuid = uuid.replaceAll("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5");
		return uuid;
	}
	public static String getTime(){

        Calendar now = Calendar.getInstance();
        return now.get(Calendar.HOUR_OF_DAY) + ":" + now.get(Calendar.MINUTE);
    }
	public static boolean isNetworkAvailable(final Context context, boolean canShowErrorDialogOnFail) {

        boolean isNetworkAvailable = false;

        if (context != null) {

            ConnectivityManager connectivityManager
                    = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

            isNetworkAvailable = activeNetworkInfo != null && activeNetworkInfo.isConnected();

            if (!isNetworkAvailable && canShowErrorDialogOnFail) {

                Log.v("TAG", "context : " + context.toString());

                if (context instanceof Activity) {

                    ((Activity) context).runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            /*Util.displayAlert((Activity) context,
                                    context.getString(R.string.app_name),
                                    context.getString(R.string.alert_internet),
                                    false);*/
                        }
                    });

                }
            }
        }

        return isNetworkAvailable;
    }
	public static void runOnUIThread(Runnable runnable) {
        runOnUIThread(runnable, 0);
    }

    public static void runOnUIThread(Runnable runnable, long delay) {
        if (delay == 0) {
            MainApplication.getAppHandler().post(runnable);
        } else {
            MainApplication.getAppHandler().postDelayed(runnable, delay);
        }
    }

    public static void cancelRunOnUIThread(Runnable runnable) {
        MainApplication.getAppHandler().removeCallbacks(runnable);
    }
    public static void checkDisplaySize() {
        try {
            Configuration configuration = MainApplication.getAppContext().getResources().getConfiguration();
            usingHardwareInput = configuration.keyboard != Configuration.KEYBOARD_NOKEYS && configuration.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO;
            WindowManager manager = (WindowManager) MainApplication.getAppContext().getSystemService(Context.WINDOW_SERVICE);
            if (manager != null) {
                Display display = manager.getDefaultDisplay();
                if (display != null) {
                    display.getMetrics(displayMetrics);
                    if (Build.VERSION.SDK_INT < 13) {
                        displaySize.set(display.getWidth(), display.getHeight());
                    } else {
                        display.getSize(displaySize);
                    }
                    Log.d("tmessages", "display size = " + displaySize.x + " " + displaySize.y + " " + displayMetrics.xdpi + "x" + displayMetrics.ydpi);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*
        keyboardHidden
        public static final int KEYBOARDHIDDEN_NO = 1
        Constant for keyboardHidden, value corresponding to the keysexposed resource qualifier.

        public static final int KEYBOARDHIDDEN_UNDEFINED = 0
        Constant for keyboardHidden: a value indicating that no value has been set.

        public static final int KEYBOARDHIDDEN_YES = 2
        Constant for keyboardHidden, value corresponding to the keyshidden resource qualifier.

        hardKeyboardHidden
        public static final int HARDKEYBOARDHIDDEN_NO = 1
        Constant for hardKeyboardHidden, value corresponding to the physical keyboard being exposed.

        public static final int HARDKEYBOARDHIDDEN_UNDEFINED = 0
        Constant for hardKeyboardHidden: a value indicating that no value has been set.

        public static final int HARDKEYBOARDHIDDEN_YES = 2
        Constant for hardKeyboardHidden, value corresponding to the physical keyboard being hidden.

        keyboard
        public static final int KEYBOARD_12KEY = 3
        Constant for keyboard, value corresponding to the 12key resource qualifier.

        public static final int KEYBOARD_NOKEYS = 1
        Constant for keyboard, value corresponding to the nokeys resource qualifier.

        public static final int KEYBOARD_QWERTY = 2
        Constant for keyboard, value corresponding to the qwerty resource qualifier.
         */
    }
    public static final String getMD5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString().toUpperCase(Locale.US); // return md5

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
