package ir.saeedrahimi.poolnegar;

import android.content.Context;
import android.content.SharedPreferences;

import ir.saeedrahimi.poolnegar.customs.Utils;

public class PreferencesManager {
 
    Context mContext;
 
    public SharedPreferences settings;
    SharedPreferences.Editor editor;
 
    public PreferencesManager(Context mContext) {
 
        settings = mContext.getSharedPreferences("saeedr_shared_prefs", 0);
        editor = settings.edit();
    }

    public String getSavedPin() {
        return settings.getString("savedPin","");
    }
    public void setSavedPin(String pin) {
        editor.putString("savedPin", Utils.getMD5(pin));
        editor.commit();
    }
    // —– HideZeroBills —–
    public boolean getHideZeroBills() {
        return settings.getBoolean("hidezerobills", true);
    }
 
    public void setHideZeroBills(boolean value) {
        editor.putBoolean("hidezerobills", value);
        editor.commit();
    }
    // —– HideClosedProjects —–
    public boolean getHideClosedProjects() {
        return settings.getBoolean("hideclosedprojects", true);
    }
 
    public void setHideClosedProjects(boolean value) {
        editor.putBoolean("hideclosedprojects", value);
        editor.commit();
    }
    // —– Activation Code —–
    public String getActivationCode() {
        return settings.getString("actcd", "");
    }

    public void setActivationCode(String value) {
        editor.putString("actcd", value);
        editor.commit();
    }
    // —–  Code —–
    public String getPinCode() {
        return settings.getString("pincode", "");
    }

    public void setPinCode(String value) {
        editor.putString("pincode", value);
        editor.commit();
    }

}