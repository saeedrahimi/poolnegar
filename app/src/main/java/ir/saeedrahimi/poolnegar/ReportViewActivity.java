package ir.saeedrahimi.poolnegar;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ViewSwitcher;

import ir.saeedrahimi.poolnegar.databinding.ActivityReportViewBinding;

public class ReportViewActivity extends BasePremiumActivity<ActivityReportViewBinding> implements View.OnClickListener {
    ViewSwitcher mViewSwitcher;



    @Override
    protected void onActivityFirstInit(Bundle savedInstanceState) {
     initComponents();
    }

    private void initComponents() {

        findViewById(R.id.btn_list).setOnClickListener(this);
        findViewById(R.id.btn_rep).setOnClickListener(this);
        mViewSwitcher = (ViewSwitcher) findViewById(R.id.view_switcher);
        String html = getIntent().getStringExtra("html");
        Log.d("tag",html);
        WebView webView = (WebView) findViewById(R.id.web_view);
        webView.loadDataWithBaseURL("file:///android_asset/",html, "text/html; charset=utf-8", "UTF-8",null);
    }

    @Override
    protected ActivityReportViewBinding getViewBinding() {
        return ActivityReportViewBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void onActionBarRightToolClicked() {

    }

    @Override
    protected void onActionBarLeftToolClicked() {

    }

    @Override
    protected String getActionBarTitle() {
        return "گزارشات";
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_list:
                mViewSwitcher.showNext();
                break;
            case R.id.btn_rep:
                mViewSwitcher.showPrevious();
                break;
            default:

        }
    }

}
