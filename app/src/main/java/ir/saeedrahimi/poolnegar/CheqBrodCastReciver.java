package ir.saeedrahimi.poolnegar;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
public class CheqBrodCastReciver
  extends BroadcastReceiver
{
  private Context context;
  
  public void a(int paramInt, String paramString)
  {
    if (paramInt != -2)
    {
    	Intent cheqIntent = new Intent(this.context, CheqService.class);
    	cheqIntent.putExtra("cheqUniqueId", paramInt);
    	cheqIntent.putExtra("cheqDate", paramString);
    	PendingIntent pendIndent = PendingIntent.getService(this.context, paramInt, cheqIntent, Intent.FILL_IN_DATA);
    	Notification  noty = new Notification.Builder(this.context)
    		.setContentText("یاداوری چک")
    		.setContentText("چک دارید")
    		.setContentIntent(pendIndent)
    		.setAutoCancel(true)
    		.setSmallIcon(R.drawable.ic_launcher).build();
      ((NotificationManager)this.context.getSystemService(Context.NOTIFICATION_SERVICE)).notify(paramInt, noty);
      return;
    }
    Log.d("Cheq Brodcast reciver", "Brodcast reciver work , but there is no extra");
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    int i = -2;
    this.context = paramContext;
    String str = "";
    if (paramIntent.getExtras() != null)
    {
      str = paramIntent.getStringExtra("cheqDate");
      i = paramIntent.getIntExtra("cheqUniqueId", i);
      Log.d("broadCast cheqUniqueID:", i+"");
      Log.d("broadCast cheqDate is ", str);
    }
    a(i, str);
  }
}
