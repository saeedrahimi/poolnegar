package ir.saeedrahimi.poolnegar.Sms;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.SmsMessage;
import android.util.Log;

import ir.saeedrahimi.poolnegar.models.SmsObject;
import ir.saeedrahimi.poolnegar.notification.NotificationFactory;

public class SmsBroadcastReciever {
    private String getFullMessage(SmsMessage[] messages) {
        StringBuilder fullMessage = new StringBuilder();
        int i = messages.length;
        for (byte b = 0; b < i; b++)
            fullMessage.append(messages[b].getMessageBody());
        return fullMessage.toString();
    }
    public void onReceive(Context context, Intent intent) {
        try {
            if (intent.getAction().equalsIgnoreCase("android.provider.Telephony.SMS_RECEIVED")) {
                SharedPreferences sharedPreferences = context.getSharedPreferences("artemPreference", 0);
                boolean readSmsEnabled = sharedPreferences.getBoolean("smsreadtransaction", false);
                boolean smsNotificationEnabled = sharedPreferences.getBoolean("smssendnotification", true);
                if (readSmsEnabled ) {
                    Object[] pdus = (Object[]) intent.getExtras().get("pdus");
                    SmsMessage[] messages = new SmsMessage[pdus.length];
                    for (int i = 0; i < pdus.length; i++)
                        messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    String fullMessage = getFullMessage(messages);
                    String originatingAddress = messages[0].getOriginatingAddress();
                    PatternProvider patternProvider = new PatternProvider();

                    SmsObject smsObject = TextTransactionHandler.createSMSTransactionHandler(fullMessage, originatingAddress, patternProvider).process(context);
                    if (smsNotificationEnabled && smsObject != null) {
                        NotificationFactory notificationFactory = new NotificationFactory(context);

                        notificationFactory.sendSMSNotification(context, smsObject);
                    }
                }
            }
        } catch (Exception exception) {
            Log.d(exception.getStackTrace().toString(), getClass().getName());
        }
    }
}