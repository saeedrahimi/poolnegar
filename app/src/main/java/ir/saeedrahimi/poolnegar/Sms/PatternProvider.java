package ir.saeedrahimi.poolnegar.Sms;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.List;

import ir.saeedrahimi.poolnegar.Sms.entities.SmsBank;

public class PatternProvider implements IPatternProvider{
    private static String PatternText() {
        try {
            File file = new File("/data/data/ir.saeedrahimi.poolnegar/bank.json");
            FileInputStream fileInputStream = new FileInputStream(file);
            try {
                FileChannel fileChannel = fileInputStream.getChannel();
                MappedByteBuffer mappedByteBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0L, fileChannel.size());
                return Charset.defaultCharset().decode(mappedByteBuffer).toString();
            } finally {
                fileInputStream.close();
            }
        } catch (IOException iOException) {
            iOException.printStackTrace();
            return null;
        }
    }

    public List<SmsBank> getAll() {
        String pattern = PatternText();
        Type type = (new TypeToken<List<SmsBank>>() {

        }).getType();
        return (List<SmsBank>) (new Gson()).fromJson(pattern, type);
    }

    public SmsBank getSMSBankByNumber(String number) {
        String str = number.replace("+98", "").trim();
        for (SmsBank smsBank : getAll()) {
            String[] arrayOfString = smsBank.getNumbers();
            int i = arrayOfString.length;
            for (byte b = 0; b < i; b++) {
                String str1 = arrayOfString[b].replace("+98", "").trim();
                if (str.length() > str1.length()) {
                    number = str.substring(str.length() - str1.length());
                } else {
                    number = str;
                }
                if (str1.equals(number))
                    return smsBank;
            }
        }
        return null;
    }
}
