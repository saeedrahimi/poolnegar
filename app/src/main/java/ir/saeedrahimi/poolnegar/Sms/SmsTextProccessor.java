package ir.saeedrahimi.poolnegar.Sms;

import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ir.saeedrahimi.poolnegar.Sms.entities.SmsBank;
import ir.saeedrahimi.poolnegar.Sms.entities.TransactionType;
import ir.saeedrahimi.poolnegar.customs.DateTimeData;
import ir.saeedrahimi.poolnegar.customs.utils.DateTimeParser;
import ir.saeedrahimi.poolnegar.customs.utils.FormatAcountNumber;
import ir.saeedrahimi.poolnegar.models.SmsObject;

public class SmsTextProccessor implements ITextProccessor<SmsObject> {
    private SmsBank _bank;

    private String _message;

    private String accountNumberModel = "[[accountNumber]]";

    private String cashModel = "[[amount]]";

    public String currency;

    private String dateTimeModel = "[[date]]";

    private String timeModel = "[[time]]";

    private String transactionTypeModel = "[[tType]]";

    private String variableModel = "[[var]]";

    public SmsTextProccessor(String message, SmsBank smsBank) {
        this._message = message;
        this._bank = smsBank;
    }

    private String ReplacePatterns(TransactionType transactionType) { return transactionType.getTemplate().replace(this.accountNumberModel, transactionType.getAccountNumber().getRegex()).replace(this.cashModel, transactionType.getAmount().getRegex()).replace(this.transactionTypeModel, transactionType.getTypeName()).replace(this.dateTimeModel, transactionType.getDate().getRegex()).replace(this.timeModel, transactionType.getTime().getRegex()).replace(this.variableModel, "[\\s\\S]"); }

    private int checkAccountNumber(TransactionType paramTransactionType, SmsObject paramSMSObject, int paramInt) {
        String str = retrivePatternFromMessage(paramTransactionType.getAccountNumber().getRegex());
        int i = paramInt;
        if (str != null) {
            paramSMSObject.setAccountNumber(FormatAcountNumber.formatAccountNumber(str));
            i = paramInt + 1;
        }
        return i;
    }


    private int checkAmount(TransactionType transactionType, SmsObject smsObject, int paramInt) {
        String str = retrivePatternFromMessage(transactionType.getAmount().getRegex());
        int i = paramInt;
        if (str != null) {
            str = replaceEnglishNumber(str);
            if (this.currency.equals("ریال")) {
                smsObject.setAmount(str);
            } else {
                str = str.replace(",", "");
                smsObject.setAmount(str.substring(0, str.length() - 1));
            }
            i = paramInt + 1;
        }
        return i;
    }

    private int checkDate(TransactionType transactionType, SmsObject smsObject, int paramInt) {
        String str = retrivePatternFromMessage(transactionType.getDate().getRegex());
        int i = paramInt;
        if (str != null) {
            smsObject.setDate(DateTimeData.parse(str).toString());
            i = paramInt + 1;
        }
        return i;
    }

    private boolean checkMatchWithMessage(String paramString) { return Pattern.compile(paramString.replace("\\\\", "\\")).matcher(this._message).matches(); }

    private int checkTime(TransactionType transactionType, SmsObject smsObject, int paramInt) {
        String str = retrivePatternFromMessage(transactionType.getTime().getRegex());
        int i = paramInt;
        if (str != null) {
            smsObject.setTime((new DateTimeParser()).formatTime(str));
            i = paramInt + 1;
        }
        return i;
    }


    private int checkType(TransactionType paramTransactionType, String paramString, int paramInt) {
        int i;
        if (paramTransactionType.getTypeIdentity() != null) {
            i = paramInt;
            if (retrivePatternFromMessage(paramTransactionType.getTypeIdentity()) != null)
                i = paramInt + 1;
        } else {
            if (!paramTransactionType.getTypeName().equals("") || (!this._message.contains("") && !this._message.contains("+"))) {
                int j = paramInt;
                if (paramTransactionType.getTypeName().equals("")) {
                if (!this._message.contains("")) {
                        j = paramInt;
                if (this._message.contains("-"))
                    j = paramInt + 1;
                return j;
            }
        } else {
            return j;
        }
    }
    i = paramInt + 1;
}
    return i;
            }



    private SmsObject createSMSObjectByPattern(TransactionType transactionType, String bankName) {
        SmsObject smsObject = new SmsObject();
        int i = checkType(transactionType, bankName, checkTime(transactionType, smsObject, checkAmount(transactionType, smsObject, checkAccountNumber(transactionType, smsObject, checkDate(transactionType, smsObject, 0)))));
        smsObject.setType(transactionType.getType());
        smsObject.setContent(this._message);
        smsObject.setBankName(bankName);
        smsObject.setID(i);
        smsObject.setDescription("");
        return smsObject;
    }

private String replaceEnglishNumber(String paramString) { return paramString.replace("۰", "0").replace("۱", "1").replace("۲", "2").replace("۳", "3").replace("۴", "4").replace("۵", "5").replace("۶", "6").replace("۷", "7").replace("۸", "8").replace("۹", "9").replace("۰", "0").replace("۱", "1").replace("۲", "2").replace("۳", "3").replace("۴", "4").replace("۵", "5").replace("۶", "6").replace("۷", "7").replace("۸", "8").replace("۹", "9"); }

    public SmsObject compareWithPatterns() {
        SmsBank sMSBank = this._bank;
        if (sMSBank != null) {
            Iterator<TransactionType> iterator = sMSBank.getTransactionTypes().iterator();
            while (iterator.hasNext()) {
                SmsObject smsObject = createSMSObjectByPattern(iterator.next(), this._bank.getName());
                if (smsObject.getID() > 4)
                    return smsObject;
                if (this._bank.getName().equals("") && smsObject.getID() >= 4)
                    return smsObject;
            }
        }
        return null;
    }

    public String retrivePatternFromMessage(String paramString) {
        Matcher matcher = Pattern.compile(paramString.replace("\\\\", "\\")).matcher(this._message);
        return matcher.find() ? matcher.group(0) : null;
    }

        public void setCurrency(String paramString) {
            this.currency = paramString;
        }
}
