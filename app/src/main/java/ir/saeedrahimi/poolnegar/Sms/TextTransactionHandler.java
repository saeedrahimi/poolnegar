package ir.saeedrahimi.poolnegar.Sms;


import android.content.Context;

import ir.saeedrahimi.poolnegar.Gateways.SmsGateway;
import ir.saeedrahimi.poolnegar.customs.localize.CurrencyTypes;
import ir.saeedrahimi.poolnegar.customs.localize.Localize;
import ir.saeedrahimi.poolnegar.models.SmsObject;

public class TextTransactionHandler {
    private String _fullText;

    private String _number;

    private IPatternProvider _patternProvider;

    private ITextProccessor<SmsObject> _textProccessor;

    private TextTransactionHandler(String messageBody, String number, ITextProccessor<SmsObject> textProccessor, IPatternProvider patternProvider) {
        this._fullText = messageBody;
        this._number = number;
        this._textProccessor = textProccessor;
        this._patternProvider = patternProvider;
    }

    public static TextTransactionHandler createSMSTransactionHandler(String messageBody, String number, IPatternProvider patternProvider) {
        return new TextTransactionHandler(messageBody, number, new SmsTextProccessor(messageBody, patternProvider.getSMSBankByNumber(number)), patternProvider);
    }

    public SmsObject process(Context paramContext) {
        String str;
        if (Localize.getCurrency() == CurrencyTypes.Toman) {
            str = "تومان";
        } else {
            str = "ریال";
        }
        this._textProccessor.setCurrency(str);
        SmsObject sMSObject = this._textProccessor.compareWithPatterns();
        return (new SmsGateway()).insert(sMSObject);
    }
}
