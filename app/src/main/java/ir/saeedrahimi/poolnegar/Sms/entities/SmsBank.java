package ir.saeedrahimi.poolnegar.Sms.entities;


import java.util.ArrayList;
import java.util.List;

public class SmsBank {
    private String Name;

    private String[] Numbers;

    private List<TransactionType> TransactionTypes = new ArrayList<TransactionType>();

    public String getName() { return this.Name; }

    public String[] getNumbers() { return this.Numbers; }

    public List<TransactionType> getTransactionTypes() { return this.TransactionTypes; }

    public void setName(String paramString) { this.Name = paramString; }

    public void setNumbers(String[] paramArrayOfString) { this.Numbers = paramArrayOfString; }

    public void setTransactionTypes(List<TransactionType> paramList) { this.TransactionTypes = paramList; }
}
