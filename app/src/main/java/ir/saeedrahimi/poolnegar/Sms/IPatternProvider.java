package ir.saeedrahimi.poolnegar.Sms;


import java.util.List;

import ir.saeedrahimi.poolnegar.Sms.entities.SmsBank;

public interface IPatternProvider {
    List<SmsBank> getAll();

    SmsBank getSMSBankByNumber(String paramString);
}