package ir.saeedrahimi.poolnegar.Sms.entities;

public class Element {
    private int AfterChar;

    private int BeforeChar;

    private String Format;

    private String Regex;

    public Element(String paramString1, String paramString2) {
        this.Regex = paramString2;
        this.Format = paramString1;
        this.AfterChar = -1;
        this.BeforeChar = -1;
    }

    public int getAfterChar() { return this.AfterChar; }

    public int getBeforeChar() { return this.BeforeChar; }

    public String getFormat() { return this.Format; }

    public String getRegex() { return this.Regex; }

    public void setAfterChar(int paramInt) { this.AfterChar = paramInt; }

    public void setBeforeChar(int paramInt) { this.BeforeChar = paramInt; }

    public void setFormat(String paramString) { this.Format = paramString; }

    public void setRegex(String paramString) { this.Regex = paramString; }
}

