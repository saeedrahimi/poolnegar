package ir.saeedrahimi.poolnegar.Sms.entities;

public class TransactionType {
    private Element AccountNumber;

    private Element Amount;

    private Element Date;

    private Element Description;

    private String Template;

    private Element Time;

    private int Type;

    private String TypeIdentity;

    private String TypeName;

    public Element getAccountNumber() { return this.AccountNumber; }

    public Element getAmount() { return this.Amount; }

    public Element getDate() { return this.Date; }

    public Element getDescription() { return this.Description; }

    public String getTemplate() { return this.Template; }

    public Element getTime() { return this.Time; }

    public int getType() { return this.Type; }

    public String getTypeIdentity() { return this.TypeIdentity; }

    public String getTypeName() { return this.TypeName; }

    public void setAccountNumber(Element paramElement) { this.AccountNumber = paramElement; }

    public void setAmount(Element paramElement) { this.Amount = paramElement; }

    public void setDate(Element paramElement) { this.Date = paramElement; }

    public void setDescription(Element paramElement) { this.Description = paramElement; }

    public void setTemplate(String paramString) { this.Template = paramString; }

    public void setTime(Element paramElement) { this.Time = paramElement; }

    public void setType(int paramInt) { this.Type = paramInt; }

    public void setTypeIdentity(String paramString) { this.TypeIdentity = paramString; }

    public void setTypeName(String paramString) { this.TypeName = paramString; }
}