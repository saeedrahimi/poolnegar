//package ir.saeedrahimi.poolnegar.dialogs;
//
//import android.app.Dialog;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.RadioButton;
//import android.widget.TextView;
//
//import ir.saeedrahimi.poolnegar.R;
//import ir.saeedrahimi.poolnegar.adapters.AccountBankSelectorAdapter;
//import ir.saeedrahimi.poolnegar.adapters.AccountSelectorAdapter;
//import ir.saeedrahimi.poolnegar.adapters.ProjectSelectorAdapter;
//import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
//import ir.saeedrahimi.poolnegar.database.model.Account;
//
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.LinkedHashMap;
//import java.util.List;
//
//public class DialogAccountSelector extends DialogBaseAccountSelector {
//
//    public RadioButton lastSelectRadioBtn;
//
//
//    private List<Account> projects;
//    private ProjectSelectorAdapter mProjectAdapter;
//
//
//    public void dialogBack(View view) {
//        reload();
//    }
//
//    public void submit(View view) {
//        doAccount();
//    }
//
//    public void reload() {
//        setContentView(R.layout.accountselector_dialog);
//        this.accDialog = new Dialog(this);
//        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
//        int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.90);
//        this.accDialog.getWindow().setLayout(width, height);
//        if(getIntent().getBooleanExtra("hasRadio",false))
//            this.hasRadio=true;
//        if (getIntent().getStringExtra("accType") != null) {
//            if (getIntent().getStringExtra("accType").equals("acc"))
//                accSelector();
//            else if (getIntent().getStringExtra("accType").equals("payer"))
//                selectTargetAccount();
//            else if (getIntent().getStringExtra("accType").equals("reciver"))
//                selectSourceAccount();
//            else if (getIntent().getStringExtra("accType").equals("chBank"))
//                cheqBankSelector();
//            else if (getIntent().getStringExtra("accType").equals("project"))
//                selectProjectAccount();
//        }
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
//        int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.90);
//        this.getWindow().setLayout(width, height);
//        super.onCreate(savedInstanceState);
//        requestWindowFeature(1);
//        reload();
//    }
//
//    public void doAccount() {
//        this.accDialog.dismiss();
//        Intent backIntent = new Intent();
//
//        if (this.selectedAccIds[2] == -1)
//            this.path = this.selectedAccTitles[0] + " - " + this.selectedAccTitles[1];
//        else
//            this.path = this.selectedAccTitles[0] + " - "
//                    + this.selectedAccTitles[1] + " - "
//                    + this.selectedAccTitles[2];
//
//        backIntent.putExtra("selectedTitles", selectedAccTitles);
//        backIntent.putExtra("selectedIds", selectedAccIds);
//        backIntent.putExtra("title", path);
//        setResult(RESULT_OK, backIntent);
//        finish();
//    }
//
//
//    @SuppressWarnings("rawtypes")
//    private void fillList(int parentId) {
//        DatabaseHelper _db = new DatabaseHelper(this);
//        this.tempList = new ArrayList();
//        this.tempList = _db.getSubAccountsList(parentId, true);
//    }
//
//    @SuppressWarnings({"unchecked", "rawtypes"})
//    public void FillSubAccounts(int parentId) {
//        this.accHash = new LinkedHashMap<String, List<String>>();
//        DatabaseHelper _db = new DatabaseHelper(this);
//        this.accList = new ArrayList<String>();
//        if (selectorType == 99) {
//            this.accounts = AccountDAO.getReceivableRootAccounts();
//            this.accHash =  _db.getSubAccounts(parentId, 2, true);
//
//        } else if (selectorType == 0) {
//            this.accHash = _db.getSubAccounts(parentId, 0, true);
//        } else if (selectorType == 1) {
//            this.accHash = _db.getSubAccounts(parentId, 1, true);
//        } else if (selectorType == 2) {
//            this.accHash = _db.getSubAccounts(parentId, 2, true);
//        }
//        Iterator localIterator = this.accHash.keySet().iterator();
//        if (accHash.keySet().size() > 0) {
//            do {
//                String str = (String) localIterator.next();
//                //Log.d("subAccount names ", str);
//                this.accList.add(str);
//            } while (localIterator.hasNext());
//        }
//    }
//
//    public void accSelector() {
//        List<String[]> payList = this._db.Accounts(3);
//        this.accTitles = payList.get(0);
//        this.accIds = payList.get(1);
//        this.selectorType = 2;
//        this.accListView = ((ListView) findViewById(R.id.accountselector_firstlistView));
//        this.accListView.setAdapter(new AccountSelectorAdapter(this,
//                17367043, this.accTitles,this.hasRadio));
//    }
//
//    public void selectTargetAccount() {
//        List<String[]> payList = this._db.Accounts(2);
//        this.accTitles = payList.get(0);
//        this.accIds = payList.get(1);
//        this.selectorType = 99;
//        this.accListView = ((ListView) findViewById(R.id.accountselector_firstlistView));
//        this.accListView.setAdapter(new AccountSelectorAdapter(this,
//                17367043, this.accTitles, this.hasRadio));
//    }
//
//    public void selectSourceAccount() {
//        this.accounts = AccountDAO.getReceivableRootAccounts();
//        this.selectorType = 0;
//        this.accListView = ((ListView) findViewById(R.id.accountselector_firstlistView));
//        this.accListView.setAdapter(new AccountSelectorAdapter(this,
//                17367043, accTitles, this.hasRadio));
//    }
//    public void doBank(Account item) {
//        this.accDialog.dismiss();
//        Intent backIntent = new Intent();
//        backIntent.putExtra("bankId", item.getAccID());
//        backIntent.putExtra("bankName", item.getTitle());
//        setResult(RESULT_OK, backIntent);
//        finish();
//    }
//
//    public void doProject(String accId, String title) {
//        this.accDialog.dismiss();
//        Intent backIntent = new Intent();
//        backIntent.putExtra("projectId", accId);
//        backIntent.putExtra("projectTitle", title);
//        setResult(RESULT_OK, backIntent);
//        finish();
//    }
//    public void selectProjectAccount() {
//        this.projects = this._db.Projects(true);
//        if (this.projects.size() != 0) {
//            this.accDialog = new Dialog(this);
//            this.accDialog.getWindow().setBackgroundDrawable(
//                    new ColorDrawable(Color.TRANSPARENT));
//            this.accDialog.setContentView(R.layout.dialog_projectselector);
//            this.mProjectAdapter = new ProjectSelectorAdapter(this, this,android.R.layout.simple_list_item_1, this.projects);
//            ((ListView) this.accDialog.findViewById(R.id.projectselector_List))
//                    .setAdapter(mProjectAdapter);
//            this.accDialog.show();
//            ImageView searchImg = (ImageView) accDialog.findViewById(R.id.imageView5);
//            searchImg.setImageResource(R.mipmap.ic_search_light);
//            EditText txtFilter = (EditText) accDialog.findViewById(R.id.txt_search);
//            txtFilter.addTextChangedListener(new TextWatcher() {
//
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    mProjectAdapter.getFilter().filter(s.toString());
//                }
//
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count,
//                                              int after) {
//
//                }
//
//                @Override
//                public void afterTextChanged(Editable s) {
//                }
//            });
//
//
//            accDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//                @Override
//                public void onCancel(DialogInterface dialogInterface) {
//                    setResult(RESULT_CANCELED);
//                    finish();
//                    accDialog.dismiss();
//                }
//            });
//            return;
//        }
//        final Dialog dialog = new Dialog(this);
//        dialog.requestWindowFeature(1);
//        dialog.setContentView(R.layout.dialog_warning);
//        TextView txtTitle = (TextView) dialog
//                .findViewById(R.id.yesnotitle);
//        TextView txtDesc = (TextView) dialog
//                .findViewById(R.id.yesnomessage);
//        Button btnOk = (Button) dialog.findViewById(R.id.dialog_yes);
//        Button btnCancel = (Button) dialog
//                .findViewById(R.id.dialog_no);
//        txtTitle.setText("");
//        txtDesc.setText("در حال حاضر هیچ پروژه ای تعریف نشده ، ایا مایل به ایجاد پروژه جدید میباشید ؟");
//        btnOk.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//                dialog.dismiss();
//            }
//        });
//        btnCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
//    }
//    public void cheqBankSelector() {
//        fillList(4);
//        int i1 = this.tempList.size();
//        if (i1 != 0) {
//            this.accDialog = new Dialog(this);
//            this.accDialog.requestWindowFeature(1);
//            String[] arrayOfString = new String[i1];
//            this.accDialog.setContentView(R.layout.accselector_bank_selection);
//            AccountBankSelectorAdapter localbs = new AccountBankSelectorAdapter(
//                    this, this, 17367043, arrayOfString);
//            this.accListView = ((ListView) this.accDialog
//                    .findViewById(R.id.accselector_bank_listview));
//            this.accListView.setAdapter(localbs);
//            this.accDialog.show();
//            return;
//        }
//        final Dialog dialog = new Dialog(this);
//        dialog.requestWindowFeature(1);
//        dialog.setContentView(R.layout.dialog_warning);
//        TextView localTextView1 = (TextView) dialog
//                .findViewById(R.id.yesnotitle);
//        TextView localTextView2 = (TextView) dialog
//                .findViewById(R.id.yesnomessage);
//        Button btnYes = (Button) dialog.findViewById(R.id.dialog_yes);
//        Button btnNo = (Button) dialog
//                .findViewById(R.id.dialog_no);
//        localTextView1.setText("پیام");
//        localTextView2
//                .setText("در حال حاضر هیچ بانکی تعریف نشده ، ایا مایل به ایجاد بانک جدید می باشید ؟");
//        btnNo.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                dialog.dismiss();
//
//            }
//        });
//        btnYes.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                dialog.dismiss();
//
//            }
//        });
//        dialog.show();
//    }
//}
