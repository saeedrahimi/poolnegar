package ir.saeedrahimi.poolnegar.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.customs.ArtemButton;

public class MessageDialog
{
  private final OnDialogClickListener mListener;
  
  public MessageDialog(Context context, OnDialogClickListener callback, String headText, String messageText1, String messageText2, final String token, boolean cancelable)
  {
    this.mListener = callback;
    final Dialog dlg = new Dialog(context);
    dlg.requestWindowFeature(1);
    dlg.setContentView(R.layout.dialog_message_old);
    dlg.setCancelable(cancelable);
    TextView txtMessage1 = (TextView)dlg.findViewById(R.id.messagetext1);
    TextView txtMessage2 = (TextView)dlg.findViewById(R.id.messagetext2);
    ArtemButton btnOK = (ArtemButton)dlg.findViewById(R.id.messageok);
    ((TextView)dlg.findViewById(R.id.headermessage1)).setText(headText);
    txtMessage1.setText(messageText1);
    txtMessage2.setText(messageText2);
    btnOK.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View v)
      {
        dlg.dismiss();
        if (MessageDialog.this.mListener != null) {
          MessageDialog.this.mListener.onDialogOKClick(token);
        }
      }
    });
    dlg.show();
  }
  
  public static abstract interface OnDialogClickListener
  {
    public abstract void onDialogOKClick(String paramString);
  }
}