package ir.saeedrahimi.poolnegar.dialogs;


import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ir.saeedrahimi.poolnegar.R;

public class AlertDialog
{
  public static String Show(String message)
  {
    if (message == null) {
    	message = "false";
    }
    while ((10 < message.length()) && (message.length() < 16)) {
      return message;
    }
    return "false";
  }
  
  public static void Show(Context paramContext, String erTitle, String erBody)
  {
    Dialog localDialog = new Dialog(paramContext);
    localDialog.requestWindowFeature(1);
    localDialog.setContentView(R.layout.dialog_error);
    TextView error_title = (TextView)localDialog.findViewById(R.id.error_dialog_title);
    TextView error_body = (TextView)localDialog.findViewById(R.id.error_dialog_textbody);
    Button error_btn = (Button)localDialog.findViewById(R.id.error_dialog_ok_btn);
    error_title.setText(erTitle);
    error_body.setText(erBody);
    error_btn.setText("تایید");
    error_btn.setOnClickListener(new closeClickHandler(localDialog));
    localDialog.show();
  }

  static class closeClickHandler implements View.OnClickListener
{
	  private Dialog dlg;
	  public closeClickHandler(Dialog paramDialog) {
		  this.dlg=paramDialog;
	  }
	  @Override
	  public void onClick(View paramView)
	  {
	    this.dlg.dismiss();
	  }
}
}
