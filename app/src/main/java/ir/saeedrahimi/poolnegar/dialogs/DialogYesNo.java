package ir.saeedrahimi.poolnegar.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ir.saeedrahimi.poolnegar.R;

public class DialogYesNo
{
  private onNoDialogClick NoListener;
  private final onYesNoDialogClick mListener;
  
  public DialogYesNo(Context paramContext, onYesNoDialogClick callbackYes, onNoDialogClick callbackNo, String headtext, String message1, String mesage2, boolean showCheckBox, String checkText, boolean cancelabe, final String token)
  {
    this.mListener = callbackYes;
    this.NoListener = callbackNo;
    final Dialog dlg = new Dialog(paramContext);
    dlg.requestWindowFeature(1);
    dlg.setContentView(R.layout.dialog_yesno);
    if (!cancelabe) {
      dlg.setCancelable(false);
    }
    TextView txtHead = (TextView)dlg.findViewById(R.id.yesnotitle);
    TextView txtMessageTitle = (TextView)dlg.findViewById(R.id.yesnomessagetitle);
    TextView txtMessage = (TextView)dlg.findViewById(R.id.yesnomessage);
    Button btnYes = (Button)dlg.findViewById(R.id.dialog_yes);
    Button btnNo = (Button)dlg.findViewById(R.id.dialog_no);

    txtMessageTitle.setText(message1);
    if (!mesage2.equals("")) {
      txtMessage.setText(mesage2);
    }
    txtHead.setText(headtext);
    btnYes.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View v)
      {
        dlg.dismiss();
        if (DialogYesNo.this.mListener != null)
        {
          //boolean chkVal = checkPormpt.isChecked();
          boolean checked = false;
          //if (chkVal) {
          //  checked = true;
          //}
          Log.d("Checked", checked+"");
          DialogYesNo.this.mListener.onYesNoDialog(token, checked);
        }
      }
    });
    btnNo.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        dlg.dismiss();
        if (DialogYesNo.this.NoListener != null) {
          DialogYesNo.this.NoListener.onNoDialog(token);
        }
      }
    });
    dlg.show();
  }
  
  public static abstract interface onNoDialogClick
  {
    public abstract void onNoDialog(String token);
  }
  
  public static abstract interface onYesNoDialogClick
  {
    public abstract void onYesNoDialog(String token, boolean checked);
  }
}
