package ir.saeedrahimi.poolnegar.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;

import androidx.fragment.app.FragmentActivity;
import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar;
import ir.saeedrahimi.poolnegar.customs.TypeFaceProvider;
import ir.saeedrahimi.poolnegar.customs.Utils;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.database.DAO.PersonDAO;
import ir.saeedrahimi.poolnegar.database.DAO.TransactionDAO;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_AccountSelection;

public class DialogPayBill extends FragmentActivity implements OnClickListener, DatePickerDialog.OnDateSetListener {

    private static final int RESULT_DATE = 0;
    private static final int RESULT_SELECT_SOURCE = 2;
    public Button btnRecFrom;
    EditText txtInfo;
    private String accId;
    private PersianCalendar mCalendar;
    private Button btnDate;
    private EditText edtAmount;
    private String targetAccount;
    private String selectedSourceAccount;
    private String selectedSourceAccountTitle;

    public DialogPayBill() {
        super();
    }

    public DialogPayBill(String accId, String targetAccount) {
        super();
        this.accId = accId;
        this.targetAccount = targetAccount;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.dialog_paybill);
        getWindow().getDecorView()
                .setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        this.btnDate = ((Button) findViewById(R.id.datePayBill));
        this.btnRecFrom = ((Button) findViewById(R.id.btnSourceAccount));
        this.edtAmount = ((EditText) findViewById(R.id.amountPayBill));
        this.txtInfo = ((EditText) findViewById(R.id.descriptionPayBill));
        if (getIntent().getStringExtra("targetAccount") != null)
            this.targetAccount = getIntent().getStringExtra("targetAccount");
        this.accId = getIntent().getStringExtra("accId");
        ((Button) findViewById(R.id.submit_paybill)).setOnClickListener(this);
        ((Button) findViewById(R.id.cancel_paybill)).setOnClickListener(this);
        this.mCalendar = new PersianCalendar();
        this.btnDate.setText(mCalendar.getPersianShortDate());
        setTitle("پرداخت بدهی");
        this.edtAmount.setKeyListener(DigitsKeyListener
                .getInstance("1234567890,."));

        this.edtAmount.addTextChangedListener(Utils.MoneyTextWatcher);
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.90);
        this.getWindow().setLayout(width, height);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.submit_paybill:
                submitData();
                break;
            case R.id.cancel_paybill:
                Intent backIntent = new Intent();
                backIntent.putExtra("payed", false);
                setResult(RESULT_CANCELED, backIntent);
                finish();
                break;
            default:

        }

    }

    public void selectSourceAccount() {
        Intent dlg = new Intent(this, Activity_AccountSelection.class);
        dlg.putExtra("ShowPayable", true);
        dlg.putExtra("ShowReceivable", false);
        dlg.putExtra("Title", "برداشت از حساب");
        this.startActivityForResult(dlg, RESULT_SELECT_SOURCE);
    }

    public void timePickerDilog(View paramView) {
        PersianCalendar persianCalendar = new PersianCalendar();
        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                DialogPayBill.this,
                persianCalendar.getPersianYear(),
                persianCalendar.getPersianMonth(),
                persianCalendar.getPersianDay(),
                TypeFaceProvider.get(this, "vazir.ttf")
        );
        datePickerDialog.setThemeDark(true);
        datePickerDialog.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_DATE) {
            if (resultCode == RESULT_OK) {
                int year = data.getIntExtra("year", 1393);
                int month = data.getIntExtra("month", 6);
                int day = data.getIntExtra("day", 1);
                mCalendar.setPersianDate(year, month, day);
                btnDate.setText(mCalendar.getPersianShortDate());
            }
            if (resultCode == RESULT_CANCELED) {

            }
        } else if ((requestCode == RESULT_SELECT_SOURCE)) {
            if (resultCode == RESULT_OK) {
                this.selectedSourceAccount = data.getStringExtra("SelectedAccounts");
                this.selectedSourceAccountTitle = AccountDAO.getTitleById(selectedSourceAccount);
                btnRecFrom.setText(this.selectedSourceAccountTitle);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public void submitData() {
        double cashAmount = 0.0D;
        try {
            cashAmount = Double.parseDouble(this.edtAmount.getText().toString()
                    .replace(",", ""));
        } catch (Exception localException) {
            Snackbar.make(this.getWindow().getDecorView(), "لطفا مبلغ ورودی را دوباره بررسی کنید", BaseTransientBottomBar.LENGTH_LONG).setAction("Action", null).show();

            return;
        }

        if (this.selectedSourceAccount.equals("")) {

            Snackbar.make(this.getWindow().getDecorView(), "لطفا حساب مبدا را مشخص کنید", BaseTransientBottomBar.LENGTH_LONG).setAction("Action", null).show();

            return;
        }
        if (((this.edtAmount.getText().toString().equals(""))
                && (this.edtAmount.getText().toString().equals("0")) || cashAmount == 0)) {
            Snackbar.make(this.getWindow().getDecorView(), "مبلغ وارد نشده است", BaseTransientBottomBar.LENGTH_LONG).setAction("Action", null).show();
            return;
        }

        TransactionItem transaction = new TransactionItem();
        transaction.setSourceAccountId(selectedSourceAccount);
        transaction.setSourceAccountTitle(AccountDAO.getTitleById(selectedSourceAccount));
        transaction.setTargetAccountId(targetAccount);
        transaction.setTargetAccountTitle(AccountDAO.getTitleById(targetAccount));
        transaction.setPersonId(accId);
        transaction.setPersonTitle(PersonDAO.getById(accId).getName());
        transaction.setAmount(cashAmount);
        transaction.setRegDateFa(this.mCalendar.getPersianShortDate());
        transaction.setRegDateGe(this.mCalendar.getTimeInMillis());
        transaction.setNote(this.txtInfo.getText().toString());
        transaction.setMethod(1);
        transaction.setTAG(Constants.DB_TAGS.PayBill);
        TransactionDAO.insert(transaction);

        Intent backIntent = new Intent();
        backIntent.putExtra("amount", cashAmount);
        setResult(RESULT_OK, backIntent);
        finish();

    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = "You picked the following date: "+dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
        Toast.makeText(this, date, Toast.LENGTH_SHORT);
        mCalendar.setPersianDate(year, monthOfYear, dayOfMonth);
        btnDate.setText(mCalendar.getPersianShortDate());
    }
}
