package ir.saeedrahimi.poolnegar.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ViewSwitcher;

import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;
import com.mohamadamin.persianmaterialdatetimepicker.utils.PersianCalendar;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.customs.TypeFaceProvider;

public class DialogFilter implements  OnClickListener, DatePickerDialog.OnDateSetListener {
	private onFilterSetListener mListener;
	private Context mContext;
	private Dialog dlg;
	private RadioGroup rgDate;
	private RadioButton rdExactDate;
	private RadioGroup rgAmount;
	private RadioButton rdExactAmount;
	private Button btnStartDate;
	private Button btnEndDate;
	private Button btnExactDate;
	private int curDay;
	private int curMonth;
	private int curYear;

	private int endDay;
	private int endMonth;
	private int endYear;
	String curDate;
	String endDate;

	public DialogFilter() {
	}

	public DialogFilter(Context context, onFilterSetListener callback, final String curDate)
	{
		this.curYear = Integer.parseInt(curDate.split("/")[0]);
		this.curMonth = Integer.parseInt(curDate.split("/")[1]);
		this.curDay = Integer.parseInt(curDate.split("/")[2]);
		this.endYear = Integer.parseInt(curDate.split("/")[0]);
		this.endMonth = Integer.parseInt(curDate.split("/")[1]);
		this.endDay = Integer.parseInt(curDate.split("/")[2]);
		this.endDate=curDate;
		this.mListener = callback;
		this.mContext = context;
		this.dlg = new Dialog(context);
		//dlg.requestWindowFeature(1);
		dlg.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
		dlg.setContentView(R.layout.dialog_filter);
		
		Button btnOk = (Button)dlg.findViewById(R.id.submit_filter);
		this.btnStartDate = (Button)dlg.findViewById(R.id.startdate);
		this.btnEndDate = (Button)dlg.findViewById(R.id.enddate);
		this.btnExactDate = (Button)dlg.findViewById(R.id.exactdate);
		final ViewSwitcher swDate = (ViewSwitcher) dlg.findViewById(R.id.filter_date_viewswitcher);
		final ViewSwitcher swAmount = (ViewSwitcher) dlg.findViewById(R.id.filter_amount_viewswitcher);
		btnExactDate.setText(curDate);
		btnStartDate.setText(curDate);
		btnEndDate.setText(curDate);
		this.curDate=curDate;
		btnStartDate.setOnClickListener(this);
		btnEndDate.setOnClickListener(this);
		btnExactDate.setOnClickListener(this);
		btnOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dlg.dismiss();
		        if (DialogFilter.this.mListener != null)
		        {
		        	String start_date =	"";
		        	String end_date =	"";
		        	if (rdExactDate.isChecked()) {
						start_date = btnExactDate.getText().toString();
						end_date = btnExactDate.getText().toString();
					}
		        	else{
		        		start_date = btnStartDate.getText().toString();
						end_date = btnEndDate.getText().toString();
		        	}
		          
		          DialogFilter.this.mListener.onFilterSet(start_date,end_date);
		        }	
			}
		});
		this.rgDate = ((RadioGroup) dlg.findViewById(R.id.rgDate));
		this.rdExactDate = ((RadioButton) dlg.findViewById(R.id.rd_exact_data));
		this.rgDate	.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup arg0, int arg1) {
				if (rdExactDate.isChecked()) {
					swDate.showPrevious();
					return;
				}
				swDate.showNext();
			}
		});
		this.rgAmount = ((RadioGroup) dlg.findViewById(R.id.rgAmount));
		this.rdExactAmount = ((RadioButton) dlg.findViewById(R.id.rd_exact_amount));
		this.rgAmount.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup arg0, int arg1) {
				if (rdExactAmount.isChecked()) {
					swAmount.showPrevious();
					return;
				}
				swAmount.showNext();
			}
		});
		int width=(int)(context.getResources().getDisplayMetrics().widthPixels*0.90);
		int height=(int)(context.getResources().getDisplayMetrics().heightPixels*0.90);
		dlg.getWindow().setLayout(width,height);
		dlg.show();
	}

	@Override
	public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
		String token = view.getTag();

		if(token.equals("exactdate") || token.equals("startdate")){
			this.curYear = year;
			this.curMonth = (monthOfYear + 1);
			this.curDay = dayOfMonth;
			StringBuilder sb = new StringBuilder();
			sb.append(curYear).append("/");
			sb.append(String.format("%02d", Integer.valueOf(curMonth)))
					.append("/");
			sb.append(String.format("%02d", Integer.valueOf(curDay)));
			curDate = sb.toString();

			this.btnExactDate.setText(curDate);
			this.btnStartDate.setText(curDate);
			this.btnEndDate.setText(curDate);
		}
		if(token.equals("enddate")){
			this.endYear = year;
			this.endMonth = (monthOfYear + 1);
			this.endDay = dayOfMonth;
			StringBuilder sb = new StringBuilder();
			sb.append(endYear).append("/");
			sb.append(String.format("%02d", Integer.valueOf(endMonth)))
					.append("/");
			sb.append(String.format("%02d", Integer.valueOf(endDay)));
			endDate = sb.toString();
			this.btnEndDate.setText(endDate);
		}
	}

	public static abstract interface onFilterSetListener
	{
		public abstract void onFilterSet(String startdate, String endDate);
	}


	public void timePickerDilog(String tag) {
		PersianCalendar persianCalendar = new PersianCalendar();
		persianCalendar.setPersianDate(curYear, curMonth - 1, curDay);
		DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
				DialogFilter.this,
				persianCalendar.getPersianYear(),
				persianCalendar.getPersianMonth(),
				persianCalendar.getPersianDay(),
				TypeFaceProvider.get(mContext, "vazir.ttf")
		);
		datePickerDialog.setThemeDark(true);
		datePickerDialog.show(((Activity)mContext).getFragmentManager(), tag);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.exactdate:
			timePickerDilog("exactdate");
			break;
		case R.id.startdate:
			timePickerDilog("startdate");
			break;
		case R.id.enddate:
			timePickerDilog("enddate");
			break;
	
		default:
			break;
		}
	}

}
