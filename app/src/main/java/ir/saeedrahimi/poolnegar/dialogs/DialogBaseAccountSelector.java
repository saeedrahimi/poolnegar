//package ir.saeedrahimi.poolnegar.dialogs;
//
//import android.app.Dialog;
//import android.os.Bundle;
//import android.widget.ExpandableListView;
//import android.widget.ListView;
//import android.widget.RadioButton;
//
//import java.util.LinkedHashMap;
//import java.util.List;
//
//import androidx.fragment.app.FragmentActivity;
//import ir.saeedrahimi.poolnegar.Helper.DatabaseHelper;
//import ir.saeedrahimi.poolnegar.adapters.SubAccountSelectorAdapter;
//import ir.saeedrahimi.poolnegar.database.model.Account;
//
//public abstract class DialogBaseAccountSelector extends FragmentActivity {
//
//	public DatabaseHelper _db;
//	public Dialog accDialog;
//	public String[] accTitles;
//	public String[] accIds;
//	public ListView accListView;
//	public int selectorType;
//	public List<Account> tempList;
//	public List<Account> accounts;
//	public List<String> accList;
//	public LinkedHashMap<String, List<String>> accHash;
//	public ExpandableListView accExpand;
//	public SubAccountSelectorAdapter subAdapter;
//
//	public int[] transPayToIds = { -1, -1, -1 };// 0=rootAcc,1=subAcc,2=payAcc
//	public String[] transPayeToTitles = { "", "", "" };
//	public int[] transFromIds = { -1, -1, -1 };// 0=rootAcc,1=subAcc,2=recAcc
//	public String[] transFromTitles = { "", "", "" };
//	public int[] selectedAccIds = { -1, -1, -1 };
//	public Account selectedAccount;
//	public String[] selectedAccTitles = { "", "", "" };
//	public int[] transBankId = { -1, -1, -1 }; // ai
//
//	public String path;
//
//	private List<Account> projects;
//	public RadioButton lastSelectRadioBtn;
//	public boolean hasRadio=false;
//
//    @Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//
//
//	}
//	public abstract void FillSubAccounts(int curId);
//
//	public void doAccount() {
//
//	}
//}
