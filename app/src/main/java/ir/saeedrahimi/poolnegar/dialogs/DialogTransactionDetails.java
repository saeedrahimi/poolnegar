package ir.saeedrahimi.poolnegar.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;

import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar;
import ir.saeedrahimi.poolnegar.customs.GerdButton;
import ir.saeedrahimi.poolnegar.customs.Utils;
import ir.saeedrahimi.poolnegar.database.DAO.TransactionDAO;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;

public class DialogTransactionDetails  implements DialogYesNo.onYesNoDialogClick
{
	private Context c;
	OnTransactionDelete dListener;
	private TransactionItem trans;


	public DialogTransactionDetails(final Context context, Activity parentActivity, OnDialogClickListener callback, OnTransactionDelete callbackDelete, final OnTransactionEdit callbackEdit, final TransactionItem trans)
	{
		this.trans = trans;
		this.c = context;
		this.dListener = callbackDelete;
		final Dialog dlg = new Dialog(context);
		dlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dlg.getWindow().getDecorView().setLayoutDirection(
				View.LAYOUT_DIRECTION_RTL);
		dlg.setContentView(R.layout.dialog_transactiondetail);
		dlg.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
		dlg.setOnShowListener(new DialogInterface.OnShowListener() {
			@Override
			public void onShow(DialogInterface dialogInterface) {

				int width=(int)(context.getResources().getDisplayMetrics().widthPixels*0.90);
				int height= dlg.getWindow().getDecorView().getHeight();
				dlg.getWindow().setLayout(width,height);

			}
		});
		double d = 0.0D;
		long l = 0L;
		TextView txtType = dlg.findViewById(R.id.txtCurrencySign);

		TextView txtAccountFrom = dlg.findViewById(R.id.txtSource);
		TextView txtAccountTo = dlg.findViewById(R.id.txtDestination);
		GerdButton imgFrom = dlg.findViewById(R.id.imgSourceIcon);
		GerdButton imgTo = dlg.findViewById(R.id.imgDestinationIcon);
		txtAccountFrom.setText("از: " + trans.getSourceAccountTitle());
		txtAccountTo.setText("به : " + this.trans.getTargetAccountTitle());
		imgFrom.setCr_icon(Utils.GetAccIcon(trans.getSourceAccountId()));
		imgFrom.refreshDrawableState();
		imgTo.setCr_icon(Utils.GetAccIcon(trans.getTargetAccountId()));
		imgTo.refreshDrawableState();

		TextView txtProject = dlg.findViewById(R.id.txtProject);
		LinearLayout vBoxProject = dlg.findViewById(R.id.vBox_Project);
		if(this.trans.getProjectId() == "" ){
			vBoxProject.setVisibility(View.GONE);
		}else{
			txtProject.setText("پروژه : " +  this.trans.getProjectTitle());
		}


		TextView txtAmount = dlg.findViewById(R.id.txtAmount);
		txtAmount.setText(new DecimalFormat(" ###,###.## ").format(trans.getAmount())+"");

		TextView txtDate = dlg.findViewById(R.id.txtDate);
		txtDate.setText(trans.getRegDateFa());
		TextView txtTime = dlg.findViewById(R.id.txtTime);
		PersianCalendar pc = new PersianCalendar();
		pc.setTimeInMillis(trans.getRegDateGe());
		txtTime.setText(pc.getShortTime());


		TextView txtNote = dlg.findViewById(R.id.txtNote);
		if(trans.getNote() != null && trans.getNote().length()>0){
			txtNote.setText(trans.getNote());
		}else{
			txtNote.setVisibility(View.GONE);
		}



		TextView txtReminder = dlg.findViewById(R.id.txtReminder);
		LinearLayout vBoxReminder = dlg.findViewById(R.id.vBox_Reminder);
		LinearLayout btnPay = dlg.findViewById(R.id.btnPay);
		vBoxReminder.setVisibility(View.GONE);
		btnPay.setVisibility(View.GONE);

		if(vBoxReminder.getVisibility() == View.GONE && txtNote.getVisibility() == View.GONE){
			dlg.findViewById(R.id.sepDashed).setVisibility(View.GONE);
		}
		if(trans.getMethod()==1){
			txtType.setText("-");
			txtType.setTextColor(c.getResources().getColor(R.color.Crimson));
		}
		else{
			txtType.setText("+");
			txtType.setTextColor(c.getResources().getColor(R.color.ForestGreen));
		}
		txtType.setVisibility(View.VISIBLE);

		ImageView btnEdit = dlg.findViewById(R.id.btnEdit);
		ImageView btnDelete = dlg.findViewById(R.id.btnDelete);
		ImageView btnClose = dlg.findViewById(R.id.btnClose);
		btnClose.setOnClickListener(arg0 -> dlg.dismiss());
		btnEdit.setOnClickListener(arg0 -> {
			dlg.dismiss();
			if(DialogTransactionDetails.this.trans.getTAG().contains(Constants.DB_TAGS.WorkDay))
			{
				new MessageDialog(c,null,"هشدار","این تراکنش سیستمی بوده و قابل ویرایش نیست، در صورت تمایل به ویرایش آن از بخش پیمانکار اقدام کنید.","","",true);
				return;

			}
			callbackEdit.onDialogEditTransaction(trans);

		});
		btnDelete.setOnClickListener(arg0 -> {
			dlg.dismiss();
			if(DialogTransactionDetails.this.trans.getTAG().contains(Constants.DB_TAGS.WorkDay))
			{
				new MessageDialog(c,null,"هشدار","این تراکنش سیستمی بوده و قابل حذف نیست، در صورت تمایل به حذف آن از بخش پیمانکار اقدام کرده و کارکرد آن روز را حذف کنید.","","",true);
				return;

			}
			new DialogYesNo(DialogTransactionDetails.this.c, DialogTransactionDetails.this, null, c.getString(R.string.headerdelete), c.getString(R.string.deletesingle),c.getString(R.string.deletesingle1), false, "", true, "deletesingle");
			return;

		});

		dlg.show();
	}

	public void onYesNoDialog(String token, boolean checked)
	{
		try
		{

		if (token.equals("deletesingle")) {
			Log.d("DeleteSingle",""+DialogTransactionDetails.this.trans.getSerial());
			TransactionDAO.deleteById(trans.getTransactionId());
			this.dListener.onDialogDeleteTransaction();
	}
		}
		finally
		{
		}
	}
	public static abstract interface OnDialogClickListener
	{
		public abstract void onDialogDeleteClick();
	}

	public static abstract interface OnTransactionDelete
	{
		public abstract void onDialogDeleteTransaction();
	}

	public static abstract interface OnTransactionEdit
	{
		public abstract void onDialogEditTransaction(TransactionItem trans);
	}
}