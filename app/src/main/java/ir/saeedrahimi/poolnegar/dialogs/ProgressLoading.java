package ir.saeedrahimi.poolnegar.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.TextView;

import ir.saeedrahimi.poolnegar.R;

public class ProgressLoading
{
  Dialog loading;
  
  public ProgressLoading(Context c, String message)
  {
    this.loading = new Dialog(c);
    this.loading.requestWindowFeature(1);
    loading.getWindow().getDecorView().setLayoutDirection(
            View.LAYOUT_DIRECTION_RTL);
    this.loading.setContentView(R.layout.dialog_loading);
    if(message != null && message.length()>0)
    	((TextView)this.loading.findViewById(R.id.textmessage)).setText(message);
    this.loading.setCancelable(false);
    this.loading.getWindow().setBackgroundDrawable(new ColorDrawable(0));
    this.loading.show();
  }
  
  public void done()
  {
    this.loading.dismiss();
  }
}