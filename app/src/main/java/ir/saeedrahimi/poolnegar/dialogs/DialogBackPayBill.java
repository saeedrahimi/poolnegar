package ir.saeedrahimi.poolnegar.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;

import androidx.fragment.app.FragmentActivity;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar;
import ir.saeedrahimi.poolnegar.customs.TypeFaceProvider;
import ir.saeedrahimi.poolnegar.customs.Utils;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_AccountSelection;

public class DialogBackPayBill extends FragmentActivity implements OnClickListener,DatePickerDialog.OnDateSetListener {

	private static final int RESULT_DATE = 0;
	private static final int RESULT_ACC_PAYER = 2;
	private int accId;
	private PersianCalendar mCalendar;
	public Button btnRecFrom;
	private Button btnDate;
	private EditText edtAmount;
	EditText txtInfo;
	private String targetAccountId;
	private String sourceAccountId;
	private int isEditMode;
	private int transSerial;

	public DialogBackPayBill() {
		super();
	}

	public DialogBackPayBill(int accId, String sourceAccountId) {
		super();
		this.accId = accId;
		this.sourceAccountId = sourceAccountId;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		setContentView(R.layout.dialog_backpaybill);
		getWindow().getDecorView()
				.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
		this.btnDate = ((Button) findViewById(R.id.datePayBill));
		this.btnRecFrom = ((Button) findViewById(R.id.btnTargetAccount));
		this.edtAmount = ((EditText) findViewById(R.id.amountPayBill));
		this.txtInfo = ((EditText) findViewById(R.id.descriptionPayBill));
		if (getIntent().getIntArrayExtra("transFromIds") != null)
			this.sourceAccountId = getIntent().getStringExtra("sourceAccountId");
		this.accId = getIntent().getIntExtra("accId", -1);
		((Button) findViewById(R.id.submit_paybill)).setOnClickListener(this);
		((Button) findViewById(R.id.cancel_paybill)).setOnClickListener(this);
		this.mCalendar = new PersianCalendar();
		this.btnDate.setText(mCalendar.getPersianShortDate());
		setTitle("بازگشت پرداخت بدهی");

		this.edtAmount.setKeyListener(DigitsKeyListener
				.getInstance("1234567890,."));

		this.edtAmount.addTextChangedListener(Utils.MoneyTextWatcher);
		int width=(int)(getResources().getDisplayMetrics().widthPixels*0.90);
		int height=(int)(getResources().getDisplayMetrics().heightPixels*0.90);
		this.getWindow().setLayout(width,height);
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.submit_paybill:
			submitData();
			break;
		case R.id.cancel_paybill:
			Intent backIntent = new Intent();
			backIntent.putExtra("payed", false);
			setResult(RESULT_CANCELED, backIntent);
			finish();
			break;
		default:

		}

	}

	public void accSelector_payer(View paramView) {
		Intent dlg = new Intent(this, Activity_AccountSelection.class);
		dlg.putExtra("SelectedAccounts", "payer");
		this.startActivityForResult(dlg, RESULT_ACC_PAYER);
	}

	public void timePickerDilog(View paramView) {
		PersianCalendar persianCalendar = new PersianCalendar();
		DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
				DialogBackPayBill.this,
				persianCalendar.getPersianYear(),
				persianCalendar.getPersianMonth(),
				persianCalendar.getPersianDay(),
				TypeFaceProvider.get(this,"vazir.ttf")
		);
		datePickerDialog.setThemeDark(true);
		datePickerDialog.show(getFragmentManager(), "Datepickerdialog");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == RESULT_DATE) {
			if (resultCode == RESULT_OK) {
				int year = data.getIntExtra("year", 1393);
				int month = data.getIntExtra("month", 6);
				int day = data.getIntExtra("day", 1);
				mCalendar.setPersianDate(year, month, day);
				btnDate.setText(mCalendar.getPersianShortDate());
			}
			if (resultCode == RESULT_CANCELED) {

			}
		} else if ((requestCode == RESULT_ACC_PAYER)) {
			if (resultCode == RESULT_OK) {
				this.targetAccountId = data.getStringExtra("SelectedAccounts");
				btnRecFrom.setText(AccountDAO.getTitleById(this.targetAccountId));
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}


	public void submitData() {
		double cashAmount = 0.0D;
		try {
			cashAmount = Double.parseDouble(this.edtAmount.getText().toString()
					.replace(",", ""));
		} catch (Exception localException) {
			Snackbar.make(this.getWindow().getDecorView(),"لطفا مبلغ ورودی را دوباره بررسی کنید", BaseTransientBottomBar.LENGTH_LONG).setAction("Action", null).show();
			return;
		}

		if (this.targetAccountId.equals("")) {
			Snackbar.make(this.getWindow().getDecorView(),"لطفا حساب مقصد  را مشخص کنید", BaseTransientBottomBar.LENGTH_LONG).setAction("Action", null).show();
			return;
		}
		if (((this.edtAmount.getText().toString().equals(""))
				&& (this.edtAmount.getText().toString().equals("0")) || cashAmount == 0)) {

			Snackbar.make(this.getWindow().getDecorView(),"مبلغ وارد نشده است", BaseTransientBottomBar.LENGTH_LONG).setAction("Action", null).show();
			return;
		}
		cashAmount = cashAmount * -1;
		// Start Save Process
		if (this.isEditMode == 0) {
			// TODO: Fix here
//			this._db.AddPaybillTransaction(this.transSerial,accId,
//					this.transFromIds[2], this.transFromIds[1],this.transFromIds[0],
//					this.transPayToIds[2],this.transPayToIds[1], this.transPayToIds[0],
//					cashAmount,
//					this.curDate, this.txtInfo.getText().toString());

			Intent backIntent = new Intent();
			backIntent.putExtra("amount",cashAmount );
			setResult(RESULT_OK, backIntent);
			finish();
		}

		finish();
	}
	@Override
	public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
		String date = "You picked the following date: "+dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
		Toast.makeText(this, date, Toast.LENGTH_SHORT);
		mCalendar.setPersianDate(year, monthOfYear, dayOfMonth);
		btnDate.setText(mCalendar.getPersianShortDate());
	}
}
