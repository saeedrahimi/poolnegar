//package ir.saeedrahimi.poolnegar.dialogs;
//
//import android.app.Dialog;
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.ListView;
//import android.widget.RadioButton;
//
//import ir.saeedrahimi.poolnegar.R;
//import ir.saeedrahimi.poolnegar.adapters.AccountParentSelectorAdapter;
//import ir.saeedrahimi.poolnegar.database.model.Account;
//
//
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.LinkedHashMap;
//import java.util.List;
//
//public class DialogParentAccountSelector extends DialogBaseAccountSelector {
//
//	public RadioButton lastSelectRadioBtn;
//	public int[] tempAccIds = { -1, -1, -1 };// 0=rootAcc,1=subAcc,2=payAcc
//	public String[] tempAccTitles = { "", "", "" };
//
//
//	private List<Account> projects;
//
//
//	public void dialogBack(View view) {
//		reload();
//	}
//	public void submit(View view) {
//		doAccount();
//	}
//	public void reload() {
//		setContentView(R.layout.accountselector_dialog);
//		this._db = new DatabaseHelper(this);
//		this.accDialog = new Dialog(this);
//		int width=(int)(getResources().getDisplayMetrics().widthPixels*0.90);
//		int height=(int)(getResources().getDisplayMetrics().heightPixels*0.90);
//		this.accDialog.getWindow().setLayout(width,height);
//		if (getIntent().getStringExtra("accType") != null) {
//			if (getIntent().getStringExtra("accType").equals("acc"))
//				accSelector();
//			else if (getIntent().getStringExtra("accType").equals("payer"))
//				selectTargetAccount();
//			else if (getIntent().getStringExtra("accType").equals("reciver"))
//				selectSourceAccount();
//		}
//	}
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		int width=(int)(getResources().getDisplayMetrics().widthPixels*0.90);
//		int height=(int)(getResources().getDisplayMetrics().heightPixels*0.90);
//		this.getWindow().setLayout(width,height);
//		super.onCreate(savedInstanceState);
//		requestWindowFeature(1);
//		reload();
//	}
//
//	public void doAccount() {
//		this.accDialog.dismiss();
//		Intent backIntent = new Intent();
//		this.selectedAccIds[0] = tempAccIds[0];
//		this.selectedAccIds[1] = tempAccIds[1];
//		this.selectedAccIds[2] = tempAccIds[2];
//		this.selectedAccTitles[0] = tempAccTitles[0];
//		this.selectedAccTitles[1] = tempAccTitles[1];
//		this.selectedAccTitles[2] = tempAccTitles[2];
//
//		if (this.selectedAccIds[2] == -1)
//			this.path = this.selectedAccTitles[0] + " - "+ this.selectedAccTitles[1];
//		else
//			this.path = this.selectedAccTitles[0] + " - "
//					+ this.selectedAccTitles[1] + " - "
//					+ this.selectedAccTitles[2];
//
//		backIntent.putExtra("selectedTitles", selectedAccTitles);
//		backIntent.putExtra("selectedIds", selectedAccIds);
//		backIntent.putExtra("title", path);
//		setResult(RESULT_OK, backIntent);
//		finish();
//	}
//
//
//	@SuppressWarnings("rawtypes")
//	private void fillList(int parentId) {
//		DatabaseHelper _db = new DatabaseHelper(this);
//		this.tempList = new ArrayList();
//		this.tempList = _db.getSubAccountsList(parentId, true);
//	}
//
//	@SuppressWarnings({ "unchecked", "rawtypes" })
//	public void FillSubAccounts(int parentId) {
//		this.accHash = new LinkedHashMap<String, List<String>>();
//		DatabaseHelper _db = new DatabaseHelper(this);
//		this.accList = new ArrayList<String>();
//		if(selectorType==99)
//		{
//			this.accHash = _db.getSubAccounts(parentId, 2,true);
//
//		}
//		else if (selectorType == 0) {
//			this.accHash = _db.getSubAccounts(parentId, 0, true);
//		} else if (selectorType == 1)  {
//			this.accHash = _db.getSubAccounts(parentId, 1, true);
//		}
//		 else if (selectorType == 2)  {
//				this.accHash = _db.getSubAccounts(parentId, 2, true);
//			}
//		Iterator localIterator = this.accHash.keySet().iterator();
//		do{
//			String str = (String) localIterator.next();
//			//Log.d("subAccount names ", str);
//			this.accList.add(str);
//		}while(localIterator.hasNext());
//	}
//	public void accSelector() {
//		List<String[]> payList = this._db.Accounts(3);
//		this.accTitles = payList.get(0);
//		this.accIds = payList.get(1);
//		this.selectorType = 2;
//		this.accListView = ((ListView) findViewById(R.id.accountselector_firstlistView));
//		this.accListView.setAdapter(new AccountParentSelectorAdapter(this,
//				17367043, this.accTitles));
//	}
//	public void selectTargetAccount() {
//		List<String[]> payList = this._db.Accounts(2);
//		this.accTitles = payList.get(0);
//		this.accIds = payList.get(1);
//		this.selectorType = 99;
//		this.accListView = ((ListView) findViewById(R.id.accountselector_firstlistView));
//		this.accListView.setAdapter(new AccountParentSelectorAdapter(this,
//				17367043, this.accTitles));
//	}
//
//	public void selectSourceAccount() {
//		List<String[]> recList = this._db.Accounts(1);
//		this.accTitles = recList.get(0);
//		this.accIds = recList.get(1);
//		this.selectorType = 0;
//		this.accListView = ((ListView) findViewById(R.id.accountselector_firstlistView));
//		this.accListView.setAdapter(new AccountParentSelectorAdapter(this,
//				17367043, this.accTitles));
//	}
//
//
//
//}
