//package ir.saeedrahimi.poolnegar.adapters;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.Filter;
//import android.widget.Filterable;
//import android.widget.TextView;
//
//import ir.saeedrahimi.poolnegar.CalendarTool;
//import ir.saeedrahimi.poolnegar.R;
//import ir.saeedrahimi.poolnegar.database.model.Account;
//import ir.saeedrahimi.poolnegar.dialogs.DialogAccountSelector;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class ProjectSelectorAdapter
//  extends BaseAdapter implements Filterable
//{
//  private List<Account> projects;
//  private List<Account> filteredData ;
//  private DialogAccountSelector trans;
//
//    private ItemFilter mFilter = new ItemFilter();
//  public ProjectSelectorAdapter(DialogAccountSelector dialogAccountSelector, Context paramContext, int paramInt, List<Account> data)
//  {
//    this.projects = data;
//    this.filteredData = data;
//    this.trans=dialogAccountSelector;
//  }
//
//  public View getView(final int position, View convertView, ViewGroup parent) {
//  {
//    View root = ((LayoutInflater)this.trans.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.projectselctorrow, parent, false);
//      TextView txtTitle = (TextView)root.findViewById(R.id.projectselector_name);
//      TextView txtYear = (TextView)root.findViewById(R.id.txt_year);
//      TextView txtMonth = (TextView)root.findViewById(R.id.txt_month);
//      TextView txtDay = (TextView)root.findViewById(R.id.txt_day);
//      CalendarTool ct = new CalendarTool(filteredData.get(position).getDate());
//      txtYear.setText(ct.getIranianYear()+"");
//      txtMonth.setText(ct.getMonthNameStr());
//      txtDay.setText(ct.getIranianDay()+"");
//      txtTitle.setText(this.filteredData.get(position).getTitle());
//
//      root.setOnClickListener(new OnClickListener() {
//
//		@Override
//		public void onClick(View arg0) {
//		    trans.doProject(filteredData.get(position).getAccID(),filteredData.get(position).getTitle());
//
//		}
//	});
//    return root;
//  }
//}
//    public int getCount() {
//        return filteredData.size();
//    }
//
//    public Account getItem(int position) {
//        return filteredData.get(position);
//    }
//
//    public long getItemId(int position) {
//        return position;
//    }
//
//
//    @Override
//    public Filter getFilter() {
//        return mFilter;
//    }
//
//    private class ItemFilter extends Filter {
//    @Override
//    protected FilterResults performFiltering(CharSequence constraint) {
//
//        String filterString = constraint.toString().toLowerCase();
//
//        FilterResults results = new FilterResults();
//
//        final List<Account> list = projects;
//
//        int count = list.size();
//        final ArrayList<Account> nlist = new ArrayList<Account>(count);
//
//        String filterableString ;
//
//        for (int i = 0; i < count; i++) {
//            filterableString = list.get(i).getTitle();
//            if (filterableString.toLowerCase().contains(filterString)) {
//                nlist.add(list.get(i));
//            }
//        }
//
//        results.values = nlist;
//        results.count = nlist.size();
//
//        return results;
//    }
//
//    @SuppressWarnings("unchecked")
//    @Override
//    protected void publishResults(CharSequence constraint, FilterResults results) {
//        filteredData = (ArrayList<Account>) results.values;
//        notifyDataSetChanged();
//    }
//
//}
//}
