package ir.saeedrahimi.poolnegar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.database.model.Account;

public class ProjectsListAdapter extends ArrayAdapter<Account> {
	private List<Account> itemList;
	private Context ctx;
	private int layoutId;
	private onPopUpDialogClick callbackPopup;
	private onProjectClick callbackProject;
	public ProjectsListAdapter(Context ctx, List<Account> itemList, int layoutId,onPopUpDialogClick callbackPopup,onProjectClick callbackProject) {
		super(ctx, layoutId, itemList);
		this.itemList = itemList;
		this.ctx = ctx;
		this.layoutId = layoutId;
		this.callbackPopup=callbackPopup;
		this.callbackProject=callbackProject;
	}
	@Override
	public int getCount() {
		return itemList.size();
	}
	@Override
	public Account getItem(int position) {
		return itemList.get(position);
	}
	@Override
	public long getItemId(int position) {	
		return itemList.get(position).hashCode();
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		final Account acc = itemList.get(position);
		TextView begbalance,endbalance,expbalance,inbalance,txtdate,txtTitle;

		if (convertView == null)
		{
			convertView = ((LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.items_project, parent, false);

		}
		ImageView btnMenu = ((ImageView)convertView.findViewById(R.id.accountoverflow));
		txtTitle = ((TextView)convertView.findViewById(R.id.accnameacc));
		txtTitle.setText(acc.getTitle());
		txtTitle.setBackgroundColor(getContext().getApplicationContext().getResources().getColor(R.color.transparent));
		if(acc.isClosed())
			txtTitle.setBackgroundColor(getContext().getApplicationContext().getResources().getColor(R.color.Crimson));
		txtdate = ((TextView)convertView.findViewById(R.id.sincetimeacc));
		txtdate.setText(acc.getProjectDate());

		begbalance = ((TextView)convertView.findViewById(R.id.totalbill));
		inbalance = ((TextView)convertView.findViewById(R.id.payedbills));
		expbalance = ((TextView)convertView.findViewById(R.id.remainedbills));
		endbalance = ((TextView)convertView.findViewById(R.id.endbalanceacc));

		begbalance.setText(new DecimalFormat(" ###,###.## ").format(acc.getBalance()*-1));
		inbalance.setText(new DecimalFormat(" ###,###.## ").format(acc.getTotalIncome()));
		expbalance.setText(new DecimalFormat(" ###,###.## ").format(acc.getTotalOutcome()));

		double endBalance= (acc.getBalance()+acc.getTotalOutcome())*-1;

		endbalance.setText(new DecimalFormat(" ###,###.## ").format(endBalance));
		ImageView endimg = ((ImageView)convertView.findViewById(R.id.endingbalancecolor));
		if(endBalance>0)
			endimg.setBackgroundColor(getContext().getApplicationContext().getResources().getColor(R.color.MediumSeaGreen));
		if(endBalance == 0)
			endimg.setBackgroundColor(getContext().getApplicationContext().getResources().getColor(R.color.Gray));


		btnMenu.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				PopupMenu popup = new PopupMenu(ctx, view);
				android.view.MenuInflater inflater = popup.getMenuInflater();
				if (acc.isClosed())
					inflater.inflate(R.menu.menu_overflow_accounts_closed, popup.getMenu());
				else
					inflater.inflate(R.menu.menu_overflow_accounts, popup.getMenu());
				popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(android.view.MenuItem item) {
						if (ProjectsListAdapter.this.callbackPopup != null) {
							ProjectsListAdapter.this.callbackPopup.onPopUpDialogClick(item.getItemId(),acc);
						}

						return false;
					}
				});
				popup.show();

			}
		});
		convertView.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				if (ProjectsListAdapter.this.callbackProject != null) {
					ProjectsListAdapter.this.callbackProject.onProjectClick(view, acc);
				}

			}
		});
		return convertView;
	}
	public interface onPopUpDialogClick
	{
		void onPopUpDialogClick(int token, Account acc);
	}
	public interface onProjectClick
	{
		void onProjectClick(View view, Account acc);
	}
}