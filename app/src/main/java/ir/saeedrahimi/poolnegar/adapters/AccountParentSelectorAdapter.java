//package ir.saeedrahimi.poolnegar.adapters;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.ExpandableListView;
//import android.widget.RadioButton;
//import android.widget.TextView;
//
//import ir.saeedrahimi.poolnegar.R;
//import ir.saeedrahimi.poolnegar.dialogs.DialogParentAccountSelector;
//
//import java.util.List;
//
//public class AccountParentSelectorAdapter extends ArrayAdapter<String> {
//	private DialogParentAccountSelector dlgAccSelector;
//
//	public AccountParentSelectorAdapter(
//			DialogParentAccountSelector dialogAccountSelector, int paramInt,
//			String[] paramList) {
//		super(dialogAccountSelector, paramInt, paramList);
//		this.dlgAccSelector = dialogAccountSelector;
//	}
//
//	public View getView(final int position, View convertView, ViewGroup parent) {
//		{
//			View rowView = ((LayoutInflater) this.dlgAccSelector
//					.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
//					R.layout.accselector_dialog_root_row, null, false);
//			final RadioButton rd = (RadioButton)rowView.findViewById(R.id.radioButton1);
//
//			TextView rowTitle = (TextView) rowView
//					.findViewById(R.id.accSelector_root_row);
//			final String curTitle = this.dlgAccSelector.accTitles[position];
//			rowTitle.setText(curTitle);
//			rd.setOnClickListener(new View.OnClickListener()
//			{
//				public void onClick(View v)
//				{
//					if (dlgAccSelector.lastSelectRadioBtn == (RadioButton) v) {
//						dlgAccSelector.lastSelectRadioBtn.setChecked(false);
//						return;
//					}
//
//					if (dlgAccSelector.lastSelectRadioBtn != null) {
//						dlgAccSelector.lastSelectRadioBtn.setChecked(false);
//					}
//					dlgAccSelector.tempAccIds[0] = Integer.parseInt(dlgAccSelector.accIds[position]);
//					dlgAccSelector.tempAccIds[1] = -1;
//					dlgAccSelector.tempAccIds[2] = -1;
//					dlgAccSelector.tempAccTitles[0] = dlgAccSelector.accTitles[position];
//					dlgAccSelector.tempAccTitles[1] = "";
//					dlgAccSelector.tempAccTitles[2] = "";
//					dlgAccSelector.lastSelectRadioBtn = ((RadioButton)v);
//					dlgAccSelector.lastSelectRadioBtn.setChecked(true);
//
//				}
//			});
//			rowView.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View arg0) {
//					int curId = Integer.parseInt(dlgAccSelector.accIds[position]);
//					dlgAccSelector.lastSelectRadioBtn = rd;
//					dlgAccSelector.lastSelectRadioBtn.setChecked(true);
//					dlgAccSelector.FillSubAccounts(curId);
//					for (int j = -1 + dlgAccSelector.accList.size(); j >= 0; j--) {
//						if (dlgAccSelector.accList.size() == 0) {
//							break;
//						}
//						List<String> localList = dlgAccSelector.accHash
//								.get(dlgAccSelector.accList.get(j));
//						int sellId = dlgAccSelector._db.getAccountId(
//								(String) dlgAccSelector.accList.get(j), curId);
//						if ((localList.size() == 0)
//								&& (dlgAccSelector._db.AccountHasChild(sellId))) {
//							dlgAccSelector.accList.remove(j);
//						}
//					}
//					dlgAccSelector.setContentView(R.layout.accselector_subacc_dialog);
//					dlgAccSelector.accExpand = ((ExpandableListView) dlgAccSelector.findViewById(R.id.rep_accselector_subacc_list));
//					((TextView) dlgAccSelector.findViewById(R.id.rep_accselector_subacc_title)).setText(dlgAccSelector.accTitles[position]);
//
//					dlgAccSelector.subAdapter = new SubAccountParentSelectorAdapter(
//							dlgAccSelector, dlgAccSelector.accList,
//							dlgAccSelector.accHash, dlgAccSelector.accExpand);
//					dlgAccSelector.accExpand.setAdapter(dlgAccSelector.subAdapter);
//
//					dlgAccSelector.tempAccTitles[0] = dlgAccSelector.accTitles[position];
//					dlgAccSelector.tempAccIds[0] = curId;
//					return;
//				}
//			});
//			return rowView;
//		}
//	}
//}
