package ir.saeedrahimi.poolnegar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.models.BillProjectItem;

import java.text.DecimalFormat;
import java.util.List;

public class ProjectsListAdapter_bk extends ArrayAdapter<BillProjectItem> {
	private List<BillProjectItem> itemList;
	private Context ctx;
	private int layoutId;
	public ProjectsListAdapter_bk(Context ctx, List<BillProjectItem> itemList, int layoutId) {
		super(ctx, layoutId, itemList);
		this.itemList = itemList;
		this.ctx = ctx;
		this.layoutId = layoutId;
	}
	@Override
	public int getCount() {	
		return itemList.size() ;
	}
	@Override
	public BillProjectItem getItem(int position) {	
		return itemList.get(position);
	}
	@Override
	public long getItemId(int position) {	
		return itemList.get(position).hashCode();
	}
	@Override
	public View getView(final int position,	View convertView, ViewGroup parent) {

		final BillProjectItem item = getItem(position);

		if (convertView == null) {
			convertView = ((LayoutInflater) ctx.getSystemService(
					"layout_inflater")).inflate(
							R.layout.list_item_transaction, parent, false);

		}
		TextView accFrom = (TextView) convertView.findViewById(R.id.accFrom);
		TextView accTo = (TextView) convertView.findViewById(R.id.accTo);
		TextView accAmount = (TextView) convertView.findViewById(R.id.accamount);
		ImageView accIcon = (ImageView) convertView.findViewById(R.id.accicontrans);
		TextView accDate = (TextView) convertView.findViewById(R.id.accduedate);

		accFrom.setText("پروژه: "+item.getProjectTitle());
		if(item.getMinDate()==item.getMaxDate())
        	accTo.setText(item.getMinDate() );
        else
        	accTo.setText(item.getMinDate()+" ~ " +item.getMaxDate() );
        accDate.setText("("+ item.getBillCount() +" فقره)");
        accAmount.setText( new DecimalFormat(" ###,###.## ").format(item.getBillAmount()));
        
        accIcon.setImageResource(ctx.getResources().getIdentifier("ic_project", "drawable", ctx.getPackageName()));
        
		TranslateAnimation translateAnim = new TranslateAnimation(0, 0, 200, 0 );
		convertView.clearAnimation();
		translateAnim.setDuration(500);   
		translateAnim.setFillBefore(true);   
		convertView.startAnimation(translateAnim);
		return convertView;
	}
}