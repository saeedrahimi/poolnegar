package ir.saeedrahimi.poolnegar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ViewFlipper;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import ir.saeedrahimi.poolnegar.Gateways.WorkDayDAO;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.models.PersonItem;
import ir.saeedrahimi.poolnegar.models.WorkDayItem;


public class WorkersListAdapter extends RecyclerView.Adapter<WorkersListAdapter.WorkerViewHolder> {

    private List<PersonItem> itemList;
    private Context mContext;
    private onClick listener;
    private String mDate;

    public WorkersListAdapter(Context mContext, List<PersonItem> itemList, onClick listener, String date) {
        this.itemList = itemList;
        this.mContext = mContext;
        this.listener = listener;
        this.mDate = date;
    }

    @Override
    public WorkerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.items_worker, parent, false);

        final RecyclerView.ViewHolder mViewHolder = new WorkerViewHolder(itemView);


        return new WorkerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final WorkerViewHolder holder, final int i) {
        PersonItem wi = itemList.get(i);
        holder.vName.setText(wi.getName());
        WorkDayItem day = WorkDayDAO.getByDate(wi.getId(), mDate);

        if (day != null && day.getLackHour() != 0) {
            holder.vOvertime.setText("کسر ساعت کاری: " + (int) day.getLackHour() + " ساعت ");
            holder.vOvertime.setTextColor(mContext.getResources().getColor(R.color.ColorAccent));
        } else if (day != null && day.getOvertime() != 0) {
            holder.vOvertime.setText("اضافه کاری: " + (int) day.getOvertime() + " ساعت ");
            holder.vOvertime.setTextColor(mContext.getResources().getColor(R.color.Teal));
        } else
            holder.vOvertime.setText("");

        if (day != null && day.getProjectID() != "") {
            String projectTitle = AccountDAO.selectByID(day.getProjectID()).getTitle();
            holder.vProject.setText("پروژه: " + projectTitle);
        } else
            holder.vProject.setText("");
        final int pos = holder.getAdapterPosition();
        final PersonItem workerItem = itemList.get(pos);

        holder.itemView.setOnClickListener(v -> {
            if (WorkersListAdapter.this.listener != null) {

                WorkersListAdapter.this.listener.onWorkerClick(v, workerItem);
            }
        });

        holder.btnPresent.setOnClickListener(v -> {
            holder.vfCommands.showNext();
            WorkersListAdapter.this.listener.onCommandClick(v, workerItem, Commands.PRESENT, i);
        });
        holder.btnAbsent.setOnClickListener(v -> {
            holder.vfCommands.showPrevious();
            WorkersListAdapter.this.listener.onCommandClick(v, workerItem, Commands.ABSENT, i);
        });
        holder.btnProject.setOnClickListener(v -> WorkersListAdapter.this.listener.onCommandClick(v, workerItem, Commands.PROJECT, i));
        holder.btnIncOvertime.setOnClickListener(v -> WorkersListAdapter.this.listener.onCommandClick(v, workerItem, Commands.INCOVERTIME, i));
        holder.btnDecOvertime.setOnClickListener(v -> WorkersListAdapter.this.listener.onCommandClick(v, workerItem, Commands.DECOVERTIME, i));
        holder.vCard.setOnClickListener(v -> WorkersListAdapter.this.listener.onCommandClick(v, workerItem, Commands.CARD, i));
        if (WorkDayDAO.isPresent(wi, mDate))
            holder.vfCommands.setDisplayedChild(1);
        else
            holder.vfCommands.setDisplayedChild(0);

        Animation in = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_out_right);

// set the animation type to ViewSwitcher
        holder.vfCommands.setInAnimation(in);
        holder.vfCommands.setOutAnimation(out);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    static class WorkerViewHolder extends RecyclerView.ViewHolder {
        View vCard;
        TextView vName;
        TextView vOvertime;
        TextView vProject;
        ViewFlipper vfCommands;
        ImageButton btnPresent;
        ImageButton btnAbsent;
        ImageButton btnIncOvertime;
        ImageButton btnDecOvertime;
        ImageButton btnProject;

        WorkerViewHolder(View v) {
            super(v);
            vCard =  v.findViewById(R.id.llai1);
            vName = v.findViewById(R.id.accnameacc);
            vOvertime = v.findViewById(R.id.txt_overtime);
            vProject = v.findViewById(R.id.txt_project);
            vfCommands = v.findViewById(R.id.vf_commands);
            btnPresent = v.findViewById(R.id.btn_present);
            btnAbsent = v.findViewById(R.id.btn_absent);
            btnIncOvertime = v.findViewById(R.id.btn_incovertime);
            btnDecOvertime = v.findViewById(R.id.btn_decovertime);
            btnProject = v.findViewById(R.id.btn_project);
        }

    }

    public interface onClick {
        void onWorkerClick(View view, PersonItem acc);

        void onCommandClick(View view, PersonItem acc, int commandType, int position);
    }

    public interface Commands {

        int PRESENT = 1;
        int ABSENT = 2;
        int PROJECT = 3;
        int INCOVERTIME = 4;
        int DECOVERTIME = 5;
        int CARD = 6;
    }

    public void setDate(String date) {
        this.mDate = date;
        this.notifyDataSetChanged();
    }

}