package ir.saeedrahimi.poolnegar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;

public class TransactionListAdapter extends ArrayAdapter<TransactionItem> {
	private List<TransactionItem> itemList;
	private Context ctx;
	private int layoutId;
	private int mLastPosition;
	public TransactionListAdapter(Context ctx, List<TransactionItem> itemList, int layoutId) {
		super(ctx, layoutId, itemList);
		this.itemList = itemList;
		this.ctx = ctx;
		this.layoutId = layoutId;
	}
	@Override
	public int getCount() {	
		return itemList.size() ;
	}
	@Override
	public TransactionItem getItem(int position) {
		return itemList.get(position);
	}
	@Override
	public long getItemId(int position) {	
		return itemList.get(position).hashCode();
	}
	@Override
	public View getView(final int position,	View convertView, ViewGroup parent) {

		final TransactionItem item = getItem(position);

		if (convertView == null) {
			convertView = ((LayoutInflater) ctx.getSystemService(
					Context.LAYOUT_INFLATER_SERVICE)).inflate(
							R.layout.list_item_transaction, parent, false);

		}
		TextView accFrom = (TextView) convertView.findViewById(R.id.accFrom);
		TextView accTo = (TextView) convertView.findViewById(R.id.accTo);
		TextView accAmount = (TextView) convertView.findViewById(R.id.accamount);
		ImageView accIcon = (ImageView) convertView.findViewById(R.id.accicontrans);
		TextView accDate = (TextView) convertView.findViewById(R.id.accduedate);
		TextView accDesc = (TextView) convertView.findViewById(R.id.accdesc);
		TextView accPrj = (TextView) convertView.findViewById(R.id.accProject);
		
		if(item.getNote().length()>1)
			accDesc.setText("توضیح: " + item.getNote());
		else
			accDesc.setVisibility(View.GONE);
		if(item.getProjectTitle().length()>1)
			accPrj.setText("پروژه: "+ item.getProjectTitle());
		else
			accPrj.setVisibility(View.GONE);
		String dirTo = "به: " + item.getTargetAccountTitle();

		String dirFrom = "از: " + item.getSourceAccountTitle();
		accFrom.setText(dirFrom);
		accTo.setText(dirTo);
		accAmount.setText(new DecimalFormat(" ###,###.## ")
		.format(item.getAmount()));
		accDate.setText(item.getRegDateFa());

		if (item.getMethod() == 0)
			accIcon.setBackgroundColor(ctx.getResources().getColor(R.color.LimeGreen));
		else
			accIcon.setBackgroundColor(ctx.getResources().getColor(R.color.outcome));
		
		float initialTranslation = (mLastPosition <= position ? 500f : -500f);

        convertView.setTranslationY(initialTranslation);
        convertView.animate()
                .setInterpolator(new DecelerateInterpolator(1.0f))
                .translationY(0f)
                .setDuration(350l)
                .setListener(null);

        // Keep track of the last position we loaded
        mLastPosition = position;
		/*TranslateAnimation translateAnim = new TranslateAnimation(0, 0, 200, 0 );
		convertView.clearAnimation();
		translateAnim.setDuration(500);   
		translateAnim.setFillBefore(true);   
		convertView.startAnimation(translateAnim);*/
		return convertView;
	}
}