package ir.saeedrahimi.poolnegar.adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar;
import ir.saeedrahimi.poolnegar.database.DAO.TransactionDAO;
import ir.saeedrahimi.poolnegar.models.WorkDayItem;

public class WorkDayListAdapter extends ArrayAdapter<WorkDayItem> {
	private List<WorkDayItem> itemList;
	private Context mContext;
	private int layoutId;
	private int mLastPosition;
	public WorkDayListAdapter(Context ctx, List<WorkDayItem> itemList, int layoutId) {
		super(ctx, layoutId, itemList);
		this.itemList = itemList;
		this.mContext = ctx;
		this.layoutId = layoutId;
	}
	@Override
	public int getCount() {	
		return itemList.size() ;
	}
	@Override
	public WorkDayItem getItem(int position) {
		return itemList.get(position);
	}
	@Override
	public long getItemId(int position) {	
		return itemList.get(position).hashCode();
	}
	@Override
	public View getView(final int position,	View root, ViewGroup parent) {

		final WorkDayItem item = getItem(position);

		if (root == null) {
			root = ((LayoutInflater) mContext.getSystemService(
					Context.LAYOUT_INFLATER_SERVICE)).inflate(
							R.layout.items_workday, parent, false);

		}
		TextView accOvertime = root.findViewById(R.id.txt_overtime);
		TextView accFee = root.findViewById(R.id.txt_fee);
		TextView txtYear = root.findViewById(R.id.txt_year);
		TextView txtMonth = root.findViewById(R.id.txt_month);
		TextView txtDay = root.findViewById(R.id.txt_day);
		PersianCalendar pc = new PersianCalendar();
		pc.setPersianDate(item.getWorkFaDate());
		txtYear.setText( String.valueOf(pc.getPersianYear()));
		txtMonth.setText(pc.getPersianMonthName());
		txtDay.setText(String.valueOf(pc.getPersianDay()));

		double dayFee = TransactionDAO.selectByID(item.getTransactionID()).getAmount();
		accFee.setText(new DecimalFormat(" ###,###.## ").format(dayFee));
		if(item.getOvertime()>0){
			accOvertime.setText((int)item.getOvertime()+"");
		}
		else if(item.getLackHour()>0){
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
					accOvertime.setTextColor(mContext.getResources().getColor(R.color.ColorAccent,mContext.getTheme()));
				}else
				{
					accOvertime.setTextColor(mContext.getResources().getColor(R.color.ColorAccent));
				}
			accOvertime.setText("-"+(int)item.getLackHour());
		}else{
			accOvertime.setText("0");
		}
		/*float initialTranslation = (mLastPosition <= position ? 500f : -500f);

        convertView.setTranslationY(initialTranslation);
        convertView.animate()
                .setInterpolator(new DecelerateInterpolator(1.0f))
                .translationY(0f)
                .setDuration(350l)
                .setListener(null);

        // Keep track of the last position we loaded*/
        mLastPosition = position;
		root.setTag("day");
		return root;
	}
}