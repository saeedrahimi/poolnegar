package ir.saeedrahimi.poolnegar.adapters;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.database.model.Account;

import java.util.List;

public class ReportProjectsListAdapter extends ArrayAdapter<Account> implements CompoundButton.OnCheckedChangeListener {
	private List<Account> itemList;
	private Context ctx;
	//private int layoutId;
	private int mLastPosition;
	public SparseBooleanArray mCheckStates;
	public ReportProjectsListAdapter(Context ctx, List<Account> itemList, int layoutId) {
		super(ctx, layoutId, itemList);
		this.itemList = itemList;
		this.ctx = ctx;
		mCheckStates = new SparseBooleanArray(itemList.size());
		//this.layoutId = layoutId;
	}
	@Override
	public int getCount() {	
		return itemList.size() ;
	}
	@Override
	public Account getItem(int position) {
		return itemList.get(position);
	}
	@Override
	public long getItemId(int position) {	
		return itemList.get(position).hashCode();
	}
	@Override
	public View getView(final int position,	View convertView, ViewGroup parent) {

		final Account item = getItem(position);

		if (convertView == null) {
			convertView = ((LayoutInflater) ctx.getSystemService(
					Context.LAYOUT_INFLATER_SERVICE)).inflate(
							R.layout.report_projects_list, parent, false);

		}
		TextView accTitle = (TextView) convertView.findViewById(R.id.title);
		CheckBox chkSelect = (CheckBox) convertView.findViewById(R.id.checkBox);
		accTitle.setText(item.getTitle());

		float initialTranslation = (mLastPosition <= position ? 500f : -500f);

        convertView.setTranslationY(initialTranslation);
        convertView.animate()
                .setInterpolator(new DecelerateInterpolator(1.0f))
                .translationY(0f)
                .setDuration(350l)
                .setListener(null);
        // Keep track of the last position we loaded
        mLastPosition = position;

		chkSelect.setTag(position);
		chkSelect.setChecked(mCheckStates.get(position, false));
		chkSelect.setOnCheckedChangeListener(this);


		/*TranslateAnimation translateAnim = new TranslateAnimation(0, 0, 200, 0 );
		convertView.clearAnimation();
		translateAnim.setDuration(500);   
		translateAnim.setFillBefore(true);   
		convertView.startAnimation(translateAnim);*/
		return convertView;
	}
	public boolean isChecked(int position) {
		return mCheckStates.get(position, false);
	}

	public void setChecked(int position, boolean isChecked) {
		mCheckStates.put(position, isChecked);

	}

	public void toggle(int position) {
		setChecked(position, !isChecked(position));

	}
	@Override
	public void onCheckedChanged(CompoundButton buttonView,	 boolean isChecked) {

		mCheckStates.put((Integer) buttonView.getTag(), isChecked);

	}
}