package ir.saeedrahimi.poolnegar.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nshmura.recyclertablayout.RecyclerTabLayout;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import ir.saeedrahimi.poolnegar.R;

public class TransactionFragmentAdapter extends RecyclerTabLayout.Adapter<TransactionFragmentAdapter.ViewHolder> {

    private TransactionPagerAdapter mAdapater;

    public TransactionFragmentAdapter(ViewPager viewPager) {
        super(viewPager);

        mAdapater = (TransactionPagerAdapter) mViewPager.getAdapter();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Inflate your view.
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_recycler_tab_header, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // Bind data
        if (position == getCurrentIndicatorPosition()) {
            //Highlight view
        }
        holder.btnHeaderTitle.setText(mAdapater.getPageTitle(position));
        holder.btnHeaderTitle.setSelected(position == getCurrentIndicatorPosition());
    }

    @Override
    public int getItemCount() {
        return mAdapater.getCount();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView btnHeaderTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            btnHeaderTitle = itemView.findViewById(R.id.btnHeaderTitle);

            btnHeaderTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getViewPager().setCurrentItem(getAdapterPosition());
                }
            });
        }
    }
}

