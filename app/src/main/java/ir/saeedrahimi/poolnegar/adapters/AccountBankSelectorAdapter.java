//package ir.saeedrahimi.poolnegar.adapters;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//
//import ir.saeedrahimi.poolnegar.R;
//import ir.saeedrahimi.poolnegar.database.model.Account;
//import ir.saeedrahimi.poolnegar.dialogs.DialogAccountSelector;
//
//public class AccountBankSelectorAdapter extends ArrayAdapter {
//	private String[] accounts;
//	private DialogAccountSelector trans;
//
//	public AccountBankSelectorAdapter(
//			DialogAccountSelector dialogAccountSelector, Context paramContext,
//			int paramInt, String[] paramList) {
//		super(paramContext, paramInt, paramList);
//		this.accounts = paramList;
//		this.trans = dialogAccountSelector;
//	}
//
//	public View getView(final int position, View convertView, ViewGroup parent) {
//		{
//			View rowView = ((LayoutInflater) this.trans
//					.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
//					R.layout.accselector_bank_selectionrow, parent, false);
//
//			rowView.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View arg0) {
//					Account item = (Account) trans.tempList
//							.get(position);
//
//					trans.doBank(item);
//				}
//			});
//			return rowView;
//		}
//	}
//}
