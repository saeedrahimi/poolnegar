package ir.saeedrahimi.poolnegar;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;


import com.woxthebox.draglistview.DragListView;
import com.woxthebox.draglistview.swipe.ListSwipeHelper;
import com.woxthebox.draglistview.swipe.ListSwipeItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import ir.saeedrahimi.poolnegar.database.DAO.PersonDAO;
import ir.saeedrahimi.poolnegar.databinding.ActivityPersonOrderBinding;
import ir.saeedrahimi.poolnegar.dialogs.ProgressLoading;
import ir.saeedrahimi.poolnegar.models.PersonItem;
import ir.saeedrahimi.poolnegar.ui.adapters.PersonDraggableAdapter;
import ir.saeedrahimi.poolnegar.ui.helper.SnackBarHelper;

public class PersonOrderActivity extends BasePremiumActivity<ActivityPersonOrderBinding> implements View.OnClickListener {
    private DragListView mDragListView;
    PersonDraggableAdapter cmdAdapter;
    List<PersonItem> listPersons;
    AsyncLoadDB async;

    int mLastPosition=0;
    private boolean loading = true;


    @Override
    protected void onActivityFirstInit(Bundle savedInstanceState) {
        initComponents();
    }
    private void initComponents() {
        mDragListView = findViewById(R.id.dlvData);
        mDragListView.getRecyclerView().setVerticalScrollBarEnabled(true);
        mDragListView.setDragListListener(new DragListView.DragListListenerAdapter() {
            @Override
            public void onItemDragStarted(int position) {
            }

            @Override
            public void onItemDragEnded(int fromPosition, int toPosition) {
                if (fromPosition != toPosition) {
                    Collections.swap(listPersons,fromPosition, toPosition);
                    sortOnDatabase();
                }
            }
        });
        mDragListView.setSwipeListener(new ListSwipeHelper.OnSwipeListenerAdapter() {
            @Override
            public void onItemSwipeStarted(ListSwipeItem item) {
            }

            @Override
            public void onItemSwipeEnded(ListSwipeItem item, ListSwipeItem.SwipeDirection swipedDirection) {

                // Swipe to delete on left
                if (swipedDirection == ListSwipeItem.SwipeDirection.LEFT) {

                    Pair<Long, String> adapterItem = (Pair<Long, String>) item.getTag();
                    int pos = (int) item.getTag();
                    //mDragListView.getAdapter().removeItem(pos);

                    // TODO: Fix This
                    //_db.deleteDefaultTransaction(mAdapter.getItem(which).getTransactionId());
                    //listPersons.remove(mAdapter.getItem(which));
                    //listPersons.notify();
                    RefreshAll();
                    //mAdapter.remove(mAdapter.getItem(which));


                    sortOnDatabase();
                }
            }
        });

        this.listPersons = new ArrayList<>();

        setupListRecyclerView();
        RefreshAll();
    }
    private void setupListRecyclerView() {
        mDragListView.setLayoutManager(new LinearLayoutManager(this));
        cmdAdapter = new PersonDraggableAdapter(listPersons,R.layout.list_item_transaction_defaults, R.id.drag_handle, true);
        mDragListView.setAdapter(cmdAdapter, true);
        mDragListView.setCanDragHorizontally(false);
        mDragListView.setCustomDragItem(null);
    }

    private void setupGridVerticalRecyclerView() {
        mDragListView.setLayoutManager(new GridLayoutManager(this, 4));
        cmdAdapter = new PersonDraggableAdapter(listPersons,R.layout.list_item_transaction_defaults, R.id.drag_handle, true);
        mDragListView.setAdapter(cmdAdapter, true);
        mDragListView.setCanDragHorizontally(true);
        mDragListView.setCustomDragItem(null);

    }

    private void setupGridHorizontalRecyclerView() {
        mDragListView.setLayoutManager(new GridLayoutManager(this, 4, LinearLayoutManager.HORIZONTAL, false));
        cmdAdapter = new PersonDraggableAdapter(listPersons,R.layout.list_item_transaction_defaults, R.id.drag_handle, true);
        mDragListView.setAdapter(cmdAdapter, true);
        mDragListView.setCanDragHorizontally(true);
        mDragListView.setCustomDragItem(null);
    }

    @Override
    protected ActivityPersonOrderBinding getViewBinding() {
        return ActivityPersonOrderBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void onActionBarRightToolClicked() {

    }

    @Override
    protected void onActionBarLeftToolClicked() {

    }

    @Override
    protected String getActionBarTitle() {
        return "ترتیب نمایش نیروی کار";
    }

    public void RefreshAll()
    {
        if(async != null && !async.isCancelled())
            async.cancel(true);
        async = new AsyncLoadDB();
        this.listPersons.clear();
        async.execute();
    }
    public void sortOnDatabase(){
        for (PersonItem person:
                listPersons) {
            person.setOrder(listPersons.indexOf(person));
            try {
                PersonDAO.update(person);
            } catch (Exception e) {
                SnackBarHelper.showSnack(this, SnackBarHelper.SnackState.Error,"خطا در ذخیره اطلاعات");
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onClick(View v) {

    }


    class AsyncLoadDB extends AsyncTask<Integer, List<PersonItem>, List<PersonItem>> {
        ProgressLoading p;
        @Override
        protected void onPreExecute() {
            //this.p = new ProgressLoading(FragmentSettingsProjectCommands.this.getActivity(),"");

        }

        @Override
        protected List<PersonItem> doInBackground(Integer... values) {
            List<PersonItem> temp_inlist;
            temp_inlist = PersonDAO.getAll();
            PersonOrderActivity.this.listPersons.addAll(temp_inlist);

            return null;
        }

        @Override
        protected void onPostExecute(List<PersonItem> result) {
            if(this.p != null)
                this.p.done();

            if(PersonOrderActivity.this.mDragListView.getAdapter() != null)
                PersonOrderActivity.this.cmdAdapter.notifyDataSetChanged();
            else
                PersonOrderActivity.this.mDragListView.setAdapter(PersonOrderActivity.this.cmdAdapter, true);

            PersonOrderActivity.this.loading = false;

            if(this.p != null)
                this.p.done();
            super.onPostExecute(result);
        }
    }
}
