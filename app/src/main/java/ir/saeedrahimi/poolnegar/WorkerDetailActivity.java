package ir.saeedrahimi.poolnegar;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.viewpagerindicator.TitlePageIndicator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import ir.saeedrahimi.poolnegar.Gateways.WorkDayDAO;
import ir.saeedrahimi.poolnegar.adapters.TransactionListAdapter;
import ir.saeedrahimi.poolnegar.adapters.WorkDayListAdapter;
import ir.saeedrahimi.poolnegar.application.helper.KeyHelper;
import ir.saeedrahimi.poolnegar.customs.TypeFaceProvider;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.database.DAO.PersonDAO;
import ir.saeedrahimi.poolnegar.database.DAO.TransactionDAO;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
import ir.saeedrahimi.poolnegar.databinding.ActivityWorkerDetailsBinding;
import ir.saeedrahimi.poolnegar.dialogs.DialogPayBill;
import ir.saeedrahimi.poolnegar.dialogs.DialogTransactionDetails;
import ir.saeedrahimi.poolnegar.dialogs.ProgressLoading;
import ir.saeedrahimi.poolnegar.models.PersonItem;
import ir.saeedrahimi.poolnegar.models.TransactionFilter;
import ir.saeedrahimi.poolnegar.models.WorkDayItem;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_ProjectDetails;
import ir.saeedrahimi.poolnegar.ui.activities.cu.ActivityCU_Base;
import ir.saeedrahimi.poolnegar.ui.activities.cu.Activity_Transaction;
import ir.saeedrahimi.poolnegar.ui.helper.SnackBarHelper;


public class WorkerDetailActivity extends BasePremiumActivity<ActivityWorkerDetailsBinding> implements OnItemClickListener,
        DialogTransactionDetails.OnDialogClickListener,
        DialogTransactionDetails.OnTransactionEdit,
        DialogTransactionDetails.OnTransactionDelete,
        ViewPager.OnPageChangeListener
        , OnScrollListener {
    public TitlePageIndicator mIndicator;
    public CardView mHeader;
    public View mPlaceHolderView;
    public int mHeaderHeight;
    public int mMinHeaderTranslation;
    public boolean mFirstAsync = false;
    public AccelerateDecelerateInterpolator mSmoothInterpolator;
    public LinearLayout paysEmptyview;
    public LinearLayout projectsEmptyview;
    public LinearLayout daysEmptyview;
    public ListView lvPays;
    public ListView lvProjects;
    public ListView lvDays;
    cAdapter mAdapter;
    ViewPager viewPager;
    PersonItem mWorker;
    double workedDays;
    ArrayAdapter<TransactionItem> paysAdapter, projectsAdapter;
    ArrayAdapter<WorkDayItem> daysAdapter;
    List<TransactionItem> listPayTrans;
    List<WorkDayItem> listProjectTrans;
    List<WorkDayItem> listDays;
    TransactionFilter filter;
    AsyncLoadDB async;


    private int visibleThreshold = 10;
    private int currentDayPage = 0;
    private int currentPayPage = 0;
    private int currentProjectPage = 0;
    private boolean loading = true;
    private int lastDayListSize = 1;
    private int lastPayListSize = 1;
    private int lastProjectListSize = 1;
    private Integer loadType = 2;


    @Override
    protected void onActivityFirstInit(Bundle savedInstanceState) {
        initComponents();
    }

    private void initComponents() {
        mAdapter = new cAdapter(new String[]{"پروژه‌ها", "پرداخت‌ها", "حضور کاری"});
        viewPager = findViewById(R.id.bill_pager);
        viewPager.setAdapter(mAdapter);
        mIndicator = findViewById(R.id.bill_pager_titles);
        mIndicator.setViewPager(viewPager);
        viewPager.setOffscreenPageLimit(3);
        mIndicator.setOnPageChangeListener(this);
        Typeface typeface = TypeFaceProvider.get(this, "vazir.ttf");
        mIndicator.setTypeface(typeface);
        mIndicator.setSelectedColor(getResources().getColor(
                R.color.Artem));

        mSmoothInterpolator = new AccelerateDecelerateInterpolator();
        mHeaderHeight = getResources().getDimensionPixelSize(R.dimen.project_header_height);
        mMinHeaderTranslation = -mHeaderHeight + 90;
        mHeader = findViewById(R.id.cv_worker);

        Intent localBundle = getIntent();

        this.mWorker = (PersonItem) localBundle.getSerializableExtra("worker");
        this.listPayTrans = new ArrayList<>();
        this.listProjectTrans = new ArrayList<>();
        this.listDays = new ArrayList<>();

        this.paysAdapter = new TransactionListAdapter(this, listPayTrans, 0);
        //this.projectsAdapter = new TransactionListAdapter(this, listProjectTrans, 0);
        this.daysAdapter = new WorkDayListAdapter(this, listDays, 0);
        RefreshAll();
    }


    @Override
    protected ActivityWorkerDetailsBinding getViewBinding() {
        return ActivityWorkerDetailsBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void onActionBarRightToolClicked() {

    }

    @Override
    protected void onActionBarLeftToolClicked() {

    }

    @Override
    protected String getActionBarTitle() {
        return null;
    }

    private void RefreshHeader() {
        TextView lblWorkedDays = findViewById(R.id.lbl_worked_days);
        TextView lblWorkedOverTime = findViewById(R.id.lbl_worked_overtime);
        TextView lblWorkedAmount = findViewById(R.id.lbl_worked_amount);
        TextView lblPayedAmount = findViewById(R.id.lbl_payed_amount);

        mWorker = PersonDAO.getById(mWorker.getId());
        //lblWorkedDays.setText(new DecimalFormat(" ###,###.## ").format(this.workedDays * -1));
        lblWorkedDays.setText(String.format("%d روز", WorkDayDAO.getWorkerDaysCount(mWorker.getId())));
        String overtime = WorkDayDAO.getWorkerOvertimeHours(mWorker.getId(), true);
        lblWorkedOverTime.setText(overtime);
        if (overtime.contains("-"))
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                lblWorkedOverTime.setTextColor(getResources().getColor(R.color.ColorAccent, getTheme()));
            } else {
                lblWorkedOverTime.setTextColor(getResources().getColor(R.color.ColorAccent));
            }
        lblWorkedAmount.setText(new DecimalFormat(" ###,###.## ").format(PersonDAO.getTotalWorkedAmount(mWorker.getAcID())));
        lblPayedAmount.setText(new DecimalFormat(" ###,###.## ").format(PersonDAO.getPaysAmount(mWorker.getAcID())));
        setActionBarTitle("کارگر" + " - " + this.mWorker.getName());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.worker_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify height parent activity in AndroidManifest.xml.
        Intent myIntent;
        switch (item.getItemId()) {
            case R.id.mnu_worker_edit:

                myIntent = new Intent(this, EditPersonActivity.class);
                myIntent.putExtra("PersonID", this.mWorker.getId());
                myIntent.putExtra("ParentId", 0);
                startActivityForResult(myIntent, Constants.RESULTS.EDIT);
                break;
            case R.id.mnu_worker_pay:
                myIntent = new Intent(this,
                        DialogPayBill.class);

                myIntent.putExtra("targetAccount", mWorker.getBillAcId());
                myIntent.putExtra("accId", mWorker.getAcID());
                startActivityForResult(myIntent, Constants.RESULTS.PAY);
                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    public void RefreshAll() {
        RefreshHeader();
        if (async != null && !async.isCancelled())
            async.cancel(true);
        async = new AsyncLoadDB();
        this.loadType = 3;
        this.lastDayListSize = 1;
        this.lastPayListSize = 1;
        this.lastProjectListSize = 1;
        this.listDays.clear();
        this.listPayTrans.clear();
        this.listProjectTrans.clear();
        async.execute(new Integer[]{this.currentDayPage, this.currentPayPage, this.currentProjectPage, this.loadType});
    }

    @Override
    public void onDestroy() {
        if (async != null && !async.isCancelled())
            async.cancel(true);
        super.onDestroy();
    }

    public void LoadMore() {
        if (async != null && !async.isCancelled())
            async.cancel(true);
        async = new AsyncLoadDB();
        async.execute(new Integer[]{this.currentDayPage, this.currentPayPage, this.currentProjectPage, this.loadType});
    }

    @Override
    public void onItemClick(AdapterView<?> adp, View v, final int position, long arg3) {

        if (v.getTag() != null && v.getTag() == "day") {
            List<WorkDayItem> transList = this.listDays;
            final Dialog menuDialog = new Dialog(this);
            menuDialog.requestWindowFeature(1);
            final WorkDayItem wd = listDays.get(position - 1);
            menuDialog.getWindow().setBackgroundDrawable(new ColorDrawable(17170445));
            menuDialog.setContentView(R.layout.itemlongpress);
            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.95);
            int height = menuDialog.getWindow().getAttributes().height;
            menuDialog.getWindow().setLayout(width, height);
            Button btnDel = menuDialog.findViewById(R.id.fab_delete);
            Button btnEdit = menuDialog.findViewById(R.id.fab_edit);
            Button btnReport = menuDialog.findViewById(R.id.fab_reports);
            menuDialog.findViewById(R.id.ll_fab_edit).setVisibility(View.GONE);
            menuDialog.findViewById(R.id.ll_fab_report).setVisibility(View.GONE);

            TextView txtTitle = menuDialog.findViewById(R.id.txtTitle);
            txtTitle.setText(wd.getWorkFaDate());
            btnDel.setOnClickListener(arg0 -> {

                final Dialog questionDialog = new Dialog(WorkerDetailActivity.this);
                questionDialog.requestWindowFeature(1);
                questionDialog.setContentView(R.layout.dialog_warning);
                TextView error_title = questionDialog.findViewById(R.id.yesnotitle);
                TextView error_body = questionDialog.findViewById(R.id.yesnomessage);
                Button ok_btn = questionDialog.findViewById(R.id.dialog_yes);
                Button cancel_btn = questionDialog.findViewById(R.id.dialog_no);
                error_title.setText("حذف ");
                error_body.setText("آیا میخواهید کارکرد این روز را حذف کنید ؟");
                ok_btn.setOnClickListener(arg012 -> {
                    SnackBarHelper.showSnack(WorkerDetailActivity.this, SnackBarHelper.SnackState.Info, "کارکرد روز '" + wd.getWorkFaDate() + "' حذف شد.");

                    PersonDAO.setWorkerAbsent(mWorker, wd.getWorkFaDate());
                    listDays.remove(position);
                    daysAdapter.notifyDataSetChanged();
                    questionDialog.dismiss();
                    menuDialog.dismiss();


                });
                cancel_btn.setOnClickListener(arg01 -> {
                    questionDialog.dismiss();
                    menuDialog.dismiss();

                });
                questionDialog.show();
            });
            menuDialog.show();

        } else {
            if (position == 0)
                return;
            if (adp.equals(WorkerDetailActivity.this.lvPays))
                new DialogTransactionDetails(this, this, this, this, this, this.listPayTrans.get(position - 1));
           /* else
                new DialogTransactionDetails(this, this, this, this, this, this.listProjectTrans.get(position - 1), position - 1);*/
        }
    }

    @Override
    public void onDialogDeleteTransaction() {
        RefreshAll();
    }

    @Override
    public void onDialogEditTransaction(TransactionItem transaction) {
        Intent intent = new Intent(this, Activity_Transaction.class);
        intent.putExtra(KeyHelper.KEY_ENTITY, transaction);
        intent.putExtra(KeyHelper.KEY_ACTIVITY_CU_STATE, ActivityCU_Base.ACTIVITY_STATE_EDIT_MODE);
        startActivityForResult(intent, KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION);
    }

    @Override
    public void onDialogDeleteClick() {
        RefreshAll();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION) {
            if (resultCode == RESULT_OK) {
                RefreshAll();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageSelected(int position) {
        int scrollHeight = (int) (mHeader.getHeight() + mHeader.getTranslationY());


        if (position == 2) {
            if (scrollHeight == 0 && lvDays.getFirstVisiblePosition() >= 1) {
                return;
            }
            lvDays.setSelectionFromTop(1, scrollHeight);
        }
        if (position == 1) {
            if (scrollHeight == 0 && lvPays.getFirstVisiblePosition() >= 1) {
                return;
            }
            lvPays.setSelectionFromTop(1, scrollHeight);
        }
        if (position == 0) {
            if (scrollHeight == 0 && lvProjects.getFirstVisiblePosition() >= 1) {
                return;
            }
            lvProjects.setSelectionFromTop(1, scrollHeight);
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        int scrollY = getScrollY((ListView) view);
        mHeader.setTranslationY(Math.max(-scrollY, mMinHeaderTranslation));
        if (((String) view.getTag()).contains("loadMore")) {
            int l = visibleItemCount + firstVisibleItem;
            if (l >= totalItemCount && !loading) {
                if (this.viewPager.getCurrentItem() == 2) {
                    this.loadType = 0;
                    this.currentDayPage++;
                }
                if (this.viewPager.getCurrentItem() == 1) {
                    this.loadType = 1;
                    this.currentPayPage++;
                }
                if (this.viewPager.getCurrentItem() == 0) {
                    this.loadType = 2;
                    this.currentProjectPage++;
                }
                loading = true;
                LoadMore();
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView arg0, int arg1) {
    }

    public int getScrollY(ListView lv) {
        View c = lv.getChildAt(0);
        if (c == null) {
            return 0;
        }

        int firstVisiblePosition = lv.getFirstVisiblePosition();
        int top = c.getTop();

        int headerHeight = 0;
        if (firstVisiblePosition >= 1) {
            headerHeight = mPlaceHolderView.getHeight();
        }

        return -top + firstVisiblePosition * c.getHeight() + headerHeight;
    }

    class AsyncLoadDB extends AsyncTask<Integer, List<TransactionItem>, List<TransactionItem>> {
        ProgressLoading p;

        public void execute(int[] parentId) {
        }

        @Override
        protected void onPreExecute() {
            if (WorkerDetailActivity.this.lastPayListSize > 0 && WorkerDetailActivity.this.viewPager.getCurrentItem() == 1) {
                this.p = new ProgressLoading(WorkerDetailActivity.this, "");
            } else if (WorkerDetailActivity.this.lastProjectListSize > 0 && WorkerDetailActivity.this.viewPager.getCurrentItem() == 0) {
                this.p = new ProgressLoading(WorkerDetailActivity.this, "");
            }
        }

        @Override
        protected List<TransactionItem> doInBackground(Integer... values) {

            int dayPage = values[0] * WorkerDetailActivity.this.visibleThreshold;
            int payPage = values[1] * WorkerDetailActivity.this.visibleThreshold;
            int projectPage = values[2] * WorkerDetailActivity.this.visibleThreshold;
            int loadType = values[3];
            List<TransactionItem> temp_paylist = new ArrayList<>();
            List<WorkDayItem> temp_projectlist = new ArrayList<>();
            List<WorkDayItem> temp_dayslist = new ArrayList<>();
            Log.i("dayPage: ", dayPage + "");
            Log.i("payPage: ", payPage + "");
            Log.i("projectPage: ", projectPage + "");

            if (WorkerDetailActivity.this.lastDayListSize > 0 && (loadType == 0 || loadType == 3)) {

                temp_dayslist = WorkDayDAO.getAllDays(WorkerDetailActivity.this.mWorker.getId(), dayPage);
                WorkerDetailActivity.this.listDays.addAll(temp_dayslist);
                WorkerDetailActivity.this.lastDayListSize = temp_dayslist.size();
            }

            if (WorkerDetailActivity.this.lastPayListSize > 0 && (loadType == 1 || loadType == 3)) {
                temp_paylist = TransactionDAO.getAccountPayments(WorkerDetailActivity.this.mWorker.getBillAcId());
                WorkerDetailActivity.this.listPayTrans.addAll(temp_paylist);
                WorkerDetailActivity.this.lastPayListSize = temp_paylist.size();
            }
            if (WorkerDetailActivity.this.lastProjectListSize > 0 && (loadType == 2 || loadType == 3)) {
                temp_projectlist = WorkDayDAO.getAllProjects(mWorker.getId(), projectPage);
                WorkerDetailActivity.this.listProjectTrans.addAll(temp_projectlist);
                WorkerDetailActivity.this.lastProjectListSize = temp_projectlist.size();
            }

            Log.i("temp_dayslist size: ", temp_dayslist.size() + "");
            Log.i("temp_paylist size: ", temp_paylist.size() + "");
            Log.i("temp_projectlist size: ", temp_projectlist.size() + "");

            return null;
        }

        @Override
        protected void onPostExecute(List<TransactionItem> result) {
            WorkerDetailActivity.this.mPlaceHolderView = WorkerDetailActivity.this.getLayoutInflater().inflate(R.layout.project_header_placeholder, lvProjects, false);
            if (WorkerDetailActivity.this.mFirstAsync != true) {
                WorkerDetailActivity.this.lvDays.addHeaderView(mPlaceHolderView);
                WorkerDetailActivity.this.lvPays.addHeaderView(mPlaceHolderView);
                WorkerDetailActivity.this.lvProjects.addHeaderView(mPlaceHolderView);
                WorkerDetailActivity.this.lvDays.setAdapter(WorkerDetailActivity.this.daysAdapter);
                WorkerDetailActivity.this.lvPays.setAdapter(WorkerDetailActivity.this.paysAdapter);
                //WorkerDetailActivity.this.lvProjects.setAdapter(WorkerDetailActivity.this.projectsAdapter);
                ArrayAdapter<WorkDayItem> adapter;
                WorkerDetailActivity.this.lvProjects.setAdapter(
                        adapter = new ArrayAdapter<WorkDayItem>(WorkerDetailActivity.this, 17367043, WorkerDetailActivity.this.listProjectTrans) {
                            @Override
                            public View getView(final int position, View convertView, ViewGroup parent) {
                                final WorkDayItem project = WorkerDetailActivity.this.listProjectTrans.get(position);
                                TextView txtAmount, txtDay, txtTitle;

                                if (convertView == null) {
                                    convertView = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.items_workday_project, parent, false);

                                }
                                txtTitle = convertView.findViewById(R.id.txt_name);
                                txtTitle.setText(project.getInfo());
                                txtDay = convertView.findViewById(R.id.txt_day);
                                txtDay.setText((int) project.getLackHour() + "(روز) ");

                                txtAmount = convertView.findViewById(R.id.txt_amount);
                                txtAmount.setText(new DecimalFormat(" ###,###.## ").format(project.getOvertime()));


                                convertView.setOnClickListener(view -> {
                                    Intent intent = new Intent(WorkerDetailActivity.this, Activity_ProjectDetails.class);
                                    intent.putExtra("accId", project.getProjectID());
                                    intent.putExtra("name", project.getInfo());
                                    intent.putExtra("balance", AccountDAO.selectByID(project.getProjectID()).getBalance());
                                    startActivity(intent);

                                });
                                return convertView;
                            }
                        });
            }

            if (WorkerDetailActivity.this.lastDayListSize > 0 && (WorkerDetailActivity.this.loadType == 0 || WorkerDetailActivity.this.loadType == 3)) {
                WorkerDetailActivity.this.daysAdapter.notifyDataSetChanged();
            }

            if (WorkerDetailActivity.this.lastPayListSize > 0 && (WorkerDetailActivity.this.loadType == 1 || WorkerDetailActivity.this.loadType == 3)) {
                WorkerDetailActivity.this.paysAdapter.notifyDataSetChanged();
            }
            if (WorkerDetailActivity.this.lastProjectListSize > 0 && (WorkerDetailActivity.this.loadType == 2 || WorkerDetailActivity.this.loadType == 3)) {
                //WorkerDetailActivity.this.projectsAdapter.notifyDataSetChanged();
            }
            if (WorkerDetailActivity.this.listDays == null || WorkerDetailActivity.this.listDays.size() <= 0) {
                View txtNoItem = WorkerDetailActivity.this.getLayoutInflater().inflate(R.layout.blank_textview, null);
                ((TextView) txtNoItem.findViewById(R.id.blank_textview)).setText("موردی یافت نشد!");
                WorkerDetailActivity.this.daysEmptyview.addView(txtNoItem);
            }
            if (WorkerDetailActivity.this.listPayTrans == null || WorkerDetailActivity.this.listPayTrans.size() <= 0) {
                View txtNoItem = WorkerDetailActivity.this.getLayoutInflater().inflate(R.layout.blank_textview, null);
                ((TextView) txtNoItem.findViewById(R.id.blank_textview)).setText("موردی یافت نشد!");
                WorkerDetailActivity.this.paysEmptyview.addView(txtNoItem);
            }
            if (WorkerDetailActivity.this.listProjectTrans == null || WorkerDetailActivity.this.listProjectTrans.size() <= 0) {
                View txtNoItem = WorkerDetailActivity.this.getLayoutInflater().inflate(R.layout.blank_textview, null);
                ((TextView) txtNoItem.findViewById(R.id.blank_textview)).setText("موردی یافت نشد!");
                WorkerDetailActivity.this.projectsEmptyview.addView(txtNoItem);
            }


            if (WorkerDetailActivity.this.lastDayListSize == 10) {
                WorkerDetailActivity.this.lvDays.setTag("loadMore");
            } else if (loadType == 0 && !WorkerDetailActivity.this.lvDays.getTag().toString().contains("ended")) {
                if (listDays.size() > 0)
                    SnackBarHelper.showSnack(WorkerDetailActivity.this, SnackBarHelper.SnackState.Info, "به انتهای لیست حضور کاری رسیدید");
                WorkerDetailActivity.this.lvDays.setTag("ended");
            }

            if (WorkerDetailActivity.this.lastPayListSize == 10) {
                WorkerDetailActivity.this.lvPays.setTag("loadMore");
            } else if (loadType == 1 && !WorkerDetailActivity.this.lvPays.getTag().toString().contains("ended")) {
                if (listPayTrans.size() > 0)
                    SnackBarHelper.showSnack(WorkerDetailActivity.this, SnackBarHelper.SnackState.Info, "به انتهای لیست پرداختی ها رسیدید");
                WorkerDetailActivity.this.lvPays.setTag("ended");
            }
            if (WorkerDetailActivity.this.lastProjectListSize == 10) {
                WorkerDetailActivity.this.lvProjects.setTag("loadMore");
            } else if (loadType == 2 && !WorkerDetailActivity.this.lvProjects.getTag().toString().contains("ended")) {
                if (listProjectTrans.size() > 0)
                    SnackBarHelper.showSnack(WorkerDetailActivity.this, SnackBarHelper.SnackState.Info, "به انتهای لیست پروژه ها رسیدید");
                WorkerDetailActivity.this.lvProjects.setTag("ended");
            }

            WorkerDetailActivity.this.loading = false;
            if (!WorkerDetailActivity.this.mFirstAsync) {
                mIndicator.setCurrentItem(mAdapter.getCount() - 1);
                WorkerDetailActivity.this.mFirstAsync = true;
            }
            if (this.p != null)
                this.p.done();

            super.onPostExecute(result);
            WorkerDetailActivity.this.lvDays.setOnScrollListener(WorkerDetailActivity.this);
            WorkerDetailActivity.this.lvPays.setOnScrollListener(WorkerDetailActivity.this);
            WorkerDetailActivity.this.lvProjects.setOnScrollListener(WorkerDetailActivity.this);

        }
    }

    class cAdapter extends PagerAdapter {

        private final String[] TITLES;

        public cAdapter(String[] paramArrayOfString) {
            this.TITLES = paramArrayOfString;
        }

        @Override
        public int getCount() {
            return this.TITLES.length;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return this.TITLES[position];
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater localLayoutInflater = WorkerDetailActivity.this.getLayoutInflater();
            switch (position) {

                case 0:
                    View outLayout = localLayoutInflater.inflate(R.layout.layout_list, null);
                    WorkerDetailActivity.this.projectsEmptyview = outLayout.findViewById(R.id.emptyview);
                    WorkerDetailActivity.this.lvProjects = outLayout.findViewById(R.id.list);
                    WorkerDetailActivity.this.lvProjects.setTag("lvBill");
                    WorkerDetailActivity.this.lvProjects.setOnItemClickListener(WorkerDetailActivity.this);
                    container.addView(outLayout, 0);
                    return outLayout;
                case 1:
                    View listLayoutOut = localLayoutInflater.inflate(R.layout.layout_list, null);
                    WorkerDetailActivity.this.paysEmptyview = listLayoutOut.findViewById(R.id.emptyview);
                    WorkerDetailActivity.this.lvPays = listLayoutOut.findViewById(R.id.list);
                    WorkerDetailActivity.this.lvPays.setTag("lvPays");
                    WorkerDetailActivity.this.lvPays.setOnItemClickListener(WorkerDetailActivity.this);

                    container.addView(listLayoutOut, 0);
                    return listLayoutOut;
                case 2:
                    View eeee = localLayoutInflater.inflate(R.layout.layout_list, null);
                    WorkerDetailActivity.this.daysEmptyview = eeee.findViewById(R.id.emptyview);
                    WorkerDetailActivity.this.lvDays = eeee.findViewById(R.id.list);
                    //WorkerDetailActivity.this.lvDays.setPadding(5, WorkerDetailActivity.this.mHeaderHeight + 5, 5, 5);
                    WorkerDetailActivity.this.lvDays.setTag("days");
                    WorkerDetailActivity.this.lvDays.setOnItemClickListener(WorkerDetailActivity.this);
                    container.addView(eeee, 0);
                    return eeee;
                default:
                    return null;
            }

        }


    }

}
