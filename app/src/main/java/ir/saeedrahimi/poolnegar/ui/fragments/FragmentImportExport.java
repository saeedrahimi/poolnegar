package ir.saeedrahimi.poolnegar.ui.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar;
import ir.saeedrahimi.poolnegar.customs.Utils;
import ir.saeedrahimi.poolnegar.dialogs.DialogYesNo;
import ir.saeedrahimi.poolnegar.dialogs.MessageDialog;
import ir.saeedrahimi.poolnegar.ui.activities.MainActivity;

import static ir.saeedrahimi.poolnegar.Constants.PERMISSIONS.REQUEST_WRITE_EXTERNAL_STORAGE;

public class FragmentImportExport extends BaseFragment implements View.OnClickListener, MessageDialog.OnDialogClickListener, DialogYesNo.onYesNoDialogClick{

	String backupDBPath;
	String backupDBFile;
	String backupFileExt;
	Context context;
	String currentDBPath;
	List<File> restoreFiles;
	File selFile;
	File data;
	int fileexists;
	int overwrite;
	File sd;
	ListView listView;
	AsyncLoadFiles asynclf;
	public FragmentImportExport() {

	}

	public List<File> GetRestoreFiles(File path) {
		ArrayList<File> listFiles = new ArrayList<File>();
		if (!path.exists()) {
			return listFiles;
		}
		File[] subFiles = path.listFiles();
		Arrays.sort(subFiles, new Comparator<File>() {
		    public int compare(File f1, File f2) {
		        return Long.compare(f2.lastModified(), f1.lastModified());
		    }
		});
		for (File file : subFiles) {
			if (file.getName().endsWith(this.backupFileExt)) {
				listFiles.add(file);
			}
		}
		
		
		return listFiles;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_import_export,container, false);
		return view;
	}
	 public void CreateFolder()
	 {
	   File file = new File(this.sd + this.backupDBPath);
	   if (!file.exists()) {
		   file.mkdirs();
	   }
	 }
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {

		this.context = getActivity();
		this.sd = Environment.getExternalStorageDirectory();
		this.data = Environment.getDataDirectory();
		this.currentDBPath = Constants.currentDBPath;
		this.backupDBPath = Constants.backupDBPath;
		this.backupFileExt = Constants.backupFileExt;
		PersianCalendar todayCalendar = new PersianCalendar();
		todayCalendar.setDelimiter("-");
		this.backupDBFile = backupDBPath +"/poolnegar-"+ todayCalendar.getPersianShortDate()  +this.backupFileExt;
		this.fileexists = 0;
		this.overwrite = 1;
		if (new File(this.sd, this.backupDBFile).exists()) {
			this.fileexists = 1;
		}
		CreateFolder();
		getView().findViewById(R.id.newbackup).setOnClickListener(this);
		if(PackageManager.PERMISSION_GRANTED==ActivityCompat.checkSelfPermission(context,Manifest.permission.WRITE_EXTERNAL_STORAGE)){
			asynclf = new AsyncLoadFiles();
			asynclf.execute();
		}else{
			requestPermission(context);
		}
		listView = ((ListView)getView().findViewById(R.id.listdialog));

		super.onViewCreated(view, savedInstanceState);
	}

	private  void requestPermission(final Context context){
		if(ActivityCompat.shouldShowRequestPermissionRationale((Activity)context,Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
			// Provide an additional rationale to the user if the permission was not granted
			// and the user would benefit from additional context for the use of the permission.
			// For example if the user has previously denied the permission.

			new AlertDialog.Builder(context)
					.setMessage(R.string.perm_request_message)
					.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							FragmentImportExport.this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
									REQUEST_WRITE_EXTERNAL_STORAGE);
						}
					}).show();

		}else {
			FragmentImportExport.this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
					REQUEST_WRITE_EXTERNAL_STORAGE);
		}
	}
	@Override
	public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
		switch (requestCode) {
			case REQUEST_WRITE_EXTERNAL_STORAGE: {
				if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					Toast.makeText(context,"Success",
							Toast.LENGTH_SHORT).show();
					asynclf = new AsyncLoadFiles();
					asynclf.execute();
				} else {
					Toast.makeText(context,"Failed ",
							Toast.LENGTH_SHORT).show();
					super.onRequestPermissionsResult(requestCode, permissions, grantResults);
				}
				return;
			}
		}
	}
	public void Import()
	 {
	   try
	   {
	     if (this.sd.canWrite())
	     {
	       File curDBFile = new File(this.data, this.currentDBPath);
	       if (this.overwrite == 1)
	       {
	    	   new DialogYesNo(context, this, null, context.getResources().getString(R.string.headerieerror), context.getResources().getString(R.string.ieerror), context.getResources().getString(R.string.ieerror2), false, "", true, "Import");
	    	   return;
	       }
	       FileChannel inChannel = new FileInputStream(this.selFile).getChannel();
	       FileChannel outChannel = new FileOutputStream(curDBFile).getChannel();
	       outChannel.transferFrom(inChannel, 0L, inChannel.size());
	       inChannel.close();
	       outChannel.close();
	       this.overwrite = 1;
	       new MessageDialog(context, this, context.getResources().getString(R.string.success), context.getResources().getString(R.string.importcomplete), context.getResources().getString(R.string.importcomplete2), "importcomplete", true);
	       return;
	     }
	   }
	   catch (Exception e)
	   {
		 this.overwrite = 1;
	     new MessageDialog(context, null, context.getResources().getString(R.string.failed), context.getResources().getString(R.string.importfailed), context.getResources().getString(R.string.importfailed2), "importfailed", true);
		   Log.d("Import Error: ", e.getLocalizedMessage() + "");
	   }
	 }
	public void Export()
	 {
	   try
	   {
	     if (this.sd.canWrite())
	     {
	       File curFile = new File(this.data, this.currentDBPath);
	       File outFile = new File(this.sd, this.backupDBFile);
	       if (this.fileexists == 1)
	       {
	         new DialogYesNo(context, this, null, context.getResources().getString(R.string.headerieerror), context.getResources().getString(R.string.fileexists), context.getResources().getString(R.string.fileexists2), false, "", true, "Export");
	         return;
	       }
	       FileChannel inChannel = new FileInputStream(curFile).getChannel();
	       FileChannel outChannel = new FileOutputStream(outFile).getChannel();
	       outChannel.transferFrom(inChannel, 0L, inChannel.size());
	       inChannel.close();
	       outChannel.close();
	       new MessageDialog(context, null, context.getResources().getString(R.string.success), context.getResources().getString(R.string.exportcomplete), context.getResources().getString(R.string.exportcomplete2), "exportcomplete", true);
	       asynclf = new AsyncLoadFiles();
		   asynclf.execute();
	       if (new File(this.sd, this.backupDBFile).exists()) {
				this.fileexists = 1;
			}
	       return;
	     }
	   }
	   catch (Exception e)
	   {
	     new MessageDialog(context, null, context.getResources().getString(R.string.failed), context.getResources().getString(R.string.exportfailed), context.getResources().getString(R.string.exportfailed2), "exportfailed", true);
		   Log.d("Export Error: ", e.getLocalizedMessage() + "");
	   }
	 }
	public void Send()
	 {
	     if (this.sd.canWrite() && selFile.exists())
	     {
	       Intent sendIntent = new Intent();
	       sendIntent.setAction("android.intent.action.SEND");
	       sendIntent.setType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	       sendIntent.putExtra("android.intent.extra.TEXT", "نرم افزار حسابداری همراه پـول نـگار - "+ getResources().getString(ir.saeedrahimi.poolnegar.R.string.app_version));
	       sendIntent.putExtra("android.intent.extra.STREAM", Uri.fromFile(this.selFile));
	       getActivity().startActivity(Intent.createChooser(sendIntent, "اشتراک گذاری"));
	       return;
	     }
	 }
	public void Delete()
	 {
	   try
	   {
	     if (this.sd.canWrite())
	     {
	    	 new DialogYesNo(context, this, null, context.getResources().getString(R.string.headerieerror), context.getResources().getString(R.string.filedelete), context.getResources().getString(R.string.filedelete2), false, "", true, "Delete");
	     }
	   }
	   catch (Exception e)
	   {
	     new MessageDialog(context, null, context.getResources().getString(R.string.failed), context.getResources().getString(R.string.exportfailed), context.getResources().getString(R.string.exportfailed2), "exportfailed", true);
	   }
	 }
	class AsyncLoadFiles extends AsyncTask<Void, Boolean, Boolean> {
		ArrayAdapter<File> adapter;
		
		 @Override
		 protected void onPreExecute()
		  {
			 FragmentImportExport.this.restoreFiles = new ArrayList<File>();
		    super.onPreExecute();
		  }
		 @Override
		protected Boolean doInBackground(Void... paramVarArgs) {
			File dbPath = new File(FragmentImportExport.this.sd
					+ FragmentImportExport.this.backupDBPath);
			if (dbPath.exists()) {
				FragmentImportExport.this.restoreFiles = FragmentImportExport.this
						.GetRestoreFiles(dbPath);
				return true;
			}
			dbPath.mkdirs();
			Log.d("in resore page ", " there is no file database");
			return false;

		}
		@Override
		protected void onPostExecute(Boolean paramBoolean){
			
			FragmentImportExport.this.listView.setAdapter(adapter = new ArrayAdapter<File>(	FragmentImportExport.this.getActivity(),  android.R.layout.simple_list_item_1,	FragmentImportExport.this.restoreFiles) {
						@Override
						public View getView(final int position,	View convertView, ViewGroup parent) {
							TextView txtdate, txtTitle;
							
							final File file = FragmentImportExport.this.restoreFiles.get(position);
							
							if (convertView == null) {
								convertView = ((LayoutInflater) FragmentImportExport.this.getActivity().getSystemService(
												Context.LAYOUT_INFLATER_SERVICE)).inflate(
										R.layout.items_restore, parent, false);

							}
							txtTitle = ((TextView) convertView.findViewById(R.id.filetitle));
							String fileName = file.getName();
							fileName = fileName.substring(0, fileName.length()-4);
							txtTitle.setText(fileName);
							txtdate = ((TextView) convertView.findViewById(R.id.filedesc));
							txtdate.setText("حجم فایل: "+file.length()/1024+" کیلوبایت");
							if(file.length()/1024 >= 1024)
								txtdate.setText("حجم فایل: "+file.length()/1024+" مگابایت");
							
							ImageView btnMenu = ((ImageView)convertView.findViewById(R.id.restoreoverflow));
							convertView.setOnClickListener(new View.OnClickListener() {
	    						
	    						@Override
	    						public void onClick(View view) {
	    							 	PopupMenu popup = new PopupMenu(FragmentImportExport.this.getActivity(), view);
	    							 	android.view.MenuInflater inflater = popup.getMenuInflater();

	    							 	inflater.inflate(R.menu.menu_overflow_restore , popup.getMenu());
	    							 	popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
	    									
	    									@Override
	    									public boolean onMenuItemClick(android.view.MenuItem item) {
	    										selFile=file;
	    										
	    										if(item.getItemId()==R.id.menu_overflow_restore)
	    										{
	    											if(file.exists()){
	    												FragmentImportExport.this.Import();
	    											}
	    											return true;
	    										}
	    										if(item.getItemId()==R.id.menu_overflow_send)
	    										{
	    											FragmentImportExport.this.Send();
	    											return true;
	    										}
	    										if(item.getItemId()==R.id.menu_overflow_delete)
	    										{
	    											FragmentImportExport.this.Delete();
	    											return true;
	    										}
	    										return false;
	    									}
	    								});
	    							    popup.show();
	    							
	    						}
	    					});
							final View finalConvertView = convertView;
							btnMenu.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View view) {
									finalConvertView.performClick();
								}
							});
							
							return convertView;
						}
					});
		}

	}
	@Override
	public void onYesNoDialog(String token, boolean checked) {
		   if (token.equals("Import"))
		   {
		     this.overwrite = 0;
		     Import();
		   }
		   if(token.equals("Delete"))
		   {
			   selFile.delete();
			   asynclf = new AsyncLoadFiles();
				asynclf.execute();
				Crouton.makeText(getActivity(),
						"فایل پشتیبان حذف شد!", Style.INFO)
						.show();
			   return;
		   }
		   if (token.equals("Export")) {
			   String fileName = this.backupDBFile;
			   fileName = fileName.substring(0, fileName.length()-4);
			   int i=0;
			   do {
				   i++;
				   
				   
				   
			   } while (new File(this.sd, fileName + "_" + i + this.backupFileExt).exists());
			   fileName += "_" + i + this.backupFileExt;
			   this.backupDBFile = fileName;
			   Log.d("new FileName",fileName);
			   this.fileexists = 0;
			   Export();
		   return;
		   }
	}

	@Override
	public void onDialogOKClick(String paramString) {
		((MainActivity)this.context).finish();
		 new Utils(this.context).RestartApp();
		
	}

	@Override
	public void onClick(View item) {
		
		if(item.getId()==R.id.newbackup)
		{
			Export();
		}
		
	}

}
