package ir.saeedrahimi.poolnegar.ui.activities;

import android.Manifest;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import ir.saeedrahimi.poolnegar.BasePremiumActivity;
import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.PreferencesManager;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar;
import ir.saeedrahimi.poolnegar.customs.Utils;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.database.DAO.PersonDAO;
import ir.saeedrahimi.poolnegar.database.model.Account;
import ir.saeedrahimi.poolnegar.databinding.ActivityAddworkerBinding;
import ir.saeedrahimi.poolnegar.models.PersonItem;
import ir.saeedrahimi.poolnegar.ui.helper.SnackBarHelper;

import static android.R.layout.simple_spinner_dropdown_item;
import static android.R.layout.simple_spinner_item;

public class AddPersonActivity
        extends BasePremiumActivity<ActivityAddworkerBinding> implements View.OnClickListener {
  private static final int RESULT_DATE = 0;
  EditText txtInfo;
  EditText txtBalance;
  EditText txtName;
  EditText txtMobile;
  EditText txtPhone;
  EditText txtEmail;
  EditText txtFee;
  EditText txtOvertimeRate;
  EditText txtAccNumber;
  EditText txtCardNumber;
  Switch swLayout;
  Spinner spBalanceType;
  String parentId;
  PreferencesManager mPrefManager;
  boolean isWorker = false;



  @Override
  protected void onActivityFirstInit(Bundle savedInstanceState) {
    initComponents();
  }

  private void initComponents() {
    mPrefManager = new PreferencesManager(this.getApplicationContext());

    this.txtInfo = findViewById(R.id.txt_account_info);
    findViewById(R.id.layout_select_contact).setOnClickListener(this);
    txtName = findViewById(R.id.txt_name);
    txtMobile = findViewById(R.id.txt_mobile);
    txtPhone = findViewById(R.id.txt_phone);
    txtEmail = findViewById(R.id.txt_email);
    txtBalance = findViewById(R.id.txt_account_balance);
    txtFee = findViewById(R.id.txt_fee);
    txtOvertimeRate = findViewById(R.id.txt_overtime);
    txtAccNumber = findViewById(R.id.txt_account_num);
    txtCardNumber = findViewById(R.id.txt_card_number);
    swLayout = findViewById(R.id.sw_layout);
    parentId = Constants.ACCOUNTS.PERSONS;
    this.txtBalance.setKeyListener(DigitsKeyListener.getInstance("1234567890,."));
    this.txtFee.setKeyListener(DigitsKeyListener.getInstance("1234567890,."));
    this.txtOvertimeRate.setKeyListener(DigitsKeyListener.getInstance("1234567890,."));
    this.txtBalance.addTextChangedListener(Utils.MoneyTextWatcher);
    this.txtFee.addTextChangedListener(Utils.MoneyTextWatcher);
    this.txtOvertimeRate.addTextChangedListener(Utils.MoneyTextWatcher);
    swLayout.setOnCheckedChangeListener((compoundButton, b) -> {
      View cvWorker = findViewById(R.id.cv_worker);
              /*if(initialHeight<1) {
                  int temp = cvWorker.getHeight();
                  cvWorker.getLayoutParams().height= CardView.LayoutParams.WRAP_CONTENT;
                  cvWorker.requestLayout();
                  initialHeight = cvWorker.getHeight();
                  //cvWorker.getLayoutParams().height= temp;
                 // cvWorker.requestLayout();
              }*/
      if (!b) {
        YoYo.with(Techniques.SlideOutUp).duration(500).playOn(findViewById(R.id.llfee));
        YoYo.with(Techniques.SlideOutUp).duration(500).playOn(findViewById(R.id.llovertime));
        int newHeight = findViewById(R.id.sw_container).getHeight() + 85;
        collapse(cvWorker, 500, newHeight);
        isWorker =false;
      }
      if (b) {
        YoYo.with(Techniques.SlideInDown).duration(500).playOn(findViewById(R.id.llfee));
        YoYo.with(Techniques.SlideInDown).duration(500).playOn(findViewById(R.id.llovertime));
        //expand(cvWorker,500,initialHeight);
        cvWorker.getLayoutParams().height = CardView.LayoutParams.WRAP_CONTENT;
        cvWorker.requestLayout();
        isWorker = true;
      }
    });
    Bundle paramBundle = getIntent().getExtras();
    if (paramBundle != null){
      isWorker = paramBundle.getBoolean("isWorker", false);
      parentId = paramBundle.getString("parentId",Constants.ACCOUNTS.PERSONS);
    }
    Log.d("AAAA", "isworker:" + isWorker);
    if (isWorker) {
      swLayout.setChecked(true);
    }
    fillSpinner();
  }


  @Override
  protected ActivityAddworkerBinding getViewBinding() {
    return ActivityAddworkerBinding.inflate(getLayoutInflater());
  }

  @Override
  protected void onActionBarRightToolClicked() {

  }

  @Override
  protected void onActionBarLeftToolClicked() {

  }

  @Override
  protected String getActionBarTitle() {
    return null;
  }


  public static void collapse(final View v, int duration, int targetHeight) {
    int prevHeight = v.getHeight();
    ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
    valueAnimator.setInterpolator(new DecelerateInterpolator());
    valueAnimator.addUpdateListener(animation -> {
      v.getLayoutParams().height = (int) animation.getAnimatedValue();
      v.requestLayout();
    });
    valueAnimator.setInterpolator(new DecelerateInterpolator());
    valueAnimator.setDuration(duration);
    valueAnimator.start();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    super.onCreateOptionsMenu(menu);
    getMenuInflater().inflate(R.menu.main_checkcancel, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // The action bar home/up action should open or close the drawer.
    // ActionBarDrawerToggle will take care of this.
    if (item != null && item.getItemId() == android.R.id.home) {
      finish();
    }
    if (item != null && item.getItemId() == R.id.maincancel) {
      finish();
    }
    if (item != null && item.getItemId() == R.id.mainaccept) {
      submitData();
    }
    return false;
  }

/*
    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
        if (paramInt == 4) {
            Intent backIntent = new Intent();
            setResult(RESULT_CANCELED, backIntent);
            finish();
        }
        return super.onKeyDown(paramInt, paramKeyEvent);
    }
*/

  protected void onStart() {
    super.onStart();
  }

  protected void onStop() {
    super.onStop();
  }

  public void submitData() {
    String acInfo = this.txtInfo.getText().toString();
    double balance = 0;
    if (this.txtBalance.getText().toString().matches("")) {
      Log.d("balance is ", " Zero");
      balance = 0.0D;
    } else if (this.spBalanceType.getSelectedItemPosition() == 0) {
      balance = Double.parseDouble(this.txtBalance.getText().toString().replace(",", ""));
    } else {
      balance = -Double.parseDouble(this.txtBalance.getText().toString().replace(",", ""));
    }


    if (txtName.getText().length() < 1) {
      SnackBarHelper.showSnack(this, SnackBarHelper.SnackState.Warning, "لطفا نام را وارد نمایید!");
      txtName.requestFocus();
      YoYo.with(Techniques.Shake).duration(500).repeat(3).playOn(txtName);
      return;
    }
    if (isWorker && txtFee.getText().length() < 1) {
      SnackBarHelper.showSnack(this, SnackBarHelper.SnackState.Warning, "لطفا مزد روزانه را وارد نمایید!");
      txtFee.requestFocus();
      YoYo.with(Techniques.Shake).duration(500).repeat(3).playOn(txtFee);
      return;
    }
    if (isWorker && txtOvertimeRate.getText().length() < 1) {
      SnackBarHelper.showSnack(this, SnackBarHelper.SnackState.Warning, "لطفا نرخ ساعت اضافه کاری را وارد نمایید!");
      txtOvertimeRate.requestFocus();
      YoYo.with(Techniques.Shake).duration(500).repeat(3).playOn(txtOvertimeRate);
      return;
    }
    if (PersonDAO.checkDuplicateName(txtName.getText().toString(),"")) {
      SnackBarHelper.showSnack(this, SnackBarHelper.SnackState.Warning, "نام شخص تکراری می باشد.");
      txtName.requestFocus();
      YoYo.with(Techniques.Shake).duration(500).repeat(3).playOn(txtName);
      return;
    }

    Account relatedAccount = new Account();
    relatedAccount.setTitle(txtName.getText().toString());
    relatedAccount.setInfo(acInfo);
    relatedAccount.setBalance(balance);
    relatedAccount.setParentId(parentId);
    relatedAccount.setProjectDate(new PersianCalendar().getPersianShortDate());
    relatedAccount.setIsReceivable(1);
    relatedAccount.setIsPayable(1);


    relatedAccount = AccountDAO.insert(relatedAccount);
    try {
      PersonItem person = new PersonItem();
      person.setAcID(relatedAccount.getAccID());
      person.setFee(isWorker ? Double.parseDouble(txtFee.getText().toString().replace(",", "")) : 0);
      person.setOvertime(isWorker  ? Double.parseDouble(txtOvertimeRate.getText().toString().replace(",", "")) : 0);
      person.setMobile(txtMobile.getText().toString());
      person.setPhone(txtPhone.getText().toString());
      person.setEmail(txtEmail.getText().toString());
      person.setAccNumber(txtAccNumber.getText().toString());
      person.setCardNumber(txtCardNumber.getText().toString());
      person.setName(txtName.getText().toString());
      person.setHour(9);
      if (isWorker)
      {
        person.setWorker(true);
      }

      PersonDAO.insert(person);

    } catch (Exception e) {
      e.printStackTrace();
    }
    Intent backIntent = new Intent();
    backIntent.putExtra("acTitle", txtName.getText());
    setResult(RESULT_OK, backIntent);
    finish();
    return;


  }

  public void fillSpinner() {
    String[] arrayOfString = {"بدهکار", "بستانکار"};
    this.spBalanceType = findViewById(R.id.sp_account_balance_type);
    ArrayAdapter localArrayAdapter = new ArrayAdapter(this, simple_spinner_item, arrayOfString);
    localArrayAdapter.setDropDownViewResource(simple_spinner_dropdown_item);
    this.spBalanceType.setAdapter(localArrayAdapter);

    return;
  }

  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == RESULT_DATE) {
      if (resultCode == RESULT_OK) {

      }
    }
    if (requestCode == Constants.RESULTS.CONTACT) {
      if (resultCode == RESULT_OK) {
        getContactData(data);

      }
    }
    super.onActivityResult(requestCode, resultCode, data);

  }

  private void getContactData(Intent data) {
    Uri contactData = data.getData();
    txtMobile.setText("");
    txtPhone.setText("");
    txtName.setText("");
    txtEmail.setText("");
    String number = "";
    Cursor cursor = getContentResolver().query(contactData, null, null, null, null);
    cursor.moveToFirst();
    String name = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
    txtName.setText(name);

    String hasPhone = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.HAS_PHONE_NUMBER));
    String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
    if (hasPhone.equals("1")) {
      Cursor phones = getContentResolver().query
              (ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                      ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                              + " = " + contactId, null, null);

      while (phones.moveToNext()) {

        number = phones.getString(phones.getColumnIndex
                (ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("[-() ]", "");
        if (phones.getPosition() == 0)
          txtMobile.setText(phone_number_correction(number));
        else if (phones.getPosition() == 1)
          txtPhone.setText(phone_number_correction(number));
      }
      phones.close();
    } else {
      SnackBarHelper.showSnack(this, SnackBarHelper.SnackState.Warning, "این مخاطب شماره ای ندارد.");
    }

    cursor.close();
    Cursor emailCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
            null,
            ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{contactId}, null);

    assert emailCursor != null;
    if (emailCursor.moveToFirst()) {
      String phone = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
      int type = emailCursor.getInt(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
      String s = (String) ContactsContract.CommonDataKinds.Email.getTypeLabel(getResources(), type, "");
      txtEmail.setText(phone);
    }
    emailCursor.close();
  }

  private String phone_number_correction(String number) {
    String str = number.replace(" ", "").replace("+98", "0");
    if (str.substring(0, 2).equals("98")) {
      str = "0" + str.substring(2);
    }
    return str;
  }

  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.layout_select_contact:
        if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)) {
          startActivityForResult(new Intent("android.intent.action.PICK", ContactsContract.Contacts.CONTENT_URI), Constants.RESULTS.CONTACT);
        } else {
          requestPermission();
        }

        break;
    }
  }

  private void requestPermission() {
    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)) {
      // Provide an additional rationale to the user if the permission was not granted
      // and the user would benefit from additional context for the use of the permission.
      // For example if the user has previously denied the permission.

      new AlertDialog.Builder(this)
              .setMessage(R.string.perm_request_message)
              .setPositiveButton(R.string.ok, (dialog, which) -> ActivityCompat.requestPermissions(AddPersonActivity.this, new String[]{Manifest.permission.READ_CONTACTS},
                      Constants.PERMISSIONS.READ_CONTACTS)).show();

    } else {
      ActivityCompat.requestPermissions(AddPersonActivity.this, new String[]{Manifest.permission.READ_CONTACTS},
              Constants.PERMISSIONS.READ_CONTACTS);
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
    switch (requestCode) {
      case Constants.PERMISSIONS.READ_CONTACTS: {
        if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          startActivityForResult(new Intent("android.intent.action.PICK", ContactsContract.Contacts.CONTENT_URI), Constants.RESULTS.CONTACT);
        } else {
          SnackBarHelper.showSnack(this, SnackBarHelper.SnackState.Warning, "مجوز دسترسی به مخاطبین داده نشد!","دسترسی", view -> requestPermission());
          super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
      }
    }
  }
}
