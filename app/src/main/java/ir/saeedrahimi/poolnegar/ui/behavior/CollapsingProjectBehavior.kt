package ir.saeedrahimi.poolnegar.ui.behavior

import android.content.Context
import android.graphics.Point
import android.util.AttributeSet
import android.view.View
import android.view.WindowManager
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.AppBarLayout
import ir.saeedrahimi.poolnegar.R

@Suppress("unused")
class CollapsingProjectBehavior(private val context: Context, attrs: AttributeSet) :
        CoordinatorLayout.Behavior<LinearLayout>(context, attrs) {

    private lateinit var appBar: View
    private lateinit var headerproject: View
    private lateinit var projectImage: View
    private lateinit var projectTextContainer: View
    private lateinit var projectName: View
    private lateinit var projectSubtitle: View
    private lateinit var projectMisc: View

    private lateinit var btnDelete: View
    private lateinit var btnLock: View

    private lateinit var windowSize: Point
    private var appBarHeight: Int = 0
    private val projectImageSizeSmall: Int
    private val projectImageSizeBig: Int
    private val projectImageMaxMargin: Int
    private var toolBarHeight: Int = 0
    private var projectTextContainerMaxHeight: Int = 0
    private var projectNameHeight: Int = 0

    private var normalizedRange: Float = 0.toFloat()

    private val displaySize: Point
        get() {
            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = wm.defaultDisplay
            val size = Point()
            display.getSize(size)
            return size
        }

    init {
        normalizedRange = 0f
        projectImageSizeSmall = context.resources.getDimension(R.dimen.project_small_size).toInt()
        projectImageSizeBig = context.resources.getDimension(R.dimen.project_big_size).toInt()
        projectImageMaxMargin = context.resources.getDimension(R.dimen.project_image_margin_max).toInt()
    }

    override fun layoutDependsOn(parent: CoordinatorLayout, child: LinearLayout, dependency: View): Boolean {
        val isDependencyAnAppBar = dependency is AppBarLayout
        if (isDependencyAnAppBar) {
            initialize(child, dependency)
        }
        return isDependencyAnAppBar
    }

    private fun initialize(child: LinearLayout, dependency: View) {
        windowSize = displaySize
        appBar = dependency
        appBarHeight = appBar.height
        headerproject = child
        projectImage = headerproject.findViewById(R.id.projectImage)
        projectImage.pivotX = 0f
        projectImage.pivotY = 0f
        projectTextContainer = headerproject.findViewById(R.id.projectTextContainer)
        projectTextContainer.pivotX = 0f
        projectTextContainer.pivotY = 0f
        projectName = projectTextContainer.findViewById(R.id.projectName)
        projectNameHeight = projectName.height
        projectSubtitle = projectTextContainer.findViewById(R.id.projectSubtitle)
        projectMisc = projectTextContainer.findViewById(R.id.projectMisc)
        btnDelete = projectTextContainer.findViewById(R.id.btnDelete)
        btnLock = projectTextContainer.findViewById(R.id.btnLock)
        val projectSubtitleMaxHeight = calculateMaxHeightFromTextView((projectSubtitle as TextView?)!!)
        val projectMiscMaxHeight = calculateMaxHeightFromTextView((projectMisc as TextView?)!!)
        projectTextContainerMaxHeight = projectNameHeight + projectSubtitleMaxHeight + projectMiscMaxHeight
    }

    private fun calculateMaxHeightFromTextView(textView: TextView): Int {
        val widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(windowSize.x, View.MeasureSpec.AT_MOST)
        val heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        textView.measure(widthMeasureSpec, heightMeasureSpec)
        return textView.measuredHeight
    }

    override fun onDependentViewChanged(parent: CoordinatorLayout, child: LinearLayout, dependency: View): Boolean {
        val isDependencyAnAppBar = dependency is AppBarLayout
        if (isDependencyAnAppBar) {
            toolBarHeight = appBar.findViewById<View>(R.id.toolbar).height
            updateNormalizedRange()
            updateOffset()
        }
        return isDependencyAnAppBar
    }

    private fun updateNormalizedRange() {
        val abl = appBar as AppBarLayout
        normalizedRange = normalize(
                currentValue = appBar.y + abl.totalScrollRange,
                minValue = 0f,
                maxValue = abl.totalScrollRange.toFloat()
        )

        normalizedRange = 1f - normalizedRange
    }

    private fun normalize(currentValue: Float, minValue: Float, maxValue: Float): Float {
        val dividend = currentValue - minValue
        val divisor = maxValue - minValue
        return dividend / divisor
    }

    private fun updateOffset() {
        updateHeaderprojectOffset()
        updateprojectImageSize()
        updateprojectImageMargins()
        updateprojectTextContainerHeight()
        updateprojectTextMargin()
        updateSubtitleAndMiscAlpha()
    }

    private fun updateHeaderprojectOffset() {
        headerproject.y = appBar.y
    }

    private fun updateprojectImageSize() {
        val updatedValue = getUpdatedInterpolatedValue(projectImageSizeBig.toFloat(), projectImageSizeSmall.toFloat()).toInt()

        val lp = projectImage.layoutParams as LinearLayout.LayoutParams
        lp.height = updatedValue
        lp.width = updatedValue
        projectImage.layoutParams = lp
    }

    private fun updateprojectImageMargins() {
        val targetOpenAppbarValue = calculateprojectImageSmallMargin().toFloat()
        val updatedValue = getUpdatedInterpolatedValue(projectImageMaxMargin.toFloat(), targetOpenAppbarValue).toInt()

        val lp = projectImage.layoutParams as LinearLayout.LayoutParams
        lp.bottomMargin = updatedValue
        lp.leftMargin = updatedValue
        lp.rightMargin = updatedValue
        projectImage.layoutParams = lp
    }

    private fun calculateprojectImageSmallMargin(): Int {
        val halfToolbarHeight = toolBarHeight / 2
        val halfprojectImageSmall = projectImageSizeSmall / 2
        return halfToolbarHeight - halfprojectImageSmall
    }

    private fun updateprojectTextContainerHeight() {
        val updatedValue = getUpdatedInterpolatedValue(projectTextContainerMaxHeight.toFloat(), toolBarHeight.toFloat()).toInt()

        val lp = projectTextContainer.layoutParams as LinearLayout.LayoutParams
        lp.height = updatedValue
        projectTextContainer.layoutParams = lp
    }

    private fun updateprojectTextMargin() {
        val targetOpenAppbarValue = calculateprojectTextMargin().toFloat()
        val updatedValue = getUpdatedInterpolatedValue(0f, targetOpenAppbarValue).toInt()

        val lp = projectName.layoutParams as RelativeLayout.LayoutParams
        lp.topMargin = updatedValue
        projectName.layoutParams = lp
    }

    private fun calculateprojectTextMargin(): Int {
        val halfToolbarHeight = toolBarHeight / 2
        val halfprojectTextHeight = projectNameHeight / 2
        return halfToolbarHeight - halfprojectTextHeight
    }

    private fun updateSubtitleAndMiscAlpha() {
        val updatedValue = getUpdatedInterpolatedValue(1f, 0f)
        val poweredValue = Math.pow(updatedValue.toDouble(), 6.0).toFloat()

        projectSubtitle.alpha = poweredValue
        projectMisc.alpha = poweredValue
        btnDelete.alpha = poweredValue
        btnLock.alpha = poweredValue
    }

    private fun getIntercept(m: Float, x: Float, b: Float): Float {
        return m * x + b
    }

    private fun getUpdatedInterpolatedValue(openSizeTarget: Float, closedSizeTarget: Float): Float {
        return getIntercept(closedSizeTarget - openSizeTarget, normalizedRange, openSizeTarget)
    }
}