package ir.saeedrahimi.poolnegar.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.viewpagerindicator.TitlePageIndicator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.adapters.ProjectsListAdapter_bk;
import ir.saeedrahimi.poolnegar.adapters.TransactionListAdapter;
import ir.saeedrahimi.poolnegar.application.helper.KeyHelper;
import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar;
import ir.saeedrahimi.poolnegar.customs.TypeFaceProvider;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
import ir.saeedrahimi.poolnegar.dialogs.DialogBackPayBill;
import ir.saeedrahimi.poolnegar.dialogs.DialogFilter;
import ir.saeedrahimi.poolnegar.dialogs.DialogFilter.onFilterSetListener;
import ir.saeedrahimi.poolnegar.dialogs.DialogPayBill;
import ir.saeedrahimi.poolnegar.dialogs.DialogTransactionDetails;
import ir.saeedrahimi.poolnegar.dialogs.ProgressLoading;
import ir.saeedrahimi.poolnegar.models.BillItem;
import ir.saeedrahimi.poolnegar.models.BillProjectItem;
import ir.saeedrahimi.poolnegar.models.TransactionFilter;
import ir.saeedrahimi.poolnegar.ui.activities.cu.ActivityCU_Base;
import ir.saeedrahimi.poolnegar.ui.activities.cu.Activity_Transaction;

public class FragmentPayBillsD extends BaseFragment implements OnItemClickListener, onFilterSetListener, DialogTransactionDetails.OnDialogClickListener, DialogTransactionDetails.OnTransactionEdit, DialogTransactionDetails.OnTransactionDelete, OnScrollListener {
	private static final int RESULT_PAYBILL = 101;
	private cAdapter mAdapter;
	private ViewPager viewPager;
	private int[] parentId;
	private LinearLayout inEmptyview;
	private LinearLayout billEmptyview;
	private LinearLayout projectEmptyview;
	private ListView lvIn;
	private ListView lvBill;
	private ListView lvProject;
	private ArrayAdapter<TransactionItem> inAdapter,billAdapter;
	private ArrayAdapter<BillProjectItem> projectAdapter;
	private List<TransactionItem> listInTrans;
	private List<TransactionItem> listBillTrans;
	private List<BillProjectItem> listProjectTrans;
	TransactionFilter filter;
	private AsyncLoadDB async;

	private LinearLayout mHeader;
	private View mPlaceHolderView;
	private int mHeaderHeight;
	private int mMinHeaderTranslation;
	private boolean mFirstAsync = false;
	private AccelerateDecelerateInterpolator mSmoothInterpolator;

	private int visibleThreshold = 10;
	private int currentInPage = 0;
	private int currentBillPage = 0;
	private int currentProjectPage = 0;
	private boolean loading = true;
	private int lastInListSize = 10;
	private int lastBillListSize = 10;
	private int lastProjectListSize = 10;
	private Integer loadType = 2;
	private String startDate = "";
	private String endDate = "";

	private String mPayAccount;
	private String mRecAccount;
	private BillItem mBill;
	private TextView totalbill;
	private TextView remainedbills;
	private TextView payedbills;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_bill_details, container,false);

		mAdapter= new cAdapter(new String[]{"پروژه ها","پرداخت ها","بدهی ها"});
		viewPager = view.findViewById(R.id.bill_pager);
		viewPager.setOffscreenPageLimit(2);
		viewPager.setAdapter(mAdapter);
		TitlePageIndicator mIndicator = view
				.findViewById(R.id.bill_pager_titles);
		mIndicator.setViewPager(viewPager);
		mIndicator.setCurrentItem(mAdapter.getCount() - 1);
		Typeface typeface =  TypeFaceProvider.get(getActivity(), "vazir.ttf");
		mIndicator.setTypeface(typeface);
		mIndicator.setSelectedColor(getActivity().getResources().getColor(
				R.color.Artem));

		mSmoothInterpolator = new AccelerateDecelerateInterpolator();
		mHeaderHeight = getResources().getDimensionPixelSize(R.dimen.bills_header_height);
		mMinHeaderTranslation = -mHeaderHeight + 70;
		mHeader = view.findViewById(R.id.llai1);

		this.listInTrans = new ArrayList<>();
		this.listBillTrans = new ArrayList<>();
		this.listProjectTrans = new ArrayList<>();

		this.inAdapter = new TransactionListAdapter(getActivity(),listInTrans,0);
		this.billAdapter = new TransactionListAdapter(getActivity(),listBillTrans,0);
		this.projectAdapter = new ProjectsListAdapter_bk(getActivity(),listProjectTrans,0);

		this.parentId = getArguments().getIntArray("accId");
		this.mBill = (BillItem) getArguments().getSerializable("account");

		this.mPayAccount = this.mBill.getTargetAccountId();

		mRecAccount = mBill.getSourceAccountId();
		this.totalbill = view.findViewById(R.id.totalbill);
		this.payedbills = view.findViewById(R.id.payedbills);
		this.remainedbills = view.findViewById(R.id.remainedbills);


		// TODO: fix title
	//	setToolBarTitle("بدهی", this.mBill.getTitle());
		return view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		setHasOptionsMenu(true);
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		RefreshAll();
		super.onViewCreated(view, savedInstanceState);
	}

	public void RefreshAll()
	{	
		if(async != null && !async.isCancelled())
			async.cancel(true);
		async = new AsyncLoadDB();
		this.loadType = 3;
		this.lastInListSize = 10;
		this.lastBillListSize = 10;
		this.lastProjectListSize = 10;
		this.currentInPage = 0;
		this.currentBillPage = 0;
		this.currentProjectPage = 0;
		this.listInTrans.clear();
		this.listBillTrans.clear();
		this.listProjectTrans.clear();
		loadHeaderValues();
		async.execute(new Integer[]{this.currentProjectPage, this.currentInPage,this.currentBillPage, this.loadType});
	}
	private void LoadMore()
	{
		if(async != null && !async.isCancelled())
			async.cancel(true);
		async = new AsyncLoadDB();
		async.execute(new Integer[]{this.currentProjectPage , this.currentInPage,this.currentBillPage, this.loadType });
	}
	class AsyncLoadDB extends AsyncTask<Integer, List<TransactionItem>, List<TransactionItem>> {
		ProgressLoading p;
		@Override
		protected void onPreExecute() {
			if(FragmentPayBillsD.this.lastInListSize == 10 && FragmentPayBillsD.this.viewPager.getCurrentItem() == 1){
				this.p = new ProgressLoading(FragmentPayBillsD.this.getActivity(),"");
			}
			else if(FragmentPayBillsD.this.lastBillListSize == 10 && FragmentPayBillsD.this.viewPager.getCurrentItem() == 2){
				this.p = new ProgressLoading(FragmentPayBillsD.this.getActivity(),"");
			}
			else if(FragmentPayBillsD.this.lastProjectListSize == 10 && FragmentPayBillsD.this.viewPager.getCurrentItem() == 0){
				this.p = new ProgressLoading(FragmentPayBillsD.this.getActivity(),"");
			}
		}

		@Override
		protected List<TransactionItem> doInBackground(Integer... values) {
			int projectPage = values[0] * FragmentPayBillsD.this.visibleThreshold;
			int inPage = values[1] * FragmentPayBillsD.this.visibleThreshold;
			int billPage = values[2] * FragmentPayBillsD.this.visibleThreshold;

			String startDate = FragmentPayBillsD.this.startDate;
			String endDate = FragmentPayBillsD.this.endDate;
			int loadType = values[3];
			List<TransactionItem> temp_inlist = new ArrayList<>();
			List<TransactionItem> temp_billlist = new ArrayList<>();
			List<BillProjectItem> temp_projectlist = new ArrayList<>();
			Log.i("inPage: ",inPage+"" );
			Log.i("outPage: ",billPage+"" );
			Log.i("projectPage: ",projectPage+"" );
			String accBiller = "";// FragmentPayBillsD.this.mBill.getAccpayId();
			if (accBiller == "")
				accBiller = FragmentPayBillsD.this.mBill.getTargetAccountId();

			// check if all items loaded and just load needed page

			if(FragmentPayBillsD.this.lastProjectListSize == 10 && (loadType == 0 || loadType == 3)){
				// TODO: Fix Here
//				temp_projectlist = _db.getBillProjectsDetail(mBill.getProjectsIds(), mPayAccount);
				FragmentPayBillsD.this.listProjectTrans.clear();
				FragmentPayBillsD.this.listProjectTrans.addAll(temp_projectlist);
				FragmentPayBillsD.this.lastProjectListSize = temp_projectlist.size();
			}
			
			if(FragmentPayBillsD.this.lastInListSize == 10 && (loadType == 1 || loadType == 3)){
				// TODO: Fix Here
//				temp_inlist = _db.getPayedBillsTrans(FragmentPayBillsD.this.mRecAccount, accBiller, inPage,startDate, endDate);
				FragmentPayBillsD.this.listInTrans.addAll(temp_inlist);
				FragmentPayBillsD.this.lastInListSize = temp_inlist.size();
			}

			if(FragmentPayBillsD.this.lastBillListSize == 10 && (loadType == 2 || loadType == 3)){

				// TODO: Fix Here
				// temp_billlist = _db.getBilledBillsTrans(FragmentPayBillsD.this.mPayAccount,accBiller,billPage,startDate, endDate, true);
				FragmentPayBillsD.this.listBillTrans.addAll(temp_billlist);
				FragmentPayBillsD.this.lastBillListSize = temp_billlist.size();
			}

			Log.i("temp_inlist size: ",temp_inlist.size()+"" );
			Log.i("temp_outlist size: ",temp_billlist.size()+"" );

			return null;
		}

		@Override
		protected void onPostExecute(List<TransactionItem> result) {
			if(this.p != null)
				this.p.done();
			FragmentPayBillsD.this.mPlaceHolderView = getActivity().getLayoutInflater().inflate(R.layout.bills_header_placeholder, lvBill, false);
			if(FragmentPayBillsD.this.mFirstAsync != true){
				FragmentPayBillsD.this.lvBill.addHeaderView(mPlaceHolderView);
				FragmentPayBillsD.this.lvIn.addHeaderView(mPlaceHolderView);
				FragmentPayBillsD.this.lvProject.addHeaderView(mPlaceHolderView);
				FragmentPayBillsD.this.lvIn.setAdapter(FragmentPayBillsD.this.inAdapter);
				FragmentPayBillsD.this.lvBill.setAdapter(FragmentPayBillsD.this.billAdapter);
				FragmentPayBillsD.this.lvProject.setAdapter(FragmentPayBillsD.this.projectAdapter);
				FragmentPayBillsD.this.mFirstAsync = true;
			}

			if(FragmentPayBillsD.this.lastProjectListSize > 0 && (FragmentPayBillsD.this.loadType == 0 || FragmentPayBillsD.this.loadType == 3)){
				FragmentPayBillsD.this.projectAdapter.notifyDataSetChanged();
			}
			if(FragmentPayBillsD.this.lastInListSize > 0 && (FragmentPayBillsD.this.loadType == 1 || FragmentPayBillsD.this.loadType == 3)){
				FragmentPayBillsD.this.inAdapter.notifyDataSetChanged();
			}
			if(FragmentPayBillsD.this.lastBillListSize > 0 && (FragmentPayBillsD.this.loadType == 2 || FragmentPayBillsD.this.loadType == 3)){
				FragmentPayBillsD.this.billAdapter.notifyDataSetChanged();
			}
			if(this.p != null)
				this.p.done();

			if(FragmentPayBillsD.this.listInTrans == null || FragmentPayBillsD.this.listInTrans.size() <= 0)
			{
				View txtNoItem = FragmentPayBillsD.this.getActivity().getLayoutInflater().inflate(R.layout.blank_textview, null);
				((TextView)txtNoItem.findViewById(R.id.blank_textview)).setText("موردی یافت نشد!");
				FragmentPayBillsD.this.inEmptyview.addView(txtNoItem);
			}
			if(FragmentPayBillsD.this.listBillTrans == null || FragmentPayBillsD.this.listBillTrans.size() <= 0)
			{
				View txtNoItem = FragmentPayBillsD.this.getActivity().getLayoutInflater().inflate(R.layout.blank_textview, null);
				((TextView)txtNoItem.findViewById(R.id.blank_textview)).setText("موردی یافت نشد!");
				FragmentPayBillsD.this.billEmptyview.addView(txtNoItem);
			}

			if(FragmentPayBillsD.this.listProjectTrans == null || FragmentPayBillsD.this.listProjectTrans.size() <= 0)
			{
				View txtNoItem = FragmentPayBillsD.this.getActivity().getLayoutInflater().inflate(R.layout.blank_textview, null);
				((TextView)txtNoItem.findViewById(R.id.blank_textview)).setText("موردی یافت نشد!");
				FragmentPayBillsD.this.projectEmptyview.addView(txtNoItem);
			}

			if(FragmentPayBillsD.this.lastInListSize == 10){
				FragmentPayBillsD.this.lvIn.setTag("loadMore");
			}
			else if(loadType == 1 && !FragmentPayBillsD.this.lvIn.getTag().toString().contains("ended")){
				if(listInTrans.size()>0)
					Snackbar.make(FragmentPayBillsD.this.viewPager, "به انتهای لیست بدهی ها رسیدید", Snackbar.LENGTH_LONG).setAction("Action", null).show();
				FragmentPayBillsD.this.lvIn.setTag("ended");
			}
			if(FragmentPayBillsD.this.lastBillListSize == 10 ){
				FragmentPayBillsD.this.lvBill.setTag("loadMore");
			}
			else if(loadType == 2 && !FragmentPayBillsD.this.lvBill.getTag().toString().contains("ended")){
				if(listBillTrans.size()>0)
					Snackbar.make(FragmentPayBillsD.this.viewPager, "به انتهای لیست پرداخت ها رسیدید", Snackbar.LENGTH_LONG).setAction("Action", null).show();
				FragmentPayBillsD.this.lvBill.setTag("ended");
			}

			if(FragmentPayBillsD.this.lastProjectListSize == 10 ){
				FragmentPayBillsD.this.lvProject.setTag("loadMore");
			}
			else if(loadType == 0 && !FragmentPayBillsD.this.lvProject.getTag().toString().contains("ended")){
				if(listProjectTrans.size()>0)
					Snackbar.make(FragmentPayBillsD.this.viewPager, "به انتهای لیست پروژه ها رسیدید", Snackbar.LENGTH_LONG).setAction("Action", null).show();
				FragmentPayBillsD.this.lvProject.setTag("ended");
			}

			FragmentPayBillsD.this.lvBill.setOnScrollListener(FragmentPayBillsD.this);
			FragmentPayBillsD.this.lvIn.setOnScrollListener(FragmentPayBillsD.this);
			//FragmentPayBillsD.this.lvPays.setOnScrollListener(FragmentPayBillsD.this);

			FragmentPayBillsD.this.loading = false;

			if(this.p != null)
				this.p.done();
			super.onPostExecute(result);
		}
	}

	class cAdapter extends PagerAdapter{

		private final String[] TITLES;

		cAdapter(String[] paramArrayOfString)
		{
			this.TITLES = paramArrayOfString;
		}
		@Override
		public int getCount() {
			return this.TITLES.length;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View)object);
		}
		@Override
		public CharSequence getPageTitle(int position) {
			return this.TITLES[position];
		}
		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			LayoutInflater localLayoutInflater = FragmentPayBillsD.this.getActivity().getLayoutInflater();
			switch (position)
			{
			case 0: 
				View projectListLayout = localLayoutInflater.inflate(R.layout.fragment_transactions_listview, null);
				FragmentPayBillsD.this.projectEmptyview = projectListLayout.findViewById(R.id.emptyview);
				FragmentPayBillsD.this.lvProject = projectListLayout.findViewById(R.id.listviewtransactions);
				FragmentPayBillsD.this.lvProject.setTag("lvProject");
				FragmentPayBillsD.this.lvProject.setOnItemClickListener(FragmentPayBillsD.this);
				container.addView(projectListLayout, 0);
				return projectListLayout;
			case 1:
				View listLayoutOut = localLayoutInflater.inflate(R.layout.fragment_transactions_listview, null);
				FragmentPayBillsD.this.inEmptyview = listLayoutOut.findViewById(R.id.emptyview);
				FragmentPayBillsD.this.lvIn = listLayoutOut.findViewById(R.id.listviewtransactions);
				FragmentPayBillsD.this.lvIn.setTag("lvPays");
				FragmentPayBillsD.this.lvIn.setOnItemClickListener(FragmentPayBillsD.this);
				container.addView(listLayoutOut, 0);
				return listLayoutOut;
			case 2: 
				View outListLayout = localLayoutInflater.inflate(R.layout.fragment_transactions_listview, null);
				FragmentPayBillsD.this.billEmptyview = outListLayout.findViewById(R.id.emptyview);
				FragmentPayBillsD.this.lvBill = outListLayout.findViewById(R.id.listviewtransactions);
				FragmentPayBillsD.this.lvBill.setTag("lvBill");
				FragmentPayBillsD.this.lvBill.setOnItemClickListener(FragmentPayBillsD.this);
				container.addView(outListLayout, 0);
				return outListLayout;

			default: 
				return null;
			}

		}


	}
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
		inflater.inflate(R.menu.bill_detail, menu);

	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify height parent activity in AndroidManifest.xml.

		// TODO: Fix Here
//		int[] account = new int[3];
//		account[0] = mBill.getRootRecId();
//		account[1] = mBill.getSubAccRecId();
//		account[2] = mBill.getAccRecId();
//
//		int billAcc = mBill.getAccpayId();
//		if (billAcc == -1)
//			billAcc = mBill.getTargetSubAccountId();

		switch (item.getItemId()) {
		case R.id.action_filter:
			//startDate = "1393/06/29";
			//endDate = "1393/06/31";
			//RefreshAll();
			new DialogFilter(getActivity(), this, new PersianCalendar().getPersianShortDate());
			break;
		case R.id.bill_pay:
			Intent myIntent = new Intent(getActivity(),
					DialogPayBill.class);
			myIntent.putExtra("transPayToIds", mPayAccount);
			myIntent.putExtra("accId", mBill.getTargetAccountId());
			startActivityForResult(myIntent, RESULT_PAYBILL);
			break;

		case R.id.bill_backpay:
			Intent myIntent2 = new Intent(getActivity(),
					DialogBackPayBill.class);

			myIntent2.putExtra("transFromIds", mPayAccount);
			myIntent2.putExtra("accId", mBill.getTargetAccountId());
			startActivityForResult(myIntent2, RESULT_PAYBILL);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	public void onItemClick(AdapterView<?> adp, View arg1, int position, long arg3) {
		if(position == 0)
			return;
		if(adp.equals(FragmentPayBillsD.this.lvIn))
			new DialogTransactionDetails(getActivity(), getActivity(), this, this, this, this.listInTrans.get(position-1));
		else if(adp.equals(FragmentPayBillsD.this.lvBill))
			new DialogTransactionDetails(getActivity(), getActivity(), this, this, this, this.listBillTrans.get(position-1));

	}

	@Override
	public void onDialogDeleteTransaction() {
		RefreshAll();
	}

	@Override
	public void onDialogEditTransaction(TransactionItem transaction) {
			Intent localIntent = new Intent(FragmentPayBillsD.this.getActivity(), Activity_Transaction.class);
			localIntent.putExtra(KeyHelper.KEY_ENTITY,transaction);
			localIntent.putExtra(KeyHelper.KEY_ACTIVITY_CU_STATE, ActivityCU_Base.ACTIVITY_STATE_EDIT_MODE);
			startActivityForResult(localIntent, KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION);

	}

	@Override
	public void onDialogDeleteClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION) {
			if (resultCode == Activity.RESULT_OK) {
				RefreshAll();
			}
		}
		if (requestCode == RESULT_PAYBILL) {
			if (resultCode == Activity.RESULT_OK) {
				mAdapter.notifyDataSetChanged();
				double payedAmount = data.getDoubleExtra("amount", 0.0D);
				double oldPayed= this.mBill.getPayedAmount();
				this.mBill.setPayedAmount(oldPayed - payedAmount);
				this.mBill.setPayedCount(mBill.getPayedCount()+1);
				loadHeaderValues();

				Snackbar.make(FragmentPayBillsD.this.viewPager,"مبلغ "+new DecimalFormat(" ###,###.## ").format(payedAmount)+" از بدهی پرداخت شد.", Snackbar.LENGTH_LONG).setAction("Action", null).show();
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	private void loadHeaderValues(){

		totalbill.setText(new DecimalFormat(" ###,###.## ").format(this.mBill
				.getBillAmount()));
		this.payedbills.setText(new DecimalFormat(" ###,###.## ")
		.format(this.mBill.getPayedAmount()));
		this.remainedbills.setText(new DecimalFormat(" ###,###.## ")
		.format(this.mBill.getRemainAmount()));

		ImageView endimg = getView().findViewById(R.id.endingbalancecolor);

		if (this.mBill.getRemainAmount() > 0)
			endimg.setBackgroundColor(getResources().getColor(R.color.Tomato));
		if (this.mBill.getRemainAmount() == 0)
			endimg.setBackgroundColor(getResources().getColor(
					R.color.MediumSeaGreen));
	}
	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,int visibleItemCount, int totalItemCount) {		
		int scrollY = getScrollY((ListView) view);
		mHeader.setTranslationY(Math.max(-scrollY, mMinHeaderTranslation));
		if(((String)view.getTag()).contains("loadMore")){
			int l = visibleItemCount + firstVisibleItem;
			if (l >= totalItemCount && !loading) {
				if(this.viewPager.getCurrentItem()==1)
				{
					this.loadType = 1;
					this.currentInPage++;
				}
				if(this.viewPager.getCurrentItem()== 2)
				{
					this.loadType = 2;
					this.currentBillPage++;
				}
				loading = true;
				LoadMore();
			}
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
	}
	private int getScrollY(ListView lv) {
		View c = lv.getChildAt(0);
		if (c == null) {
			return 0;
		}

		int firstVisiblePosition = lv.getFirstVisiblePosition();
		int top = c.getTop();

		int headerHeight = 0;
		if (firstVisiblePosition >= 1) {
			headerHeight = mPlaceHolderView.getHeight();
		}

		return -top + firstVisiblePosition * c.getHeight() + headerHeight;
	}



	@Override
	public void onFilterSet(String startDate, String endDate) {
		FragmentPayBillsD.this.startDate=startDate;
		FragmentPayBillsD.this.endDate = endDate;
		Log.d("------------", this.startDate);
		Log.d("------------", this.endDate);
		FragmentPayBillsD.this.RefreshAll();
	}


}
