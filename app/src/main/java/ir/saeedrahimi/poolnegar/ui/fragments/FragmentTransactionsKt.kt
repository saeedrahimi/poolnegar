package ir.saeedrahimi.poolnegar.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import com.nshmura.recyclertablayout.RecyclerTabLayout
import ir.saeedrahimi.poolnegar.R
import ir.saeedrahimi.poolnegar.R.layout
import ir.saeedrahimi.poolnegar.adapters.TransactionFragmentAdapter
import ir.saeedrahimi.poolnegar.adapters.TransactionPagerAdapter
import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar
import ir.saeedrahimi.poolnegar.ui.helper.FontHelper
import java.util.*


class FragmentTransactionsKt : BaseFragment(), View.OnClickListener {

    var mCalendar: PersianCalendar = PersianCalendar()
    var adapter: TransactionPagerAdapter? = null
    var viewPager: ViewPager? = null
    var recyclerTabLayout: RecyclerTabLayout? = null
    var currentMonthIndex: Int = 0;

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View? = inflater.inflate(layout.fragment_transactions, container, false)
        viewPager = view?.findViewById<ViewPager>(R.id.transactionViewPager)
        adapter = childFragmentManager.let { TransactionPagerAdapter(it) }
        viewPager?.offscreenPageLimit = 2
        val dateArray = mutableListOf<String>()
        val dateTitleArray = mutableListOf<String>()
       // get array for past year
        mCalendar = PersianCalendar()
        mCalendar.setPersianDate(mCalendar.persianYear-1,1,1)
        for (i in 1..12) {
            dateArray += mCalendar.persianShortDate
            dateTitleArray += mCalendar.persianMonthName + " " + mCalendar.persianYear
            mCalendar.addPersianDate(Calendar.MONTH, 1)
        }
        //  get array for current year
        mCalendar = PersianCalendar()
        mCalendar.setPersianDate(mCalendar.persianYear,1,1)
        for (i in 1..12) {
            dateArray += mCalendar.persianShortDate
            when (mCalendar.persianMonth) {
                PersianCalendar().persianMonth -> {
                    dateTitleArray += "همین ماه"
                    currentMonthIndex = dateTitleArray.size-1
                }
                PersianCalendar().persianMonth -1 -> {
                    dateTitleArray += "ماه قبل"
                }
                PersianCalendar().persianMonth + 1 -> {
                    dateTitleArray += "ماه بعد"
                }
                else -> {
                    dateTitleArray += mCalendar.persianMonthName
                }
            }
            mCalendar.addPersianDate(Calendar.MONTH,1)
        }
//        //  get array for next year
        mCalendar = PersianCalendar()
        mCalendar.setPersianDate(mCalendar.persianYear+1,1,1)
        for (i in 1..12) {
            dateArray += mCalendar.persianShortDate
            dateTitleArray += mCalendar.persianMonthName + " " + mCalendar.persianYear
            mCalendar.addPersianDate(Calendar.MONTH, 1)
        }
        for (i in 0 until dateArray.size) {
            adapter?.addFragments(FragmentTransactions.newInstanceWithDate(i,dateArray[i]), dateTitleArray[i])

        }

        //adding pagerAdapter to ViewPager
        viewPager?.adapter = adapter
        //setting up viewPager with tablayout
        view?.findViewById<ImageView>(R.id.btnFilter)?.setOnClickListener(this)
        recyclerTabLayout = view?.findViewById<RecyclerTabLayout>(R.id.transactionTabLayout)

        FontHelper.setViewTextBoldStyleTypeFace(recyclerTabLayout)
        FontHelper.setViewTextBoldStyleTypeFace(view?.findViewById(R.id.vBoxSearchOptions))
        recyclerTabLayout?.setUpWithAdapter(TransactionFragmentAdapter(viewPager))
        recyclerTabLayout?.setCurrentItem(currentMonthIndex,false   )
        recyclerTabLayout?.setIndicatorColor(R.color.Transparent)
        return view
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(false)
        super.onCreate(savedInstanceState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shouldRefresh = false
    }



    var shouldRefresh: Boolean = false;
    override fun onResume() {
        if(shouldRefresh){

            val currentItem = viewPager?.currentItem
            viewPager?.adapter = adapter
            if(currentItem != null){
                recyclerTabLayout?.setCurrentItem(currentItem,false   )
            }else{
                recyclerTabLayout?.setCurrentItem(currentMonthIndex,false   )
            }
            shouldRefresh = false
        }

        super.onResume()
    }
    override fun onPause() {
        shouldRefresh = true

        super.onPause()
    }
    override fun onAttach(context: Context) {
        shouldRefresh = false
        super.onAttach(context)
    }

    override fun onClick(item: View?) {
        when (item?.id) {
            R.id.btnFilter -> {
                    Toast.makeText(context,"در دست توسعه",Toast.LENGTH_SHORT).show()
            }
        }
    }



    companion object {
        fun newInstance(instance: Int): FragmentTransactionsKt {
            val args = Bundle()
            args.putInt(ARGS_INSTANCE, instance)
            val fragment = FragmentTransactionsKt()
            fragment.arguments = args
            return fragment
        }
    }
}
