package ir.saeedrahimi.poolnegar.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ir.saeedrahimi.poolnegar.R;

public class FragmentHelp extends BaseFragment {

	public FragmentHelp() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_about, container,
				false);
		return view;
	}

}
