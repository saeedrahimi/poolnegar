package ir.saeedrahimi.poolnegar.ui.views

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ir.saeedrahimi.poolnegar.R
import ir.saeedrahimi.poolnegar.customs.GripView
import ir.saeedrahimi.poolnegar.database.model.TransactionItem
import java.text.DecimalFormat

class DefaultTransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val txtTitle: TextView = itemView.findViewById(R.id.acctitle)
    private val dragHandle: GripView = itemView.findViewById(R.id.drag_handle)
    private val separator: View = itemView.findViewById(R.id.separator)
    private val icon: ImageView = itemView.findViewById(R.id.accicontrans)

    fun bind(transaction: TransactionItem) {
        txtTitle.text = transaction.title

        if (transaction.method == 0)
            icon.setBackgroundColor(itemView.context.resources.getColor(R.color.LimeGreen))
        else
            icon.setBackgroundColor(itemView.context.resources.getColor(R.color.outcome))

    }

    fun hideSeparator(hide: Boolean) {
        if (hide) {
            separator.visibility = View.GONE
        } else {
            separator.visibility = View.VISIBLE
        }
    }
}