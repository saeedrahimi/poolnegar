package ir.saeedrahimi.poolnegar.ui.adapters;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

public abstract class BaseFilterableRecyclerViewAdapter<T, K extends ViewBinding> extends RecyclerView.Adapter<BaseFilterableRecyclerViewAdapter.ViewHolder>  implements Filterable {
    public Context mContext;
    K mBinding;
    private List<T> mOriginalList = new ArrayList<>();
    private List<T> mDataList = new ArrayList<>();
    private List<Integer> mSelectedItems = new ArrayList<Integer>();
    private boolean mIsFilterable = false;
    private RecyclerViewEventListener recyclerViewEventListener;
    private RecyclerView recyclerView;

    public BaseFilterableRecyclerViewAdapter(Context context, RecyclerViewEventListener listener) {
        setContext(context);
        setRecyclerViewEventListener(listener);
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }



    public RecyclerViewEventListener getRecyclerViewEventListener() {
        return recyclerViewEventListener;
    }

    public void setRecyclerViewEventListener(RecyclerViewEventListener recyclerViewEventListener) {
        this.recyclerViewEventListener = recyclerViewEventListener;
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    public void setIsFilterable(boolean isFilterable) {
        this.mIsFilterable = isFilterable;
    }

    public List<Integer> getSelectedItems() {
        return mSelectedItems;
    }

    public void addToSelectedItems(int position) {
        if (!getSelectedItems().contains(position))
            this.mSelectedItems.add(position);
    }

    public void removeFromSelectedItems(int position) {
        this.mSelectedItems.remove(new Integer(position));
    }


    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public int getPosition(T item) {
        return mDataList.indexOf(item);
    }

    public T getItem(int position) {
        if (position < 0 || position > mDataList.size()) {
            return null;
        }
        return mDataList.get(position);
    }

    public void clearAll() {
        mOriginalList.clear();
        mDataList.clear();
    }

    public void addRange(List<T> items) {
        try {
            if (this.mIsFilterable) {
                mOriginalList.addAll(items);
            }
            mDataList.addAll(items);

            this.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addItem(T item) {
        if (this.mIsFilterable) {
            mOriginalList.add(item);
        }
        mDataList.add(item);

        this.notifyDataSetChanged();
    }

    public void removeItem(int position) {
        if (mIsFilterable) {
            mOriginalList.remove(position);
        }
        mDataList.remove(position);
        this.notifyDataSetChanged();
    }

    public void removeItem(T item) {
        if (mIsFilterable) {
            mOriginalList.remove(item);
        }
        mDataList.remove(item);
        this.notifyDataSetChanged();
    }

    public List<T> getDataList() {
        if (this.mDataList == null)
            this.mDataList = new ArrayList<T>();
        return this.mDataList;
    }

    public void setDataList(List<T> data) {
        this.mDataList = data;
    }

    public List<T> getOriginalList() {
        if (this.mOriginalList == null)
            this.mOriginalList = new ArrayList<T>();
        return this.mOriginalList;
    }


    public static interface RecyclerViewEventListener {

        public void onItemSelectionChanged(int position, boolean checkedState);

        public void onSelectionModeChanged(boolean selectionMode);

        public void onViewTapped(int viewType, int position);

        public void onContextMenuTapped(int position, int actionId);

        public void onDataChanged(int viewType, int position);
    }

    public static class ViewHolder<W extends ViewBinding> extends RecyclerView.ViewHolder {

        W binding;

        public ViewHolder(W b) {
            super(b.getRoot());
            binding = b;
        }
    }
    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                mDataList.clear();

                if (TextUtils.isEmpty(charSequence)) {
                    mDataList.addAll(mOriginalList);
                } else {
                    ArrayList<T> filteredList = new ArrayList<>();
                    for (T object : mOriginalList) {
                        filterObject(filteredList, object, charSequence.toString());
                    }
                    mDataList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mDataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                //noinspection unchecked
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (recyclerView != null) {
                            recyclerView.getRecycledViewPool().clear();
                        }
                        notifyDataSetChanged();
                    }
                }, 100);
            }
        };
    }

    protected abstract void filterObject(ArrayList<T> filteredList, T object, String searchText);
}
