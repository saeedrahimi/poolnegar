package ir.saeedrahimi.poolnegar.ui.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;

import com.beautycoder.pflockscreen.security.PFSecurityManager;

import ir.saeedrahimi.poolnegar.PreferencesManager;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.AppInitializeHelper;
import ir.saeedrahimi.poolnegar.application.GlobalParams;
import ir.saeedrahimi.poolnegar.application.helper.SharedPreferencesHelper;

public class Activity_SplashScreen extends Activity_Base {

    Bundle mBundle;
    private int SPLASH_TIME_OUT = 300;

    public Activity_SplashScreen() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(BIND_AUTO_CREATE);
        GlobalParams.loadVariablesFromDB();
        setContentView(R.layout.activity_splash);
        updateStatusBarColor(R.color.splashStart, false);
        checkBundle();
    }

    private void checkBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mBundle = bundle;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        new Handler().postDelayed(() -> initAppStart(), SPLASH_TIME_OUT);

    }

    private void initAppStart() {

        AppInitializeHelper.initialApp();
        //Do Other Initialization
        //Do Database Upgrade
        final PreferencesManager mPrefManager = new PreferencesManager(this.getApplicationContext());

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            Boolean firstLaunchPassed = SharedPreferencesHelper.getBoolean(Activity_SplashScreen.this, "firstLaunchPassed");
            Boolean isBackupExists = SharedPreferencesHelper.getBoolean(Activity_SplashScreen.this, "backupv" + pInfo.versionName);
//            if ( (firstLaunchPassed != null && !firstLaunchPassed)  && (isBackupExists != null && isBackupExists)) {
//                db.dataBackUp(this, "dbv" + pInfo.versionName);
//                mPrefManager.settings.edit().putBoolean("backupv" + pInfo.versionName, false).apply();
//                db.repairDatabase();
//            }
            SharedPreferencesHelper.putVariable(Activity_SplashScreen.this, "firstLaunchPassed", true);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        final Intent passwordIntent = new Intent(Activity_SplashScreen.this, Activity_Password.class);
        final Intent mainIntent = new Intent(Activity_SplashScreen.this, MainActivity.class);
        passwordIntent.putExtra("Redirect", mainIntent);

        PFSecurityManager.getInstance().getPinCodeHelper().isPinCodeEncryptionKeyExist(result -> {
            if (!result.getResult()) {

                Boolean firstLaunchPassed = SharedPreferencesHelper.getBoolean(Activity_SplashScreen.this, "firstLaunchPassed");
                if (firstLaunchPassed != null && firstLaunchPassed) {
                    startActivity(mainIntent);
                } else {
                    startActivity(passwordIntent);
                }
            } else {
                startActivity(passwordIntent);
            }
            finish();
        });


    }

}
