package ir.saeedrahimi.poolnegar.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.multilevelview.MultiLevelRecyclerView;
import com.multilevelview.animators.DefaultItemAnimator;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.databinding.FragmentAccountsBinding;
import ir.saeedrahimi.poolnegar.models.AccountMultiLevel;
import ir.saeedrahimi.poolnegar.ui.activities.cu.ActivityCU_Account;
import ir.saeedrahimi.poolnegar.ui.adapters.AccountBaseListAdapter;
import ir.saeedrahimi.poolnegar.ui.helper.SnackBarHelper;

import static android.app.Activity.RESULT_OK;
import static ir.saeedrahimi.poolnegar.SubAccounts.RESULT_EDIT;

public class FragmentAccounts extends Fragment_Base<FragmentAccountsBinding> {
	public List<AccountMultiLevel> listAccounts;
	MultiLevelRecyclerView multiLevelRecyclerView;


	@Override
	protected FragmentAccountsBinding getFragmentBinding(LayoutInflater inflater, ViewGroup container) {

		return FragmentAccountsBinding.inflate(inflater, container, false);
	}
	@Override
	protected void onActionBarRightToolClicked() {
		Intent accountIntent = new Intent(getActivity(), ActivityCU_Account.class);
		accountIntent.putExtra("ParentId","");
		startActivityForResult(accountIntent, Constants.RESULTS.ADD_ITEM);
	}

	@Override
	protected String getActionBarTitle() {
		return "سرفصل حساب ها";
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		super.onCreate(savedInstanceState);
	}
	@Override
	public void onViewCreated(View view,Bundle savedInstanceState) {
		multiLevelRecyclerView = view.findViewById(R.id.rcvData);
		multiLevelRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
		this.multiLevelRecyclerView.setItemAnimator(new DefaultItemAnimator());
		this.multiLevelRecyclerView.setAccordion(true);

		fillAccounts();
	}
	private void fillAccounts()
	{
		listAccounts = AccountDAO.getRecursiveByParentId("");
		AccountBaseListAdapter myAdapter = new AccountBaseListAdapter(this.getActivity(), listAccounts, multiLevelRecyclerView);
		multiLevelRecyclerView.setAdapter(myAdapter);

	}
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		super.onActivityResult(requestCode,resultCode,data);
		if (requestCode == Constants.RESULTS.ADD_ITEM) {
			if(resultCode == RESULT_OK){
				fillAccounts();
				SnackBarHelper.showSnack(this.getActivity(), SnackBarHelper.SnackState.Success ,"حساب ایجاد شد");

			}
		}
		if (requestCode == RESULT_EDIT) {
			if(resultCode == RESULT_OK){
				fillAccounts();
				SnackBarHelper.showSnack(this.getActivity(), SnackBarHelper.SnackState.Success ,"تغیرات انجام شد");

			}
		}

	}

}
