package ir.saeedrahimi.poolnegar.ui.fragments;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.LargeValueFormatter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import ir.devage.hamrahpay.HamrahPay;
import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.SubAccounts;
import ir.saeedrahimi.poolnegar.application.helper.KeyHelper;
import ir.saeedrahimi.poolnegar.customs.TypeFaceProvider;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.database.DAO.TransactionDAO;
import ir.saeedrahimi.poolnegar.database.model.Account;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
import ir.saeedrahimi.poolnegar.databinding.FragmentHomeBinding;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_GeneralBase;
import ir.saeedrahimi.poolnegar.ui.activities.cu.Activity_Transaction;

import static androidx.navigation.fragment.NavHostFragment.findNavController;

public class FragmentHome extends Fragment_Base<FragmentHomeBinding> implements View.OnClickListener {

    private static final int RESULT_ACC = 0;
    public List<Account> listAccounts;
    private int[] selectedAcc;
    private String selectedTitle;

    public FragmentHome() {

    }

    public static FragmentHome newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        FragmentHome fragment = new FragmentHome();
        fragment.setArguments(args);
        return fragment;
    }

    public void fillList() {
        ArrayList<Integer> selected = new ArrayList<Integer>();
        selected.add(1);//haz
        selected.add(3);//sand
        selected.add(12);//proj
        selected.add(13);//ashkhas
        selected.add(6);//talab
        selected.add(8);//bedehi

        // TODO: Fix here
        // this.listAccounts = _db.getAccountsBalance(selected);

    }

    private void setData(int count, float range) {

        ArrayList<PieEntry> values = new ArrayList<PieEntry>();
        ArrayList<String> selected = new ArrayList<>();

        selected.add(Constants.ACCOUNTS.EXPENSE);//haz
        selected.add(Constants.ACCOUNTS.Banks);//sand
        selected.add(Constants.ACCOUNTS.PROJECTS);//proj
        selected.add(Constants.ACCOUNTS.PERSONS);//ashkhas
        selected.add(Constants.ACCOUNTS.BILLS);//talab
        selected.add(Constants.ACCOUNTS.DEBT);//bedehi
        this.listAccounts = new ArrayList<>();
        for (String accountId : selected) {
            Account acc = AccountDAO.selectByID(accountId);
            double transactionsBalance = TransactionDAO.getAccountBalance(accountId);
            if (acc != null) {
                acc.setTotalBalance(acc.getBalance() + transactionsBalance);
                listAccounts.add(acc);
            }

        }


        values.add(new PieEntry((float) listAccounts.get(0).getTotalBalance(), "هزینه"));


        values.add(new PieEntry((float) listAccounts.get(1).getTotalBalance(), "صندوق"));

        values.add(new PieEntry((float) listAccounts.get(2).getTotalBalance() * -1, "پروژه"));

        values.add(new PieEntry((float) listAccounts.get(3).getTotalBalance(), "اشخاص"));

        values.add(new PieEntry((float) listAccounts.get(4).getTotalBalance(), "طلب"));
        values.add(new PieEntry((float) listAccounts.get(5).getTotalBalance(), "بدهی"));

        PieDataSet dataSet = new PieDataSet(values, "حساب ها");
        dataSet.setSliceSpace(2f);
        final int[] COLORFUL_COLORS = {
                Color.rgb(193, 37, 82), Color.rgb(255, 102, 0), Color.rgb(245, 199, 0),
                Color.rgb(106, 150, 31), Color.rgb(179, 100, 53), Color.rgb(120, 20, 85)
        };
        dataSet.setColors(COLORFUL_COLORS);
        dataSet.setSelectionShift(1f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new LargeValueFormatter(""));
        data.setValueTextSize(10f);
        data.setValueTextColor(Color.BLACK);
        binding.chart.setData(data);
        binding.chart.setDrawEntryLabels(false);
        binding.chart.invalidate();
    }

    public void ShowNewTransaction(View item) {
        //Toast.makeText(this, "Hello World", Toast.LENGTH_LONG).show();
        //Intent myIntent = new Intent((MainActivity) getActivity(), AccountPage.class);
        //((MainActivity) getActivity()).startActivity(myIntent);

    }

    private void moveOffScreen() {

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int height = display.getHeight();  // deprecated

        int offset = (int) (height * 0.20); /* percent to move */

        LinearLayout.LayoutParams rlParams =
                (LinearLayout.LayoutParams) binding.chart.getLayoutParams();
        rlParams.setMargins(0, 0, 0, -offset);
        binding.chart.setLayoutParams(rlParams);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        binding.chart.setBackgroundColor(Color.TRANSPARENT);
        moveOffScreen();

        binding.chart.getDescription().setEnabled(false);


        binding.chart.setDrawHoleEnabled(true);
        binding.chart.setHoleColor(Color.TRANSPARENT);

        binding.chart.setTransparentCircleColor(Color.WHITE);
        binding.chart.setTransparentCircleAlpha(110);

        binding.chart.setHoleRadius(58f);
        binding.chart.setTransparentCircleRadius(71f);

        binding.chart.setDrawCenterText(false);

        binding.chart.setRotationEnabled(false);
        binding.chart.setHighlightPerTapEnabled(true);

        binding.chart.setMaxAngle(180f); // HALF CHART
        binding.chart.setRotationAngle(180f);
        setData(4, 100);
        binding.chart.animateY(1400, Easing.EaseInOutQuad);

        binding.chart.getLegend().setEnabled(false);

        // entry label styling
        binding.chart.setEntryLabelColor(Color.WHITE);
        binding.chart.setEntryLabelTypeface(TypeFaceProvider.get(getContext(), "vazir.ttf"));
        binding.chart.setEntryLabelTextSize(12f);

        binding.btnHomeTrans.setOnClickListener(this);
        binding.InTrans.setOnClickListener(this);
        binding.OutTrans.setOnClickListener(this);
        setHasOptionsMenu(true);



        if (HamrahPay.isPremium(getActivity(), Constants.paySKU)) {        // Check If User Have Premium Key
            binding.txtPremium.setVisibility(View.INVISIBLE);                // Disable Button
        }

        binding.txtPremium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((Activity_GeneralBase) getActivity()).doPayment();

            }
        });
        binding.toolBar.linearLeftActionBack.setVisibility(View.GONE);
        binding.toolBar.linearLeftActionBack.setVisibility(View.GONE);
        binding.toolBar.txtToolbarTitle.setVisibility(View.GONE);
        binding.toolBar.imgLogoPoolnegar.setVisibility(View.VISIBLE);
        binding.toolBar.supportToolbar.setBackgroundColor(getResources().getColor(R.color.white));
        updateStatusBarColor(R.color.white, true);


        if (binding.chart.isShown())
            new AsyncMainAccounts(this).execute();

        this.binding.lvData.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> paramAdapterView,
                                    View paramView, int position, long paramLong) {
                String parentId = listAccounts.get(position).getAccID();
                int actionId;
                if (parentId.equals(Constants.ACCOUNTS.PROJECTS))
                    actionId = R.id.action_dashboard_to_fragmentProjects;
                else if (parentId.equals(Constants.ACCOUNTS.BILLS))
                    actionId = R.id.action_dashboard_to_fragmentPayBills;
                else {
                    gotoSubAccount(listAccounts.get(position).getTitle(), listAccounts.get(position).getAccID());
                    return;
                }
                Bundle args = new Bundle();
                args.putString("parentId", parentId);
                findNavController(FragmentHome.this).navigate(actionId, args);

            }

            private void gotoSubAccount(String title, String accID) {
                Intent subIntent = new Intent(getActivity(), SubAccounts.class);
                subIntent.putExtra("SubName", title);
                subIntent.putExtra("ParentId", accID);
                startActivity(subIntent);
            }
        });

    }

    @Override
    public void onResume() {
        new AsyncMainAccounts(this).execute();
        super.onResume();
    }

    @Override
    public void onPause() {
        // new AsyncMainAccounts(this).execute();
        super.onPause();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        //inflater.inflate(R.menu.main_reason, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify height parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onClick(View view) {
        Bundle bundle = ActivityOptions.makeScaleUpAnimation(view, 0, 0, view.getWidth(), view.getHeight()).toBundle();
        switch (view.getId()) {
            case R.id.InTrans:
                bundle.putInt(KeyHelper.KEY_TRANSACTION_METHOD, TransactionItem.TRANSACTION_METHOD_INCOME);
                findNavController(this).navigate(R.id.action_dashboard_to_activity_Transaction, bundle);
                break;
            case R.id.OutTrans:
                bundle.putInt(KeyHelper.KEY_TRANSACTION_METHOD, TransactionItem.TRANSACTION_METHOD_PAYMENT);
                findNavController(this).navigate(R.id.action_dashboard_to_activity_Transaction, bundle);
                break;
            case R.id.btn_home_trans:
                findNavController(this).navigate(R.id.action_home_to_transactions);
                ///((MainActivity) getActivity()).NavigateToTab(MainActivity.INDEX_TRANSACTIONS);
                break;
            default:
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected FragmentHomeBinding getFragmentBinding(LayoutInflater inflater, ViewGroup container) {
        return FragmentHomeBinding.inflate(getLayoutInflater(), container, false);
    }

    @Override
    protected void onActionBarRightToolClicked() {

    }

    @Override
    protected String getActionBarTitle() {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
    class AsyncMainAccounts
            extends AsyncTask<Void, Integer, String> {
        FragmentHome frag;

        AsyncMainAccounts(FragmentHome frag) {

            this.frag = frag;
        }

        @Override
        protected void onPreExecute() {
            frag.binding.lvData.setEmptyView(frag.getActivity().findViewById(R.id.firstpage_emptylist));
            //frag.fillList();
        }

        @Override
        protected String doInBackground(Void... arg0) {
            frag.fillList();

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            this.frag.binding.lvData.setAdapter(new ArrayAdapter<Account>(frag.getActivity(), android.R.layout.simple_list_item_1, frag.listAccounts) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    Account acc = frag.listAccounts.get(position);
                    TextView amount;
                    TextView name;
                    ImageView img;
                    if (convertView == null) {
                        convertView = ((LayoutInflater) frag.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.rep_balance_accounts_row, parent, false);

                    }
                    amount = convertView.findViewById(R.id.rep_balance_acc_amount_row);
                    name = convertView.findViewById(R.id.rep_balance_acc_name_row);
                    img = convertView.findViewById(R.id.rep_balance_acc_image_row);
                    int[] colors = FragmentHome.this.binding.chart.getData().getColors();
                    img.setBackgroundColor(colors[position]);
                    name.setText(acc.getTitle());

                    if (acc.getTotalBalance() >= 0.0D) {
                        amount.setText(new DecimalFormat(" ###,###.## ").format(acc.getTotalBalance()));
                        amount.setTextColor(Color.parseColor("#000000"));
                        if (acc.getTitle().equals("پروژه ها")) {
                            amount.setText(new DecimalFormat(" ###,###.## ").format(acc.getTotalBalance() * -1));
                        }
                        return convertView;
                    }

                    amount.setText(new DecimalFormat(" ###,###.## ").format(acc.getTotalBalance()));
                    amount.setTextColor(Color.parseColor("#000000"));
                    if (acc.getTitle().equals("پروژه ها")) {
                        amount.setText(new DecimalFormat(" ###,###.## ").format(acc.getTotalBalance() * -1));
                    }
                    return convertView;
                }
            });
        }
    }
}
