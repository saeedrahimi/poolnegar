package ir.saeedrahimi.poolnegar.ui.activities.cu;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.DigitsKeyListener;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;
import com.mohamadamin.persianmaterialdatetimepicker.time.RadialPickerLayout;
import com.mohamadamin.persianmaterialdatetimepicker.time.TimePickerDialog;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Objects;

import ir.saeedrahimi.calculatorinput.activities.CalculatorActivity;
import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.helper.ConversionHelper;
import ir.saeedrahimi.poolnegar.application.helper.KeyHelper;
import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar;
import ir.saeedrahimi.poolnegar.customs.TypeFaceProvider;
import ir.saeedrahimi.poolnegar.customs.Utils;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.database.DAO.TransactionDAO;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
import ir.saeedrahimi.poolnegar.database.model.TransactionLink;
import ir.saeedrahimi.poolnegar.databinding.ActivityTransactionBinding;
import ir.saeedrahimi.poolnegar.dialogs.DialogYesNo;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_AccountSelection;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_ProjectSelection;
import ir.saeedrahimi.poolnegar.ui.dialogs.MessageDialog;
import ir.saeedrahimi.poolnegar.ui.helper.ToastHelper;

public class Activity_Transaction extends ActivityCU_Base<TransactionItem, ActivityTransactionBinding> implements DialogYesNo.onYesNoDialogClick, DatePickerDialog.OnDateSetListener, OnClickListener, TimePickerDialog.OnTimeSetListener {
    PersianCalendar mCalendar;
    boolean confirmFuture = false;
    private int mTransactionMethod;
    private TransactionLink mDefaultTransactionValue;



    @Override
    protected ActivityTransactionBinding getViewBinding() {
        return ActivityTransactionBinding.inflate(getLayoutInflater());
    }

    private void getExtras() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(KeyHelper.KEY_TRANSACTION_METHOD)) {
                mTransactionMethod = bundle.getInt(KeyHelper.KEY_TRANSACTION_METHOD, 1);
            }
            if (bundle.containsKey(KeyHelper.KEY_ENTITY_DEFAULTS)) {
                mDefaultTransactionValue = (TransactionLink) bundle.getSerializable(KeyHelper.KEY_ENTITY_DEFAULTS);
                if (mDefaultTransactionValue != null) {
                    mTransactionMethod = mDefaultTransactionValue.getMethod();
                }
            }
        }
    }

    @Override
    protected void onActivityFirstInit(Bundle savedInstanceState) {

        getExtras();
        binding.btnTargetAccount.setOnClickListener(this);
        binding.btnSourceAccount.setOnClickListener(this);
        binding.btnProject.setOnClickListener(this);
        binding.btnCalculator.setOnClickListener(this);
        binding.btnDate.setOnClickListener(this);
        binding.txtTime.setOnClickListener(this);

        binding.edtAmount.setKeyListener(DigitsKeyListener
                .getInstance("1234567890,."));
        binding.edtAmount.addTextChangedListener(Utils.MoneyTextWatcher);


        mCalendar = new PersianCalendar();
    }


    @Override
    protected String getActionBarTitle() {
        return getString(R.string.title_activity_new_transaction);
    }

    @Override
    public boolean validateEntity() {
        double amount = 0;
        try {

            amount = ConversionHelper.StringToDouble(this.binding.edtAmount.getText().toString());
        } catch (Exception localException) {
            MessageDialog msg = MessageDialog.getNewInstance(0, "خطا", "لطفا مبلغ ورودی را دوباره بررسی کنید");
            msg.showNow(getSupportFragmentManager(), "");
            return false;
        }
        if (Objects.equals(this.mEntity.getTargetAccountId(), "")) {
            MessageDialog msg = MessageDialog.getNewInstance(0, "خطا", "لطفا حساب مقصد را مشخص کنید");
            msg.showNow(getSupportFragmentManager(), "");

            return false;
        }
        if (Objects.equals(this.mEntity.getSourceAccountId(), "")) {
            MessageDialog msg = MessageDialog.getNewInstance(0, "خطا", "لطفا حساب مبدا را مشخص کنید");
            msg.showNow(getSupportFragmentManager(), "");
            return false;
        }
        if (((this.binding.edtAmount.getText().toString().equals(""))
                && (this.binding.edtAmount.getText().toString().equals("0")) || amount == 0)) {
            MessageDialog msg = MessageDialog.getNewInstance(0, "خطا", "مبلغ وارد نشده است");
            msg.showNow(getSupportFragmentManager(), "");
            return false;
        }
        if (this.mActivityState == ACTIVITY_STATE_EDIT_MODE && mCalendar.after(new PersianCalendar()) && !this.confirmFuture) {
            new DialogYesNo(this, this, null, getResources().getString(R.string.headerieerror), "تراکنش برای آینده است ایا مطمئن هستید ؟", "", false, "", false, "Future");
            return false;
        }

        return true;
    }

    @Override
    public void saveData() {
        double amount = ConversionHelper.StringToDouble(this.binding.edtAmount.getText().toString());

        mEntity.setNote(this.binding.txtNote.getText().toString());
        mEntity.setAmount(amount);
        mEntity.setRegDateFa(mCalendar.getPersianShortDate());
        mEntity.setRegDateGe(mCalendar.getTimeInMillis());
        mEntity.setTitle(binding.txtTitle.getText().toString());

        // Start Save Process
        if (this.mActivityState.equals(ACTIVITY_STATE_NEW_MODE)) {
            mEntity = TransactionDAO.insert(mEntity);
            ToastHelper.showMessage(this, getString(R.string.message_transaction_new_success), "", Toast.LENGTH_SHORT, Gravity.BOTTOM);
        } else if (this.mActivityState.equals(ACTIVITY_STATE_EDIT_MODE)) {
            TransactionDAO.update(mEntity);
            ToastHelper.showMessage(this, getString(R.string.message_transaction_edit_success), "", Toast.LENGTH_SHORT, Gravity.BOTTOM);

        }
        Intent backIntent = new Intent();
        backIntent.putExtra("Entity", this.mEntity);
        backIntent.putExtra("EntityId", this.mEntity.getTransactionId());
        setResult(RESULT_OK, backIntent);
        finish();
    }

    @Override
    public void onActivityStateNew() {
        this.mEntity = new TransactionItem();

        mEntity.setMethod(mTransactionMethod);

        if (mEntity.getMethod() == TransactionItem.TRANSACTION_METHOD_INCOME) {
            binding.rdIncomeMethod.setChecked(true);
            showIncomeView();
        }
        if(mDefaultTransactionValue != null){
            mEntity.setAmount(mDefaultTransactionValue.getAmount());
            mEntity.setProjectId(mDefaultTransactionValue.getProjectId());
            mEntity.setProjectTitle(mDefaultTransactionValue.getProjectTitle());
            mEntity.setNote(mDefaultTransactionValue.getNote());
            mEntity.setSourceAccountId(mDefaultTransactionValue.getSourceAccountId());
            mEntity.setSourceAccountTitle(mDefaultTransactionValue.getSourceAccountTitle());
            mEntity.setTargetAccountId(mDefaultTransactionValue.getTargetAccountId());
            mEntity.setTargetAccountTitle(mDefaultTransactionValue.getTargetAccountTitle());
        }
        applyEntityToView();
    }

    @Override
    public void onActivityStateEdit() {
        setActionBarTitle(getString(R.string.title_activity_edit_transaction));

        mCalendar.setPersianDate(mEntity.getRegDateFa());
        mCalendar.setTimeInMillis(mEntity.getRegDateGe());

        if (mEntity.getMethod() == TransactionItem.TRANSACTION_METHOD_INCOME) {
            binding.rdIncomeMethod.setChecked(true);
            showIncomeView();
        }
        applyEntityToView();

    }
    private void applyEntityToView(){
        this.binding.txtTitle.setText(mEntity.getTitle());
        this.binding.btnDate.setText(mCalendar.getPersianLongDate());
        this.binding.txtTime.setText(mCalendar.getShortTime());
        this.binding.txtNote.setText(mEntity.getNote());
        if(mEntity.getAmount()>0){
            this.binding.edtAmount.setText(  new DecimalFormat("#.##")
                    .format(mEntity.getAmount()));
        }else{
            this.binding.edtAmount.setText("");
        }
        binding.edtAmount.addTextChangedListener(Utils.MoneyTextWatcher);
        this.binding.btnProject.setText(mEntity.getProjectTitle());
        this.binding.btnTargetAccount.setText(mEntity.getTargetAccountTitle());
        this.binding.btnSourceAccount.setText(mEntity.getSourceAccountTitle());

        binding.rgTransactionMethod.setOnCheckedChangeListener(this::onTransactionMethodChanged);

    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == KeyHelper.REQUEST_CODE_CHOOSE_TARGET_ACCOUNT)) {
            if (resultCode == RESULT_OK) {
                this.mEntity.setTargetAccountId(data.getStringExtra("SelectedAccounts"));
                this.mEntity.setTargetAccountTitle(AccountDAO.getTitleByIdRecursive(mEntity.getTargetAccountId()));

                binding.btnTargetAccount.setText(mEntity.getTargetAccountTitle());

            }
        } else if ((requestCode == KeyHelper.REQUEST_CODE_CHOOSE_SOURCE_ACCOUNT)) {
            if (resultCode == RESULT_OK) {
                this.mEntity.setSourceAccountId(data.getStringExtra("SelectedAccounts"));
                this.mEntity.setSourceAccountTitle(AccountDAO.getTitleByIdRecursive(mEntity.getSourceAccountId()));

                binding.btnSourceAccount.setText(mEntity.getSourceAccountTitle());
            }
        } else if ((requestCode == KeyHelper.REQUEST_CODE_CHOOSE_PROJECT)) {
            if (resultCode == RESULT_OK) {
                this.mEntity.setProjectId(data.getStringExtra("SelectedAccounts"));
                this.mEntity.setProjectTitle(AccountDAO.getTitleById(mEntity.getProjectId()));
                binding.btnProject.setText(this.mEntity.getProjectTitle());
            }
        } else if ((requestCode == Constants.RESULTS.CALCULATOR)) {
            if (resultCode == CalculatorActivity.REQUEST_RESULT_SUCCESSFUL) {
                String result = data.getStringExtra(CalculatorActivity.RESULT);
                binding.edtAmount.setText(result);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onYesNoDialog(String token, boolean checked) {
        if (token.equals("Future")) {
            this.confirmFuture = true;
            validateAndSave();
        }

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        mCalendar.setPersianDate(year, monthOfYear + 1, dayOfMonth);
        mEntity.setRegDateFa(mCalendar.getPersianShortDate());
        mEntity.setRegDateGe(mCalendar.getTimeInMillis());
        binding.btnDate.setText(mCalendar.getPersianLongDate());
    }

    @Override
    public void onTimeSet(RadialPickerLayout radialPickerLayout, int hourOfDay, int minute) {
        mCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mCalendar.set(Calendar.MINUTE, minute);
        mEntity.setRegDateGe(mCalendar.getTimeInMillis());
        binding.txtTime.setText(mCalendar.getShortTime());
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.btnCalculator:
                Intent intent = new Intent(this, CalculatorActivity.class);
                startActivityForResult(intent, Constants.RESULTS.CALCULATOR);
                break;
            case R.id.btnSourceAccount:
                selectSourceAccount();
                break;
            case R.id.btnTargetAccount:
                selectTargetAccount();
                break;
            case R.id.btnProject:
                selectProjectAccount();
                break;
            case R.id.btnDate:
                selectDateDialog();
                break;
            case R.id.txtTime:
                selectTimeDialog();
                break;

//            case R.id.transaction_camera:
//                if (mCameraIntentHelper != null) {
//                    //setupCameraIntentHelper();
//                    //mCameraIntentHelper.startCameraIntent();
//                    setupGalleryIntentHelper();
//                    mGalleryIntentHelper.startIntent("camera");
//                }
//                break;
//            case R.id.transaction_image:
//                if (mGalleryIntentHelper != null) {
//                    setupGalleryIntentHelper();
//                    mGalleryIntentHelper.startIntent("gallery");
//                }
//                break;
            default:

        }
    }
    private String getTargetSelectionTitle() {
        if (mEntity.getMethod() == TransactionItem.TRANSACTION_METHOD_PAYMENT) {
            return getString(R.string.label_trans_payment_target);
        }
        return getString(R.string.title_activity_account_source_selection);
    }

    private String getSourceSelectionTitle() {

        if (mEntity.getMethod() == TransactionItem.TRANSACTION_METHOD_PAYMENT) {
            return getString(R.string.title_activity_account_source_selection);
        }
        return getString(R.string.label_trans_income_target);
    }

    public void selectTargetAccount() {
        Intent dlg = new Intent(this, Activity_AccountSelection.class);
        dlg.putExtra(Activity_AccountSelection.KEY_SHOW_PAYABLE, true);
        dlg.putExtra(Activity_AccountSelection.KEY_SHOW_RECEIVABLE, false);
        dlg.putExtra("Title", getTargetSelectionTitle());
        dlg.putExtras( getTargetAccountSelectorExtras());
        this.startActivityForResult(dlg, KeyHelper.REQUEST_CODE_CHOOSE_TARGET_ACCOUNT);
    }

    public void selectSourceAccount() {
        Intent dlg = new Intent(this, Activity_AccountSelection.class);
        dlg.putExtra(Activity_AccountSelection.KEY_SHOW_PAYABLE, false);
        dlg.putExtra(Activity_AccountSelection.KEY_SHOW_RECEIVABLE, true);
        dlg.putExtra("Title", getSourceSelectionTitle());
        this.startActivityForResult(dlg, KeyHelper.REQUEST_CODE_CHOOSE_SOURCE_ACCOUNT);
    }

    private Intent getTargetAccountSelectorExtras(){
        Intent result  = new Intent();
        if(this.mEntity.getMethod() == TransactionItem.TRANSACTION_METHOD_PAYMENT){
            result.putExtra(Activity_AccountSelection.KEY_SHOW_PAYABLE, true);
            result.putExtra(Activity_AccountSelection.KEY_SHOW_RECEIVABLE, false);
        }else{

            result.putExtra(Activity_AccountSelection.KEY_SHOW_PAYABLE, false);
            result.putExtra(Activity_AccountSelection.KEY_SHOW_RECEIVABLE, true);
        }

        return result;
    }

    public void selectProjectAccount() {
        Intent dlg = new Intent(this, Activity_ProjectSelection.class);
        dlg.putExtra("Title", getString(R.string.title_activity_project_selection));
        this.startActivityForResult(dlg, KeyHelper.REQUEST_CODE_CHOOSE_PROJECT);
    }

    public void selectDateDialog() {

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                Activity_Transaction.this,
                mCalendar.getPersianYear(),
                mCalendar.getPersianMonth() - 1,
                mCalendar.getPersianDay(),
                TypeFaceProvider.get(this, "vazir.ttf")
        );
        //datePickerDialog.setThemeDark(true);
        datePickerDialog.show(getFragmentManager(), "Datepickerdialog");
    }

    public void selectTimeDialog() {
        TimePickerDialog datePickerDialog = TimePickerDialog.newInstance(this,
                mCalendar.get(Calendar.HOUR_OF_DAY),
                mCalendar.get(Calendar.MINUTE), true);
        datePickerDialog.show(getFragmentManager(), "TimePickerdialog");
    }

    private void onTransactionMethodChanged(RadioGroup group, int checkedId) {
        clearAccounts();

        if (checkedId == binding.rdIncomeMethod.getId()) {
            mEntity.setMethod(TransactionItem.TRANSACTION_METHOD_INCOME);
            showIncomeView();
        } else if (checkedId == binding.rdPaymentMethod.getId()) {
            mEntity.setMethod(TransactionItem.TRANSACTION_METHOD_PAYMENT);
            showPaymentView();
        }
    }

    private void showPaymentView() {
        binding.vBoxTargetAccount.removeView(binding.btnSourceAccount);
        binding.vBoxSourceAccount.removeView(binding.btnTargetAccount);
        binding.vBoxSourceAccount.addView(binding.btnSourceAccount);
        binding.vBoxTargetAccount.addView(binding.btnTargetAccount);
        binding.txtTargetHeader.setText(getString(R.string.label_trans_payment_target));
    }

    private void showIncomeView() {
        binding.vBoxSourceAccount.removeView(binding.btnSourceAccount);
        binding.vBoxTargetAccount.removeView(binding.btnTargetAccount);
        binding.vBoxSourceAccount.addView(binding.btnTargetAccount);
        binding.vBoxTargetAccount.addView(binding.btnSourceAccount);
        binding.txtTargetHeader.setText(getString(R.string.label_trans_income_target));
    }
    private void clearAccounts(){
        mEntity.clearAccounts();
        binding.btnSourceAccount.setText("");
        binding.btnTargetAccount.setText("");
    }
}
