package ir.saeedrahimi.poolnegar.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.viewbinding.ViewBinding;
import ir.devage.hamrahpay.HamrahPay;
import ir.devage.hamrahpay.LastPurchase;
import ir.devage.hamrahpay.SupportInfo;
import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.databinding.FragmentProjectsBinding;

abstract public class Activity_GeneralBase<T extends ViewBinding> extends Activity_Base {
	
	ImageView btnRightTool;
    ImageView btnLeftTool;
    ImageView imgLogo;
    View linearRightAction;
    View linearLeftAction;
	TextView txtActionBarTitle;
	Toolbar actionBarView;
	protected T binding;
	protected abstract T getViewBinding();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		initBundleData();
		binding = getViewBinding();
		View view = binding.getRoot();
		setContentView(view);
		
		ViewGroup parent = getWindow().getDecorView().findViewById(android.R.id.content);
		
		//FontHelper.setViewTextStyleTypeFace(parent);
		
		initActionBar();
		
		onActivityFirstInit(savedInstanceState);
	}
	
	abstract protected void onActivityFirstInit(Bundle savedInstanceState);
	
	protected void initBundleData() {
		
	}
	

	public void initActionBar() {
		
		actionBarView = findViewById(R.id.supportToolbar);
		
		if (actionBarView != null) {
			
			btnRightTool = actionBarView.findViewById(R.id.btnRightTool);
			btnLeftTool = actionBarView.findViewById(R.id.imgActionBack);
			txtActionBarTitle = actionBarView.findViewById(R.id.txtToolbarTitle);

            linearLeftAction = actionBarView.findViewById(R.id.linearLeftActionBack);
            linearRightAction = actionBarView.findViewById(R.id.linearRightAction);
            imgLogo = actionBarView.findViewById(R.id.imgLogoPoolnegar);
			//FontHelper.setViewTextStyleTypeFace(actionBarView);

			if(btnRightTool != null){

                btnRightTool.setOnClickListener(view -> onActionBarRightToolClicked());
            }

			if(btnLeftTool != null) {
				btnLeftTool.setOnClickListener(view -> onActionBarLeftToolClicked());
			}

			if(linearLeftAction != null) {
				linearLeftAction.setOnClickListener(view -> onActionBarLeftToolClicked());
			}
			if(linearRightAction != null) {
				linearRightAction.setOnClickListener(view -> onActionBarRightToolClicked());
			}
			setActionBarTitle(getActionBarTitle());
			setSupportActionBar(actionBarView);
		}
	}
	
	abstract protected void onActionBarRightToolClicked();
	
	abstract protected void onActionBarLeftToolClicked();
	
	abstract protected String getActionBarTitle();

	public void setActionBarTitle(String title) {
		txtActionBarTitle.setText(title);
	}
	public void setActionBarLeftToolImageId(int id) {
		if (actionBarView != null)
			btnLeftTool = (ImageButton) actionBarView.findViewById(id);
	}
	
	public void setActionBarRightToolImageId(int id) {
		if (actionBarView != null)
			btnRightTool = (ImageButton) actionBarView.findViewById(id);
	}

	private HamrahPay hp=null;

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if(intent.getScheme().equals("hamrahpay"))
		{
			hp.verifyPayment();
		}
	}

	public void doPayment(){
		hp = new HamrahPay(Activity_GeneralBase.this)                // اکتیویتی که می خواهید از آنجا پرداخت انجام شود
				.sku(Constants.paySKU)                               // اضافه کردن شناسه به صفحه پرداخت
				.setApplicationScheme("poolnegar.test.rc") // باید با مقدار دلخواه خود جایگزین نمایید.
				//.enableChromeCustomeTab()
				.listener(new HamrahPay.Listener() {        // لیسنر برای آگاهی شما از موفق بودن یا نبودن پرداخت
					@Override
					public void onErrorOccurred(String status, String message) {
						// مشکلی در پرداخت روی داده است یا کاربر پرداخت را انجام نداده است
						Toast.makeText(Activity_GeneralBase.this,message,Toast.LENGTH_SHORT).show();
						Log.e("HamrahPay", status + ": " + message);
					}

					@Override
					public void onPaymentSucceed(String payCode) {
						// کاربر با موفقیت پرداخت را انجام داده است
						Toast.makeText(Activity_GeneralBase.this,"پرداخت موفقیت آمیز بوده است",Toast.LENGTH_SHORT).show();
						Log.i("HamrahPay", "payCode: " + payCode);
					}

					@Override
					public void onGetLastPurchaseInfo(LastPurchase lastPurchase) {

					}

					@Override
					public void onGetSupportInfo(SupportInfo supportInfo) {

					}

					@Override
					public void onGetDeviceID(String deviceID) {

					}
				})
				.setShouldShowHamrahpayDialog(false)
				.startPayment(); // شروع عملیات پرداخت
		// ------ end ---------------
	}
	
}
