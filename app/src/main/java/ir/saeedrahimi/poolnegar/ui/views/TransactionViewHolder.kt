package ir.saeedrahimi.poolnegar.ui.views

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ir.saeedrahimi.poolnegar.R
import ir.saeedrahimi.poolnegar.database.model.TransactionItem
import java.text.DecimalFormat

class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val txtSourceTitle: TextView = itemView.findViewById(R.id.accFrom)
    private val txtTargetTitle: TextView = itemView.findViewById(R.id.accTo)
    private val txtProjectTitle: TextView = itemView.findViewById(R.id.accProject)
    private val txtDate: TextView = itemView.findViewById(R.id.accduedate)
    private val txtAmount: TextView = itemView.findViewById(R.id.accamount)
    private val txtNote: TextView = itemView.findViewById(R.id.accdesc)
    private val separator: View = itemView.findViewById(R.id.separator)
    private val icon: ImageView = itemView.findViewById(R.id.accicontrans)

    fun bind(transaction: TransactionItem) {
        txtSourceTitle.text =  "از: " +  transaction.sourceAccountTitle
        txtTargetTitle.text =  "به: " + transaction.targetAccountTitle
        txtProjectTitle.text = "پروژه: "+  transaction.projectTitle
        txtDate.text = transaction.regDateFa
        txtAmount.text = DecimalFormat(" ###,###.## ")
                .format(transaction.amount)
        txtNote.text = transaction.note

        if(transaction.projectId == "")
        {
            txtProjectTitle.visibility = View.GONE
        }
        if (transaction.note.length > 1)
            txtNote.text = "توضیح: " + transaction.note
        else
            txtNote.visibility = View.GONE



        if (transaction.method == 0)
            icon.setBackgroundColor(itemView.context.resources.getColor(R.color.LimeGreen))
        else
            icon.setBackgroundColor(itemView.context.resources.getColor(R.color.outcome))

    }

    fun hideSeparator(hide: Boolean) {
        if (hide) {
            separator.visibility = View.GONE
        } else {
            separator.visibility = View.VISIBLE
        }
    }
}