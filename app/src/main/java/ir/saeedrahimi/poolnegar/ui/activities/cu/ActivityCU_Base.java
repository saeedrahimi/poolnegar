package ir.saeedrahimi.poolnegar.ui.activities.cu;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.viewbinding.ViewBinding;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.helper.KeyHelper;
import ir.saeedrahimi.poolnegar.database.model.BaseModel;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_Base;

public abstract class ActivityCU_Base<E extends BaseModel, T extends ViewBinding> extends Activity_Base implements View.OnClickListener {
    public static final Integer ACTIVITY_STATE_NEW_MODE = 2;
    public static final Integer ACTIVITY_STATE_EDIT_MODE = 3;

    public E mEntity;
    public T binding;
    Toolbar actionBarView;
    TextView txtActionBarTitle;
    View lvMainAction;
    View linearLeftAction;
    public Integer mActivityState = ACTIVITY_STATE_NEW_MODE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateStatusBarColor(R.color.ColorAccent, false);

        binding = getViewBinding();
        View view = binding.getRoot();
        setContentView(view);

        getBundleData();
        initView(savedInstanceState);
    }

    public void getBundleData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(KeyHelper.KEY_ACTIVITY_CU_STATE))
                this.mActivityState = Integer.valueOf(bundle.getInt(KeyHelper.KEY_ACTIVITY_CU_STATE));
            if (bundle.containsKey(KeyHelper.KEY_ENTITY))
                this.mEntity = (E) bundle.getSerializable(KeyHelper.KEY_ENTITY);
        }
    }

    private void initView(Bundle savedInstanceState) {

        initBaseComponents();
        onActivityFirstInit(savedInstanceState);
        if (this.mActivityState.equals(ACTIVITY_STATE_NEW_MODE)) {
            onActivityStateNew();
        } else if (this.mActivityState.equals(ACTIVITY_STATE_EDIT_MODE)) {
            onActivityStateEdit();
        }
    }


    @Override
    public void onBackPressed() {

        overridePendingTransition(R.anim.no_anitmation, R.anim.fade_out_dialog);
        super.onBackPressed();
    }

    public final void initBaseComponents() {
        updateStatusBarColor(R.color.green_light, false);

        this.actionBarView = findViewById(R.id.supportToolbar);
        this.txtActionBarTitle = this.actionBarView.findViewById(R.id.txtToolbarTitle);
        setActionBarTitle(getActionBarTitle());
        this.lvMainAction = findViewById(R.id.lv_action);

        linearLeftAction = actionBarView.findViewById(R.id.linearLeftActionBack);
        if (linearLeftAction != null) {
            linearLeftAction.setOnClickListener(this);
        }
        if (lvMainAction != null) {
            lvMainAction.setOnClickListener(this);
        }
    }

    public void onClick(View view) {
        int viewId = view.getId();
        switch (viewId) {
            case R.id.lv_action:
                validateAndSave();
                break;
            case R.id.linearLeftActionBack:
                onBackPressed();
                break;
        }
    }

    public void validateAndSave() {
        if (validateEntity()) {
            saveData();
            overridePendingTransition(R.anim.no_anitmation, R.anim.fade_out_dialog);
        }
    }

    public void alignBtnAcceptToBottom(int elementId) {

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) lvMainAction.getLayoutParams();
        if (elementId == 0) {
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
        }else{

            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, -1);
        }

    }

    abstract protected String getActionBarTitle();

    public void setActionBarTitle(String title) {
        txtActionBarTitle.setText(title);
    }

    public abstract boolean validateEntity();

    public abstract void saveData();

    public abstract void onActivityStateNew();

    public abstract void onActivityStateEdit();

    protected abstract T getViewBinding();
    abstract protected void onActivityFirstInit(Bundle savedInstanceState);
}
