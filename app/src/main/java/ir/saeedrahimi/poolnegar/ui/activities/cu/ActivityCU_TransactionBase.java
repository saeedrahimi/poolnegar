package ir.saeedrahimi.poolnegar.ui.activities.cu;

import androidx.viewbinding.ViewBinding;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;

import android.os.Bundle;

public abstract class ActivityCU_TransactionBase<T extends ViewBinding> extends ActivityCU_Base<TransactionItem, T> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
