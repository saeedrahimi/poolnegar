package ir.saeedrahimi.poolnegar.ui.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.kennyc.view.MultiStateView;

import java.lang.ref.WeakReference;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.helper.KeyHelper;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.database.model.Account;
import ir.saeedrahimi.poolnegar.databinding.ActivityEntityRecyclerListSelectionBinding;
import ir.saeedrahimi.poolnegar.ui.activities.cu.ActivityCU_Account;
import ir.saeedrahimi.poolnegar.ui.adapters.BaseFilterableRecyclerViewAdapter;
import ir.saeedrahimi.poolnegar.ui.adapters.ProjectRecyclerViewAdapter;
import ir.saeedrahimi.poolnegar.ui.helper.DelayTextWatcher;
import ir.saeedrahimi.poolnegar.ui.helper.SnackBarHelper;

public class Activity_ProjectSelection extends Activity_GeneralBase<ActivityEntityRecyclerListSelectionBinding> implements View.OnClickListener, BaseFilterableRecyclerViewAdapter.RecyclerViewEventListener {


    private List<Account> mProjectList;
    private ProjectRecyclerViewAdapter mAdapter;
    private LoadProjectsTask mLoadProjectsTask;

    private void getExtras() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("Title")) {
                String title = bundle.getString("Title");
                setActionBarTitle(title);
            }
        }
    }

    @Override
    protected void onActivityFirstInit(Bundle savedInstanceState) {
        getExtras();
        binding.rcvData.setLayoutManager(new LinearLayoutManager(this));
        this.mAdapter = new ProjectRecyclerViewAdapter(this, this);
        this.mAdapter.setIsFilterable(true);
        binding.rcvData.setLayoutManager(new LinearLayoutManager(this));
        binding.rcvData.setAdapter(mAdapter);
        binding.lvAction.setOnClickListener(this);
        binding.searchToolbar.txtSearch.addTextChangedListener(new DelayTextWatcher() {
            @Override
            protected void onAfterTextChanged() {

                Activity_ProjectSelection.this.mAdapter.getFilter().filter(binding.searchToolbar.txtSearch.getText().toString());
            }
        });
        ReloadDataAsync();
    }

    @Override
    protected ActivityEntityRecyclerListSelectionBinding getViewBinding() {
        return ActivityEntityRecyclerListSelectionBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void onActionBarRightToolClicked() {


    }

    @Override
    protected void onActionBarLeftToolClicked() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    protected String getActionBarTitle() {
        return getResources().getString(R.string.title_activity_account_selection);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lv_action:
                Intent accountIntent = new Intent(this, ActivityCU_Account.class);
                accountIntent.putExtra("ParentId", Constants.ACCOUNTS.PROJECTS);
                startActivityForResult(accountIntent, KeyHelper.REQUEST_CODE_CU_ACCOUNT);
                break;
        }
    }
    public void ReloadDataAsync() {
        if (mLoadProjectsTask != null && !mLoadProjectsTask.isCancelled())
            mLoadProjectsTask.cancel(true);

        mLoadProjectsTask = new LoadProjectsTask(this);
        mLoadProjectsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == KeyHelper.REQUEST_CODE_CU_ACCOUNT) {
            if(resultCode == RESULT_OK){
                ReloadDataAsync();
                SnackBarHelper.showSnack(this, SnackBarHelper.SnackState.Success ,"حساب ایجاد شد");

            }
        }

    }

    @Override
    public void onItemSelectionChanged(int position, boolean checkedState) {

    }

    @Override
    public void onSelectionModeChanged(boolean selectionMode) {

    }

    @Override
    public void onViewTapped(int viewType, int position) {
        Intent intent = new Intent();

        intent.putExtra("SelectedAccounts", mAdapter.getItem(position).getAccID());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onContextMenuTapped(int position, int actionId) {

    }

    @Override
    public void onDataChanged(int viewType, int position) {

    }

    private static class LoadProjectsTask extends AsyncTask<Void, Void, List<Account>> {
        private WeakReference<Activity_ProjectSelection> fragmentReference;

        LoadProjectsTask(Activity_ProjectSelection context) {
            fragmentReference = new WeakReference<>(context);
        }

        @Override
        protected void onPreExecute() {
            // get a reference to the activity if it is still there
            Activity_ProjectSelection activity = fragmentReference.get();
            if (activity == null || activity.isDestroyed()) return;

            activity.binding.multiStateView.setViewState(MultiStateView.ViewState.LOADING);
        }

        @Override
        protected List<Account> doInBackground(Void... voids) {
            Activity_ProjectSelection activity = fragmentReference.get();
            if (activity == null || activity.isDestroyed()) return null;

            return AccountDAO.getChildren(Constants.ACCOUNTS.PROJECTS);
        }

        @Override
        protected void onPostExecute(List<Account> result) {

            Activity_ProjectSelection activity = fragmentReference.get();
            if (activity == null || activity.isDestroyed()) return;


            activity.mAdapter.clearAll();
            activity.mAdapter.addRange(result);

            if (result.size() > 0) {
                activity.binding.multiStateView.setViewState(MultiStateView.ViewState.CONTENT);
            } else {
                activity.binding.multiStateView.setViewState(MultiStateView.ViewState.EMPTY);
            }
        }
    }


}
