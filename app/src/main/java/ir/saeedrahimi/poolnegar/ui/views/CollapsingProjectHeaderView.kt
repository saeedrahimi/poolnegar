package ir.saeedrahimi.poolnegar.ui.views

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import ir.saeedrahimi.poolnegar.R

class CollapsingProjectHeaderView : LinearLayout {

    private var isProjectLocked: Boolean = true;
    private var projectDrawable: Int = 0
    private var projectName: String? = null
    private var subtitle: String? = null
    private var misc: String? = null
    private var miscIcon: Int = 0
    private var projectNameTextSize: Int = 0
    private var projectSubtitleTextSize: Int = 0
    private var projectMiscTextSize: Int = 0

    private lateinit var projectImage: ImageView
    private lateinit var projectNameTextView: TextView
    private lateinit var subtitleTextView: TextView
    private lateinit var miscTextView: TextView
    private lateinit var btnEdit: ImageButton
    private lateinit var btnDelete: ImageButton
    private lateinit var btnLock: ImageButton

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {

        val a = context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.CollapsingProjectHeaderView,
                0, 0)

        try {
            projectDrawable = a.getResourceId(R.styleable.CollapsingProjectHeaderView_projectImage, 0)
            projectName = a.getString(R.styleable.CollapsingProjectHeaderView_projectName)
            subtitle = a.getString(R.styleable.CollapsingProjectHeaderView_projectSubtitle)
            misc = a.getString(R.styleable.CollapsingProjectHeaderView_projectMisc)
            miscIcon = a.getResourceId(R.styleable.CollapsingProjectHeaderView_projectMiscIcon, 0)
            projectNameTextSize = a.getResourceId(R.styleable.CollapsingProjectHeaderView_projectNameTextSizeSp, 20)
            projectSubtitleTextSize = a.getResourceId(R.styleable.CollapsingProjectHeaderView_projectSubtitleTextSizeSp, 12)
            projectMiscTextSize = a.getResourceId(R.styleable.CollapsingProjectHeaderView_projectMiscTextSizeSp, 15)
        } finally {
            a.recycle()
        }

        val inflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        inflater.inflate(R.layout.view_collapsing_project_header, this, true)

        loadViews()
        applyAttributes()
    }

    private fun loadViews() {
        projectImage = this.findViewById(R.id.projectImage)
        projectNameTextView = this.findViewById(R.id.projectName)
        subtitleTextView = this.findViewById(R.id.projectSubtitle)
        miscTextView = this.findViewById(R.id.projectMisc)
        btnEdit = this.findViewById(R.id.btnEdit)
        btnDelete = this.findViewById(R.id.btnDelete)
        btnLock = this.findViewById(R.id.btnLock)
    }

    private fun applyAttributes() {
        projectImage.setImageResource(projectDrawable)
        projectNameTextView.text = projectName
        projectNameTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, projectNameTextSize.toFloat())
        subtitleTextView.text = subtitle
        subtitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, projectSubtitleTextSize.toFloat())
        miscTextView.text = misc
        miscTextView.setCompoundDrawablesWithIntrinsicBounds(0,0, miscIcon, 0)
        miscTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, projectMiscTextSize.toFloat())
    }

    fun getProjectDrawable(): Int {
        return projectDrawable
    }

    fun setProjectDrawable(projectDrawable: Int) {
        this.projectDrawable = projectDrawable
        projectImage.setImageResource(projectDrawable)
    }

    fun getProjectName(): String? {
        return projectName
    }

    fun setProjectName(projectName: String) {
        this.projectName = projectName
        projectNameTextView.text = projectName
    }

    fun getSubtitle(): String? {
        return subtitle
    }

    fun setSubtitle(subtitle: String) {
        this.subtitle = subtitle
        subtitleTextView.text = subtitle
    }

    fun getMisc(): String? {
        return misc
    }

    fun setMisc(misc: String) {
        this.misc = misc
        miscTextView.text = misc
    }

    fun getMiscIcon(): Int {
        return miscIcon
    }

    fun setMiscIcon(miscIcon: Int) {
        this.miscIcon = miscIcon
        miscTextView.setCompoundDrawablesWithIntrinsicBounds(miscIcon, 0, 0, 0)
    }

    fun setDeleteListener(listener: OnClickListener) {
        this.btnDelete.setOnClickListener(listener)
    }

    fun setEditListener(listener: OnClickListener) {
        this.btnEdit.setOnClickListener(listener)
    }
    fun setLockListener(listener: OnClickListener) {
        this.btnLock.setOnClickListener(listener)
    }

    fun setProjectLockView() {
        this.btnLock.setImageResource(R.drawable.ic_lock_outline_black_24dp)
        this.btnDelete.visibility  = View.GONE
        this.isProjectLocked = true

    }
    fun setProjectUnlockView() {
        this.btnLock.setImageResource(R.drawable.ic_lock_open_black_24dp)
        this.btnDelete.visibility  = View.VISIBLE
        this.isProjectLocked = false

    }
    fun isProjectLocked(): Boolean {
        return this.isProjectLocked
    }


}