package ir.saeedrahimi.poolnegar.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.beautycoder.pflockscreen.PFFLockScreenConfiguration;
import com.beautycoder.pflockscreen.fragments.PFLockScreenFragment;
import com.beautycoder.pflockscreen.security.PFResult;
import com.beautycoder.pflockscreen.security.PFSecurityManager;
import com.beautycoder.pflockscreen.security.callbacks.PFPinCodeHelperCallback;
import com.beautycoder.pflockscreen.viewmodels.PFPinCodeViewModel;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.PreferencesManager;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.helper.SharedPreferencesHelper;

public class Activity_Password extends Activity_Base {

    private final PFLockScreenFragment.OnPFLockScreenLoginListener mLoginListener =
            new PFLockScreenFragment.OnPFLockScreenLoginListener() {

                @Override
                public void onCodeInputSuccessful() {
                    Toast.makeText(Activity_Password.this, "رمزعبور صحیح است", Toast.LENGTH_SHORT).show();
                    redirect();
                }

                @Override
                public void onFingerprintSuccessful() {
                    Toast.makeText(Activity_Password.this, "اثرانگشت مجاز", Toast.LENGTH_SHORT).show();
                    redirect();
                }

                @Override
                public void onPinLoginFailed() {
                    Toast.makeText(Activity_Password.this, "رمزعبور اشتباه است", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFingerprintLoginFailed() {
                    Toast.makeText(Activity_Password.this, "اثرانگشت غیرمجاز", Toast.LENGTH_SHORT).show();
                }
            };
    private final PFLockScreenFragment.OnPFLockScreenCodeCreateListener mCodeCreateListener =
            new PFLockScreenFragment.OnPFLockScreenCodeCreateListener() {
                @Override
                public void onCodeCreated(String encodedCode) {
                    Toast.makeText(Activity_Password.this, "رمزعبور فعال شد", Toast.LENGTH_SHORT).show();
                    SharedPreferencesHelper.putVariable(Activity_Password.this, "pincode", encodedCode);
                    SharedPreferencesHelper.putVariable(Activity_Password.this, "password_enabled", true);
                    redirect();
                }

                @Override
                public void onNewCodeValidationFailed() {
                    Toast.makeText(Activity_Password.this, "رمزعبور و تکرار آن یکسان نیستند", Toast.LENGTH_SHORT).show();
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        updateStatusBarColor(R.color.splashStart, false);
        showLockScreenFragment();
    }

    private void showLockScreenFragment() {
        new PFPinCodeViewModel().isPinCodeEncryptionKeyExist().observe(
                this,
                new Observer<PFResult<Boolean>>() {
                    @Override
                    public void onChanged(@Nullable PFResult<Boolean> result) {
                        if (result == null) {
                            return;
                        }
                        if (result.getError() != null) {
                            Toast.makeText(Activity_Password.this, "خطا هنگام دریافت رمزعبور", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        showLockScreenFragment(result.getResult());
                    }
                }
        );
    }

    private void showLockScreenFragment(boolean isPinExist) {
        final PFFLockScreenConfiguration.Builder builder = new PFFLockScreenConfiguration.Builder(this)
                .setTitle(isPinExist ? "رمزعبور یا اثرانگشت خود را وارد کنید" : "تعیین رمزعبور ورود به برنامه")
                .setCodeLength(4)
                .setLeftButton("فراموشی رمزعبور")
                .setNewCodeValidation(true)
                .setNewCodeValidationTitle("لطفا رمزعبور را دوباره وارد کنید")
                .setUseFingerprint(true);
        final PFLockScreenFragment fragment = new PFLockScreenFragment();

        fragment.setOnLeftButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Activity_Password.this, "Left button pressed", Toast.LENGTH_LONG).show();
            }
        });

        builder.setMode(isPinExist
                ? PFFLockScreenConfiguration.MODE_AUTH
                : PFFLockScreenConfiguration.MODE_CREATE);
        if (isPinExist) {
            String pinCode = SharedPreferencesHelper.getString(Activity_Password.this, "pincode");
            if(pinCode !=null){
                fragment.setEncodedPinCode(pinCode);
                fragment.setLoginListener(mLoginListener);
            }else {
                PFSecurityManager.getInstance().getPinCodeHelper().delete(new PFPinCodeHelperCallback<Boolean>() {
                    @Override
                    public void onResult(PFResult<Boolean> result) {

                        SharedPreferencesHelper.putVariable(Activity_Password.this, "password_enabled", false);
                    }
                });
            }
        }

        fragment.setConfiguration(builder.build());
        fragment.setCodeCreateListener(mCodeCreateListener);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_view, fragment).commit();

    }

    private void redirect() {
        Intent intent;
        if (getIntent().hasExtra("Redirect")) {
            intent = (Intent) getIntent().getParcelableExtra("Redirect");
        } else if (getIntent().hasExtra("request_result")) {
            Intent backIntent = new Intent();
            setResult(Constants.RESULTS.RESULT_OK, backIntent);
            finish();
            return;
        } else {
            intent = new Intent(getBaseContext(), MainActivity.class);
        }
        startActivity(intent);
        finish();
    }
}
