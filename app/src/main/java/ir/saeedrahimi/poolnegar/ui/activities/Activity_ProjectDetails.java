package ir.saeedrahimi.poolnegar.ui.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.TextView;

import com.kennyc.view.MultiStateView;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.helper.KeyHelper;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.database.DAO.LinkDAO;
import ir.saeedrahimi.poolnegar.database.DAO.TransactionDAO;
import ir.saeedrahimi.poolnegar.database.model.Account;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
import ir.saeedrahimi.poolnegar.database.model.TransactionLink;
import ir.saeedrahimi.poolnegar.databinding.ActivityProjectDetailsBinding;
import ir.saeedrahimi.poolnegar.databinding.FragmentProjectCommandsBinding;
import ir.saeedrahimi.poolnegar.databinding.FragmentTransactionsListviewBinding;
import ir.saeedrahimi.poolnegar.dialogs.DialogTransactionDetails;
import ir.saeedrahimi.poolnegar.models.TransactionFilter;
import ir.saeedrahimi.poolnegar.ui.activities.cu.ActivityCU_Account;
import ir.saeedrahimi.poolnegar.ui.activities.cu.ActivityCU_Base;
import ir.saeedrahimi.poolnegar.ui.activities.cu.Activity_Transaction;
import ir.saeedrahimi.poolnegar.ui.adapters.TransactionLinkDraggableAdapter;
import ir.saeedrahimi.poolnegar.ui.adapters.TransactionSimpleAdapter;
import ir.saeedrahimi.poolnegar.ui.views.TransactionViewHolder;


public class Activity_ProjectDetails extends Activity_Base implements
        DialogTransactionDetails.OnDialogClickListener,
        DialogTransactionDetails.OnTransactionEdit,
        DialogTransactionDetails.OnTransactionDelete,
        ViewPager.OnPageChangeListener,
        TransactionSimpleAdapter.onTransactionClick,
        TransactionLinkDraggableAdapter.onTransactionLinkClick {

    public SmartTabLayout mViewPagerTab;
    public AccelerateDecelerateInterpolator mSmoothInterpolator;
    protected ActivityProjectDetailsBinding binding;
    ProjectDetailsPagerAdapter mPagerAdapter;
    ViewPager mViewPager;
    RecyclerView.Adapter<TransactionViewHolder> mIncomeAdapter, mPaymentAdapter;
    MultiStateView mIncomeMultiStateView, mPaymentMultiStateView;
    TransactionLinkDraggableAdapter mLinkAdapter;
    List<TransactionItem> mListIncome;
    List<TransactionItem> mListPayment;
    List<TransactionLink> mListLink;
    TransactionFilter filter;
    Account mProjectAccount;
    LoadDetailsTask loadDetailsTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityProjectDetailsBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        updateStatusBarColor(R.color.green_light, false);

        initComponents();
    }

    public void setActionBarTitle(String title) {

        binding.projectHeaderView.setProjectName(title);
    }


    private void initComponents() {
        mPagerAdapter = new ProjectDetailsPagerAdapter(this, new String[]{"پرداخت ها", "دریافت ها", "عملیات"});
        mViewPager = binding.viewPager;
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setCurrentItem(2);
        mViewPagerTab = binding.viewpagertab;
        mViewPagerTab.setViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(3);
        mViewPagerTab.setOnPageChangeListener(this);


        mSmoothInterpolator = new AccelerateDecelerateInterpolator();


        Intent localBundle = getIntent();
        this.mProjectAccount = (Account) localBundle.getSerializableExtra(KeyHelper.KEY_ENTITY);
        if (mProjectAccount == null) {
            finish();
        }

        binding.projectHeaderView.setEditListener(this::onBtnEditClick);
        binding.projectHeaderView.setDeleteListener(this::onBtnDeleteClick);
        binding.projectHeaderView.setLockListener(this::onBtnLockClick);
        if (mProjectAccount.isClosed()) {
            binding.projectHeaderView.setProjectUnlockView();
        } else {
            binding.projectHeaderView.setProjectLockView();

        }
        setActionBarTitle(mProjectAccount.getTitle());


        this.mListIncome = new ArrayList<>();
        this.mListPayment = new ArrayList<>();
        this.mListLink = new ArrayList<>();

        this.mIncomeAdapter = new TransactionSimpleAdapter(mListIncome, this);
        this.mPaymentAdapter = new TransactionSimpleAdapter(mListPayment, this);


        this.mLinkAdapter = new TransactionLinkDraggableAdapter(mListLink, this, R.layout.list_item_transaction_defaults, R.id.drag_handle, true, true);


        ReloadDataAsync();
    }


    @Override
    public void onDestroy() {
        if (loadDetailsTask != null && !loadDetailsTask.isCancelled())
            loadDetailsTask.cancel(true);
        super.onDestroy();
        binding = null;
    }

    @Override
    public void onTransactionClick(TransactionItem trans) {
        new DialogTransactionDetails(this, this, this, this, this, trans);

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onTransactionLinkClick(@Nullable TransactionLink link) {
        if (link == null) return;
        Intent localIntent = new Intent(this, Activity_Transaction.class);
        localIntent.putExtra(KeyHelper.KEY_ENTITY_DEFAULTS, link);
        startActivityForResult(localIntent, KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION);
    }

    @Override
    public void onTransactionLinkDelete(@NotNull TransactionLink link, int position) {

    }

    private void ReloadDataAsync() {
        if (loadDetailsTask != null && !loadDetailsTask.isCancelled())
            loadDetailsTask.cancel(true);

        loadDetailsTask = new LoadDetailsTask(this);
        loadDetailsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onDialogDeleteTransaction() {
        ReloadDataAsync();
    }

    @Override
    public void onDialogEditTransaction(TransactionItem transaction) {
        Intent intent = new Intent(this, Activity_Transaction.class);
        intent.putExtra(KeyHelper.KEY_ENTITY, transaction);
        intent.putExtra(KeyHelper.KEY_ACTIVITY_CU_STATE, ActivityCU_Base.ACTIVITY_STATE_EDIT_MODE);
        startActivityForResult(intent, KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION);
    }

    @Override
    public void onDialogDeleteClick() {
        ReloadDataAsync();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION) {
            if (resultCode == RESULT_OK) {
                ReloadDataAsync();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageSelected(int position) {

    }

    private void onBtnEditClick(View v) {
        Intent accountIntent = new Intent(this, ActivityCU_Account.class);
        accountIntent.putExtra(KeyHelper.KEY_ENTITY, mProjectAccount);
        accountIntent.putExtra(KeyHelper.KEY_ACTIVITY_CU_STATE, ActivityCU_Base.ACTIVITY_STATE_EDIT_MODE);
        startActivityForResult(accountIntent, KeyHelper.REQUEST_CODE_CU_ACCOUNT);
    }

    private void onBtnLockClick(View v) {
        if (mProjectAccount.isClosed()) {
            openProject();
        } else {
            closeProject();
        }


    }

    private void onBtnDeleteClick(View v) {
        final Dialog questionDialog = new Dialog(this);
        questionDialog.requestWindowFeature(1);
        questionDialog.setContentView(R.layout.dialog_warning);
        TextView error_title = questionDialog.findViewById(R.id.yesnotitle);
        TextView error_body = questionDialog.findViewById(R.id.yesnomessage);
        Button ok_btn = questionDialog.findViewById(R.id.dialog_yes);
        Button cancel_btn = questionDialog.findViewById(R.id.dialog_no);
        error_title.setText("حذف پروژه");
        error_body.setText("این پروژه و تمامی تراکنش‌های مربوط به آن حذف خواهد شد، آیا اطمینان دارید؟");
        ok_btn.setOnClickListener(arg0 -> {

            Log.d("deleting Project", "" + mProjectAccount.getTitle());
            AccountDAO.deleteById(mProjectAccount.getAccID());
            TransactionDAO.deleteByProjectId(mProjectAccount.getAccID());

            Activity_ProjectDetails.this.setResult(RESULT_OK);
            Activity_ProjectDetails.this.finish();
            questionDialog.dismiss();
        });
        cancel_btn.setOnClickListener(arg0 -> {
            questionDialog.dismiss();

        });
        questionDialog.show();
    }

    private void openProject() {
        mProjectAccount.setClosed(0);
        AccountDAO.update(mProjectAccount);
        ReloadDataAsync();
        binding.projectHeaderView.setProjectLockView();
    }

    private void closeProject() {
        final Dialog questionDialog = new Dialog(this);
        questionDialog.requestWindowFeature(1);
        questionDialog.setContentView(R.layout.dialog_warning);
        TextView error_title = questionDialog.findViewById(R.id.yesnotitle);
        TextView error_body = questionDialog.findViewById(R.id.yesnomessage);
        Button ok_btn = questionDialog.findViewById(R.id.dialog_yes);
        Button cancel_btn = questionDialog.findViewById(R.id.dialog_no);
        error_title.setText("بستن پروژه ");
        error_body.setText("ایا میخواهید این پروژه را ببندید ؟");
        ok_btn.setOnClickListener(arg0 -> {

            mProjectAccount.setClosed(1);
            AccountDAO.update(mProjectAccount);
            ReloadDataAsync();
            questionDialog.dismiss();
            binding.projectHeaderView.setProjectUnlockView();
        });
        cancel_btn.setOnClickListener(arg0 -> questionDialog.dismiss());
        questionDialog.show();
    }

    private static class LoadDetailsTask extends AsyncTask<Void, Void, Void> {

        private WeakReference<Activity_ProjectDetails> activityReference;

        LoadDetailsTask(Activity_ProjectDetails context) {
            activityReference = new WeakReference<>(context);
        }

        @Override
        protected void onPreExecute() {
            Activity_ProjectDetails activity = activityReference.get();
            if (activity == null || activity.isDestroyed()) return;
        }

        @Override
        protected Void doInBackground(Void... values) {
            Activity_ProjectDetails activity = activityReference.get();
            if (activity == null || activity.isDestroyed()) return null;

            List<TransactionItem> inList = TransactionDAO.getAllByProject(activity.mProjectAccount.getAccID(), 0, "", "");
            activity.mListIncome.clear();
            activity.mListIncome.addAll(inList);

            List<TransactionItem> outList = TransactionDAO.getAllByProject(activity.mProjectAccount.getAccID(), 1, "", "");
            activity.mListPayment.clear();
            activity.mListPayment.addAll(outList);

            List<TransactionLink> linkList = LinkDAO.getProjectLinks();
            activity.mListLink.clear();
            activity.mListLink.addAll(linkList);

            activity.mProjectAccount.setTotalOutcome(TransactionDAO.getProjectIncomeBalance(activity.mProjectAccount.getAccID()));
            activity.mProjectAccount.setTotalOutcome(TransactionDAO.getProjectPaymentBalance(activity.mProjectAccount.getAccID()));

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Activity_ProjectDetails activity = activityReference.get();
            if (activity == null || activity.isDestroyed()) return;

            if (activity.mListIncome.size() > 0) {
                activity.mIncomeMultiStateView.setViewState(MultiStateView.ViewState.CONTENT);
            } else {
                activity.mIncomeMultiStateView.setViewState(MultiStateView.ViewState.EMPTY);
            }
            activity.mIncomeAdapter.notifyDataSetChanged();

            if (activity.mListPayment.size() > 0) {
                activity.mPaymentMultiStateView.setViewState(MultiStateView.ViewState.CONTENT);
            } else {
                activity.mPaymentMultiStateView.setViewState(MultiStateView.ViewState.EMPTY);
            }
            activity.mPaymentAdapter.notifyDataSetChanged();
            activity.mLinkAdapter.notifyDataSetChanged();

            String desc = "پرداخت: " + activity.mProjectAccount.getTotalOutcome();
            desc +=  "سود: " + new DecimalFormat(" ###,###.## ")
                    .format((activity.mProjectAccount.getBalance() + activity.mProjectAccount.getTotalOutcome()) * -1);
            desc += "دریافت: " + activity.mProjectAccount.getTotalIncome();
            activity.binding.projectHeaderView.setMisc(desc);

            //inbalance.setText(new DecimalFormat(" ###,###.## ").format(ProjectDetailActivity.this.account.getTotalIncome()));
            //expbalance.setText(new DecimalFormat(" ###,###.## ").format(ProjectDetailActivity.this.account.getTotalOutcome()));
            //endbalance.setText(new DecimalFormat(" ###,###.## ").format(endBalance));

            //ImageView endimg = ProjectDetailActivity.this.findViewById(R.id.endingbalancecolor);

//            if (endBalance > 0)
//                endimg.setBackgroundColor(ProjectDetailActivity.this.getResources().getColor(R.color.MediumSeaGreen));
//            if (endBalance == 0)
//                endimg.setBackgroundColor(ProjectDetailActivity.this.getResources().getColor(R.color.Gray));
//            LinearLayout endLayout = ProjectDetailActivity.this.findViewById(R.id.layout_endingbalance);
//            endLayout.setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View arg0) {
//                    double introVal = (account.getIntroPercent() * endBalance) / 100;
//                    double workVal = (account.getWorkPercent() * endBalance) / 100;
//                    AlertDialog.Show(ProjectDetailActivity.this, "تفکیک سود", "سود معرف: (%" + account.getIntroPercent() + ")" + new DecimalFormat(" ###,###.## ").format(introVal) + "\n\nسود باقی: (%" + (100 - account.getIntroPercent()) + ")" + new DecimalFormat(" ###,###.## ").format(endBalance - introVal) + "\n-----------------\n" + "سود انجام کار: (%" + account.getWorkPercent() + ")" + new DecimalFormat(" ###,###.## ").format(workVal) + "\n\n" + "سود باقی: (%" + (100 - account.getWorkPercent()) + ")" + new DecimalFormat(" ###,###.## ").format(endBalance - workVal) + "\n");
//
//                }
//            });
            super.onPostExecute(result);
        }
    }

    static class ProjectDetailsPagerAdapter extends PagerAdapter {

        private final String[] TITLES;
        private WeakReference<Activity_ProjectDetails> activityReference;

        ProjectDetailsPagerAdapter(Activity_ProjectDetails context, String[] titles) {
            activityReference = new WeakReference<>(context);
            this.TITLES = titles;
        }

        @Override
        public int getCount() {
            return this.TITLES.length;
        }

        @Override
        public boolean isViewFromObject(@NotNull View arg0, @NotNull Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, @NotNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return this.TITLES[position];
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            Activity_ProjectDetails activity = activityReference.get();
            if (activity == null || activity.isDestroyed()) return null;
            switch (position) {

                case 0:
                    FragmentTransactionsListviewBinding outBinding = FragmentTransactionsListviewBinding.inflate(activity.getLayoutInflater(), null, false);
                    outBinding.rcvData.setLayoutManager(new LinearLayoutManager(activity));
                    outBinding.rcvData.setAdapter(activity.mPaymentAdapter);
                    outBinding.rcvData.setTag("lvBill");
                    activity.mPaymentMultiStateView = outBinding.multiStateView;
                    container.addView(outBinding.getRoot(), 0);
                    return outBinding.getRoot();
                case 1:

                    FragmentTransactionsListviewBinding inBinding = FragmentTransactionsListviewBinding.inflate(activity.getLayoutInflater(), null, false);

                    inBinding.rcvData.setLayoutManager(new LinearLayoutManager(activity));
                    inBinding.rcvData.setAdapter(activity.mIncomeAdapter);
                    inBinding.rcvData.setTag("lvPays");
                    activity.mIncomeMultiStateView = inBinding.multiStateView;

                    container.addView(inBinding.getRoot(), 0);
                    return inBinding.getRoot();
                case 2:

                    FragmentProjectCommandsBinding cmdBinding = FragmentProjectCommandsBinding.inflate(activity.getLayoutInflater(), null, false);
                    cmdBinding.dlvData.setLayoutManager(new LinearLayoutManager(activity));
                    cmdBinding.dlvData.setLayoutManager(new GridLayoutManager(activity, 3));

                    cmdBinding.dlvData.setAdapter(activity.mLinkAdapter, true);
                    cmdBinding.dlvData.setCustomDragItem(null);
                    cmdBinding.dlvData.setCanDragHorizontally(false);

                    cmdBinding.dlvData.setTag("Cmd");
                    container.addView(cmdBinding.getRoot(), 0);
                    return cmdBinding.getRoot();
                default:
                    return null;
            }
        }


    }


}

