package ir.saeedrahimi.poolnegar.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar;
import ir.saeedrahimi.poolnegar.database.model.Account;
import ir.saeedrahimi.poolnegar.databinding.ListItemProjectBinding;

public class ProjectRecyclerViewAdapter extends BaseFilterableRecyclerViewAdapter<Account, ListItemProjectBinding> {

    public ProjectRecyclerViewAdapter(Context context, RecyclerViewEventListener recyclerViewEventListener) {
        super(context, recyclerViewEventListener);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.mBinding = ListItemProjectBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new ViewHolder<>(this.mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Account project = getItem(position);
        ((ListItemProjectBinding)holder.binding).txtName.setText(project.getTitle());
        PersianCalendar pc = new PersianCalendar();
        pc.setPersianDate(project.getProjectDate());

        ((ListItemProjectBinding)holder.binding).txtYear.setText(String.valueOf(pc.getPersianYear()));
        ((ListItemProjectBinding)holder.binding).txtDay.setText(String.valueOf(pc.getPersianDay()));
        ((ListItemProjectBinding)holder.binding).txtMonth.setText(pc.getPersianMonthName());
        holder.itemView.setOnClickListener(v -> {
            getRecyclerViewEventListener().onViewTapped(0, position);
        });
        if (project.isClosed()) {
            ((ListItemProjectBinding)holder.binding).txtName.setTextColor(mContext.getResources().getColor(R.color.Red));
        }
    }

    @Override
    protected void filterObject(ArrayList<Account> filteredList, Account object, String searchText) {
        if (object.getTitle().toLowerCase().contains(searchText) || object.getInfo().toLowerCase().contains(searchText)) {
            filteredList.add(object);
        }
    }

}
