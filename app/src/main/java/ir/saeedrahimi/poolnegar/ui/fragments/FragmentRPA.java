package ir.saeedrahimi.poolnegar.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.viewpagerindicator.TitlePageIndicator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.adapters.TransactionListAdapter;
import ir.saeedrahimi.poolnegar.application.helper.KeyHelper;
import ir.saeedrahimi.poolnegar.customs.TypeFaceProvider;
import ir.saeedrahimi.poolnegar.database.DAO.TransactionDAO;
import ir.saeedrahimi.poolnegar.database.model.Account;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
import ir.saeedrahimi.poolnegar.dialogs.DialogTransactionDetails;
import ir.saeedrahimi.poolnegar.dialogs.ProgressLoading;
import ir.saeedrahimi.poolnegar.models.TransactionFilter;
import ir.saeedrahimi.poolnegar.ui.activities.cu.ActivityCU_Base;
import ir.saeedrahimi.poolnegar.ui.activities.cu.Activity_Transaction;

public class FragmentRPA extends BaseFragment implements OnItemClickListener,
        DialogTransactionDetails.OnDialogClickListener,
        DialogTransactionDetails.OnTransactionEdit,
        DialogTransactionDetails.OnTransactionDelete,
        ViewPager.OnPageChangeListener
        , OnScrollListener {
    public LinearLayout mHeader;
    public View mPlaceHolderView;
    public int mHeaderHeight;
    public int mMinHeaderTranslation;
    public boolean mFirstAsync = true;
    public AccelerateDecelerateInterpolator mSmoothInterpolator;
    public LinearLayout inEmptyview;
    public LinearLayout outEmptyview;
    public ListView lvIn;
    public ListView lvOut;
    public LinearLayout inListLayout;
    cAdapter mAdapter;
    ViewPager viewPager;
    int[] accountIds;
    String selectedAccount;
    String title;
    double beginBalance;
    ArrayAdapter<TransactionItem> inAdapter, outAdapter;
    List<TransactionItem> listInTrans;
    List<TransactionItem> listOutTrans;
    TransactionFilter filter;
    Account account;
    AsyncLoadDB async;
    private int visibleThreshold = 10;
    private int currentInPage = 0;
    private int currentOutPage = 0;
    private boolean loading = true;
    private int lastInListSize = 1;
    private int lastOutListSize = 1;
    private Integer loadType = 2;


    public static FragmentRPA newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        FragmentRPA fragment = new FragmentRPA();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_report_acc, container, false);

        mAdapter = new cAdapter(new String[]{"پرداخت ها", "دریافت ها"});
        viewPager = (ViewPager) view.findViewById(R.id.bill_pager);
        viewPager.setAdapter(mAdapter);
        TitlePageIndicator mIndicator = (TitlePageIndicator) view
                .findViewById(R.id.bill_pager_titles);
        mIndicator.setViewPager(viewPager);
        mIndicator.setCurrentItem(mAdapter.getCount() - 1);
        mIndicator.setOnPageChangeListener(this);
        Typeface typeface = TypeFaceProvider.get(getActivity(), "vazir.ttf");
        mIndicator.setTypeface(typeface);
        mIndicator.setSelectedColor(getActivity().getResources().getColor(
                R.color.Artem));

        mSmoothInterpolator = new AccelerateDecelerateInterpolator();
        mHeaderHeight = getResources().getDimensionPixelSize(R.dimen.project_header_height);
        mMinHeaderTranslation = -mHeaderHeight + 70;
        mHeader = (LinearLayout) view.findViewById(R.id.llai1);

        Bundle localBundle = getArguments();
        if (localBundle != null) {
            this.accountIds = localBundle.getIntArray("account");
            this.title = localBundle.getString("name");
        }
        TextView begbalance = ((TextView) view.findViewById(R.id.begbalance));

        begbalance.setText(new DecimalFormat(" ###,###.## ").format(this.beginBalance * -1));

        // TODO: fix title
        //setToolBarTitle("گزارش حساب",this.title);


        this.listInTrans = new ArrayList<TransactionItem>();
        this.listOutTrans = new ArrayList<TransactionItem>();

        this.inAdapter = new TransactionListAdapter(getActivity(), listInTrans, 0);
        this.outAdapter = new TransactionListAdapter(getActivity(), listOutTrans, 0);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        //RefreshAll();
        super.onViewCreated(view, savedInstanceState);
    }

    public void RefreshAll() {

        if (async != null && !async.isCancelled())
            async.cancel(true);
        async = new AsyncLoadDB();
        this.loadType = 2;
        this.lastInListSize = 1;
        this.lastOutListSize = 1;
        this.listInTrans.clear();
        this.listOutTrans.clear();
        async.execute(new Integer[]{this.currentInPage, this.currentOutPage, this.loadType});
    }

    public void LoadMore() {
        if (async != null && !async.isCancelled())
            async.cancel(true);
        async = new AsyncLoadDB();
        async.execute(new Integer[]{this.currentInPage, this.currentOutPage, this.loadType});
    }

    @Override
    public void onItemClick(AdapterView<?> adp, View arg1, int position, long arg3) {
        if (position == 0)
            return;
        if (((ListView) adp).equals(FragmentRPA.this.lvIn))
            new DialogTransactionDetails(getActivity(), getActivity(), this, this, this, this.listInTrans.get(position - 1));
        else
            new DialogTransactionDetails(getActivity(), getActivity(), this, this, this, this.listOutTrans.get(position - 1));

    }

    @Override
    public void onDialogDeleteTransaction() {
        RefreshAll();
    }

    @Override
    public void onDialogEditTransaction(TransactionItem transaction) {
        Intent intent = new Intent(FragmentRPA.this.getActivity(), Activity_Transaction.class);
        intent.putExtra(KeyHelper.KEY_ENTITY, transaction);
        intent.putExtra(KeyHelper.KEY_ACTIVITY_CU_STATE, ActivityCU_Base.ACTIVITY_STATE_EDIT_MODE);
        startActivityForResult(intent, KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION);
    }

    @Override
    public void onDialogDeleteClick() {
        RefreshAll();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION) {
            if (resultCode == Activity.RESULT_OK) {
                RefreshAll();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPageSelected(int position) {
        int scrollHeight = (int) (mHeader.getHeight() + mHeader.getTranslationY());


        if (position == 1) {
            if (scrollHeight == 0 && lvIn.getFirstVisiblePosition() >= 1) {
                return;
            }
            lvIn.setSelectionFromTop(1, scrollHeight);
        }
        if (position == 0) {
            if (scrollHeight == 0 && lvOut.getFirstVisiblePosition() >= 1) {
                return;
            }
            lvOut.setSelectionFromTop(1, scrollHeight);
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        int scrollY = getScrollY((ListView) view);
        mHeader.setTranslationY(Math.max(-scrollY, mMinHeaderTranslation));
        if (((String) view.getTag()).contains("loadMore")) {
            int l = visibleItemCount + firstVisibleItem;
            if (l >= totalItemCount && !loading) {
                if (this.viewPager.getCurrentItem() == 1) {
                    this.loadType = 0;
                    this.currentInPage++;
                }
                if (this.viewPager.getCurrentItem() == 0) {
                    this.loadType = 1;
                    this.currentOutPage++;
                }
                loading = true;
                LoadMore();
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView arg0, int arg1) {
        // TODO Auto-generated method stub

    }

    public int getScrollY(ListView lv) {
        View c = lv.getChildAt(0);
        if (c == null) {
            return 0;
        }

        int firstVisiblePosition = lv.getFirstVisiblePosition();
        int top = c.getTop();

        int headerHeight = 0;
        if (firstVisiblePosition >= 1) {
            headerHeight = mPlaceHolderView.getHeight();
        }

        return -top + firstVisiblePosition * c.getHeight() + headerHeight;
    }

    class AsyncLoadDB extends AsyncTask<Integer, List<TransactionItem>, List<TransactionItem>> {
        ProgressLoading p;

        public void execute(int[] parentId) {
        }

        @Override
        protected void onPreExecute() {
//			if(FragmentRPA.this.lastInListSize > 0 && FragmentRPA.this.viewPager.getCurrentItem() == 1){
////				this.p = new ProgressLoading(FragmentRPA.this.getActivity(),"");
//			}
//			else if(FragmentRPA.this.lastOutListSize > 0 && FragmentRPA.this.viewPager.getCurrentItem() == 0){
////				this.p = new ProgressLoading(FragmentRPA.this.getActivity(),"");
//			}
        }

        @Override
        protected List<TransactionItem> doInBackground(Integer... values) {

            int inPage = values[0] * FragmentRPA.this.visibleThreshold;
            int outPage = values[1] * FragmentRPA.this.visibleThreshold;
            int loadType = values[2];
            List<TransactionItem> temp_inlist = new ArrayList<TransactionItem>();
            List<TransactionItem> temp_outlist = new ArrayList<TransactionItem>();
            Log.i("inPage: ", inPage + "");
            Log.i("outPage: ", outPage + "");
            // check if all items loaded and just load needed page
            if (FragmentRPA.this.lastInListSize > 0 && (loadType == 0 || loadType == 2)) {
                temp_inlist = TransactionDAO.getAllByAccount(FragmentRPA.this.selectedAccount, 0, "", "");
                FragmentRPA.this.listInTrans.addAll(temp_inlist);
                FragmentRPA.this.lastInListSize = temp_inlist.size();
            }
            if (FragmentRPA.this.lastOutListSize > 0 && (loadType == 1 || loadType == 2)) {
                temp_outlist = TransactionDAO.getAllByAccount(FragmentRPA.this.selectedAccount, 1, "", "");
                FragmentRPA.this.listOutTrans.addAll(temp_outlist);
                FragmentRPA.this.lastOutListSize = temp_outlist.size();
            }
            if (FragmentRPA.this.mFirstAsync == true) {
                FragmentRPA.this.account = new Account();
                FragmentRPA.this.account.setTotalIncome(TransactionDAO.getAccountBalance(FragmentRPA.this.selectedAccount));
                FragmentRPA.this.account.setTotalOutcome(TransactionDAO.getAccountBalance(FragmentRPA.this.selectedAccount));
                // TODO: Was:
//				FragmentRPA.this.account.setTotalIncome(_db.getAccountsActivityBalance(FragmentRPA.this.accountIds, 0, "9999/99/99",""));
//				FragmentRPA.this.account.setTotalOutcome(_db.getAccountsActivityBalance(FragmentRPA.this.accountIds, 1, "9999/99/99", ""));
            }
            Log.i("temp_inlist size: ", temp_inlist.size() + "");
            Log.i("temp_outlist size: ", temp_outlist.size() + "");

            return null;
        }

        @Override
        protected void onPostExecute(List<TransactionItem> result) {
            FragmentRPA.this.mPlaceHolderView = getActivity().getLayoutInflater().inflate(R.layout.project_header_placeholder, lvOut, false);
            if (FragmentRPA.this.mFirstAsync == true) {
                FragmentRPA.this.account = new Account();

                FragmentRPA.this.account.setTotalIncome(TransactionDAO.getAccountBalance(FragmentRPA.this.selectedAccount));
                FragmentRPA.this.account.setTotalOutcome(TransactionDAO.getAccountBalance(FragmentRPA.this.selectedAccount));
                // TODO: Was:
//				FragmentRPA.this.account.setTotalIncome(_db.getAccountsActivityBalance(FragmentRPA.this.accountIds, 0, "9999/99/99",""));
//				FragmentRPA.this.account.setTotalOutcome(_db.getAccountsActivityBalance(FragmentRPA.this.accountIds, 1, "9999/99/99", ""));

                FragmentRPA.this.lvOut.addHeaderView(mPlaceHolderView);
                FragmentRPA.this.lvIn.addHeaderView(mPlaceHolderView);
                FragmentRPA.this.lvIn.setAdapter(FragmentRPA.this.inAdapter);
                FragmentRPA.this.lvOut.setAdapter(FragmentRPA.this.outAdapter);
                FragmentRPA.this.mFirstAsync = false;
            }
            if (FragmentRPA.this.lastInListSize > 0 && (FragmentRPA.this.loadType == 0 || FragmentRPA.this.loadType == 2)) {
                FragmentRPA.this.inAdapter.notifyDataSetChanged();
            }
            if (FragmentRPA.this.lastOutListSize > 0 && (FragmentRPA.this.loadType == 1 || FragmentRPA.this.loadType == 2)) {
                FragmentRPA.this.outAdapter.notifyDataSetChanged();
            }
            if (FragmentRPA.this.listInTrans == null || FragmentRPA.this.listInTrans.size() <= 0) {
                View txtNoItem = FragmentRPA.this.getActivity().getLayoutInflater().inflate(R.layout.blank_textview, null);
                ((TextView) txtNoItem.findViewById(R.id.blank_textview)).setText("موردی یافت نشد!");
                FragmentRPA.this.inEmptyview.addView(txtNoItem);
            }
            if (FragmentRPA.this.listOutTrans == null || FragmentRPA.this.listOutTrans.size() <= 0) {
                View txtNoItem = FragmentRPA.this.getActivity().getLayoutInflater().inflate(R.layout.blank_textview, null);
                ((TextView) txtNoItem.findViewById(R.id.blank_textview)).setText("موردی یافت نشد!");
                FragmentRPA.this.outEmptyview.addView(txtNoItem);
            }

            TextView inbalance = ((TextView) FragmentRPA.this.getView().findViewById(R.id.inbalance));
            TextView expbalance = ((TextView) FragmentRPA.this.getView().findViewById(R.id.expbalance));
            TextView endbalance = ((TextView) FragmentRPA.this.getView().findViewById(R.id.endbalance));
            double endBalance = (FragmentRPA.this.beginBalance + FragmentRPA.this.account.getTotalOutcome()) * -1;
            inbalance.setText(new DecimalFormat(" ###,###.## ").format(FragmentRPA.this.account.getTotalIncome()));
            expbalance.setText(new DecimalFormat(" ###,###.## ").format(FragmentRPA.this.account.getTotalOutcome()));
            endbalance.setText(new DecimalFormat(" ###,###.## ").format(endBalance));

            ImageView endimg = ((ImageView) FragmentRPA.this.getView().findViewById(R.id.endingbalancecolor));


            if (FragmentRPA.this.lastInListSize > 0) {
                FragmentRPA.this.lvIn.setTag("loadMore");
            } else if (loadType == 0 && !FragmentRPA.this.lvIn.getTag().toString().contains("ended")) {
                if (listInTrans.size() > 0)
                    Crouton.makeText(getActivity(),
                            "به انتهای لیست دریافتی ها رسیدید", Style.INFO).show();
                FragmentRPA.this.lvIn.setTag("ended");
            }
            if (FragmentRPA.this.lastOutListSize > 0) {
                FragmentRPA.this.lvOut.setTag("loadMore");
            } else if (loadType == 1 && !FragmentRPA.this.lvOut.getTag().toString().contains("ended")) {
                if (listOutTrans.size() > 0)
                    Crouton.makeText(getActivity(),
                            "به انتهای لیست پرداختی ها رسیدید", Style.INFO).show();
                FragmentRPA.this.lvOut.setTag("ended");
            }

            FragmentRPA.this.loading = false;

            if (this.p != null)
                this.p.done();
            super.onPostExecute(result);

            FragmentRPA.this.lvOut.setOnScrollListener(FragmentRPA.this);
            FragmentRPA.this.lvIn.setOnScrollListener(FragmentRPA.this);
        }
    }

    class cAdapter extends PagerAdapter {

        private final String[] TITLES;

        public cAdapter(String[] paramArrayOfString) {
            this.TITLES = paramArrayOfString;
        }

        @Override
        public int getCount() {
            return this.TITLES.length;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == (View) arg1;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return this.TITLES[position];
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater localLayoutInflater = FragmentRPA.this.getActivity().getLayoutInflater();
            switch (position) {

                case 0:
                    View outLayout = localLayoutInflater.inflate(R.layout.fragment_transactions_listview, null);
                    FragmentRPA.this.outEmptyview = ((LinearLayout) outLayout.findViewById(R.id.emptyview));
                    FragmentRPA.this.lvOut = ((ListView) outLayout.findViewById(R.id.listviewtransactions));
                    FragmentRPA.this.lvOut.setTag("");
                    FragmentRPA.this.lvOut.setOnItemClickListener(FragmentRPA.this);
                    container.addView(outLayout, 0);
                    return outLayout;
                case 1:
                    View listLayoutOut = localLayoutInflater.inflate(R.layout.fragment_transactions_listview, null);
                    FragmentRPA.this.inEmptyview = ((LinearLayout) listLayoutOut.findViewById(R.id.emptyview));
                    FragmentRPA.this.lvIn = ((ListView) listLayoutOut.findViewById(R.id.listviewtransactions));
                    FragmentRPA.this.lvIn.setTag("");
                    FragmentRPA.this.lvIn.setOnItemClickListener(FragmentRPA.this);

                    container.addView(listLayoutOut, 0);
                    return listLayoutOut;
                default:
                    return null;
            }

        }


    }

}
