package ir.saeedrahimi.poolnegar.ui.fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.kennyc.view.MultiStateView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import ir.saeedrahimi.poolnegar.PreferencesManager;
import ir.saeedrahimi.poolnegar.adapters.TransactionListAdapter;
import ir.saeedrahimi.poolnegar.application.helper.KeyHelper;
import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar;
import ir.saeedrahimi.poolnegar.database.DAO.TransactionDAO;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
import ir.saeedrahimi.poolnegar.databinding.FragmentTransactionsListBinding;
import ir.saeedrahimi.poolnegar.dialogs.DialogTransactionDetails;
import ir.saeedrahimi.poolnegar.ui.activities.cu.ActivityCU_Base;
import ir.saeedrahimi.poolnegar.ui.activities.cu.Activity_Transaction;

import static android.app.Activity.RESULT_OK;

public class FragmentTransactions extends Fragment_Base<FragmentTransactionsListBinding> implements AdapterView.OnItemClickListener, DialogTransactionDetails.OnDialogClickListener, DialogTransactionDetails.OnTransactionDelete, DialogTransactionDetails.OnTransactionEdit {

    public String order = "act_date DESC";
    private String startDate, endDate;
    private LoadTransactionsTask mLoadTransactionsTask;
    private List<TransactionItem> transactionItems;
    private ArrayAdapter<TransactionItem> mAdapter;

    public static FragmentTransactions newInstanceWithDate(int instance, String startDate) {
        PersianCalendar calendar = new PersianCalendar();
        calendar.parse(startDate);
        calendar.setPersianDate(calendar.getPersianYear(), calendar.getPersianMonth(), calendar.getPersianMonthDaysCount());

        String endDate = calendar.getPersianShortDate();
        Bundle args = new Bundle();
        args.putInt("args", instance);
        args.putString("startDate", startDate);
        args.putString("endDate", endDate);
        FragmentTransactions fragment = new FragmentTransactions();
        fragment.setArguments(args);
        return fragment;
    }

    public static FragmentTransactions newInstanceWithDate(int instance, String startDate, String endDate) {
        Bundle args = new Bundle();
        args.putInt("args", instance);
        args.putString("startDate", startDate);
        args.putString("endDate", endDate);
        FragmentTransactions fragment = new FragmentTransactions();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected FragmentTransactionsListBinding getFragmentBinding(LayoutInflater inflater, ViewGroup container) {
        return FragmentTransactionsListBinding.inflate(inflater, container, false);
    }

    @Override
    protected void onActionBarRightToolClicked() {

    }

    @Override
    protected String getActionBarTitle() {
        return "تراکنش‌ها";
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        PreferencesManager mPrefManager = new PreferencesManager(this.getActivity().getApplicationContext());
        transactionItems = new ArrayList<>();
        mAdapter = new TransactionListAdapter(getActivity(), transactionItems, 0);
        binding.lvData.setAdapter(mAdapter);
        binding.lvData.setOnItemClickListener(this);

        Bundle localBundle = getArguments();
        if (localBundle != null) {
            this.startDate = localBundle.getString("startDate");
            this.endDate = localBundle.getString("endDate");
        }

        ReloadDataAsync();
    }

    @Override
    public void onItemClick(AdapterView<?> adp, View arg1, int position, long arg3) {
        new DialogTransactionDetails(getActivity(), getActivity(), this, this, this, this.transactionItems.get(position));

    }

    @Override
    public void onDialogDeleteClick() {

    }

    @Override
    public void onDialogDeleteTransaction() {

        ReloadDataAsync();
    }

    @Override
    public void onDialogEditTransaction(TransactionItem transaction) {
        Intent intent = new Intent(FragmentTransactions.this.getActivity(), Activity_Transaction.class);
        intent.putExtra(KeyHelper.KEY_ENTITY, transaction);
        intent.putExtra(KeyHelper.KEY_ACTIVITY_CU_STATE, ActivityCU_Base.ACTIVITY_STATE_EDIT_MODE);
        startActivityForResult(intent, KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION) {
            if (resultCode == RESULT_OK) {
                ReloadDataAsync();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void ReloadDataAsync() {
        if (mLoadTransactionsTask != null && !mLoadTransactionsTask.isCancelled())
            mLoadTransactionsTask.cancel(true);

        mLoadTransactionsTask = new LoadTransactionsTask(this);
        mLoadTransactionsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private List<TransactionItem> getData() {
        return TransactionDAO.getAll(2, startDate, endDate);
    }


    private static class LoadTransactionsTask extends AsyncTask<Void, Void, List<TransactionItem>> {
        private WeakReference<FragmentTransactions> fragmentReference;

        LoadTransactionsTask(FragmentTransactions context) {
            fragmentReference = new WeakReference<>(context);
        }

        @Override
        protected void onPreExecute() {
            // get a reference to the activity if it is still there
            FragmentTransactions fragment = fragmentReference.get();
            if (fragment == null || fragment.isDetached()) return;

            fragment.binding.multiStateView.setViewState(MultiStateView.ViewState.LOADING);
        }

        @Override
        protected List<TransactionItem> doInBackground(Void... voids) {
            FragmentTransactions fragment = fragmentReference.get();
            if (fragment == null || fragment.isDetached()) return null;


            return fragment.getData();
        }

        @Override
        protected void onPostExecute(List<TransactionItem> result) {
            FragmentTransactions fragment = fragmentReference.get();
            if (fragment == null || fragment.isDetached() || fragment.binding == null) return;


            fragment.transactionItems.clear();
            fragment.transactionItems.addAll(result);
            fragment.mAdapter.notifyDataSetChanged();

            if (fragment.transactionItems.size() > 0) {
                fragment.binding.multiStateView.setViewState(MultiStateView.ViewState.CONTENT);
            } else {
                fragment.binding.multiStateView.setViewState(MultiStateView.ViewState.EMPTY);
            }
        }
    }
}
