package ir.saeedrahimi.poolnegar.ui.fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.woxthebox.draglistview.DragListView;
import com.woxthebox.draglistview.swipe.ListSwipeHelper;
import com.woxthebox.draglistview.swipe.ListSwipeItem;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.navigation.NavOptions;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.helper.KeyHelper;
import ir.saeedrahimi.poolnegar.database.DAO.LinkDAO;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
import ir.saeedrahimi.poolnegar.database.model.TransactionLink;
import ir.saeedrahimi.poolnegar.databinding.FragmentSettingsProjectCommandsBinding;
import ir.saeedrahimi.poolnegar.ui.activities.cu.Activity_TransactionLink;
import ir.saeedrahimi.poolnegar.models.TransactionFilter;
import ir.saeedrahimi.poolnegar.ui.adapters.TransactionLinkDraggableAdapter;

import static androidx.navigation.fragment.NavHostFragment.findNavController;

public class Fragment_SettingsProjectCommands extends Fragment_Base<FragmentSettingsProjectCommandsBinding> implements View.OnClickListener, TransactionLinkDraggableAdapter.onTransactionLinkClick {

    TransactionLinkDraggableAdapter cmdAdapter;
    List<TransactionLink> linkItems;
    TransactionFilter filter;
    LoadLinksTask loadLinksTask;
    View v;
    private DragListView mDragListView;


    @Override
    public void onViewCreated(@NonNull View view, @androidx.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponents();
    }

    private void initComponents() {
        mDragListView = binding.dlvData;
        mDragListView.getRecyclerView().setVerticalScrollBarEnabled(true);
        mDragListView.setDragListListener(new DragListView.DragListListenerAdapter() {
            @Override
            public void onItemDragStarted(int position) {
            }

            @Override
            public void onItemDragEnded(int fromPosition, int toPosition) {
                if (fromPosition != toPosition) {
                    TransactionLink link = linkItems.get(fromPosition);
                    linkItems.remove(fromPosition);
                    linkItems.add(toPosition,link);
                    sortOnDatabase();
                }
            }
        });
        mDragListView.setSwipeListener(new ListSwipeHelper.OnSwipeListenerAdapter() {
            @Override
            public void onItemSwipeStarted(ListSwipeItem item) {
            }

            @Override
            public void onItemSwipeEnded(ListSwipeItem item, ListSwipeItem.SwipeDirection swipedDirection) {
            }
        });
        this.linkItems = new ArrayList<>();

        setupListRecyclerView();
        ReloadDataAsync();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION) {
            if (resultCode == Constants.RESULTS.RESULT_OK) {
                ReloadDataAsync();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected FragmentSettingsProjectCommandsBinding getFragmentBinding(LayoutInflater inflater, ViewGroup container) {
        return FragmentSettingsProjectCommandsBinding.inflate(inflater, container, false);
    }

    @Override
    protected void onActionBarRightToolClicked() {
        Intent myIntent2 = new Intent(getActivity(), Activity_TransactionLink.class);
        startActivityForResult(myIntent2, KeyHelper.REQUEST_CODE_CU_ACC_TRANSACTION);
    }

    @Override
    protected String getActionBarTitle() {
        return "تراکنشهای پیشفرض پروژه";
    }

    private void ReloadDataAsync() {
        if (loadLinksTask != null && !loadLinksTask.isCancelled())
            loadLinksTask.cancel(true);

        loadLinksTask = new LoadLinksTask(this);
        loadLinksTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
    @Override
    public void onClick(View view) {
    }

    private void setupListRecyclerView() {
        mDragListView.setLayoutManager(new LinearLayoutManager(getContext()));
        cmdAdapter = new TransactionLinkDraggableAdapter(linkItems, this, R.layout.list_item_transaction_defaults, R.id.drag_handle, false, false);
        mDragListView.setAdapter(cmdAdapter, true);
        mDragListView.setCanDragHorizontally(false);
    }

    private void setupGridVerticalRecyclerView() {
        mDragListView.setLayoutManager(new GridLayoutManager(getContext(), 4));
        cmdAdapter = new TransactionLinkDraggableAdapter(linkItems, this, R.layout.list_item_transaction_defaults, R.id.drag_handle, true, false);
        mDragListView.setAdapter(cmdAdapter, true);
        mDragListView.setCanDragHorizontally(true);

    }

    private void setupGridHorizontalRecyclerView() {
        mDragListView.setLayoutManager(new GridLayoutManager(getContext(), 4, LinearLayoutManager.HORIZONTAL, false));
        cmdAdapter = new TransactionLinkDraggableAdapter(linkItems, this, R.layout.list_item_transaction_defaults, R.id.drag_handle, true, false);
        mDragListView.setAdapter(cmdAdapter, true);
        mDragListView.setCanDragHorizontally(true);
    }

    public void sortOnDatabase() {
        for (TransactionLink link : linkItems) {
            link.setLinkOrder(linkItems.indexOf(link));
            LinkDAO.update(link);
        }
        cmdAdapter.notifyDataSetChanged();

    }

    @Override
    public void onTransactionLinkClick(TransactionLink link) {

    }

    @Override
    public void onTransactionLinkDelete(TransactionLink link, int position) {
        LinkDAO.deleteById(link.getLinkId());
        linkItems.remove(position);
        cmdAdapter.notifyDataSetChanged();
    }

    private static class LoadLinksTask extends AsyncTask<Void, Void, List<TransactionLink>> {
        private WeakReference<Fragment_SettingsProjectCommands> fragmentReference;

        LoadLinksTask(Fragment_SettingsProjectCommands context) {
            fragmentReference = new WeakReference<>(context);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected List<TransactionLink> doInBackground(Void... voids) {
            Fragment_SettingsProjectCommands fragment = fragmentReference.get();
            if (fragment == null || fragment.isDetached()) return null;


            return LinkDAO.getProjectLinks();
        }

        @Override
        protected void onPostExecute(List<TransactionLink> result) {
            Fragment_SettingsProjectCommands fragment = fragmentReference.get();
            if (fragment == null || fragment.isDetached() || fragment.binding == null) return;


            fragment.linkItems.clear();
            fragment.linkItems.addAll(result);
            fragment.cmdAdapter.notifyDataSetChanged();

        }
    }
}
