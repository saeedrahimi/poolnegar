package ir.saeedrahimi.poolnegar.ui.activities.cu;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.DigitsKeyListener;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;

import com.mohamadamin.persianmaterialdatetimepicker.date.DatePickerDialog;

import java.text.DecimalFormat;

import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.helper.ConversionHelper;
import ir.saeedrahimi.poolnegar.application.helper.KeyHelper;
import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar;
import ir.saeedrahimi.poolnegar.customs.TypeFaceProvider;
import ir.saeedrahimi.poolnegar.customs.Utils;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.database.model.Account;
import ir.saeedrahimi.poolnegar.databinding.ActivitiyCuAccountBinding;
import ir.saeedrahimi.poolnegar.dialogs.AlertDialog;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_AccountSelection;

public class ActivityCU_Account
        extends ActivityCU_Base<Account, ActivitiyCuAccountBinding> implements DatePickerDialog.OnDateSetListener, View.OnClickListener {
    String parentId = "null";
    PersianCalendar mCalendar = new PersianCalendar();

    public void prepareCheckBoxes() {
        if (this.parentId.equals(Constants.ACCOUNTS.EXPENSE)) {
            this.binding.editpageReciveable.setChecked(false);
            this.binding.editpagePayable.setChecked(true);
        } else if (this.parentId.equals(Constants.ACCOUNTS.PROJECTS)) {
            this.binding.editpageReciveable.setChecked(true);
            this.binding.editpagePayable.setChecked(false);
            this.binding.vBoxPayable.setVisibility(View.GONE);
            this.binding.vBoxReceivable.setVisibility(View.GONE);
        } else {
            this.binding.editpageReciveable.setChecked(false);
            this.binding.editpagePayable.setChecked(true);
        }
    }

    public void fillSpinner() {
        String[] arrayOfString = {"بدهکار", "بستانکار"};
        ArrayAdapter<String> localArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, arrayOfString);
        localArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        this.binding.editpageSinnper.setAdapter(localArrayAdapter);
        if ((this.parentId.equals(Constants.ACCOUNTS.INCOME)) || (this.parentId.equals(Constants.ACCOUNTS.DEMAND)) || (this.parentId.equals(Constants.ACCOUNTS.DEBT))) {
            this.binding.editpageSinnper.setSelection(1);
            if ((this.parentId.equals(Constants.ACCOUNTS.INCOME)) || (this.parentId.equals(Constants.ACCOUNTS.DEMAND))) {
                this.binding.editpageSinnper.setEnabled(false);
            }
        }
        if (this.parentId.equals(Constants.ACCOUNTS.PROJECTS)) {
            this.binding.editpageSinnper.setSelection(1);
            this.binding.editpageSinnper.setVisibility(View.GONE);
        } else if (!this.parentId.equals(Constants.ACCOUNTS.EXPENSE)) {
            this.binding.editpageSinnper.setSelection(0);
        } else {
            this.binding.editpageSinnper.setEnabled(false);
        }
    }

    @Override
    protected ActivitiyCuAccountBinding getViewBinding() {
        return ActivitiyCuAccountBinding.inflate(getLayoutInflater());
    }

    private void getExtras() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("ParentId")) {
                this.parentId = bundle.getString("ParentId");
            }
        }
    }

    @Override
    protected void onActivityFirstInit(Bundle savedInstanceState) {

        getExtras();
        binding.editpageDate.setOnClickListener(this);

        binding.btnParentAccount.setOnClickListener(this);

        binding.editpageDate.setText(new PersianCalendar().getPersianShortDate());


        this.binding.editpageBalance.setKeyListener(DigitsKeyListener
                .getInstance("1234567890,."));
        this.binding.editpageBalance.addTextChangedListener(Utils.MoneyTextWatcher);
        prepareCheckBoxes();
        fillSpinner();
    }

    @Override
    protected String getActionBarTitle() {
        return "ایجاد حساب";
    }

    @Override
    public boolean validateEntity() {
        if (binding.editpageTitle.getText().toString().matches("")) {
            AlertDialog.Show(this, "خطا", "نام حساب وارد نشده است");
            return false;
        }
        if (!binding.editpageTitle.getText().toString().equals(mEntity.getTitle()) && AccountDAO.ExistsInParent(this.parentId, binding.editpageTitle.getText().toString())) {
            AlertDialog.Show(this, "خطا", "نام حساب تکراری است ،لطفا نام دیگری انتخاب کنید ");
            return false;
        }
        if (!this.binding.editpageReciveable.isChecked() && !this.binding.editpagePayable.isChecked()) {
            AlertDialog.Show(this, "خطا", "این حساب باید امکان پرداخت یا دریافت را داشته باشد");
            return false;
        }


        return true;
    }

    @Override
    public void saveData() {
        if (this.binding.editpageBalance.getText().toString().matches("")) {
            mEntity.setBalance(0);
        } else if (this.binding.editpageSinnper.getSelectedItemPosition() == 0) {
            mEntity.setBalance(ConversionHelper.StringToDouble(binding.editpageBalance.getText().toString()));
        } else {
            mEntity.setBalance(-ConversionHelper.StringToDouble(binding.editpageBalance.getText().toString()));
        }

        mEntity.setTitle(binding.editpageTitle.getText().toString());
        mEntity.setParentId(this.parentId);
        mEntity.setInfo(binding.editpageInfo.getText().toString());
        mEntity.setIsReceivable(this.binding.editpageReciveable.isChecked() ? 1 : 0);
        mEntity.setIsPayable(this.binding.editpagePayable.isChecked() ? 1 : 0);

        if (this.parentId.equals(Constants.ACCOUNTS.PROJECTS)) {
            mEntity.setIsProject(1);
        }

        if (mActivityState.equals(ACTIVITY_STATE_NEW_MODE)) {
            AccountDAO.insert(mEntity);
        } else if (mActivityState.equals(ACTIVITY_STATE_EDIT_MODE)) {
            AccountDAO.update(mEntity);
        }
        Intent intent = new Intent();
        intent.putExtra("Entity", this.mEntity);
        intent.putExtra("EntityId", this.mEntity.getAccID());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onActivityStateNew() {
        this.mEntity = new Account();
        if (this.parentId.equals(Constants.ACCOUNTS.PROJECTS)) {
            prepareForProject();
        }
        mEntity.setProjectDate(mCalendar.getPersianShortDate());
    }

    @Override
    public void onActivityStateEdit() {
        setActionBarTitle("ویرایش حساب");
        this.parentId = mEntity.getParentId();
        this.binding.editpageTitle.setText(mEntity.getTitle());
        this.binding.editpageBalance.setText(new DecimalFormat("#.##")
                .format(mEntity.getBalance()));
        this.binding.editpageInfo.setText(mEntity.getInfo());
        this.binding.editpageDate.setText(mEntity.getProjectDate());
        this.binding.editpagePayable.setChecked(mEntity.getIsPayable() == 1);
        this.binding.editpageReciveable.setChecked(mEntity.getIsReceivable() == 1);

        if (this.mEntity.getIsProject() == 1) {
            prepareForProject();
        }
    }

    private void prepareForProject() {
        if (mActivityState.equals(ACTIVITY_STATE_EDIT_MODE)) {
            setActionBarTitle("ویرایش پـروژه");
        } else {
            setActionBarTitle("ایجاد پـروژه");
        }
        binding.lladate.setVisibility(View.VISIBLE);
        binding.llaParent.setVisibility(View.GONE);
        binding.headera4.setText("بودجه پروژه");
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
        if (paramInt == 4) {
            Intent backIntent = new Intent();
            setResult(RESULT_CANCELED, backIntent);
            finish();
        }
        return super.onKeyDown(paramInt, paramKeyEvent);
    }

    public void selectDateDialog() {
        PersianCalendar persianCalendar = new PersianCalendar();
        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                this,
                persianCalendar.getPersianYear(),
                persianCalendar.getPersianMonth() - 1,
                persianCalendar.getPersianDay(),
                TypeFaceProvider.get(this, "vazir.ttf")
        );
        datePickerDialog.show(getFragmentManager(), "DatePickerDialog");
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == KeyHelper.REQUEST_CODE_CHOOSE_ACCOUNT_GROUP)) {
            if (resultCode == RESULT_OK) {
                this.parentId = data.getStringExtra("SelectedAccounts");
                this.mEntity.setParentId(this.parentId);
                binding.btnParentAccount.setText(AccountDAO.getTitleByIdRecursive(this.parentId));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        mCalendar.setPersianDate(year, monthOfYear + 1, dayOfMonth);
        String date = mCalendar.getPersianShortDate();
        mEntity.setProjectDate(date);
        binding.editpageDate.setText(date);
    }

    public void selectParentAccount() {
        Intent dlg = new Intent(this, Activity_AccountSelection.class);
        dlg.putExtra("ShowPayable", false);
        dlg.putExtra("ShowReceivable", true);
        dlg.putExtra("Title", "انتخاب حساب مرجع");
        this.startActivityForResult(dlg, KeyHelper.REQUEST_CODE_CHOOSE_ACCOUNT_GROUP);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btnParentAccount:
                selectParentAccount();
                break;
            case R.id.editpage_date:
                selectDateDialog();
                break;
        }
    }
}
