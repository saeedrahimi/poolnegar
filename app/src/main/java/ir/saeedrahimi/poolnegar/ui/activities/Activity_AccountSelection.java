package ir.saeedrahimi.poolnegar.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.kennyc.view.MultiStateView;

import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.databinding.ActivityAccountSelectionBinding;
import ir.saeedrahimi.poolnegar.models.AccountMultiLevel;
import ir.saeedrahimi.poolnegar.ui.activities.cu.ActivityCU_Account;
import ir.saeedrahimi.poolnegar.ui.adapters.AccountSelectListAdapter;
import ir.saeedrahimi.poolnegar.ui.helper.SnackBarHelper;

public class Activity_AccountSelection extends Activity_GeneralBase<ActivityAccountSelectionBinding> implements View.OnClickListener {

    public static final String KEY_SHOW_PAYABLE = "ShowPayable";
    public static final String KEY_SHOW_RECEIVABLE = "ShowReceivable";
    public List<AccountMultiLevel> mAccountList;
    AccountSelectListAdapter mAdapter;
    boolean mShowPayable = false;
    boolean mShowReceivable = true;

    private void getExtras() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(KEY_SHOW_PAYABLE)) {
                mShowPayable = bundle.getBoolean(KEY_SHOW_PAYABLE, false);
            }
            if (bundle.containsKey(KEY_SHOW_RECEIVABLE)) {
                mShowReceivable = bundle.getBoolean(KEY_SHOW_RECEIVABLE, true);
            }
            if (bundle.containsKey("Title")) {
                String title = bundle.getString("Title");
                setActionBarTitle(title);
            }
        }
    }

    @Override
    protected void onActivityFirstInit(Bundle savedInstanceState) {
        getExtras();
        this.binding.rcvData.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new AccountSelectListAdapter(this, mAccountList, this.binding.rcvData);
        this.binding.rcvData.setAdapter(mAdapter);
        this.binding.rcvData.setOnItemClick((view, item, position) -> {
            if (!item.hasChildren()) {
                Intent intent = new Intent();
                intent.putExtra("SelectedAccounts", mAccountList.get(position).associatedObject.getAccID());
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        findViewById(R.id.lv_action).setOnClickListener(this);
        fillAccounts();
        //If you want to already opened Multi-RecyclerView just call openTill where is parameter is
        // position to corresponding each level.
        //mMultiLevelRecyclerView.openTill(0, 1, 2, 3);
    }

    @Override
    protected ActivityAccountSelectionBinding getViewBinding() {
        return ActivityAccountSelectionBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void onActionBarRightToolClicked() {


    }

    @Override
    protected void onActionBarLeftToolClicked() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    protected String getActionBarTitle() {
        return getResources().getString(R.string.title_activity_account_selection);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lv_action:
                Intent accountIntent = new Intent(this, ActivityCU_Account.class);
                accountIntent.putExtra("ParentId","");
                startActivityForResult(accountIntent, Constants.RESULTS.ADD_ITEM);
                break;
        }
    }
    private void fillAccounts()
    {
        binding.multiStateView.setViewState(MultiStateView.ViewState.LOADING);
        mAccountList = AccountDAO.getRecursiveByParentId("", mShowPayable, mShowReceivable);
        mAdapter = new AccountSelectListAdapter(this, mAccountList, this.binding.rcvData);
        this.binding.rcvData.setAdapter(mAdapter);
        if(mAccountList.size()>0){
            binding.multiStateView.setViewState(MultiStateView.ViewState.CONTENT);
        }else{
            binding.multiStateView.setViewState(MultiStateView.ViewState.EMPTY);
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == Constants.RESULTS.ADD_ITEM) {
            if(resultCode == RESULT_OK){
                fillAccounts();

                SnackBarHelper.showSnack(this, SnackBarHelper.SnackState.Success ,"حساب ایجاد شد");

            }
        }

    }
}
