package ir.saeedrahimi.poolnegar.ui.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.PreferencesManager;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.models.BillItem;
import ir.saeedrahimi.poolnegar.ui.activities.cu.ActivityCU_Account;
import ir.saeedrahimi.poolnegar.ui.activities.MainActivity;

public class FragmentPayBills extends BaseFragment {
	private static final int RESULT_DATE = 0;

	private static final int RESULT_OK = 0;

	private static final int RESULT_CANCELED = 0;

	Calendar O = Calendar.getInstance();
	private int curDay;
	private int curMonth;
	private int curYear;
	String curDate;
	String startDate,endDate;
	private Button btnDate;
	boolean hideZero;
	ProgressDialog pgDialog;
	AsyncLoadDB acpb;
	List<BillItem> listAcc;

	ListView listView;
	TextView pgTitle;
	PreferencesManager mPrefManager;
	int parentId;

	public String order = "act_date DESC";
	
	public void showLoading()
	  {
	    this.pgDialog = new ProgressDialog(this.getActivity());
	    this.pgDialog.setMessage("در حال بارگذاری");
	    this.pgDialog.setCancelable(false);
	    this.pgDialog.show();
	  }
	public void hideLoading()
	  {
	    if(this.pgDialog!=null && this.pgDialog.isShowing())
	    	pgDialog.dismiss();
	  }
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view=inflater.inflate(R.layout.fragment_bills,container, false);

	    this.listAcc = new ArrayList<BillItem>();
	    this.listView = ((ListView)view.findViewById(R.id.rep_balance_list));//c
	    ((ViewSwitcher)view.findViewById(R.id.rep_balance_viewSwitcher)).showNext();
	    this.listView.setEmptyView(((TextView)view.findViewById(R.id.emptylist)));
	    Bundle localBundle = getArguments();
	    if (localBundle != null)
	    {
	      this.parentId = localBundle.getInt("parentId",0);
	    }
		return view;
	}
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO: fix title
		// setToolBarTitle("لیست بدهی ها","");
		mPrefManager = new PreferencesManager(this.getActivity().getApplicationContext());
		this.hideZero=mPrefManager.getHideZeroBills();
		acpb=new AsyncLoadDB();
		asyncLoadData();
	}
	public void asyncLoadData(){
		
		acpb = new AsyncLoadDB();
    	acpb.execute(new Integer[]{ parentId });
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
	    setHasOptionsMenu(true);
	    super.onCreate(savedInstanceState);

	}


	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
	    super.onCreateOptionsMenu(menu, inflater);
	    menu.clear();
	    inflater.inflate(R.menu.paybill, menu);

	    menu.getItem(0).getSubMenu().getItem(0).setChecked(!this.hideZero);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify height parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
        case R.id.project_new:
        	Intent myIntent = new Intent((MainActivity)getActivity(), ActivityCU_Account.class);
        	myIntent.putExtra("ParentId", Constants.ACCOUNTS.PROJECTS_INT +"");
    		((MainActivity)getActivity()).startActivity(myIntent); 
        case R.id.action_only_with_price:
        	this.hideZero = !hideZero;
        	mPrefManager.setHideZeroBills(this.hideZero);
            item.setChecked(!item.isChecked());
            asyncLoadData();
            return true;
        case R.id.action_sort_default:
        	this.order = "Act_amount desc";
            item.setChecked(true);
            asyncLoadData();
            return true;
        case R.id.action_sort_price_desc:
        	this.order = "Act_amount desc";
        	//Collections.sort(this.transactionItems.subList(1, this.transactionItems.size()));
        	item.setChecked(true);
            asyncLoadData();
            return true;
        case R.id.action_sort_price_asc:
        	this.order = "Act_amount asc";
        	item.setChecked(true);
            asyncLoadData();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
    public void timePickerDilog(View paramView)
    {
/*    	Intent localIntent = new Intent(this.getActivity(), DialogDatePicker.class);
		localIntent.putExtra("year", curYear);
		localIntent.putExtra("month", curMonth);
		localIntent.putExtra("day", curDay);
		startActivityForResult(localIntent, RESULT_DATE);*/
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	if (requestCode == RESULT_DATE) {
    		if(resultCode == RESULT_OK){
    			this.curYear=data.getIntExtra("year", 1393);
    			this.curMonth=data.getIntExtra("month", 6);
    			this.curDay=data.getIntExtra("day", 1);
    			StringBuilder sb = new StringBuilder();
				sb.append(curYear).append("/");
				sb.append(String.format("%02d", Integer.valueOf(curMonth))).append("/");
				sb.append(String.format("%02d", Integer.valueOf(curDay)));
				curDate = sb.toString();
				btnDate.setText(curDate);

    		}
    		if(resultCode == RESULT_CANCELED)
    		{
    			
    		}
    	}
    } 
    
class AsyncLoadDB
    extends AsyncTask
    {
    	ArrayAdapter<BillItem> adapter;
    	@Override
    	protected void onPreExecute()
    	{
    		FragmentPayBills.this.listAcc.clear();
    		FragmentPayBills.this.listView.setAdapter(
    				adapter = new ArrayAdapter<BillItem>(FragmentPayBills.this.getActivity(), 17367043, FragmentPayBills.this.listAcc){
				@Override
				public View getView(final int position, View convertView, ViewGroup parent)
				  {
					final BillItem acc = FragmentPayBills.this.listAcc.get(position);
					TextView remainedbills,txtdate,txtTitle;
					
					if (convertView == null)
				    {
				      convertView = ((LayoutInflater)FragmentPayBills.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.items_paybill, parent, false);
		
				    }
					txtTitle = ((TextView)convertView.findViewById(R.id.accnameacc));
					txtTitle.setText(acc.getTitle());
					txtdate = ((TextView)convertView.findViewById(R.id.sincetimeacc));
					txtdate.setText(acc.getRegDateFa());
					
				    remainedbills = ((TextView)convertView.findViewById(R.id.sincetimeacc));
					remainedbills.setText(new DecimalFormat(" ###,###.## ").format(acc.getRemainAmount()));
    					
					if(acc.getRemainAmount()>0)
						remainedbills.setTextColor(FragmentPayBills.this.getResources().getColor(R.color.Tomato));
					if(acc.getRemainAmount() == 0)
						remainedbills.setTextColor(FragmentPayBills.this.getResources().getColor(R.color.Green));
			        
					
					convertView.setOnClickListener(new View.OnClickListener()
			          {
				            public void onClick(View view)
				            {
				            	Fragment frag = new FragmentPayBillsD();
				                Bundle args = new Bundle();
				                args.putSerializable("account",acc);
				                // TODO: check target accid
				                args.putString("accId", acc.getTargetAccountId()  );
				                frag.setArguments(args);
				                FragmentTransaction fgt = FragmentPayBills.this.getActivity().getSupportFragmentManager().beginTransaction();
				                //fgt.replace(R.id.content_frame, frag,"billDetail");
				                //fgt.addToBackStack("projectTransDetail");
				                //fgt.commit();
				              
				            }
				          });
					return convertView;
				  }
			});
    	}

    	@Override
    	protected Boolean doInBackground(Object... paramVarArgs) {
				// TODO: Fix here
				//List<BillItem> listBillP=_db.getBillAllTrans(FragmentPayBills.this.hideZero,"","",FragmentPayBills.this.order);
				// FragmentPayBills.this.listAcc.addAll(listBillP);
			return Boolean.valueOf(true);
    	}

		@Override
		protected void onPostExecute(Object result) {
			adapter.notifyDataSetChanged();
			super.onPostExecute(result);
		}

    }
}
