package ir.saeedrahimi.poolnegar.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beautycoder.pflockscreen.security.PFResult;
import com.beautycoder.pflockscreen.security.PFSecurityManager;
import com.beautycoder.pflockscreen.security.callbacks.PFPinCodeHelperCallback;

import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_Password;
import ir.saeedrahimi.poolnegar.ui.dialogs.YesNoDialog;
import ir.saeedrahimi.poolnegar.ui.helper.SnackBarHelper;

import static androidx.navigation.fragment.NavHostFragment.findNavController;

public class FragmentSettings extends BaseFragment implements View.OnClickListener, YesNoDialog.OnYesNoDialogResultListener {
    private static final int REQUEST_CODE_ENABLE = 11;
    private static final int REQUEST_CODE_DISABLE = 12;
    private View mView;


    public FragmentSettings() {

    }

    public static FragmentSettings newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        FragmentSettings fragment = new FragmentSettings();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_settings, container,
                false);

        mView.findViewById(R.id.btn_accounts_command).setOnClickListener(this);
        mView.findViewById(R.id.btn_project_command).setOnClickListener(this);
        mView.findViewById(R.id.btnBackup).setOnClickListener(this);
        mView.findViewById(R.id.btn_pin).setOnClickListener(this);
        mView.findViewById(R.id.btn_pin_disable).setOnClickListener(this);

        return mView;
    }


    @Override
    public void onClick(View view) {
        final Intent intent;
        switch (view.getId()) {
            case R.id.btn_accounts_command:
                findNavController(this).navigate(R.id.action_settings_to_accounts);
                break;
            case R.id.btn_project_command:
                findNavController(this).navigate(R.id.action_settings_to_projectCommands);
                break;
            case R.id.btnBackup:
                findNavController(this).navigate(R.id.action_settings_to_importExport);
                break;
            case R.id.nav_about:
                findNavController(this).navigate(R.id.action_settings_to_about);
                break;
            case R.id.btn_pin:
                intent = new Intent(getActivity(), Activity_Password.class);
                intent.putExtra("request_result", true);
                PFSecurityManager.getInstance().getPinCodeHelper().isPinCodeEncryptionKeyExist(new PFPinCodeHelperCallback<Boolean>() {
                    @Override
                    public void onResult(PFResult<Boolean> result) {
                        if(result.getResult() == false){
                            startActivityForResult(intent, REQUEST_CODE_ENABLE);
                        }else{

                            SnackBarHelper.showSnack(getActivity(), SnackBarHelper.SnackState.Info, "رمز عبور فعال است");
                        }
                    }
                });
                break;

            case R.id.btn_pin_disable:
                YesNoDialog dialog = YesNoDialog.getNewInstance(REQUEST_CODE_DISABLE,"حذف رمزعبور","رمز عبور حذف خواهد شد، اطمینان دارید؟","بله","لغو",this,null,true);
                dialog.show(getChildFragmentManager(), "disable_pin");
                break;
            default:
                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CODE_ENABLE:
                if (resultCode == Constants.RESULTS.RESULT_OK) {
                    mView.findViewById(R.id.btn_pin_disable).setEnabled(false);
                    PFSecurityManager.getInstance().getPinCodeHelper().isPinCodeEncryptionKeyExist(new PFPinCodeHelperCallback<Boolean>() {
                        @Override
                        public void onResult(PFResult<Boolean> result) {
                            if (result.getResult()) {
                                SnackBarHelper.showSnack(getActivity(), SnackBarHelper.SnackState.Info, "رمزعبور غعال شد");
                                mView.findViewById(R.id.btn_pin_disable).setEnabled(true);
                            } else {

                                SnackBarHelper.showSnack(getActivity(), SnackBarHelper.SnackState.Error, "رمزعبور غعال نشد");
                            }
                        }
                    });
                }
                break;
            case REQUEST_CODE_DISABLE:
                if (resultCode == Constants.RESULTS.RESULT_OK) {
                    mView.findViewById(R.id.btn_pin_disable).setEnabled(false);
                    PFSecurityManager.getInstance().getPinCodeHelper().delete(new PFPinCodeHelperCallback<Boolean>() {
                        @Override
                        public void onResult(PFResult<Boolean> result) {
                            if (result.getResult()) {
                                SnackBarHelper.showSnack(getActivity(), SnackBarHelper.SnackState.Info, "رمزعبور غیرغعال شد");
                                mView.findViewById(R.id.btn_pin_disable).setEnabled(false);
                            } else {

                                SnackBarHelper.showSnack(getActivity(), SnackBarHelper.SnackState.Error, "رمزعبور غیرغعال نشد");
                            }
                        }
                    });
                }
                break;
        }
    }

    @Override
    public void OnYesDialogResult(int requestCode, Object TagValue, boolean questionTwo) {
        if(requestCode == REQUEST_CODE_DISABLE){
            Intent intent = new Intent(getActivity(), Activity_Password.class);
            intent.putExtra("request_result", true);
            startActivityForResult(intent, REQUEST_CODE_DISABLE);
        }
    }

    @Override
    public void OnNoDialogResult(int RequestCode, Object TagValue) {

    }

    @Override
    public void OnCancelDialogResult(int RequestCode, Object TagValue) {

    }
}
