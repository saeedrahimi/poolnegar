package ir.saeedrahimi.poolnegar.ui.dialogs;

import androidx.fragment.app.DialogFragment;

public class BaseDialogFragment extends DialogFragment {
	
	public DialogDismissListener getDialogDismissListener() {
		return mDialogDismissListener;
	}
	
	public void setDialogDismissListener(DialogDismissListener mDialogDismissListener) {
		this.mDialogDismissListener = mDialogDismissListener;
	}
	
	public interface DialogDismissListener {
		public void onCancel();
		
		public void onDismiss();
	}
	
	private DialogDismissListener mDialogDismissListener = null;
	
	
	@Override
	public void onDismiss(android.content.DialogInterface dialog) {
		super.onDismiss(dialog);
		if (mDialogDismissListener != null) {
			mDialogDismissListener.onDismiss();
		}
	}
	
	@Override
	public void onCancel(android.content.DialogInterface dialog) {
		super.onCancel(dialog);
		if (mDialogDismissListener != null) {
			mDialogDismissListener.onCancel();
		}
	}
	
}
