package ir.saeedrahimi.poolnegar.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.saeedrahimi.poolnegar.R
import ir.saeedrahimi.poolnegar.database.model.TransactionItem
import ir.saeedrahimi.poolnegar.ui.views.TransactionViewHolder


class TransactionSimpleAdapter(private var items: List<TransactionItem>, private var listener: onTransactionClick) : RecyclerView.Adapter<TransactionViewHolder>() {


    fun setItems(items: List<TransactionItem>) {
        this.items = items
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_transaction, parent, false)
        return TransactionViewHolder(view)
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        if (position == items.size + 1) {
            holder.hideSeparator(true)
        }
        holder.bind(items[position])

        holder.itemView.setOnClickListener { v: View? ->
           listener.onTransactionClick(items[position])
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    interface onTransactionClick {
        fun onTransactionClick(transactionItem: TransactionItem?)
    }
}