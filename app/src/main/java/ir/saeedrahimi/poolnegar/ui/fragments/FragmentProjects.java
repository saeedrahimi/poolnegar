package ir.saeedrahimi.poolnegar.ui.fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.kennyc.view.MultiStateView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.PreferencesManager;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.helper.KeyHelper;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.database.model.Account;
import ir.saeedrahimi.poolnegar.databinding.FragmentProjectsBinding;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_ProjectDetails;
import ir.saeedrahimi.poolnegar.ui.activities.cu.ActivityCU_Account;
import ir.saeedrahimi.poolnegar.ui.adapters.BaseFilterableRecyclerViewAdapter;
import ir.saeedrahimi.poolnegar.ui.adapters.ProjectRecyclerViewAdapter;
import ir.saeedrahimi.poolnegar.ui.helper.SnackBarHelper;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class FragmentProjects extends Fragment_Base<FragmentProjectsBinding> implements  BaseFilterableRecyclerViewAdapter.RecyclerViewEventListener {
    public boolean mFirstAsync = false;
    String startDate, endDate;
    private ProjectRecyclerViewAdapter adapter;
    private PreferencesManager mPrefManager;
    private boolean loading = true;
    private boolean hideClosed;
    private List<Account> listProjects;
    private LoadProjectsTask mLoadProjectsTask;
    private int m_TotalPages;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initActionBar();

        this.listProjects = new ArrayList<>();

        mPrefManager = new PreferencesManager(this.getActivity().getApplicationContext());
        this.hideClosed = mPrefManager.getHideClosedProjects();
        this.adapter = new ProjectRecyclerViewAdapter(getActivity(), this);
        binding.rcvData.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rcvData.setAdapter(adapter);
        ReloadDataAsync();
    }

    @Override
    protected FragmentProjectsBinding getFragmentBinding(LayoutInflater inflater, ViewGroup container) {

        return FragmentProjectsBinding.inflate(inflater, container, false);
    }


    private void initActionBar() {
        binding.mainToolBar.supportToolbar.setBackgroundColor(getResources().getColor(R.color.green_light));
        updateStatusBarColor(R.color.green_light, false);
        txtRightAction.setVisibility(View.GONE);
    }

    @Override
    protected void onActionBarRightToolClicked() {

        Intent accountIntent = new Intent(getActivity(), ActivityCU_Account.class);
        accountIntent.putExtra("ParentId", Constants.ACCOUNTS.PROJECTS);
        startActivityForResult(accountIntent, KeyHelper.REQUEST_CODE_CU_ACCOUNT);
    }


    protected String getActionBarTitle() {
        return getString(R.string.title_activity_projects);
    }


    public void ReloadDataAsync() {
        if (mLoadProjectsTask != null && !mLoadProjectsTask.isCancelled())
            mLoadProjectsTask.cancel(true);

        mLoadProjectsTask = new LoadProjectsTask(this);
        mLoadProjectsTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify height parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.project_new:

                Intent accountIntent = new Intent(getActivity(), ActivityCU_Account.class);
                accountIntent.putExtra("ParentId", Constants.ACCOUNTS.PROJECTS);
                startActivityForResult(accountIntent, KeyHelper.REQUEST_CODE_CU_ACCOUNT);

                break;
            case R.id.hideclosed:
                this.hideClosed = !hideClosed;
                mPrefManager.setHideClosedProjects(this.hideClosed);
                item.setChecked(!item.isChecked());
                ReloadDataAsync();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == KeyHelper.REQUEST_CODE_CU_ACCOUNT) {
            if (resultCode == RESULT_OK) {

                ReloadDataAsync();
                SnackBarHelper.showSnack(getActivity(), SnackBarHelper.SnackState.Info, "تغیرات انجام شد", null, null);
            }
            if (resultCode == RESULT_CANCELED) {

            }
        }

    }

    @Override
    public void onItemSelectionChanged(int position, boolean checkedState) {

    }

    @Override
    public void onSelectionModeChanged(boolean selectionMode) {

    }

    @Override
    public void onViewTapped(int viewType, int position) {
        Account acc = this.adapter.getItem(position);
        Intent intent = new Intent(getActivity(), Activity_ProjectDetails.class);
        intent.putExtra(KeyHelper.KEY_ENTITY, acc);
        startActivity(intent);
    }

    @Override
    public void onContextMenuTapped(int position, int actionId) {

    }

    @Override
    public void onDataChanged(int viewType, int position) {

    }

    private static class LoadProjectsTask extends AsyncTask<Void, Void, List<Account>> {
        private WeakReference<FragmentProjects> fragmentReference;

        LoadProjectsTask(FragmentProjects context) {
            fragmentReference = new WeakReference<>(context);
        }

        @Override
        protected void onPreExecute() {
            // get a reference to the activity if it is still there
            FragmentProjects fragment = fragmentReference.get();
            if (fragment == null || fragment.isDetached()) return;

            fragment.binding.multiStateView.setViewState(MultiStateView.ViewState.LOADING);
        }

        @Override
        protected List<Account> doInBackground(Void... voids) {
            FragmentProjects fragment = fragmentReference.get();
            if (fragment == null || fragment.isDetached()) return null;

            return AccountDAO.getChildren(Constants.ACCOUNTS.PROJECTS);
        }

        @Override
        protected void onPostExecute(List<Account> result) {

            FragmentProjects fragment = fragmentReference.get();
            if (fragment == null || fragment.isDetached()) return;


            fragment.adapter.clearAll();
            fragment.adapter.addRange(result);

            if (fragment.adapter.getItemCount() > 0) {
                fragment.binding.multiStateView.setViewState(MultiStateView.ViewState.CONTENT);
            } else {
                fragment.binding.multiStateView.setViewState(MultiStateView.ViewState.EMPTY);
            }
        }
    }


}
