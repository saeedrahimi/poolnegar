package ir.saeedrahimi.poolnegar.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.woxthebox.draglistview.DragItemAdapter
import ir.saeedrahimi.poolnegar.R
import ir.saeedrahimi.poolnegar.customs.GripView
import ir.saeedrahimi.poolnegar.database.model.TransactionItem
import ir.saeedrahimi.poolnegar.models.PersonItem


class PersonDraggableAdapter(private var items: List<PersonItem>,
                                         private val mLayoutId: Int = 0,
                                         private val mGrabHandleId: Int = 0,
                                         private val mDragOnLongPress: Boolean = false) :

        DragItemAdapter<PersonItem, PersonDraggableAdapter.PersonDraggableViewHolder>() {

    fun setItems(items: List<PersonItem>) {
        this.items = items
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonDraggableViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(mLayoutId, parent, false)
        return PersonDraggableViewHolder(view, mGrabHandleId, mDragOnLongPress)
    }

    override fun onBindViewHolder(holder: PersonDraggableViewHolder, position: Int) {
        super.onBindViewHolder(holder, position);
        if (position == items.size + 1) {
            holder.hideSeparator(true)
        }
        holder.bind(items[position])

        holder.itemView.tag = position;
    }


    override fun getItemCount(): Int {
        return items.size
    }

    override fun getUniqueItemId(position: Int): Long {

        return mItemList[position].hashCode().toLong();
    }

    class PersonDraggableViewHolder(itemView: View, handleResId: Int, dragOnLongPress: Boolean) : DragItemAdapter.ViewHolder(itemView, handleResId, dragOnLongPress) {
        private val txtTitle: TextView = itemView.findViewById(R.id.acctitle)
        private val dragHandle: GripView = itemView.findViewById(R.id.drag_handle)
        private val separator: View = itemView.findViewById(R.id.separator)
        private val icon: ImageView = itemView.findViewById(R.id.accicontrans)

        fun bind(transaction: PersonItem) {

            txtTitle.text = transaction.name

        }

        fun hideSeparator(hide: Boolean) {
            if (hide) {
                separator.visibility = View.GONE
            } else {
                separator.visibility = View.VISIBLE
            }
        }

        override fun onItemClicked(view: View) {
            //Toast.makeText(view.context, "Item clicked", Toast.LENGTH_SHORT).show()
        }

        override fun onItemLongClicked(view: View): Boolean {
            // Toast.makeText(view.context, "Item long clicked", Toast.LENGTH_SHORT).show()
            return true
        }
    }
}