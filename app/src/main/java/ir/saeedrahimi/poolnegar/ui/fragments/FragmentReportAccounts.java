package ir.saeedrahimi.poolnegar.ui.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import ir.saeedrahimi.poolnegar.ui.activities.Activity_ProjectDetails;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.database.model.Account;
import ir.saeedrahimi.poolnegar.ui.activities.MainActivity;

public class FragmentReportAccounts extends BaseFragment {
    private static final int RESULT_DATE = 0;

    private static final int RESULT_OK = 0;

    private static final int RESULT_CANCELED = 0;
    Calendar O = Calendar.getInstance();
    private int curDay;
    private int curMonth;
    private int curYear;
    String curDate;
    String startDate, endDate;
    private Button btnDate;

    ProgressDialog pgDialog;

    List<Account> listAcc;

    LinkedHashMap<Account, ArrayList<Account>> hashAcc;

    ExpandableListView expandList;

    ListView listView;


    int parentId;

    public void showLoading() {
        this.pgDialog = new ProgressDialog(this.getActivity());
        this.pgDialog.setMessage("در حال بارگذاری");
        this.pgDialog.setCancelable(false);
        this.pgDialog.show();
    }

    public void hideLoading() {
        if (this.pgDialog != null && this.pgDialog.isShowing())
            pgDialog.dismiss();
    }

    public void fillExpandList(int parentId) {
        //TODO: FIX HERE
//        this.hashAcc = this._db.getChildBalances(parentId, "9999/99/99", "", 1, "Y");
        this.listAcc = new ArrayList<Account>();
        this.parentId = parentId;

        for (Iterator<Account> iterator = hashAcc.keySet().iterator(); iterator.hasNext(); ) {
            Account acc = (Account) iterator.next();

            //TODO: FIX HERE
//            acc.setTotalIncome(_db.getProjectIncomeBalance(acc.getAccID(), "", "9999/99/99"));
//            acc.setTotalOutcome(_db.getProjectOutcomeBalance(acc.getAccID(), "", "9999/99/99"));
//            Log.d("FragmentAccounts: '" + acc.getTitle() + "-" + acc.getAccID() + "' inBalance", acc.getTotalIncome() + "");
//            Log.d("FragmentAccounts: '" + acc.getTitle() + "-" + acc.getAccID() + "' outBalance", acc.getTotalOutcome() + "");
            this.listAcc.add(acc);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_bills, container, false);
        ((MainActivity) getActivity()).setTitle("لیست حساب ها");
        this.listAcc = new ArrayList<Account>();
        this.hashAcc = new LinkedHashMap<Account, ArrayList<Account>>();
        this.listView = ((ListView) view.findViewById(R.id.rep_balance_list));//c
        this.expandList = ((ExpandableListView) view.findViewById(R.id.rep_balance_expandableList));//d
        Bundle localBundle = getArguments();

        if (localBundle != null) {
            this.parentId = localBundle.getInt("parentId");
        }

        AsyncReportBalance acb = new AsyncReportBalance(this);
        acb.execute(new Integer[]{parentId});
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.main_reason, menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify height parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void timePickerDilog(View paramView) {
/*        Intent localIntent = new Intent(this.getActivity(), DialogDatePicker.class);
        localIntent.putExtra("year", curYear);
        localIntent.putExtra("month", curMonth);
        localIntent.putExtra("day", curDay);
        startActivityForResult(localIntent, RESULT_DATE);*/
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_DATE) {
            if (resultCode == RESULT_OK) {
                this.curYear = data.getIntExtra("year", 1393);
                this.curMonth = data.getIntExtra("month", 6);
                this.curDay = data.getIntExtra("day", 1);
                StringBuilder sb = new StringBuilder();
                sb.append(curYear).append("/");
                sb.append(String.format("%02d", Integer.valueOf(curMonth))).append("/");
                sb.append(String.format("%02d", Integer.valueOf(curDay)));
                curDate = sb.toString();
                btnDate.setText(curDate);

            }
            if (resultCode == RESULT_CANCELED) {

            }
        }
    }


    class AsyncReportBalance
            extends AsyncTask<Integer, Integer, Integer> {
        FragmentReportAccounts page;

        AsyncReportBalance(FragmentReportAccounts page) {

            this.page = page;
        }

        @Override
        protected void onPreExecute() {
            page.showLoading();
        }

        @Override
        protected Integer doInBackground(Integer... paramVarArgs) {
            if ((paramVarArgs != null) && (paramVarArgs[0] != null)) {
                int parentId = paramVarArgs[0].intValue();
                this.page.fillExpandList(parentId);
                return Integer.valueOf(parentId);
            }
            return Integer.valueOf(-1);
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (result != -1) {
                this.page.expandList.setAdapter(new ExpandableListAdapter() {


                    @Override
                    public boolean isChildSelectable(int position, int paramInt2) {
                        return true;
                    }

                    @Override
                    public boolean hasStableIds() {
                        return false;
                    }

                    @Override
                    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, ViewGroup parent) {
                        Account acc = page.listAcc.get(groupPosition);
                        TextView amount;
                        TextView name;
                        if (convertView == null) {
                            convertView = ((LayoutInflater) page.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.rep_balance_subacc_header_row, parent, false);

                        }
                        amount = ((TextView) convertView.findViewById(R.id.rep_balance_subacc_headrow_totAmount));
                        name = ((TextView) convertView.findViewById(R.id.rep_balance_subacc_headrow_name));
                        name.setText(acc.getTitle());

                        if (acc.getTotalBalance() >= 0.0D) {
                            amount.setText(new DecimalFormat(" ###,###.## ").format(acc.getTotalBalance()));
                            amount.setTextColor(Color.parseColor("#000000"));
                            if (acc.getTitle().equals("درآمد ها")) {
                                amount.setText(new DecimalFormat("( ###,###.## )").format(acc.getTotalBalance()));
                                amount.setTextColor(Color.parseColor("#8FBC8F"));
                            }
                            return convertView;
                        }

                        amount.setText(new DecimalFormat(" ###,###.## ").format(acc.getTotalBalance()));
                        amount.setTextColor(Color.parseColor("#000000"));
                        if (acc.getTitle().equals("درآمد ها")) {
                            amount.setText(new DecimalFormat("( ###,###.## )").format(acc.getTotalBalance()));
                            amount.setTextColor(Color.parseColor("#FF0000"));
                        }
                        return convertView;
                    }

                    @Override
                    public long getGroupId(int position) {
                        return position;
                    }

                    @Override
                    public int getGroupCount() {
                        return page.listAcc.size();
                    }

                    @Override
                    public Object getGroup(int position) {
                        return page.listAcc.get(position);
                    }

                    @Override
                    public long getCombinedGroupId(long paramLong) {
                        return 0;
                    }

                    @Override
                    public long getCombinedChildId(long paramLong1, long paramLong2) {
                        return 0;
                    }

                    @Override
                    public int getChildrenCount(int groupPosition) {
                        return page.hashAcc.get(page.listAcc.get(groupPosition)).size();
                    }

                    @Override
                    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
                        Account acc = page.hashAcc.get(page.listAcc.get(groupPosition)).get(childPosition);
                        TextView amount, name;
                        if (convertView == null) {
                            convertView = ((LayoutInflater) page.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.rep_balance_subacc_item_row, parent, false);

                        }
                        amount = ((TextView) convertView.findViewById(R.id.rep_balance_subacc_itemrow_totAmount));
                        name = ((TextView) convertView.findViewById(R.id.rep_balance_subacc_itemrow_name));
                        name.setText(acc.getTitle());

                        if (acc.getTotalBalance() >= 0.0D) {
                            amount.setText(new DecimalFormat(" ###,###.## ").format(acc.getTotalBalance()));
                            amount.setTextColor(Color.parseColor("#000000"));
                            if (acc.getTitle().equals("درآمد ها")) {
                                amount.setText(new DecimalFormat("( ###,###.## )").format(acc.getTotalBalance()));
                                amount.setTextColor(Color.parseColor("#8FBC8F"));
                            }
                            return convertView;
                        }

                        amount.setText(new DecimalFormat(" ###,###.## ").format(acc.getTotalBalance()));
                        amount.setTextColor(Color.parseColor("#000000"));
                        if (acc.getTitle().equals("درآمد ها")) {
                            amount.setText(new DecimalFormat("( ###,###.## )").format(acc.getTotalBalance()));
                            amount.setTextColor(Color.parseColor("#FF0000"));
                        }
                        return convertView;
                    }

                    @Override
                    public long getChildId(int groupPosition, int childPosition) {
                        return childPosition;
                    }

                    @Override
                    public Object getChild(int groupPosition, int childPosition) {
                        return null;
                    }

                    @Override
                    public boolean areAllItemsEnabled() {
                        return false;
                    }

                    @Override
                    public void registerDataSetObserver(
                            DataSetObserver paramDataSetObserver) {

                    }

                    @Override
                    public void unregisterDataSetObserver(
                            DataSetObserver paramDataSetObserver) {

                    }

                    @Override
                    public boolean isEmpty() {
                        return false;
                    }

                    @Override
                    public void onGroupExpanded(int position) {

                    }

                    @Override
                    public void onGroupCollapsed(int position) {

                    }
                });


            }
            page.hideLoading();
        }

        class AsyncReportProject
                extends AsyncTask<Integer, Integer, Integer> {
            FragmentReportAccounts page;

            AsyncReportProject(FragmentReportAccounts page) {

                this.page = page;
            }

            @Override
            protected void onPreExecute() {
                //frag.showLoading();
            }

            @Override
            protected Integer doInBackground(Integer... paramVarArgs) {
                if ((paramVarArgs != null) && (paramVarArgs[0] != null)) {
                    int parentId = paramVarArgs[0].intValue();
                    this.page.fillExpandList(parentId);
                    return Integer.valueOf(parentId);
                }
                return Integer.valueOf(-1);
            }

            @Override
            protected void onPostExecute(Integer result) {
                if (result != -1) {
                    this.page.listView.setAdapter(new ArrayAdapter<Account>(page.getActivity(), 17367043, page.listAcc) {
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            final Account acc = page.listAcc.get(position);
                            TextView begbalance, endbalance, expbalance, inbalance, txtdate, txtTitle;

                            if (convertView == null) {
                                convertView = ((LayoutInflater) page.getActivity().getSystemService(ContextWrapper.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.items_account, parent, false);

                            }
                            ImageView btnMenu = ((ImageView) convertView.findViewById(R.id.accountoverflow));
                            txtTitle = ((TextView) convertView.findViewById(R.id.accnameacc));
                            txtTitle.setText(acc.getTitle());
                            txtdate = ((TextView) convertView.findViewById(R.id.sincetimeacc));
                            txtdate.setText(acc.getProjectDate());

                            begbalance = ((TextView) convertView.findViewById(R.id.totalbill));
                            inbalance = ((TextView) convertView.findViewById(R.id.payedbills));
                            expbalance = ((TextView) convertView.findViewById(R.id.remainedbills));
                            endbalance = ((TextView) convertView.findViewById(R.id.endbalanceacc));

                            begbalance.setText(new DecimalFormat(" ###,###.## ").format(acc.getBalance() * -1));
                            inbalance.setText(new DecimalFormat(" ###,###.## ").format(acc.getTotalIncome()));
                            expbalance.setText(new DecimalFormat(" ###,###.## ").format(acc.getTotalOutcome()));

                            double endBalance = (acc.getBalance() + acc.getTotalOutcome()) * -1;

                            endbalance.setText(new DecimalFormat(" ###,###.## ").format(endBalance));
                            ImageView endimg = ((ImageView) convertView.findViewById(R.id.endingbalancecolor));
                            if (endBalance > 0)
                                endimg.setBackgroundColor(page.getResources().getColor(R.color.MediumSeaGreen));
                            if (endBalance == 0)
                                endimg.setBackgroundColor(page.getResources().getColor(R.color.Gray));


                            btnMenu.setOnClickListener(view -> {
                                PopupMenu popup = new PopupMenu(page.getActivity(), view);
                                MenuInflater inflater = popup.getMenuInflater();
                                inflater.inflate(R.menu.main, popup.getMenu());
                                popup.setOnMenuItemClickListener(arg0 -> {
                                    // TODO Auto-generated method stub
                                    return false;
                                });
                                popup.show();

                            });
                            convertView.setOnClickListener(view -> {
                                Intent intent = new Intent(getActivity(), Activity_ProjectDetails.class);
                                intent.putExtra("accId", acc.getAccID());
                                intent.putExtra("since", acc.getProjectDate());
                                intent.putExtra("name", acc.getTitle());
                                intent.putExtra("balance", acc.getBalance());
                                startActivity(intent);


                            });
                            return convertView;
                        }
                    });
                }
                //frag.hideLoading();
            }

        }

    }

}
