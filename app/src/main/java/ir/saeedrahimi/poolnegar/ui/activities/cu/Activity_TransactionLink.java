package ir.saeedrahimi.poolnegar.ui.activities.cu;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.Objects;

import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.R.id;
import ir.saeedrahimi.poolnegar.application.helper.KeyHelper;
import ir.saeedrahimi.poolnegar.customs.Utils;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.database.DAO.LinkDAO;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
import ir.saeedrahimi.poolnegar.database.model.TransactionLink;
import ir.saeedrahimi.poolnegar.databinding.ActivityTransactionLinkBinding;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_AccountSelection;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_ProjectSelection;
import ir.saeedrahimi.poolnegar.ui.dialogs.MessageDialog;


public class Activity_TransactionLink extends ActivityCU_Base<TransactionLink, ActivityTransactionLinkBinding> implements OnClickListener {

    private int mTransactionMethod = TransactionItem.TRANSACTION_METHOD_PAYMENT;

    public Activity_TransactionLink() {
        super();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case id.btnSourceAccount:
                selectSourceAccount();
                break;
            case id.btnTargetAccount:
                selectTargetAccount();
                break;
            case id.btnProject:
                selectProjectAccount();
                break;
            default:

        }

    }

    @Override
    protected String getActionBarTitle() {
        return getString(R.string.title_activity_transaction_link);
    }

    @Override
    public boolean validateEntity() {
        if (binding.txtTitle.getText().toString().equals("")) {
            MessageDialog msg = MessageDialog.getNewInstance(0, "خطا", "لطفا عنوان را وارد کنید");
            msg.showNow(getSupportFragmentManager(), "");
            return false;
        }
        return true;
    }

    @Override
    public void saveData() {
        double cashAmount;
        try {
            cashAmount = Double.parseDouble(binding.edtAmount
                    .toString().replace(",", ""));
        } catch (Exception localException) {
            cashAmount = 0;
        }


        mEntity.setTitle(binding.txtTitle.getText().toString());
        mEntity.setAmount(cashAmount);
        mEntity.setNote(binding.txtNote.getText().toString());
        mEntity.setLinkOrder(0);
        mEntity.setIsProjectLink(!Objects.equals(mEntity.getProjectId(), ""));

        // Start Save Process
        mEntity = LinkDAO.insert(mEntity);
        Toast.makeText(this, "میانبر تراکنش با موفقیت ذخیره شد", Toast.LENGTH_SHORT).show();


        Intent backIntent = new Intent();
        backIntent.putExtra("EntityId", this.mEntity.getLinkId());
        setResult(RESULT_OK, backIntent);
        finish();
    }

    @Override
    protected ActivityTransactionLinkBinding getViewBinding() {
        return ActivityTransactionLinkBinding.inflate(getLayoutInflater());
    }

    private void getExtras() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey(KeyHelper.KEY_TRANSACTION_METHOD)) {
                mTransactionMethod = bundle.getInt(KeyHelper.KEY_TRANSACTION_METHOD, TransactionItem.TRANSACTION_METHOD_PAYMENT);
            }
        }
    }

    @Override
    protected void onActivityFirstInit(Bundle savedInstanceState) {
        getExtras();
        binding.btnTargetAccount.setOnClickListener(this);
        binding.btnSourceAccount.setOnClickListener(this);
        binding.btnProject.setOnClickListener(this);
        binding.edtAmount.setKeyListener(DigitsKeyListener
                .getInstance("1234567890,."));
        binding.edtAmount.addTextChangedListener(Utils.MoneyTextWatcher);

        setContentView(binding.getRoot());
    }

    @Override
    public void onActivityStateNew() {
        mEntity = new TransactionLink();
        mEntity.setMethod(mTransactionMethod);

        if (mEntity.getMethod() == TransactionItem.TRANSACTION_METHOD_INCOME) {
            binding.rdIncomeMethod.setChecked(true);
            showIncomeView();
        }
        applyEntityToView();
    }

    @Override
    public void onActivityStateEdit() {

        if (mEntity.getMethod() == TransactionItem.TRANSACTION_METHOD_INCOME) {
            binding.rdIncomeMethod.setChecked(true);
            showIncomeView();
        }
        applyEntityToView();
    }

    private void applyEntityToView() {
        this.binding.txtTitle.setText(mEntity.getTitle());
        this.binding.txtNote.setText(mEntity.getNote());
        this.binding.edtAmount.setText(new DecimalFormat("#.##")
                .format(mEntity.getAmount()));
        this.binding.btnProject.setText(mEntity.getProjectTitle());
        this.binding.btnTargetAccount.setText(mEntity.getTargetAccountTitle());
        this.binding.btnSourceAccount.setText(mEntity.getSourceAccountTitle());

        binding.rgTransactionMethod.setOnCheckedChangeListener(this::onTransactionMethodChanged);

    }

    private String getTargetSelectionTitle() {
        if (mEntity.getMethod() == TransactionItem.TRANSACTION_METHOD_PAYMENT) {
            return getString(R.string.label_trans_payment_target);
        }
        return getString(R.string.title_activity_account_source_selection);
    }

    private String getSourceSelectionTitle() {
        if (mEntity.getMethod() == TransactionItem.TRANSACTION_METHOD_PAYMENT) {
            return getString(R.string.title_activity_account_source_selection);
        }
        return getString(R.string.label_trans_income_target);
    }

    public void selectTargetAccount() {
        Intent dlg = new Intent(this, Activity_AccountSelection.class);
        dlg.putExtra("ShowPayable", false);
        dlg.putExtra("ShowReceivable", true);
        dlg.putExtra("Title", getTargetSelectionTitle());
        this.startActivityForResult(dlg, KeyHelper.REQUEST_CODE_CHOOSE_TARGET_ACCOUNT);
    }

    public void selectSourceAccount() {
        Intent dlg = new Intent(this, Activity_AccountSelection.class);
        dlg.putExtra("ShowPayable", true);
        dlg.putExtra("ShowReceivable", false);
        dlg.putExtra("Title", getSourceSelectionTitle());
        this.startActivityForResult(dlg, KeyHelper.REQUEST_CODE_CHOOSE_SOURCE_ACCOUNT);
    }

    public void selectProjectAccount() {
        Intent dlg = new Intent(this, Activity_ProjectSelection.class);
        dlg.putExtra("Title", getString(R.string.title_activity_project_selection));
        this.startActivityForResult(dlg, KeyHelper.REQUEST_CODE_CHOOSE_PROJECT);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode == KeyHelper.REQUEST_CODE_CHOOSE_TARGET_ACCOUNT)) {
            if (resultCode == RESULT_OK) {
                this.mEntity.setTargetAccountId(data.getStringExtra("SelectedAccounts"));
                this.mEntity.setTargetAccountTitle(AccountDAO.getTitleByIdRecursive(mEntity.getTargetAccountId()));

                binding.btnTargetAccount.setText(mEntity.getTargetAccountTitle());

            }
        } else if ((requestCode == KeyHelper.REQUEST_CODE_CHOOSE_SOURCE_ACCOUNT)) {
            if (resultCode == RESULT_OK) {
                this.mEntity.setSourceAccountId(data.getStringExtra("SelectedAccounts"));
                this.mEntity.setSourceAccountTitle(AccountDAO.getTitleByIdRecursive(mEntity.getSourceAccountId()));

                binding.btnSourceAccount.setText(mEntity.getSourceAccountTitle());
            }
        } else if ((requestCode == KeyHelper.REQUEST_CODE_CHOOSE_PROJECT)) {
            if (resultCode == RESULT_OK) {
                this.mEntity.setProjectId(data.getStringExtra("SelectedAccounts"));
                this.mEntity.setProjectTitle(AccountDAO.getTitleById(mEntity.getProjectId()));
                binding.btnProject.setText(this.mEntity.getProjectTitle());
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding = null;
    }


    private void onTransactionMethodChanged(RadioGroup group, int checkedId) {
        clearAccounts();

        if (checkedId == binding.rdIncomeMethod.getId()) {
            mEntity.setMethod(TransactionItem.TRANSACTION_METHOD_INCOME);
            showIncomeView();
        } else if (checkedId == binding.rdPaymentMethod.getId()) {
            mEntity.setMethod(TransactionItem.TRANSACTION_METHOD_PAYMENT);
            showPaymentView();
        }
    }

    private void showPaymentView() {
        binding.vBoxTargetAccount.removeView(binding.btnSourceAccount);
        binding.vBoxSourceAccount.removeView(binding.btnTargetAccount);
        binding.vBoxSourceAccount.addView(binding.btnSourceAccount);
        binding.vBoxTargetAccount.addView(binding.btnTargetAccount);
        binding.txtTargetHeader.setText(getString(R.string.label_trans_payment_target));
    }

    private void showIncomeView() {
        binding.vBoxSourceAccount.removeView(binding.btnSourceAccount);
        binding.vBoxTargetAccount.removeView(binding.btnTargetAccount);
        binding.vBoxSourceAccount.addView(binding.btnTargetAccount);
        binding.vBoxTargetAccount.addView(binding.btnSourceAccount);
        binding.txtTargetHeader.setText(getString(R.string.label_trans_income_target));
    }

    private void clearAccounts() {
        mEntity.clearAccounts();
        binding.btnSourceAccount.setText("");
        binding.btnTargetAccount.setText("");
    }
}
