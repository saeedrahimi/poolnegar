package ir.saeedrahimi.poolnegar.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.multilevelview.MultiLevelAdapter;
import com.multilevelview.MultiLevelRecyclerView;
import com.multilevelview.OnRecyclerItemClickListener;

import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.models.AccountMultiLevel;


public class AccountBaseListAdapter extends MultiLevelAdapter {

    protected Holder mViewHolder;
    protected Context mContext;
    protected List mListItems;
    protected AccountMultiLevel mItem;
    protected MultiLevelRecyclerView mMultiLevelRecyclerView;
    protected OnRecyclerItemClickListener onRecyclerItemClickListener;

    public AccountBaseListAdapter(Context mContext, List<AccountMultiLevel> mListItems, MultiLevelRecyclerView mMultiLevelRecyclerView) {
        super(mListItems);
        this.mListItems = mListItems;
        this.mContext = mContext;
        this.mMultiLevelRecyclerView = mMultiLevelRecyclerView;
    }

    public void setOnItemClick(OnRecyclerItemClickListener onItemClick) {
        this.onRecyclerItemClickListener = onItemClick;
    }

    private void setExpandButton(ImageView expandButton, boolean isExpanded) {
        // set the icon based on the current state
        expandButton.setImageResource(isExpanded ? R.drawable.ic_keyboard_arrow_down_black_24dp : R.drawable.ic_keyboard_arrow_up_black_24dp);
    }

    @Override
    @NonNull
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_account, parent, false));
    }

    @Override
    public void updateItemList(List list) {
        super.updateItemList(list);
        this.mListItems = list;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        mViewHolder = (Holder) holder;
        mItem = (AccountMultiLevel) mListItems.get(position);

        switch (getItemViewType(position)) {
            case 1:
                holder.itemView.setBackgroundColor(Color.parseColor("#efefef"));
                break;
            case 2:
                holder.itemView.setBackgroundColor(Color.parseColor("#dedede"));
                break;
            default:
                holder.itemView.setBackgroundColor(Color.parseColor("#ffffff"));
                break;
        }
        mViewHolder.mTitle.setText(mItem.associatedObject.getTitle());

        if (mItem.hasChildren() && mItem.getChildren().size() > 0) {
            setExpandButton(mViewHolder.mExpandIcon, mItem.isExpanded());
            mViewHolder.mExpandButton.setVisibility(View.VISIBLE);
        } else {
            mViewHolder.mExpandButton.setVisibility(View.GONE);
        }
        if (position > 0) {
            mViewHolder.mDivider.setVisibility(View.VISIBLE);
        }
        Log.e("MuditLog", mItem.getLevel() + " " + mItem.getPosition() + " " + mItem.isExpanded() + "");

        // indent child items
        // Note: the parent item should start at zero to have no indentation
        // e.g. in populateFakeData(); the very first Item shold be instantiate like this: Item item = new Item(0);
        float density = mContext.getResources().getDisplayMetrics().density;
        ((ViewGroup.MarginLayoutParams) mViewHolder.mTextBox.getLayoutParams()).setMarginEnd((int) ((getItemViewType(position) * 15) * density + 0.5f));

//        mViewHolder.itemView.setOnClickListener(v -> {
//
//            if (!mItem.hasChildren() && onRecyclerItemClickListener != null) {
//                onRecyclerItemClickListener.onItemClick(v, mItem, position);
//            }
//
//
//        });

        //set click listener on LinearLayout because the click area is bigger than the ImageView
        mViewHolder.mExpandButton.setOnClickListener(v -> {
            // set click event on expand button here
            mMultiLevelRecyclerView.toggleItemsGroup(position);
            // rotate the icon based on the current state
            // but only here because otherwise we'd see the animation on expanded items too while scrolling
            mViewHolder.mExpandIcon.animate().rotation(mItem.isExpanded() ? -180 : 0).start();

            Toast.makeText(mContext, String.format(Locale.ENGLISH, "Item at position %d is expanded: %s",
                    position, mItem.isExpanded()), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public int getItemCount() {
        return mListItems != null ? mListItems.size() : 0;
    }

    protected class Holder extends RecyclerView.ViewHolder {

        TextView mTitle;
        ImageView mExpandIcon;
        ImageView mTreeIcon;
        ImageButton mBtnOption;
        View mDivider;
        View mSelectIndicator;
        RelativeLayout mTextBox, mExpandButton;

        Holder(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.txtName);
            mExpandIcon = itemView.findViewById(R.id.image_view);
            mTreeIcon = itemView.findViewById(R.id.imgTree);
            mDivider = itemView.findViewById(R.id.dividerLine);
            mTextBox = itemView.findViewById(R.id.itemView);
            mExpandButton = itemView.findViewById(R.id.expand_field);
            mSelectIndicator = itemView.findViewById(R.id.vSelectedItemIndicator);
            mBtnOption = itemView.findViewById(R.id.imgOption);
        }
    }

}