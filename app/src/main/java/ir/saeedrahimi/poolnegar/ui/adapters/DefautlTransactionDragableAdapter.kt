package ir.saeedrahimi.poolnegar.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.woxthebox.draglistview.DragItemAdapter
import com.woxthebox.draglistview.swipe.ListSwipeItem
import ir.saeedrahimi.poolnegar.R
import ir.saeedrahimi.poolnegar.customs.GripView
import ir.saeedrahimi.poolnegar.database.model.TransactionItem
import ir.saeedrahimi.poolnegar.database.model.TransactionLink
import kotlinx.android.synthetic.main.dialog_transactiondetail.view.*
import kotlinx.android.synthetic.main.list_item_transaction_defaults.view.*


class TransactionLinkDraggableAdapter(private var items: List<TransactionLink>,
                                      private var listener: onTransactionLinkClick,
                                      private val mLayoutId: Int = 0,
                                      private val mGrabHandleId: Int = 0,
                                      private val mDragOnLongPress: Boolean = false, private val disableDrag: Boolean) :

        DragItemAdapter<TransactionLink, TransactionLinkDraggableAdapter.TransactionLinkDraggableViewHolder>() {

    fun setItems(items: List<TransactionLink>) {
        this.items = items
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionLinkDraggableViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(mLayoutId, parent, false)
        return TransactionLinkDraggableViewHolder(listener, view, mGrabHandleId, mDragOnLongPress)
    }

    override fun onBindViewHolder(holder: TransactionLinkDraggableViewHolder, position: Int) {
        super.onBindViewHolder(holder, position);
        if (position == items.size + 1) {
            holder.hideSeparator(true)
        }
        holder.bind(items[position] , position)
        holder.itemView.tag = position
        if(disableDrag){
            holder.dragHandle.visibility = View.GONE
            holder.swipeItem.supportedSwipeDirection = ListSwipeItem.SwipeDirection.NONE
        }

    }


    override fun getItemCount(): Int {
        return items.size
    }

    interface onTransactionLinkClick {
        fun onTransactionLinkClick(link: TransactionLink)
        fun onTransactionLinkDelete(link: TransactionLink, position: Int )
    }

    override fun getUniqueItemId(position: Int): Long {
        if(mItemList != null && mItemList[position] != null )
            return mItemList[position].hashCode().toLong()
        return 0;
    }

    class TransactionLinkDraggableViewHolder(private var listener: onTransactionLinkClick, itemView: View, handleResId: Int, dragOnLongPress: Boolean) : DragItemAdapter.ViewHolder(itemView, handleResId, dragOnLongPress) {
        private val txtTitle: TextView = itemView.findViewById(R.id.acctitle)
        val dragHandle: GripView = itemView.findViewById(R.id.drag_handle)
        private val separator: View = itemView.findViewById(R.id.separator)
        private val icon: ImageView = itemView.findViewById(R.id.accicontrans)
        val swipeItem: ListSwipeItem = itemView.rootView as ListSwipeItem
        private val btnDelete: ImageView = itemView.findViewById(R.id.btn_delete)

        fun bind(link: TransactionLink, position: Int) {

            swipeItem.supportedSwipeDirection = ListSwipeItem.SwipeDirection.LEFT


            txtTitle.text = link.title

            if (link.method == 0)
                icon.setBackgroundColor(itemView.context.resources.getColor(R.color.LimeGreen))
            else
                icon.setBackgroundColor(itemView.context.resources.getColor(R.color.outcome))
            txtTitle.setOnClickListener {
                listener.onTransactionLinkClick(link)
            }
            btnDelete.setOnClickListener {
                listener.onTransactionLinkDelete(link, position)
            }

        }

        fun hideSeparator(hide: Boolean) {
            if (hide) {
                separator.visibility = View.GONE
            } else {
                separator.visibility = View.VISIBLE
            }
        }

        override fun onItemClicked(view: View) {
            // Toast.makeText(view.context, "Item clicked", Toast.LENGTH_SHORT).show()
        }

        override fun onItemLongClicked(view: View): Boolean {
            // Toast.makeText(view.context, "Item long clicked", Toast.LENGTH_SHORT).show()
            return true
        }

    }
}