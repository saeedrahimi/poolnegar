package ir.saeedrahimi.poolnegar.ui.activities;

import android.animation.Animator;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import ir.saeedrahimi.PhoneFormat.PhoneFormat;
import ir.saeedrahimi.poolnegar.BaseFragmentActivity;
import ir.saeedrahimi.poolnegar.FileLog;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.MainApplication;
import ir.saeedrahimi.poolnegar.customs.ArtemTextView;
import ir.saeedrahimi.poolnegar.customs.LayoutHelper;
import ir.saeedrahimi.poolnegar.customs.SlideView;
import ir.saeedrahimi.poolnegar.customs.TypefaceSpan;
import ir.saeedrahimi.poolnegar.customs.Utils;
import ir.saeedrahimi.poolnegar.dialogs.AlertDialog;
import ir.saeedrahimi.poolnegar.dialogs.DialogYesNo;
import ir.saeedrahimi.poolnegar.dialogs.ProgressLoading;
import ir.saeedrahimi.poolnegar.receivers.SmsListener.DidReceivedSMS;

public class ActActivity extends BaseFragmentActivity implements OnClickListener , DialogYesNo.onYesNoDialogClick{

	String deviceId;
	private int currentViewNum = 0;
    private SlideView[] views = new SlideView[5];
    private ProgressLoading progressLoading;
    protected View fragmentView;
	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		
		ActionBar actionBar = getActionBar();
	    actionBar.setDisplayHomeAsUpEnabled(false);
		setToolBarTitle("فعالسازی پولنگار", "");
		/*((Button)findViewById(R.id.btn_send)).setOnClickListener(this);
		EditText txt = (EditText)findViewById(R.id.txt_id);
		
		txt.setText(deviceId);
		TextWatcher tw = setReadOnly(txt, true, null);*/
		deviceId= new Utils(this).getDeviceId();
		boolean hasNetwork = getIntent().getBooleanExtra("network",false);
		
		fragmentView = new ScrollView(this);
        ScrollView scrollView = (ScrollView) fragmentView;
        scrollView.setFillViewport(true);

        FrameLayout frameLayout = new FrameLayout(this);
        scrollView.addView(frameLayout);
        ScrollView.LayoutParams layoutParams = (ScrollView.LayoutParams) frameLayout.getLayoutParams();
        layoutParams.width = ScrollView.LayoutParams.MATCH_PARENT;
        layoutParams.height = ScrollView.LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.TOP | Gravity.LEFT;
        frameLayout.setLayoutParams(layoutParams);

        views[0] = new PhoneView(this);
        views[1] = new LoginActivitySmsView(this);
        views[2] = new PhoneView(this);
        views[3] = new PhoneView(this);
        views[4] = new PhoneView(this);
        for (int a = 0; a < 5; a++) {
            views[a].setVisibility(a == 0 ? View.VISIBLE : View.GONE);
            frameLayout.addView(views[a]);
            FrameLayout.LayoutParams layoutParams1 = (FrameLayout.LayoutParams) views[a].getLayoutParams();
            layoutParams1.width = LayoutHelper.MATCH_PARENT;
            layoutParams1.height = a == 0 ? LayoutHelper.WRAP_CONTENT : LayoutHelper.MATCH_PARENT;
            layoutParams1.leftMargin = Utils.dp(18);
            layoutParams1.rightMargin = Utils.dp(18);
            layoutParams1.topMargin = Utils.dp(30);
            layoutParams1.gravity = Gravity.TOP | Gravity.LEFT;
            views[a].setLayoutParams(layoutParams1);
        }
        setContentView(fragmentView);
        views[0].onShow();
        actionBar.setTitle(views[0].getHeaderName());
	}
	public static TextWatcher setReadOnly(final EditText edt, final boolean readOnlyState, TextWatcher remove) {
	    edt.setCursorVisible(!readOnlyState);
	    TextWatcher tw = null;
	    final String text = edt.getText().toString();
	    if (readOnlyState) {
	            tw = new TextWatcher() {
					
	            @Override
	            public void afterTextChanged(Editable s) {

	            }
	            @Override
	            //saving the text before change
	            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	            }

	            @Override
	            // and replace it with content if it is about to change
	            public void onTextChanged(CharSequence s, int start,int before, int count) {
	                edt.removeTextChangedListener(this);
	                edt.setText(text);
	                edt.addTextChangedListener(this);
	            }
	        };
	        edt.addTextChangedListener(tw);
	        return tw;
	    } else {
	        edt.removeTextChangedListener(remove);
	        return remove;
	    }
	}
	@Override
	protected void onResume() {
	    super.onResume();
	}

	@Override
	protected void onPause() {
	    super.onPause();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//super.onCreateOptionsMenu(menu);
		//getMenuInflater().inflate(R.menu.main_checkcancel, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item != null && item.getItemId() == R.id.btn_send) {
			
		}

		return false;
	}
	@Override
	public void onClick(View v) {
		if (v != null && v.getId() == R.id.btn_send) {
			new DialogYesNo(this, this, null, getResources().getString(R.string.headerieerror), "سریال از طریق پیامک به سرور نرم افزار ارسال خواهد شد "+"\n"+"از ممکن بودن ارسال پیامک اطمینان حاصل نمایید (شارژ، آنتن و ...)"+"\n\n"+">>ادامه می دهید؟", "", false, "", false, "sendSms");
		}
	}
	
	@Override
	public void onYesNoDialog(String token, boolean checked) {
		if (token.equals("sendSms")) {
			
			new ValidatePhone().execute("09354857744",deviceId);
			//sendSms("+989354857744",deviceId);
		}
	}
	private boolean doubleBackToExitPressedOnce = false;
	@Override
	public void onBackPressed() {
		 if (doubleBackToExitPressedOnce) {
		        super.onBackPressed();
		        return;
		    }

		    this.doubleBackToExitPressedOnce = true;
		    Toast.makeText(this, "برای خروج دوباره دکمه بازگشت را بزنید",Toast.LENGTH_SHORT).show();

		    new Handler().postDelayed(new Runnable() {
		        @Override
		        public void run() {
		            doubleBackToExitPressedOnce=false;                       
		        }
		    }, 2000);
	}
	public class PhoneView extends SlideView implements AsyncResponse {

        private EditText phoneField;
        private TextView countryButton;

        private boolean ignoreSelection = false;
        private boolean ignoreOnTextChange = false;
        private boolean ignoreOnPhoneChange = false;
        private boolean nextPressed = false;
        public PhoneView(Context context) {
            super(context);

            setOrientation(VERTICAL);
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setLayoutDirection(LAYOUT_DIRECTION_LTR);
            linearLayout.setOrientation(HORIZONTAL);
            addView(linearLayout);
            LayoutParams layoutParams = (LayoutParams) linearLayout.getLayoutParams();
            layoutParams.width = LayoutHelper.MATCH_PARENT;
            layoutParams.height = LayoutHelper.WRAP_CONTENT;
            layoutParams.topMargin = Utils.dp(20);
            linearLayout.setLayoutParams(layoutParams);

            TextView textView = new TextView(context);
            textView.setText("+98");
            textView.setTextColor(0xff212121);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            textView.setPadding(Utils.dp(10), 0, 0, 0);
            textView.setGravity(Gravity.LEFT);
            textView.setTextDirection(TEXT_DIRECTION_LTR);
            linearLayout.addView(textView);
            layoutParams = (LayoutParams) textView.getLayoutParams();
            layoutParams.width = LayoutHelper.WRAP_CONTENT;
            layoutParams.height = LayoutHelper.WRAP_CONTENT;
            layoutParams.rightMargin = Utils.dp(16);
            layoutParams.leftMargin = Utils.dp(-9);
            textView.setLayoutParams(layoutParams);
            /*
            EditText codeField = new EditText(context);
            codeField.setTextColor(0xff212121);
            Utils.clearCursorDrawable(codeField);
            codeField.setPadding(Utils.dp(10), 0, 0, 0);
            codeField.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            codeField.setMaxLines(1);
            codeField.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            codeField.setEnabled(false);
            codeField.setText("98");
            linearLayout.addView(codeField);
            layoutParams = (LayoutParams) codeField.getLayoutParams();
            layoutParams.width = Utils.dp(55);
            layoutParams.height = Utils.dp(36);
            layoutParams.rightMargin = Utils.dp(16);
            layoutParams.leftMargin = Utils.dp(-9);
            codeField.setLayoutParams(layoutParams);
            */
            phoneField = new EditText(context);
            phoneField.setInputType(InputType.TYPE_CLASS_PHONE);
            phoneField.setTextColor(0xff212121);
            phoneField.setHintTextColor(0xff979797);
            phoneField.setPadding(0, 0, 0, 0);
            Utils.clearCursorDrawable(phoneField);
            phoneField.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            phoneField.setMaxLines(1);
            phoneField.setTextDirection(TEXT_DIRECTION_LTR);
            phoneField.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            phoneField.setImeOptions(EditorInfo.IME_ACTION_NEXT | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
            phoneField.setFocusable(true);
            phoneField.setFocusableInTouchMode(true);
            linearLayout.addView(phoneField);
            layoutParams = (LayoutParams) phoneField.getLayoutParams();
            layoutParams.width = LayoutHelper.MATCH_PARENT;
            layoutParams.height = Utils.dp(36);
            phoneField.setLayoutParams(layoutParams);
            phoneField.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    if (ignoreOnPhoneChange) {
                        return;
                    }
                    if (count == 1 && after == 0 && s.length() > 1) {
                        String phoneChars = "0123456789";
                        String str = s.toString();
                        String substr = str.substring(start, start + 1);
                        if (!phoneChars.contains(substr)) {
                            ignoreOnPhoneChange = true;
                            StringBuilder builder = new StringBuilder(str);
                            int toDelete = 0;
                            for (int a = start; a >= 0; a--) {
                                substr = str.substring(a, a + 1);
                                if (phoneChars.contains(substr)) {
                                    break;
                                }
                                toDelete++;
                            }
                            builder.delete(Math.max(0, start - toDelete), start + 1);
                            str = builder.toString();
                            if (PhoneFormat.strip(str).length() == 0) {
                                phoneField.setText("");
                            } else {
                                phoneField.setText(str);
                                updatePhoneField();
                            }
                            
                            ignoreOnPhoneChange = false;
                        }
                        if(start == 1 && s.toString() == "0")
                        {
                        	phoneField.setText("");
                        }
                    }
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (ignoreOnPhoneChange) {
                        return;
                    }
                    updatePhoneField();
                }
            });
            phoneField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    if (i == EditorInfo.IME_ACTION_NEXT) {
                        onNextPressed();
                        return true;
                    }
                    return false;
                }
            });

            
            textView = new ArtemTextView (context);
            textView.setText("ما به شماره همراهتون یک پیامک حاوی کد تاییدیه میفرستیم");
            textView.setTextColor(0xff757575);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
            textView.setGravity(Gravity.LEFT);
            textView.setLineSpacing(Utils.dp(2), 1.0f);
            addView(textView);
            layoutParams = (LayoutParams) textView.getLayoutParams();
            layoutParams.width = LayoutHelper.WRAP_CONTENT;
            layoutParams.height = LayoutHelper.WRAP_CONTENT;
            layoutParams.topMargin = Utils.dp(28);
            layoutParams.bottomMargin = Utils.dp(10);
            layoutParams.gravity = Gravity.RIGHT;
            textView.setLayoutParams(layoutParams);
            
            Utils.showKeyboard(phoneField);
            phoneField.requestFocus();

        }
        private void updatePhoneField() {
            ignoreOnPhoneChange = true;
            try {
                String codeText = "98";
                String phoneText = phoneField.getText().toString();
                if(phoneText.startsWith("0") || phoneText.startsWith("+"))
                	phoneText = phoneText.substring(1);
                String phone = PhoneFormat.getInstance().format("+" + codeText + phoneText);
                
                int idx = phone.indexOf(" ");
                if (idx != -1) {
                    String resultCode = PhoneFormat.stripExceptNumbers(phone.substring(0, idx));
                    if (!codeText.equals(resultCode)) {
                        phone = PhoneFormat.getInstance().format(phoneField.getText().toString()).trim();
                        phoneField.setText(phone);
                        int len = phoneField.length();
                        phoneField.setSelection(phoneField.length());
                    } else {
                        phoneField.setText(phone.substring(idx).trim());
                        int len = phoneField.length();
                        phoneField.setSelection(phoneField.length());
                    }
                } else {
                    phoneField.setSelection(phoneField.length());
                }
            } catch (Exception e) {
            	Log.d("tmessages", e.getMessage());
            }
            ignoreOnPhoneChange = false;
        }
        @Override
        public void onNextPressed() {
            if (nextPressed) {
                return;
            }
            if (phoneField.length() == 0) {
            	needShowAlert(getString(R.string.app_name), "Invalid Phone Number");
                return;
            }
            String phone = PhoneFormat.stripExceptNumbers("+98"+phoneField.getText());


            final Bundle params = new Bundle();
            params.putString("phone", "+98" + phoneField.getText());
            try {
                params.putString("ephone", PhoneFormat.stripExceptNumbers("98 " + PhoneFormat.stripExceptNumbers(phoneField.getText().toString())));
            } catch (Exception e) {
                Log.d("tmessages", e.getMessage());
                params.putString("ephone", "+" + phone);
            }
            
            params.putString("phoneFormated", phone);
            nextPressed = true;
            progressLoading = new ProgressLoading(ActActivity.this, "شکیبا باشید");
            ValidatePhone asyncTask = new ValidatePhone();
            asyncTask.delegate=this;
            asyncTask.execute(phone,deviceId);
            //needShowProgress();

        }

		@Override
		public void processFinish(String output) {
			nextPressed = false;
			progressLoading.done();
			final Bundle params = new Bundle();
			params.putString("phone", "+98" + phoneField.getText());
			String phone = PhoneFormat.stripExceptNumbers("+98"+phoneField.getText());
			try {
                params.putString("ephone", PhoneFormat.stripExceptNumbers("98 " + PhoneFormat.stripExceptNumbers(phoneField.getText().toString())));
            } catch (Exception e) {
                Log.d("tmessages", e.getMessage());
                params.putString("ephone", "+" + phone);
            }
            
            params.putString("phoneFormated", phone);
			setPage(1, true, params, false);
		}
        @Override
        public void onShow() {
            super.onShow();
            if (phoneField != null) {
                phoneField.requestFocus();
                phoneField.setSelection(phoneField.length());
            }
        }

        @Override
        public String getHeaderName() {
            return "تایید شماره";
        }

        @Override
        public void saveStateParams(Bundle bundle) {
            String phone = phoneField.getText().toString();
            if (phone != null && phone.length() != 0) {
                bundle.putString("phoneview_phone", phone);
            }
        }

        @Override
        public void restoreStateParams(Bundle bundle) {
            String phone = bundle.getString("phoneview_phone");
            if (phone != null) {
                phoneField.setText(phone);
            }
        }
    }
	public class LoginActivitySmsView extends SlideView implements DidReceivedSMS, AsyncResponse {

        private String phoneHash;
        private String requestPhone;
        private String emailPhone;
        private EditText codeField;
        private TextView confirmTextView;
        private TextView timeText;
        private TextView problemText;
        private Bundle currentParams;
        private final Context context;
        private Timer timeTimer;
        private Timer codeTimer;
        private final Object timerSync = new Object();
        private volatile int time = 60000;
        private volatile int codeTime = 15000;
        private double lastCurrentTime;
        private double lastCodeTime;
        private boolean waitingForSms = false;
        private boolean nextPressed = false;
        private String lastError = "";

        public LoginActivitySmsView(Context context) {
            super(context);

            setOrientation(VERTICAL);
            this.context = context;
            confirmTextView = new TextView(context);
            confirmTextView.setTextColor(0xff757575);
            confirmTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
            confirmTextView.setGravity(Gravity.RIGHT);
            confirmTextView.setLineSpacing(Utils.dp(2), 1.0f);
            addView(confirmTextView);
            LayoutParams layoutParams = (LayoutParams) confirmTextView.getLayoutParams();
            layoutParams.width = LayoutHelper.WRAP_CONTENT;
            layoutParams.height = LayoutHelper.WRAP_CONTENT;
            layoutParams.gravity = Gravity.LEFT;
            confirmTextView.setLayoutParams(layoutParams);

            codeField = new EditText(context);
            codeField.setTextColor(0xff212121);
            codeField.setHint("کد");
            Utils.clearCursorDrawable(codeField);
            codeField.setHintTextColor(0xff979797);
            codeField.setImeOptions(EditorInfo.IME_ACTION_NEXT | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
            codeField.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
            codeField.setInputType(InputType.TYPE_CLASS_PHONE);
            codeField.setMaxLines(1);
            codeField.setPadding(0, 0, 0, 0);
            addView(codeField);
            layoutParams = (LayoutParams) codeField.getLayoutParams();
            layoutParams.width = LayoutHelper.MATCH_PARENT;
            layoutParams.height = Utils.dp(36);
            layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
            layoutParams.topMargin = Utils.dp(20);
            codeField.setLayoutParams(layoutParams);
            codeField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    if (i == EditorInfo.IME_ACTION_NEXT) {
                        onNextPressed();
                        return true;
                    }
                    return false;
                }
            });

            timeText = new TextView(context);
            timeText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
            timeText.setTextColor(0xff757575);
            timeText.setLineSpacing(Utils.dp(2), 1.0f);
            timeText.setGravity(Gravity.RIGHT);
            addView(timeText);
            layoutParams = (LayoutParams) timeText.getLayoutParams();
            layoutParams.width = LayoutHelper.WRAP_CONTENT;
            layoutParams.height = LayoutHelper.WRAP_CONTENT;
            layoutParams.gravity = Gravity.RIGHT ;
            layoutParams.topMargin = Utils.dp(30);
            timeText.setLayoutParams(layoutParams);

            problemText = new TextView(context);
            problemText.setText("کد را دریافت نکرده ای؟");
            problemText.setVisibility(time < 1000 ? VISIBLE : GONE);
            problemText.setGravity(Gravity.RIGHT);
            problemText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
            problemText.setTextColor(0xff4d83b3);
            problemText.setLineSpacing(Utils.dp(2), 1.0f);
            problemText.setPadding(0, Utils.dp(2), 0, Utils.dp(12));
            addView(problemText);
            layoutParams = (LayoutParams) problemText.getLayoutParams();
            layoutParams.width = LayoutHelper.WRAP_CONTENT;
            layoutParams.height = LayoutHelper.WRAP_CONTENT;
            layoutParams.gravity = Gravity.RIGHT;
            layoutParams.topMargin = Utils.dp(20);
            problemText.setLayoutParams(layoutParams);
            problemText.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        PackageInfo pInfo = MainApplication.getAppContext().getPackageManager().getPackageInfo(MainApplication.getAppContext().getPackageName(), 0);
                        String version = String.format(Locale.US, "%s (%d)", pInfo.versionName, pInfo.versionCode);

                        Intent mailer = new Intent(Intent.ACTION_SEND);
                        mailer.setType("message/rfc822");
                        mailer.putExtra(Intent.EXTRA_EMAIL, new String[]{"saeedr.com@gmail.com"});
                        mailer.putExtra(Intent.EXTRA_SUBJECT, "Android registration/login issue " + version + " " + emailPhone);
                        mailer.putExtra(Intent.EXTRA_TEXT, "Phone: " + requestPhone + "\nApp version: " + version + "\nOS version: SDK " + Build.VERSION.SDK_INT + "\nDevice Name: " + Build.MANUFACTURER + Build.MODEL + "\nLocale: " + Locale.getDefault() + "\nError: " + lastError);
                        getContext().startActivity(Intent.createChooser(mailer, "Send email..."));
                    } catch (Exception e) {
                        needShowAlert(LoginActivitySmsView.this.context.getString(R.string.app_name), LoginActivitySmsView.this.context.getString(R.string.NoMailInstalled));
                    }
                }
            });

            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setGravity((Gravity.RIGHT) | Gravity.CENTER_VERTICAL);
            addView(linearLayout);
            layoutParams = (LayoutParams) linearLayout.getLayoutParams();
            layoutParams.width = LayoutHelper.MATCH_PARENT;
            layoutParams.height = LayoutHelper.MATCH_PARENT;
            layoutParams.gravity = (Gravity.RIGHT);
            linearLayout.setLayoutParams(layoutParams);

            TextView wrongNumber = new TextView(context);
            wrongNumber.setGravity((Gravity.RIGHT) | Gravity.CENTER_HORIZONTAL);
            wrongNumber.setTextColor(0xff4d83b3);
            wrongNumber.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
            wrongNumber.setLineSpacing(Utils.dp(2), 1.0f);
            wrongNumber.setPadding(0, Utils.dp(24), 0, 0);
            linearLayout.addView(wrongNumber);
            layoutParams = (LayoutParams) wrongNumber.getLayoutParams();
            layoutParams.width = LayoutHelper.WRAP_CONTENT;
            layoutParams.height = LayoutHelper.WRAP_CONTENT;
            layoutParams.gravity = Gravity.BOTTOM | (Gravity.RIGHT);
            layoutParams.bottomMargin = Utils.dp(10);
            wrongNumber.setLayoutParams(layoutParams);
            wrongNumber.setText(context.getString(R.string.WrongNumber));
            wrongNumber.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                    setPage(0, true, null, true);
                }
            });
        }

        @Override
        public String getHeaderName() {
            return context.getString(R.string.YourCode);
        }

        @Override
        public void setParams(Bundle params) {
            if (params == null) {
                return;
            }
            codeField.setText("");
            Utils.setWaitingForSms(true);
            currentParams = params;
            waitingForSms = true;
            String phone = params.getString("phone");
            emailPhone = params.getString("ephone");
            requestPhone = params.getString("phoneFormated");
            phoneHash = params.getString("phoneHash");
            time = params.getInt("calltime");

            if (phone == null) {
                return;
            }

            String number = PhoneFormat.getInstance().format(phone);
            String str = String.format(context.getString(R.string.SentSmsCode) + " %s", number);
            try {
                SpannableStringBuilder stringBuilder = new SpannableStringBuilder(str);
                TypefaceSpan span = new TypefaceSpan(Utils.getTypeface("fonts/vazir.ttf"));
                int idx = str.indexOf(number);
                stringBuilder.setSpan(span, idx, idx + number.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                confirmTextView.setText(stringBuilder);
            } catch (Exception e) {
                FileLog.e("tmessages", e);
                confirmTextView.setText(str);
            }

            Utils.showKeyboard(codeField);
            codeField.requestFocus();

            destroyTimer();
            destroyCodeTimer();
            lastCurrentTime = System.currentTimeMillis();
            problemText.setVisibility(time < 1000 ? VISIBLE : GONE);

            createTimer();
        }

        private void createCodeTimer() {
            if (codeTimer != null) {
                return;
            }
            codeTime = 15000;
            codeTimer = new Timer();
            lastCodeTime = System.currentTimeMillis();
            codeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    double currentTime = System.currentTimeMillis();
                    double diff = currentTime - lastCodeTime;
                    codeTime -= diff;
                    lastCodeTime = currentTime;
                    Utils.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            if (codeTime <= 1000) {
                                problemText.setVisibility(VISIBLE);
                                destroyCodeTimer();
                            }
                        }
                    });
                }
            }, 0, 1000);
        }

        private void destroyCodeTimer() {
            try {
                synchronized (timerSync) {
                    if (codeTimer != null) {
                        codeTimer.cancel();
                        codeTimer = null;
                    }
                }
            } catch (Exception e) {
                FileLog.e("tmessages", e);
            }
        }

        private void createTimer() {
            if (timeTimer != null) {
                return;
            }
            timeTimer = new Timer();
            timeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    double currentTime = System.currentTimeMillis();
                    double diff = currentTime - lastCurrentTime;
                    time -= diff;
                    lastCurrentTime = currentTime;
                    Utils.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            if (time >= 1000) {
                                int minutes = time / 1000 / 60;
                                int seconds = time / 1000 - minutes * 60;
                                timeText.setText( String.format(context.getString(R.string.CallText),  minutes, seconds));
                            } else {
                                timeText.setText(context.getString(R.string.Calling));
                                destroyTimer();
                                createCodeTimer();
                            }
                        }
                    });
                }
            }, 0, 1000);
        }

        private void destroyTimer() {
            try {
                synchronized (timerSync) {
                    if (timeTimer != null) {
                        timeTimer.cancel();
                        timeTimer = null;
                    }
                }
            } catch (Exception e) {
                FileLog.e("tmessages", e);
            }
        }

        @Override
        public void onNextPressed() {
            if (nextPressed) {
                return;
            }
            nextPressed = true;
            waitingForSms = false;
            Utils.setWaitingForSms(false);
            destroyTimer();
            progressLoading = new ProgressLoading(ActActivity.this, "شکیبا باشید");;
            CheckPhoneCode asyncTask = new CheckPhoneCode();
            asyncTask.delegate=this;
            asyncTask.execute(requestPhone,deviceId,codeField.getText().toString());
            
        }
        @Override
		public void processFinish(String output) {
			// TODO Auto-generated method stub
			
		}
        @Override
        public void onBackPressed() {
            destroyTimer();
            destroyCodeTimer();
            currentParams = null;
            Utils.setWaitingForSms(false);
            waitingForSms = false;
        }

        @Override
        public void onDestroyActivity() {
            super.onDestroyActivity();
            Utils.setWaitingForSms(false);
            destroyTimer();
            destroyCodeTimer();
            waitingForSms = false;
        }

        @Override
        public void onShow() {
            super.onShow();
            if (codeField != null) {
                codeField.requestFocus();
                codeField.setSelection(codeField.length());
            }
        }
        @Override
    	public void gotSms(final String m) {
    	        Utils.runOnUIThread(new Runnable() {
                    @Override
                    public void run() {
                        if (!waitingForSms) {
                            return;
                        }
                        if (codeField != null) {
                            codeField.setText("" + m);
                            onNextPressed();
                        }
                    }
                });
    	}

        @Override
        public void saveStateParams(Bundle bundle) {
            String code = codeField.getText().toString();
            if (code != null && code.length() != 0) {
                bundle.putString("smsview_code", code);
            }
            if (currentParams != null) {
                bundle.putBundle("smsview_params", currentParams);
            }
            if (time != 0) {
                bundle.putInt("time", time);
            }
        }

        @Override
        public void restoreStateParams(Bundle bundle) {
            currentParams = bundle.getBundle("smsview_params");
            if (currentParams != null) {
                setParams(currentParams);
            }
            String code = bundle.getString("smsview_code");
            if (code != null) {
                codeField.setText(code);
            }
            Integer t = bundle.getInt("time");
            if (t != 0) {
                time = t;
            }
        }

		
    }
	public void needShowAlert(String title, String text) {
        if (text == null) {
            return;
        }
        AlertDialog.Show(this, title, text);
    }
	public interface AsyncResponse {
	    void processFinish(String output);
	}

    public void setPage(int page, boolean animated, Bundle params, boolean back) {
        if (android.os.Build.VERSION.SDK_INT > 13) {
            final SlideView outView = views[currentViewNum];
            final SlideView newView = views[page];
            currentViewNum = page;
            //actionBar.setBackButtonImage(newView.needBackButton() ? R.drawable.ic_ab_back : 0);

            newView.setParams(params);
            setToolBarTitle(newView.getHeaderName(), "");
            newView.onShow();
            newView.setX(back ? - Utils.displaySize.x : Utils.displaySize.x);
            outView.animate().setInterpolator(new AccelerateDecelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    outView.setVisibility(View.GONE);
                    outView.setX(0);
                }

                @Override
                public void onAnimationCancel(Animator animator) {
                }

                @Override
                public void onAnimationRepeat(Animator animator) {
                }
            }).setDuration(300).translationX(back ? Utils.displaySize.x : - Utils.displaySize.x).start();
            newView.animate().setInterpolator(new AccelerateDecelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    newView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                }

                @Override
                public void onAnimationCancel(Animator animator) {
                }

                @Override
                public void onAnimationRepeat(Animator animator) {
                }
            }).setDuration(300).translationX(0).start();
        } else {
        	getActionBar().setDisplayHomeAsUpEnabled(true);
            //actionBar.setBackButtonImage(views[page].needBackButton() ? R.drawable.ic_ab_back : 0);
            views[currentViewNum].setVisibility(View.GONE);
            currentViewNum = page;
            views[page].setParams(params);
            views[page].setVisibility(View.VISIBLE);
            setToolBarTitle(views[page].getHeaderName(),"");
            views[page].onShow();
        }
    }
	class ValidatePhone extends AsyncTask<String, Void, String> {
		public AsyncResponse delegate=null;

	    private final String NAMESPACE = "urn:saeedmsgapi";
		private final String URL = "http://www.saeedr.com/messenger/api/index.php?wsdl";
		private final String SOAP_ACTION = "urn:saeedmsgapi#validate";
		private final String METHOD_NAME = "validate";
	    protected String doInBackground(String... params) {
	        try {
	        	 SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
	     	    request.addProperty("phoneNumber", params[0]);
	     	    request.addProperty("deviceId", params[1]);
	     	    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
	     	            SoapEnvelope.VER11);
	     	    envelope.setOutputSoapObject(request);
	     	    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
	     	   androidHttpTransport.call(SOAP_ACTION, envelope);
		        SoapObject resultRequestSOAP = (SoapObject) envelope.bodyIn;  
		        
	     	    return resultRequestSOAP.getProperty("return").toString();
	        } catch (Exception e) {
	            e.printStackTrace();
	            return null;
	        }
	    }

	    protected void onPostExecute(String result) {

	    	delegate.processFinish(result);
	    	
	    }
	}
	class CheckPhoneCode extends AsyncTask<String, Void, String> {
		public AsyncResponse delegate=null;

	    private final String NAMESPACE = "urn:saeedmsgapi";
		private final String URL = "http://www.saeedr.com/messenger/api/index.php?wsdl";
		private final String SOAP_ACTION = "urn:saeedmsgapi#checkcode";
		private final String METHOD_NAME = "checkcode";
	    protected String doInBackground(String... params) {
	        try {
	        	 SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
	     	    request.addProperty("phoneNumber", params[0]);
	     	    request.addProperty("deviceId", params[1]);
	     	   request.addProperty("code", params[2]);
	     	    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
	     	            SoapEnvelope.VER11);
	     	    envelope.setOutputSoapObject(request);
	     	    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
	     	   androidHttpTransport.call(SOAP_ACTION, envelope);
		        SoapObject resultRequestSOAP = (SoapObject) envelope.bodyIn;  
		        
	     	    return resultRequestSOAP.getProperty("return").toString();
	        } catch (Exception e) {
	            e.printStackTrace();
	            return null;
	        }
	    }

	    protected void onPostExecute(String result) {

	    	delegate.processFinish(result);
	    	
	    }
	}
}
