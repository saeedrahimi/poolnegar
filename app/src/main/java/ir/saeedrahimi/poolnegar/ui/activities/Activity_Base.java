package ir.saeedrahimi.poolnegar.ui.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.GlobalParams;
import ir.saeedrahimi.poolnegar.application.MainApplication;
import ir.saeedrahimi.poolnegar.application.helper.PermissionHelper;
import ir.saeedrahimi.poolnegar.database.AppDatabaseManager;

import java.util.List;

abstract public class Activity_Base extends AppCompatActivity {
	
	public PermissionHelper permissionHelper = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		permissionHelper = new PermissionHelper(this);
		GlobalParams.setApplicationActivityContext(getApplicationContext());
		GlobalParams.setApplicationActivityHandler(MainApplication.getAppHandler());
		baseInitialization();
		
	}
	
	private void baseInitialization() {
		//AppDatabaseManager.initializeInstance();
	}
	
	public String getName() {
		return getClass().getSimpleName();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	protected void onPostResume() {
		super.onPostResume();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
	}
	
	@Override
	public void recreate() {
		super.recreate();
		
		startActivity(getIntent());
		finish();
		
	}

	public void updateStatusBarColor(int colorResource, boolean isLightStatusBar){
		if (Build.VERSION.SDK_INT >= 21) {
			Window window = getWindow();
			View view = window.getDecorView();
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
			window.setStatusBarColor(getResources().getColor(colorResource));
			if (Build.VERSION.SDK_INT >= 23) {
				if(isLightStatusBar){
					view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

				}else {
					view.setSystemUiVisibility(view.getSystemUiVisibility() & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

				}
			}
		}
	}
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		List<Fragment> fragments = getSupportFragmentManager().getFragments();
		if (fragments != null) {
			for (Fragment fragment : fragments) {
				if (fragment != null) {
					fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
				}
			}
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		List<Fragment> fragments = getSupportFragmentManager().getFragments();
		if (fragments != null) {
			for (Fragment fragment : fragments) {
				if (fragment != null) {
					fragment.onActivityResult(requestCode, resultCode, data);
				}
			}
		}
	}
	
}
