package ir.saeedrahimi.poolnegar.ui.adapters;

import android.content.Context;
import android.view.View;

import com.multilevelview.MultiLevelRecyclerView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import ir.saeedrahimi.poolnegar.models.AccountMultiLevel;


public class AccountSelectListAdapter extends AccountBaseListAdapter {


    public AccountSelectListAdapter(Context mContext, List<AccountMultiLevel> mListItems, MultiLevelRecyclerView mMultiLevelRecyclerView) {
        super(mContext, mListItems, mMultiLevelRecyclerView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        mViewHolder.mBtnOption.setVisibility(View.GONE);
        if(!mItem.hasChildren()){
            mViewHolder.mSelectIndicator.setVisibility(View.VISIBLE);
        }

    }


}