package ir.saeedrahimi.poolnegar.ui.activities

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.floatingactionbutton.FloatingActionButton.OnVisibilityChangedListener
import com.ncapdevi.fragnav.FragNavController
import com.roughike.bottombar.BottomBar
import ir.saeedrahimi.poolnegar.R
import ir.saeedrahimi.poolnegar.databinding.ActivityMainBinding
import ir.saeedrahimi.poolnegar.ui.activities.cu.Activity_Transaction
import ir.saeedrahimi.poolnegar.ui.helper.FontHelper
import ir.saeedrahimi.poolnegar.ui.helper.setupWithNavController

class MainActivity : Activity_GeneralBase<ActivityMainBinding>(), View.OnClickListener {
    var activity_MainContext: MainActivity? = null
        private set
    private var isSecondaryMenuOpen = false
    private var countBackPressed = 0
    private var mBottomBar: BottomBar? = null
    var mToolBar: Toolbar? = null
    private var fab: FloatingActionButton? = null
    private var fabVisibilityChanged: OnVisibilityChangedListener? = null
    override fun onActivityFirstInit(savedInstanceState: Bundle?) {
        activity_MainContext = this
        initComponents()
        if (savedInstanceState == null) {
            setupBottomNavigationBar()
        }
        //checkStuff();
    }

    private fun initComponents() {
        mBottomBar = binding.bottomBar;
        FontHelper.setViewTextStyleTypeFace(mBottomBar)
//        mBottomBar?.setOnTabSelectListener(OnTabSelectListener {
//            //MainActivity.this.OnTabSelected(tabId);
//        })
        mBottomBar?.selectTabAtPosition(INDEX_DASHBOARD)
        fab = binding.bottomFab
        fab?.setOnClickListener(this)
        fabVisibilityChanged = object : OnVisibilityChangedListener() {
            override fun onShown(fab: FloatingActionButton) {
                super.onShown(fab)
            }

            override fun onHidden(fab: FloatingActionButton) {
                super.onHidden(fab)
                if (binding.bottomAppBar.fabAlignmentMode == BottomAppBar.FAB_ALIGNMENT_MODE_CENTER) {
                    binding.bottomAppBar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_END
                    mBottomBar?.visibility = View.GONE
                    binding.bottomBarSecondary.visibility = View.VISIBLE
                    fab.setImageDrawable(getDrawable(R.drawable.ic_reply_black_24dp))
                    isSecondaryMenuOpen = true
                } else {
                    binding.bottomAppBar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_CENTER
                    mBottomBar?.visibility = View.VISIBLE
                    binding.bottomBarSecondary.visibility = View.GONE
                    fab.setImageDrawable(getDrawable(R.drawable.ic_action_plus))
                    isSecondaryMenuOpen = false
                }
                fab.show()
            }
        }
        binding.btnInTrans.setOnClickListener(this)
        binding.btnOutTrans.setOnClickListener(this)
    }

    private var currentNavController: LiveData<NavController>? = null
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        // Now that BottomNavigationBar has restored its instance state
        // and its selectedItemId, we can proceed with setting up the
        // BottomNavigationBar with Navigation
        setupBottomNavigationBar();
    }

    /**
     * Called on first creation and when restoring state.
     */
    private fun setupBottomNavigationBar() {

        val navGraphIds = listOf(R.navigation.home, R.navigation.settings, R.navigation.trans)

        // Setup the bottom navigation view with a list of navigation graphs
        val controller = mBottomBar?.setupWithNavController(
                navGraphIds = navGraphIds,
                fragmentManager = supportFragmentManager,
                containerId = R.id.nav_host_container,
                intent = intent
        )

        // Whenever the selected controller changes, setup the action bar.
        controller?.observe(this, Observer { navController ->
            setupActionBarWithNavController(navController)
        })
        currentNavController = controller
    }

    override fun onSupportNavigateUp(): Boolean {
        return currentNavController!!.value!!.navigateUp()

    }

    override fun initActionBar() {
        super.initActionBar()
    }

    override fun getViewBinding(): ActivityMainBinding {
        return ActivityMainBinding.inflate(layoutInflater)
    }
    override fun onActionBarRightToolClicked() {}
    override fun onActionBarLeftToolClicked() {}
    override fun getActionBarTitle(): String {
        return "PoolNegar"
    }

    fun NavigateToTab(tabPosition: Int) {
        mBottomBar!!.selectTabAtPosition(tabPosition)
    }

    override fun onBackPressed() {
        if (isSecondaryMenuOpen) {
            fab!!.performClick()
        }else{

            super.onBackPressed()
        }
    }

    //************************* End *************************
    override fun onClick(view: View) {
        var intent: Intent? = null
        when (view.id) {
            R.id.btnInTrans -> {
                fab!!.performClick()
                intent = Intent(this, Activity_Transaction::class.java)
                intent.putExtra("TransactionMethod", 0)
            }
            R.id.btnOutTrans -> {
                fab!!.performClick()
                intent = Intent(this, Activity_Transaction::class.java)
                intent.putExtra("TransactionMethod", 1)
            }
            R.id.bottomFab -> {
                fab!!.hide(fabVisibilityChanged)
                invalidateOptionsMenu()
            }
        }
        if (intent != null) {
            val bundle = ActivityOptions.makeScaleUpAnimation(view, 0, 0, view.width, view.height).toBundle()
            startActivity(intent, bundle)
        }
    }

    companion object {
        const val INDEX_DASHBOARD = FragNavController.TAB1
        const val INDEX_TRANSACTIONS = FragNavController.TAB2
        const val INDEX_REPORTS = FragNavController.TAB3
        const val INDEX_SETTINGS = FragNavController.TAB4
    }

}