package ir.saeedrahimi.poolnegar;

import android.Manifest;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.method.DigitsKeyListener;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar;
import ir.saeedrahimi.poolnegar.customs.Utils;
import ir.saeedrahimi.poolnegar.database.DAO.AccountDAO;
import ir.saeedrahimi.poolnegar.database.DAO.PersonDAO;
import ir.saeedrahimi.poolnegar.database.model.Account;
import ir.saeedrahimi.poolnegar.models.PersonItem;

import static android.R.layout.simple_spinner_dropdown_item;
import static android.R.layout.simple_spinner_item;

public class EditPersonActivity
        extends BaseFragmentActivity implements View.OnClickListener {
    private static final int RESULT_DATE = 0;
    EditText txtInfo;
    EditText txtBalance;
    EditText txtName;
    EditText txtMobile;
    EditText txtPhone;
    EditText txtEmail;
    EditText txtFee;
    EditText txtOvertimeRate;
    EditText txtAccNumber;
    EditText txtCardNumber;
    Switch swLayout;
    Spinner spBalanceType;
    PersonItem mPerson;
    int parentId;
    PreferencesManager mPrefManager;
    boolean isWorker = false;

    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.activity_addworker);
        mToolBar = findViewById(R.id.tool_bar);
        prepareToolbar(mToolBar);
        mPrefManager = new PreferencesManager(this.getApplicationContext());

        this.txtInfo = findViewById(R.id.txt_account_info);
        setToolBarTitle("افزودن شخص جدید", " ");
        findViewById(R.id.layout_select_contact).setOnClickListener(this);
        txtName = findViewById(R.id.txt_name);
        txtMobile = findViewById(R.id.txt_mobile);
        txtPhone = findViewById(R.id.txt_phone);
        txtEmail = findViewById(R.id.txt_email);
        txtBalance = findViewById(R.id.txt_account_balance);
        txtFee = findViewById(R.id.txt_fee);
        txtOvertimeRate = findViewById(R.id.txt_overtime);
        txtAccNumber = findViewById(R.id.txt_account_num);
        txtCardNumber = findViewById(R.id.txt_card_number);
        swLayout = findViewById(R.id.sw_layout);
        parentId = Constants.ACCOUNTS.PERSONS_INT;
        this.txtFee.setKeyListener(DigitsKeyListener.getInstance("1234567890,."));
        this.txtOvertimeRate.setKeyListener(DigitsKeyListener.getInstance("1234567890,."));
        this.txtFee.addTextChangedListener(Utils.MoneyTextWatcher);
        this.txtOvertimeRate.addTextChangedListener(Utils.MoneyTextWatcher);
        swLayout.setOnCheckedChangeListener((compoundButton, b) -> {
            View cvWorker = findViewById(R.id.cv_worker);
            /*if(initialHeight<1) {
                int temp = cvWorker.getHeight();
                cvWorker.getLayoutParams().height= CardView.LayoutParams.WRAP_CONTENT;
                cvWorker.requestLayout();
                initialHeight = cvWorker.getHeight();
                //cvWorker.getLayoutParams().height= temp;
               // cvWorker.requestLayout();
            }*/
            if (!b) {
                YoYo.with(Techniques.SlideOutUp).duration(500).playOn(findViewById(R.id.llfee));
                YoYo.with(Techniques.SlideOutUp).duration(500).playOn(findViewById(R.id.llovertime));
                int newHeight = findViewById(R.id.sw_container).getHeight() + 85;
                collapse(cvWorker, 500, newHeight);
                isWorker = false;
            }
            if (b) {
                YoYo.with(Techniques.SlideInDown).duration(500).playOn(findViewById(R.id.llfee));
                YoYo.with(Techniques.SlideInDown).duration(500).playOn(findViewById(R.id.llovertime));
                //expand(cvWorker,500,initialHeight);
                cvWorker.getLayoutParams().height = CardView.LayoutParams.WRAP_CONTENT;
                cvWorker.requestLayout();
                isWorker = true;
            }
        });
        paramBundle = getIntent().getExtras();
        if (paramBundle != null)
            isWorker = paramBundle.getBoolean("isWorker", false);
        if (isWorker) {
            swLayout.setChecked(true);
        }
        fillSpinner();
        prepareEdit();
    }

    public void prepareEdit() {
        if (getIntent().getIntExtra("PersonID",0) != 0) {
            mPerson =  PersonDAO.getById(getIntent().getStringExtra("PersonID"));
            setToolBarTitle("ویرایش شخص", mPerson.getName());
            txtName.setText(mPerson.getName());
            txtMobile.setText(mPerson.getMobile());
            txtPhone.setText(mPerson.getPhone());
            txtEmail.setText(mPerson.getEmail());
            txtBalance.setText(String.format("%.0f", mPerson.getBalance()));
            if (mPerson.getBalance() > 0)
                spBalanceType.setSelection(0);
            if (mPerson.getBalance() < 0)
                spBalanceType.setSelection(1);
            if (mPerson.isWorker()) {
                swLayout.setChecked(true);
                txtFee.setText(String.format("%.0f", mPerson.getFee()));
                txtOvertimeRate.setText(String.format("%.0f", mPerson.getOvertime()));
            }
            txtAccNumber.setText(mPerson.getAccNumber());
            txtCardNumber.setText(mPerson.getCardNumber());
            txtInfo.setText(mPerson.getInfo());
        }
    }

    public static void collapse(final View v, int duration, int targetHeight) {
        int prevHeight = v.getHeight();
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.addUpdateListener(animation -> {
            v.getLayoutParams().height = (int) animation.getAnimatedValue();
            v.requestLayout();
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(duration);
        valueAnimator.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main_checkcancel, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (item != null && item.getItemId() == android.R.id.home) {
            finish();
        }
        if (item != null && item.getItemId() == R.id.maincancel) {
            finish();
        }
        if (item != null && item.getItemId() == R.id.mainaccept) {
            submitData();
        }
        return false;
    }

    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
        if (paramInt == 4) {
            Intent backIntent = new Intent();
            setResult(RESULT_CANCELED, backIntent);
            finish();
        }
        return super.onKeyDown(paramInt, paramKeyEvent);
    }

    protected void onStart() {
        super.onStart();
    }

    protected void onStop() {
        super.onStop();
    }

    public void submitData() {
        String acInfo = this.txtInfo.getText().toString();
        double balance = 0;
        double fee = 0;
        double overtime = 0;
        if (this.txtFee.getText().toString().matches(""))
            fee = 0.0D;
        else
            fee = Double.parseDouble(this.txtFee.getText().toString().replace(",", ""));

        if (this.txtOvertimeRate.getText().toString().matches(""))
            overtime = 0.0D;
        else
            overtime = Double.parseDouble(this.txtOvertimeRate.getText().toString().replace(",", ""));


        if (this.txtBalance.getText().toString().matches("")) {
            balance = 0.0D;
        } else if (this.spBalanceType.getSelectedItemPosition() == 0) {
            balance = Double.parseDouble(this.txtBalance.getText().toString().replace(",", ""));
        } else {
            balance = -Double.parseDouble(this.txtBalance.getText().toString().replace(",", ""));
        }


        if (txtName.getText().length() < 1) {
            Snackbar.make(mToolBar, "لطفا نام را وارد نمایید!", BaseTransientBottomBar.LENGTH_LONG).setAction("Action", null).show();
            txtName.requestFocus();
            YoYo.with(Techniques.Shake).duration(500).repeat(3).playOn(txtName);
            return;
        }
        if (isWorker && txtFee.getText().length() < 1) {
            Snackbar.make(mToolBar, "لطفا مزد روزانه را وارد نمایید!", BaseTransientBottomBar.LENGTH_LONG).setAction("Action", null).show();
            txtFee.requestFocus();
            YoYo.with(Techniques.Shake).duration(500).repeat(3).playOn(txtFee);
            return;
        }
        if (isWorker && txtOvertimeRate.getText().length() < 1) {
            Snackbar.make(mToolBar, "لطفا نرخ ساعت اضافه کاری را وارد نمایید!", BaseTransientBottomBar.LENGTH_LONG).setAction("Action", null).show();
            txtOvertimeRate.requestFocus();
            YoYo.with(Techniques.Shake).duration(500).repeat(3).playOn(txtOvertimeRate);
            return;
        }
        if (PersonDAO.checkDuplicateName(txtName.getText().toString(),mPerson.getName())) {
            Snackbar.make(mToolBar, "نام شخص تکراری می باشد.", BaseTransientBottomBar.LENGTH_LONG).setAction("Action", null).show();
            txtName.requestFocus();
            YoYo.with(Techniques.Shake).duration(500).repeat(3).playOn(txtName);
            return;
        }

        String billId = "";
        Account relatedAccount = new Account();
        relatedAccount.setAccID(mPerson.getAcID());
        relatedAccount.setTitle(this.txtName.toString());
        relatedAccount.setBalance(balance);
        relatedAccount.setInfo(acInfo);
        relatedAccount.setIsPayable(1);
        relatedAccount.setIsReceivable(1);
        relatedAccount.setProjectDate(new PersianCalendar().getPersianShortDate());
        try {
            if (isWorker) {
                mPerson.setWorker(true);
                if(mPerson.getBillAcId() == ""){
                    relatedAccount.setInfo("ایجاد خودکار بابت ثبت کارکرد نیرو");
                    relatedAccount.setBalance(0);
                    relatedAccount.setParentId(Constants.ACCOUNTS.WorkerBill);
                    relatedAccount.setAccID("");
                    relatedAccount = AccountDAO.insert(relatedAccount);
                    mPerson.setBillAcId(relatedAccount.getAccID());
                }else{
                    relatedAccount.setAccID(mPerson.getBillAcId());
                    relatedAccount.setBalance(0);
                    AccountDAO.update(relatedAccount);
                }

            }else{
                AccountDAO.update(relatedAccount);
            }
            mPerson.setName(this.txtName.getText().toString());
            mPerson.setMobile(txtMobile.getText().toString());
            mPerson.setPhone(txtPhone.getText().toString());
            mPerson.setEmail(txtEmail.getText().toString());
            mPerson.setBalance(balance);
            mPerson.setFee(fee);
            mPerson.setOvertime(overtime);
            mPerson.setAccNumber(txtAccNumber.getText().toString());
            mPerson.setCardNumber(txtCardNumber.getText().toString());
            mPerson.setInfo(acInfo);
            PersonDAO.update(mPerson);

        } catch (Exception e) {
            if (!billId.equals("") && !mPerson.isWorker())
                AccountDAO.deleteById(billId);
            e.printStackTrace();
        }
        Intent backIntent = new Intent();
        backIntent.putExtra("personName", txtName.getText());
        setResult(RESULT_OK, backIntent);
        finish();
    }

    public void fillSpinner() {
        String[] arrayOfString = {"بدهکار", "بستانکار"};
        this.spBalanceType = findViewById(R.id.sp_account_balance_type);
        ArrayAdapter localArrayAdapter = new ArrayAdapter(this, simple_spinner_item, arrayOfString);
        localArrayAdapter.setDropDownViewResource(simple_spinner_dropdown_item);
        this.spBalanceType.setAdapter(localArrayAdapter);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_DATE) {
            if (resultCode == RESULT_OK) {

            }
        }
        if (requestCode == Constants.RESULTS.CONTACT) {
            if (resultCode == RESULT_OK) {
                getContactData(data);

            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void getContactData(Intent data) {
        Uri contactData = data.getData();
        txtMobile.setText("");
        txtPhone.setText("");
        txtName.setText("");
        txtEmail.setText("");
        String number = "";
        Cursor cursor = getContentResolver().query(contactData, null, null, null, null);
        cursor.moveToFirst();
        String name = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));
        txtName.setText(name);

        String hasPhone = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.HAS_PHONE_NUMBER));
        String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
        if (hasPhone.equals("1")) {
            Cursor phones = getContentResolver().query
                    (ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                    + " = " + contactId, null, null);

            while (phones.moveToNext()) {

                number = phones.getString(phones.getColumnIndex
                        (ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("[-() ]", "");
                if (phones.getPosition() == 0)
                    txtMobile.setText(phone_number_correction(number));
                else if (phones.getPosition() == 1)
                    txtPhone.setText(phone_number_correction(number));
            }
            phones.close();
        } else {
            Snackbar.make(mToolBar, "این مخاطب شماره ای ندارد.", BaseTransientBottomBar.LENGTH_LONG).setAction("Action", null).show();
        }

        cursor.close();
        Cursor emailCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{contactId}, null);

        if (emailCursor.moveToFirst()) {
            String phone = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
            int type = emailCursor.getInt(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
            String s = (String) ContactsContract.CommonDataKinds.Email.getTypeLabel(getResources(), type, "");
            txtEmail.setText(phone);
        }
        emailCursor.close();
    }

    private String phone_number_correction(String number) {
        String str = number.replace(" ", "").replace("+98", "0");
        if (str.substring(0, 2).equals("98")) {
            str = "0" + str.substring(2);
        }
        return str;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.layout_select_contact) {
            if (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)) {
                startActivityForResult(new Intent("android.intent.action.PICK", ContactsContract.Contacts.CONTENT_URI), Constants.RESULTS.CONTACT);
            } else {
                requestPermission();
            }
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.

            new AlertDialog.Builder(this)
                    .setMessage(R.string.perm_request_message)
                    .setPositiveButton(R.string.ok, (dialog, which) -> ActivityCompat.requestPermissions(EditPersonActivity.this, new String[]{Manifest.permission.READ_CONTACTS},
                            Constants.PERMISSIONS.READ_CONTACTS)).show();

        } else {
            ActivityCompat.requestPermissions(EditPersonActivity.this, new String[]{Manifest.permission.READ_CONTACTS},
                    Constants.PERMISSIONS.READ_CONTACTS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.PERMISSIONS.READ_CONTACTS: {
                if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivityForResult(new Intent("android.intent.action.PICK", ContactsContract.Contacts.CONTENT_URI), Constants.RESULTS.CONTACT);
                } else {
                    Snackbar.make(mToolBar, "مجوز دسترسی به مخاطبین داده نشد!", Snackbar.LENGTH_SHORT).setAction("Action", view -> requestPermission()).show();
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
                return;
            }
        }
    }
}
