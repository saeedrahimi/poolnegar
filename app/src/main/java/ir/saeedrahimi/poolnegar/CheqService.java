package ir.saeedrahimi.poolnegar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.Calendar;

import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar;
import ir.saeedrahimi.poolnegar.models.CheqReminderItem;

public class CheqService
  extends Service
{
  
  boolean toadayCheq;
  CheqReminderItem remindItem;
  private int cheqId;
  private String remindDate;
  
  private long getSnoozedTime()
  {
    PersianCalendar pc = new PersianCalendar();
    pc.set(Calendar.HOUR_OF_DAY, 18);
    pc.set(Calendar.MINUTE, 58);

    return pc.getTimeInMillis();
  }
  
  public void a(final Context paramContext)
  {
    this.remindItem = new CheqReminderItem();
    final WindowManager winManager = (WindowManager)paramContext.getApplicationContext().getSystemService(WINDOW_SERVICE);
    WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
    localLayoutParams.gravity = 17;
    localLayoutParams.type = 2003;
    localLayoutParams.width = -2;
    localLayoutParams.height = -2;
    localLayoutParams.alpha = 1.0F;
    localLayoutParams.packageName = paramContext.getPackageName();
    localLayoutParams.buttonBrightness = 1.0F;
    localLayoutParams.windowAnimations = 16973826;
    final View localView = View.inflate(paramContext.getApplicationContext(), R.layout.cheq_reminder_dialog, null);
    Button btnPass = localView.findViewById(R.id.reminder_pass);
    Button btnSnooz = localView.findViewById(R.id.reminder_snooz);
    TextView txtDate = localView.findViewById(R.id.cheq_reminder_finaldate);
    TextView txtAmount = localView.findViewById(R.id.cheq_reminder_amount);
    TextView txtPayTo = localView.findViewById(R.id.cheq_reminder_payto);
    TextView txtSerial = localView.findViewById(R.id.cheq_reminder_cheqserial);
    TextView txtBank = localView.findViewById(R.id.cheq_reminder_bankname);
    //this.remindItem = this._db.getCheqReminder(this.cheqId);
    txtDate.setText("تاریخ: " + this.remindDate);
    txtAmount.setText("مبلغ: " + new DecimalFormat(" ###,###.## ").format(this.remindItem.cheqAmount));
    txtSerial.setText("سریال: " + this.remindItem.serial);
    txtPayTo.setText("گیرنده: " + this.remindItem.reciversTitles[2] + "  " + this.remindItem.reciversTitles[1] + "  " + this.remindItem.reciversTitles[0]);
    //txtBank.setText("بانک: " + this._db.getAccountTitle(this.remindItem.bankId));
	
    if (this.remindDate.equals(new PersianCalendar().getPersianShortDate())) {
      btnSnooz.setText("بی خیال");
      this.toadayCheq = true;
    }else{
    	btnSnooz.setText("یاداوری کن");
    	this.toadayCheq = false;
    }
    
      btnPass.setOnClickListener(arg0 -> {
          //_db.PassUnpassCheq(cheqId, remindItem.transId, remindItem.accId, 1, remindDate);
          winManager.removeView(localView);
          stopSelf();

      });
      btnSnooz.setOnClickListener(arg0 -> {
          //localButton2.setOnClickListener(new ek(this, paramContext, localWindowManager, localView));
          if (cheqId != -2)
          {
            if (toadayCheq) {
                CancelAlarm(paramContext, cheqId);
            }
            SnoozAlarm(paramContext, remindDate);
          }
          winManager.removeView(localView);
          stopSelf();
          return;

      });
      winManager.addView(localView, localLayoutParams);
      return;
    
  }
  
  public void CancelAlarm(Context paramContext, int cheqId)
  {
    PendingIntent localPendingIntent = PendingIntent.getBroadcast(paramContext, cheqId, new Intent(paramContext, CheqBrodCastReciver.class), Intent.FILL_IN_DATA);
    ((AlarmManager)paramContext.getSystemService(ALARM_SERVICE)).cancel(localPendingIntent);
    localPendingIntent.cancel();
  }
  
  public void SnoozAlarm(Context paramContext, String date)
  {
    Intent localIntent = new Intent(paramContext, CheqBrodCastReciver.class);
    localIntent.putExtra("cheqUniqueId", this.cheqId);
    localIntent.putExtra("cheqDate", date);
    PendingIntent localPendingIntent = PendingIntent.getBroadcast(paramContext, this.cheqId, localIntent, 0);
    ((AlarmManager)paramContext.getSystemService(ALARM_SERVICE)).set(AlarmManager.RTC_WAKEUP, getSnoozedTime(), localPendingIntent);
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public void onCreate()
  {
    super.onCreate();
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    if (paramIntent != null) {
      if (paramIntent.getExtras() != null)
      {
        this.cheqId = paramIntent.getIntExtra("cheqUniqueId", -2);
        this.remindDate = paramIntent.getStringExtra("cheqDate");
        Log.d("intent Service extra is", this.cheqId+"");
        Log.d("intent Service extra is", this.remindDate+"");
        a(this);
      }
    }
    else
    	stopSelf();
   return super.onStartCommand(paramIntent, paramInt1, paramInt2);
     
  }
}
