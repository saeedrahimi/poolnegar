package ir.saeedrahimi.poolnegar;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import ir.saeedrahimi.poolnegar.application.helper.KeyHelper;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
import ir.saeedrahimi.poolnegar.ui.activities.cu.Activity_Transaction;
import ir.saeedrahimi.poolnegar.ui.activities.MainActivity;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_Password;
import ir.saeedrahimi.poolnegar.ui.fragments.FragmentTransactions;

/**
 * Implementation of App Widget functionality.
 */
public class NewAppWidget extends AppWidgetProvider {

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {


        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);

        Intent passwordIntentIncome = new Intent(context, Activity_Password.class);
        Intent incomeIntent = new Intent(context, Activity_Transaction.class);
        passwordIntentIncome.putExtra("Redirect", incomeIntent);
        incomeIntent.putExtra(KeyHelper.KEY_TRANSACTION_METHOD, TransactionItem.TRANSACTION_METHOD_INCOME);
        incomeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        views.setOnClickPendingIntent(R.id.add_income, PendingIntent.getActivity(context,0,incomeIntent,0));


        Intent passwordIntentOutcome = new Intent(context, Activity_Password.class);
        Intent outcomeIntent = new Intent(context, Activity_Transaction.class);
        outcomeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        outcomeIntent.putExtra(KeyHelper.KEY_TRANSACTION_METHOD, TransactionItem.TRANSACTION_METHOD_PAYMENT);
        passwordIntentOutcome.putExtra("Redirect", outcomeIntent);
        views.setOnClickPendingIntent(R.id.add_outcome, PendingIntent.getActivity(context,0,passwordIntentOutcome,0));


        Intent passwordIntentTransactions = new Intent(context, Activity_Password.class);
        Intent transactionsIntent = new Intent(context, FragmentTransactions.class);
        passwordIntentTransactions.putExtra("Redirect", transactionsIntent);

        views.setOnClickPendingIntent(R.id.show_all, PendingIntent.getActivity(context,0,transactionsIntent,0));

        Intent passwordIntentMain = new Intent(context, Activity_Password.class);
        Intent mainIntent = new Intent("android.intent.action.MAIN");
        mainIntent.addCategory("android.intent.category.LAUNCHER");
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mainIntent.setComponent(new ComponentName(context.getPackageName(), MainActivity.class.getName()));
        passwordIntentOutcome.putExtra("Redirect", mainIntent);
        views.setOnClickPendingIntent(R.id.logo, PendingIntent.getActivity(context,0,mainIntent,0));

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

