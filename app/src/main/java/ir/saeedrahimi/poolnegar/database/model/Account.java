package ir.saeedrahimi.poolnegar.database.model;


import ir.saeedrahimi.poolnegar.Constants;

public class Account extends  BaseModel {
    private String title;
    private String accID;
    private double balance;
    private String info;
    private double totalBalance;
    private double totalIncome;
    private double totalOutcome;
    private String date;
    private int closed = 0;
    private int[] accGroup;
    private int isProject;
    private String parentId;
    private String bankId;
    private String imageId;
    private int isSystematic;
    private int isLeaf;
    private int isLocked;
    private int isPayable;
    private int isReceivable;
    private String path;

    public Account(){

    }

    public Account(String title, String parentId, double balance, String info, boolean isReceivable, boolean isPayable){
        setTitle(title);
        setParentId(parentId);
        setBalance(balance);
        setInfo(info);
        setIsPayable(isPayable ? 1 : 0);
        setIsReceivable(isReceivable ? 1 : 0);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getAccID() {
        return accID;
    }

    public void setAccID(String acID) {
        this.accID = acID;
    }


    public double getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(double totalBalance) {
        this.totalBalance = totalBalance;
    }

    public String getProjectDate() {
        return date;
    }

    public void setProjectDate(String date) {
        this.date = date;
    }

    public double getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(double totalIncome) {
        this.totalIncome = totalIncome;
    }

    public double getTotalOutcome() {
        return totalOutcome;
    }

    public void setTotalOutcome(double totalOutcome) {
        this.totalOutcome = totalOutcome;
    }

    public boolean isClosed() {
        return closed == 1;
    }

    public void setClosed(int closed) {
        this.closed = closed;
    }

    public int[] getAccGroup() {
        return accGroup;
    }

    public void setAccGroup(int[] accGroup) {
        this.accGroup = accGroup;
    }


    public int getIsProject() {
        return isProject;
    }

    public void setIsProject(int isProject) {
        this.isProject = isProject;
    }

    public String getParentId() {
        return this.parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;

        if(parentId == Constants.ACCOUNTS.PROJECTS){
            setIsProject(1);
        }else{
            setIsProject(0);
        }
    }
    public String getBankId() {
        return this.bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public int getIsReceivable() {
        return isReceivable;
    }

    public void setIsReceivable(int isReceivable) {
        this.isReceivable = isReceivable;
    }

    public int getIsPayable() {
        return isPayable;
    }

    public void setIsPayable(int isPayable) {
        this.isPayable = isPayable;
    }

    public int getIsLocked() {
        return isLocked;
    }

    public void setIsLocked(int isLocked) {
        this.isLocked = isLocked;
    }


    public int getIsSystematic() {
        return isSystematic;
    }
    public boolean IsSystematic() {
        return isSystematic == 1;
    }

    public void setIsSystematic(int isSystematic) {
        this.isSystematic = isSystematic;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
