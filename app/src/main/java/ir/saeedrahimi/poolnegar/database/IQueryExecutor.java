package ir.saeedrahimi.poolnegar.database;

import android.database.sqlite.SQLiteDatabase;

public interface IQueryExecutor {

    public Object run(SQLiteDatabase database);
}
