package ir.saeedrahimi.poolnegar.database.model;

import java.lang.annotation.Retention;

import androidx.annotation.IntDef;

import static java.lang.annotation.RetentionPolicy.SOURCE;

public class TransactionLink extends BaseModel
{
  private String linkId;
  private String Title;
  private String projectId ;
  private String projectTitle = "-";
  private String personId ;
  private String personTitle = "";
  private double amount;
  private int method;
  private int linkOrder;
  private boolean IsProjectLink;

  private String targetAccountId;
  private String targetAccountTitle;
  private String sourceAccountId;
  private String sourceAccountTitle;
  private String note;

  public TransactionLink(){
  }
  public String getTitle() {
    return Title;
  }
  public void setTitle(String title) {
    Title = title;
  }


  @TransactionItem.TransactionMethod
  public int getMethod() {
    return this.method;
  }

  public void setMethod(@TransactionItem.TransactionMethod int set) {
    this.method = set;
  }


  public double getAmount()
  {
    return this.amount;
  }
  public void setAmount(double amount)
  {
    this.amount = amount;
  }
  public String getProjectId()
  {
    return this.projectId;
  }
  public void setProjectId(String paramInt)
  {
    this.projectId = paramInt;
  }
  public String getProjectTitle() {
    return projectTitle;
  }
  public void setProjectTitle(String projectTitle) {
    this.projectTitle = projectTitle;
  }
  public String getPersonTitle() {
    return personTitle;
  }
  public void setPersonTitle(String personTitle) {
    this.personTitle = personTitle;
  }
  public String getPersonId() {
    return personId;
  }
  public void setPersonId(String personId) {
    this.personId = personId;
  }
  public String getTargetAccountId() {
    return targetAccountId;
  }
  public void setTargetAccountId(String targetAccountId) {
    this.targetAccountId = targetAccountId;
  }
  public String getTargetAccountTitle() {
    return targetAccountTitle;
  }
  public void setTargetAccountTitle(String targetAccountTitle) {
    this.targetAccountTitle = targetAccountTitle;
  }
  public String getSourceAccountId() {
    return sourceAccountId;
  }
  public void setSourceAccountId(String sourceAccountId) {
    this.sourceAccountId = sourceAccountId;
  }
  public String getSourceAccountTitle() {
    return sourceAccountTitle;
  }
  public void setSourceAccountTitle(String sourceAccountTitle) {
    this.sourceAccountTitle = sourceAccountTitle;
  }
  public String getLinkId() {
    return linkId;
  }
  public void setLinkId(String linkId) {
    this.linkId = linkId;
  }

  public int getLinkOrder() {
    return linkOrder;
  }

  public void setLinkOrder(int linkOrder) {
    this.linkOrder = linkOrder;
  }

  public boolean isProjectLink() {
    return IsProjectLink;
  }

  public void setIsProjectLink(boolean isProjectLink) {
    IsProjectLink = isProjectLink;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  public void clearAccounts(){
    this.setTargetAccountId("");
    this.setSourceAccountId("");
    this.setTargetAccountTitle("");
    this.setSourceAccountTitle("");
  }
}



