
package ir.saeedrahimi.poolnegar.database.model;

import java.io.Serializable;

public class BaseModel
        implements Serializable {

    private long CreatedAt;
    private long UpdatedAt;
    private String Editor;
    private String EditorDeviceId;
    private String Tag;

    public long getCreatedAt() {
        return CreatedAt;
    }

    public void setCreatedAt(long createdAt) {
        CreatedAt = createdAt;
    }

    public long getUpdatedAt() {
        return UpdatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        UpdatedAt = updatedAt;
    }

    public String getEditor() {
        return Editor;
    }

    public void setEditor(String editor) {
        Editor = editor;
    }

    public String getEditorDeviceId() {
        return EditorDeviceId;
    }

    public void setEditorDeviceId(String editorDeviceId) {
        EditorDeviceId = editorDeviceId;
    }

    public String getTag() {
        return Tag;
    }

    public void setTag(String tag) {
        Tag = tag;
    }
}
