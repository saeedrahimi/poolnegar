package ir.saeedrahimi.poolnegar.database.DAO;

/**
 * Created by Saeed on 3/19/2017.
 */

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.Gateways.WorkDayDAO;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.MainApplication;
import ir.saeedrahimi.poolnegar.application.helper.L;
import ir.saeedrahimi.poolnegar.application.persian.calendar.PersianCalendar;
import ir.saeedrahimi.poolnegar.application.persian.calendar.util.PersianCalendarUtils;
import ir.saeedrahimi.poolnegar.customs.Utils;
import ir.saeedrahimi.poolnegar.database.AppDatabaseManager;
import ir.saeedrahimi.poolnegar.database.model.Account;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;
import ir.saeedrahimi.poolnegar.models.PersonItem;
import ir.saeedrahimi.poolnegar.models.WorkDayItem;

public class PersonDAO {

    public static String getCreateTable() {
        return MainApplication.getAppContext().getString(R.string.sql_Person_CreateTable);
    }

    public static String getDropTable() {
        return MainApplication.getAppContext().getString(R.string.sql_Person_DropTable);
    }
    public static void create() {
        AppDatabaseManager.getInstance().executeQuery(database -> {
            try {

                String createCommand = MainApplication.getAppContext().getString(R.string.sql_Person_CreateTable);
                database.execSQL(createCommand);
                return true;
            } catch (Exception e) {

                String msg = "PersonDAO - create : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });

    }
    public static void deleteById(final String id) {
        Boolean result;
        result = (Boolean) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        id
                };

                String deleteCommand = MainApplication.getAppContext().getString(R.string.sql_Person_DeleteById);
                database.execSQL(deleteCommand, bindArgs);
                return true;
            } catch (Exception e) {

                String msg = "PersonDAO - deleteById : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });

    }

    public static List<PersonItem> getAll() {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String query = MainApplication.getAppContext().getString(R.string.sql_Person_SelectAll);

                Cursor cursor = database.rawQuery(query, null);

                List<PersonItem> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "PersonDAO - selectAll : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return (List<PersonItem>) result;
    }


    public static PersonItem getById(String id) {
        List<PersonItem> list = getAllWhere("PersonId = " + id);
        if (list.size() > 0)
            return list.get(0);
        return null;
    }

    public static PersonItem getByAccountID(String id) {
        List<PersonItem> list = getAllWhere("AccountId = " + id);
        if (list.size() > 0)
            return list.get(0);
        return null;
    }


    public static boolean checkDuplicateName(final String name, final String exclude) {
        boolean result;
        String queruResult = (String) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                int existResult = 0;
                String[] bindArgs = {
                        name, exclude
                };

                String query = MainApplication.getAppContext().getString(R.string.sql_Person_Duplicate);

                Cursor cursor = database.rawQuery(query, bindArgs);

                if (cursor != null && cursor.moveToFirst()) {
                    existResult = cursor.getInt(0);
                }
                closeCursor(cursor);

                return String.valueOf(existResult);
            } catch (Exception e) {

                String msg = "AccountDAO - hasChild : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });
        result = queruResult == "1";
        return result;
    }

    public static List<PersonItem> getAllWhere(String where) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String query = MainApplication.getAppContext().getString(R.string.sql_Person_SelectAll);
                String orderPart = MainApplication.getAppContext().getString(R.string.sql_Person_Order_Part);
                query += where + orderPart;
                Cursor cursor = database.rawQuery(query, null);

                List<PersonItem> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "PersonDAO - getAllWhere : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return (List<PersonItem>) result;
    }

    public static PersonItem insert(final PersonItem person) {

        PersonItem result;
        result = (PersonItem) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        String.valueOf(person.getId()),
                        String.valueOf(person.getAcID()),
                        String.valueOf(person.getBillAcId()),
                        String.valueOf(person.getName()),
                        String.valueOf(person.getInfo()),
                        String.valueOf(person.getMobile()),
                        String.valueOf(person.getPhone()),
                        String.valueOf(person.getEmail()),
                        String.valueOf(person.getAccNumber()),
                        String.valueOf(person.getCardNumber()),
                        String.valueOf(person.getImage()),
                        String.valueOf(person.isWorker() ? 1 : 0),
                        String.valueOf(person.getFee()),
                        String.valueOf(person.getOvertime()),
                        String.valueOf(person.getBalance()),
                        String.valueOf(person.getOrder()),
                        String.valueOf(0),
                        String.valueOf(person.getCreatedAt()),
                        String.valueOf(person.getUpdatedAt()),
                        String.valueOf(person.getEditor()),
                        String.valueOf(person.getEditorDeviceId()),
                        String.valueOf(person.getTag()),
                        String.valueOf(""),
                };
                database.execSQL(MainApplication.getAppContext().getString(R.string.sql_Person_Insert), bindArgs);
                return person;
            } catch (Exception e) {

                String msg = "PersonDAO - insert : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return result;
    }

    public static boolean update(final PersonItem person) {

        Boolean result;
        result = (Boolean) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        String.valueOf(person.getAcID()),
                        String.valueOf(person.getBillAcId()),
                        String.valueOf(person.getName()),
                        String.valueOf(person.getInfo()),
                        String.valueOf(person.getMobile()),
                        String.valueOf(person.getPhone()),
                        String.valueOf(person.getEmail()),
                        String.valueOf(person.getAccNumber()),
                        String.valueOf(person.getCardNumber()),
                        String.valueOf(person.getImage()),
                        String.valueOf(person.isWorker() ? 1 : 0),
                        String.valueOf(person.getFee()),
                        String.valueOf(person.getOvertime()),
                        String.valueOf(person.getBalance()),
                        String.valueOf(person.getOrder()),
                        String.valueOf(0),
                        String.valueOf(person.getCreatedAt()),
                        String.valueOf(person.getUpdatedAt()),
                        String.valueOf(person.getEditor()),
                        String.valueOf(person.getEditorDeviceId()),
                        String.valueOf(person.getTag()),
                        String.valueOf(""),
                        String.valueOf(person.getId()),
                };
                database.execSQL(MainApplication.getAppContext().getString(R.string.sql_Person_Update), bindArgs);
                return true;
            } catch (Exception e) {

                String msg = "PersonDAO - update : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return result;
    }

    public static double getPaysAmount(String acID) {
        PersonItem person = getByAccountID(acID);
        double amount = TransactionDAO.getAccountBalance(person.getBillAcId());

        return amount;
    }

    public static double getTotalWorkedAmount(String id) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        String.valueOf(id)
                };

                String query = MainApplication.getAppContext().getString(R.string.sql_Person_WorkDay_TotalWork);
                Cursor cursor = database.rawQuery(query, bindArgs);

                List<PersonItem> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "PersonDAO - getTotalWorkedAmount : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return (double) result;

    }


    public static boolean setWorkerPresent(PersonItem person, String date) {
        PersianCalendar pc = new PersianCalendar();
        pc.setPersianDate(date);
        WorkDayItem day = WorkDayDAO.getByDate(person.getId(), date);

        if (day != null && PersianCalendarUtils.IsEqualPersianDates(date, day.getWorkFaDate()))
            return false;
        WorkDayItem wdItem = new WorkDayItem();
        wdItem.setPersonId(person.getId());
        wdItem.setWorkFaDate(date);
        wdItem.setRegDate(new PersianCalendar().getPersianShortDate());
        wdItem.setRegTime(Utils.getTime());
        try {

            Account personAccount = AccountDAO.selectByID(person.getAcID());
            Account billAccount = AccountDAO.selectByID(person.getBillAcId());
            if ( billAccount == null){

                Account autoAccount = new Account();
                autoAccount.setTitle(person.getName());
                autoAccount.setInfo( "ایجاد خودکار بابت ثبت کارکرد نیرو");
                autoAccount.setBalance(0);
                autoAccount.setIsPayable(1);
                autoAccount.setIsReceivable(1);
                autoAccount.setParentId(Constants.ACCOUNTS.WorkerBill);
                autoAccount.setProjectDate(date);
                billAccount = AccountDAO.insert(autoAccount);
            }

            TransactionItem transaction = new TransactionItem();
            transaction.setSerial(0);
            transaction.setMethod(1);
            transaction.setSourceAccountId(person.getBillAcId());
            transaction.setSourceAccountTitle(billAccount.getTitle());
            assert personAccount != null;
            transaction.setTargetAccountId(personAccount.getAccID());
            transaction.setTargetAccountTitle(personAccount.getTitle());
            transaction.setAmount(person.getFee());
            transaction.setRegDateFa(date);
            transaction.setRegDateGe(pc.getTimeInMillis());
            transaction.setNote("کارکرد روزمزد");
            transaction.setTAG(Constants.DB_TAGS.WorkDay);

            transaction = TransactionDAO.insert(transaction);

            wdItem.setTransactionID(transaction.getTransactionId());
            wdItem = WorkDayDAO.insert(wdItem);

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        if (wdItem.getId() != "")
            return true;
        return false;
    }

    public static boolean setWorkerAbsent(PersonItem worker, String date) {
        WorkDayItem day = WorkDayDAO.getByDate(worker.getId(), date);
        if (day != null && PersianCalendarUtils.IsEqualPersianDates(date, day.getWorkFaDate())) {
            TransactionDAO.deleteById(day.getTransactionID());

            WorkDayDAO.deleteById(day.getId());
            return true;
        }
        return false;


    }

    public static boolean increaseOverTime(PersonItem worker, String date) {
        WorkDayItem day = WorkDayDAO.getByDate(worker.getId(), date);


        if (day != null && PersianCalendarUtils.IsEqualPersianDates(date, day.getWorkFaDate())) {
            if (day.getLackHour() > 0) {
                day.setLackHour(day.getLackHour() - 1);
            } else {
                day.incOvertime();
            }

            day.setRegDate(new PersianCalendar().getPersianShortDate());
            day.setRegTime(Utils.getTime());
            try {
                String extraInfo = "اضافه کاری: " + (int) day.getOvertime() + " ساعت";
                if (day.getLackHour() > 0)
                    extraInfo = "کسر ساعت کاری: " + (int) day.getLackHour() + " ساعت";

                TransactionItem transaction = TransactionDAO.selectByID(day.getTransactionID());
                assert transaction != null;
                transaction.setAmount(calculateOvertimePrice(worker, day));
                transaction.setRegDateFa(day.getWorkFaDate());
                transaction.setNote(  "کارکرد روزمزد"+ "\n" + extraInfo);
                TransactionDAO.update(transaction);

                WorkDayDAO.update(day);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    public static double calculateOvertimePrice(PersonItem person, WorkDayItem day) {
        double price;
        price = person.getFee() + (person.getOvertime() * day.getOvertime());
        if (day.getLackHour() > 0) {
            price = Math.round(person.getFee() - ((person.getFee() / 9) * day.getLackHour()));
        }
        return price;
    }

    public static boolean decreaseOverTime(PersonItem worker, String date) {
        WorkDayItem day = WorkDayDAO.getByDate(worker.getId(), date);

        if (day != null && PersianCalendarUtils.IsEqualPersianDates(date, day.getWorkFaDate())) {
            if (day.getOvertime() == 0) {
                day.setLackHour(day.getLackHour() + 1);
                if (day.getLackHour() == worker.getHour()) {
                    setWorkerAbsent(worker, date);
                    return true;
                }
            } else {
                day.decOvertime();
            }
            day.setRegDate(new PersianCalendar().getPersianShortDate());
            day.setRegTime(Utils.getTime());
            try {
                String extraInfo = "اضافه کاری: " + (int) day.getOvertime() + " ساعت";
                if (day.getLackHour() > 0)
                    extraInfo = "کسر ساعت کاری: " + (int) day.getLackHour() + " ساعت";

                TransactionItem transaction = TransactionDAO.selectByID(day.getTransactionID());
                assert transaction != null;
                transaction.setAmount(calculateOvertimePrice(worker, day));
                transaction.setRegDateFa(day.getWorkFaDate());
                transaction.setNote(  "کارکرد روزمزد"+ "\n" + extraInfo);
                TransactionDAO.update(transaction);

                WorkDayDAO.update(day);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    public static boolean setProject(PersonItem worker, String date, String projectId) {

        WorkDayItem day = WorkDayDAO.getByDate(worker.getId(), date);

        if (day != null && PersianCalendarUtils.IsEqualPersianDates(date, day.getWorkFaDate())) {
            day.setProjectID(projectId);
            day.setRegDate(new PersianCalendar().getPersianShortDate());
            day.setRegTime(Utils.getTime());
            try {
                WorkDayDAO.update(day);
                TransactionItem transaction = TransactionDAO.selectByID(day.getTransactionID());
                assert transaction != null;
                transaction.setProjectId(projectId);
                TransactionDAO.update(transaction);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }





    private static void closeCursor(Cursor cursor) {
        try {

            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {

            String msg = "PersonDAO - closeCursor : " + e.getMessage();
            L.e(msg);
            throw new RuntimeException(msg);
        }
    }

    private static PersonItem cursorToData(Cursor cursor) {
        try {

            int idIndex = cursor.getColumnIndex(Table.COLUMN_PersonID);
            int accountIndex = cursor.getColumnIndex(Table.COLUMN_AccountId);
            int nameIndex = cursor.getColumnIndex(Table.COLUMN_Name);
            int mobileIndex = cursor.getColumnIndex(Table.COLUMN_Mobile);
            int phoneIndex = cursor.getColumnIndex(Table.COLUMN_Phone);
            int emailIndex = cursor.getColumnIndex(Table.COLUMN_Email);
            int isWorkerIndex = cursor.getColumnIndex(Table.COLUMN_IsWorker);
            int feeIndex = cursor.getColumnIndex(Table.COLUMN_Fee);
            int overtimeIndex = cursor.getColumnIndex(Table.COLUMN_OverTimeFee);
            int billAccIndex = cursor.getColumnIndex(Table.COLUMN_BillAccountId);
            int balanceIndex = cursor.getColumnIndex(Table.COLUMN_Balance);
            int infoIndex = cursor.getColumnIndex(Table.COLUMN_Info);
            int orderIndex = cursor.getColumnIndex(Table.COLUMN_Order);
            int depositIndex = cursor.getColumnIndex(Table.COLUMN_DepositNumber);
            int cardIndex = cursor.getColumnIndex(Table.COLUMN_CardNumber);
            int imageIndex = cursor.getColumnIndex(Table.COLUMN_Image);
            int createdIndex = cursor.getColumnIndex(Table.COLUMN_CREATEDAT);
            int updatedIndex = cursor.getColumnIndex(Table.COLUMN_UPDATEDAT);


            PersonItem item = new PersonItem();
            item.setId(cursor.getString(idIndex));
            item.setAcID(cursor.getString(accountIndex));
            item.setName(cursor.getString(nameIndex));
            item.setMobile(cursor.getString(mobileIndex));
            item.setPhone(cursor.getString(phoneIndex));
            item.setEmail(cursor.getString(emailIndex));
            item.setWorker(cursor.getInt(isWorkerIndex) == 1);
            item.setAccNumber(cursor.getString(depositIndex));
            item.setCardNumber(cursor.getString(cardIndex));
            item.setImage(cursor.getBlob(imageIndex));
            item.setFee(cursor.getInt(feeIndex));
            item.setOvertime(cursor.getInt(overtimeIndex));
            item.setBillAcId(cursor.getString(billAccIndex));
            item.setBalance(cursor.getInt(balanceIndex));
            item.setInfo(cursor.getString(infoIndex));
            item.setOrder(cursor.getInt(orderIndex));
            item.setCreatedAt(cursor.getLong(createdIndex));
            item.setUpdatedAt(cursor.getLong(updatedIndex));

            return item;
        } catch (Exception e) {

            String msg = "PersonDAO - cursorToMasterData : " + e.getMessage();
            L.e(msg);
            throw new RuntimeException(msg);
        }
    }

    private static List<PersonItem> manageCursor(Cursor cursor) {
        try {
            List<PersonItem> dataList = new ArrayList<>();

            if (cursor != null) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    PersonItem account = cursorToData(cursor);
                    dataList.add(account);
                    cursor.moveToNext();
                }
            }
            return dataList;
        } catch (Exception e) {

            String msg = "PersonDAO - manageCursor : " + e.getMessage();
            L.e(msg);
            throw new RuntimeException(msg);
        }
    }

    public interface Table {

        String COLUMN_PersonID = "PersonId";
        String COLUMN_AccountId = "AccountId";
        String COLUMN_BillAccountId = "BillAccountId";
        String COLUMN_Name = "PersonName";
        String COLUMN_Mobile = "Mobile";
        String COLUMN_Phone = "Phone";
        String COLUMN_Email = "Email";
        String COLUMN_DepositNumber = "DepositNumber";
        String COLUMN_CardNumber = "CardNumber";
        String COLUMN_Image = "Image";
        String COLUMN_IsWorker = "IsWorker";
        String COLUMN_Fee = "Fee";
        String COLUMN_OverTimeFee = "OverTimeFee";
        String COLUMN_Balance = "Balance";
        String COLUMN_Info = "Info";
        String COLUMN_Order = "PersonOrder";


        String COLUMN_CREATEDAT = "CreatedAt";
        String COLUMN_UPDATEDAT = "UpdatedAt";


    }
}