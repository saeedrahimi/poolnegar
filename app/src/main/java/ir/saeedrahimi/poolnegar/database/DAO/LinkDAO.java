package ir.saeedrahimi.poolnegar.database.DAO;

/**
 * Created by Saeed on 3/19/2017.
 */

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.MainApplication;
import ir.saeedrahimi.poolnegar.application.helper.L;
import ir.saeedrahimi.poolnegar.database.AppDatabaseManager;
import ir.saeedrahimi.poolnegar.database.model.TransactionLink;

public class LinkDAO {

    public static String getCreateTable() {
        return MainApplication.getAppContext().getString(R.string.sql_Link_CreateTable);
    }

    public static String getDropTable() {
        return MainApplication.getAppContext().getString(R.string.sql_Link_DropTable);
    }

    public static void create() {
        AppDatabaseManager.getInstance().executeQuery(database -> {
            try {

                String createCommand = MainApplication.getAppContext().getString(R.string.sql_Link_CreateTable);
                database.execSQL(createCommand);
                return true;
            } catch (Exception e) {

                String msg = "LinkDAO - create : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
    }

    public static void deleteById(final String id) {
        Boolean result;
        result = (Boolean) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        id
                };

                String deleteAllCommand = MainApplication.getAppContext().getString(R.string.sql_Link_DeleteById);
                database.execSQL(deleteAllCommand, bindArgs);
                return true;
            } catch (Exception e) {

                String msg = "LinkDAO - deleteById : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
    }

    public static void deleteByProjectId(final String id) {
        Boolean result;
        result = (Boolean) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        id
                };

                String deleteAllCommand = MainApplication.getAppContext().getString(R.string.sql_Link_DeleteBy_ProjectId);
                database.execSQL(deleteAllCommand, bindArgs);
                return true;
            } catch (Exception e) {

                String msg = "LinkDAO - deleteByProjectId : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
    }

    public static List<TransactionLink> getAll() {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String query = MainApplication.getAppContext().getString(R.string.sql_Link_SelectAll);

                Cursor cursor = database.rawQuery(query, null);

                List<TransactionLink> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "LinkDAO - selectAll : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });

        return (List<TransactionLink>) result;
    }

    public static List<TransactionLink> getProjectLinks() {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String query = MainApplication.getAppContext().getString(R.string.sql_Link_SelectAll);
                query += " AND " + Table.COLUMN_IS_PROJECT_LINK + " = 1 ";
                String orderPart = MainApplication.getAppContext().getString(R.string.sql_Link_Select_Order_Part);
                query += orderPart;
                Cursor cursor = database.rawQuery(query, null);

                List<TransactionLink> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "LinkDAO - getProjectLinks : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });

        return (List<TransactionLink>) result;
    }
    public static List<TransactionLink> getAll(final int method) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {

                String whereClause = "";

                String query = MainApplication.getAppContext().getString(R.string.sql_Link_SelectAll);
                if(method == 2){
                    whereClause += " AND (" + Table.COLUMN_METHOD + " = 1";
                    whereClause += " OR " + Table.COLUMN_METHOD + " = 0)";
                } else{

                    whereClause += " AND " + Table.COLUMN_METHOD + " = " + method;
                }
                String orderPart = MainApplication.getAppContext().getString(R.string.sql_Link_Select_Order_Part);
                query += whereClause + " " + orderPart;

                Cursor cursor = database.rawQuery(query, null);

                List<TransactionLink> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "LinkDAO - getAllByDate : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });

        return (List<TransactionLink>) result;
    }


    public static List<TransactionLink> getAllByProject(final String projectId) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {

                String query = MainApplication.getAppContext().getString(R.string.sql_Link_SelectAll);

                if (projectId != "") {
                    query += " AND ProjectId = '" + projectId + "' ";
                }

                query += " " + MainApplication.getAppContext().getString(R.string.sql_Link_Select_Order_Part);

                Cursor cursor = database.rawQuery(query, null);

                List<TransactionLink> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "LinkDAO - getAllByProject : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });

        return (List<TransactionLink>) result;
    }



    public static TransactionLink selectByID(final String Id) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        String.valueOf(Id)};

                String query = MainApplication.getAppContext().getString(R.string.sql_Link_SelectByID);

                Cursor cursor = database.rawQuery(query, bindArgs);

                List<TransactionLink> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return (dataList.size() > 0) ? dataList.get(0) : null;
            } catch (Exception e) {

                String msg = "LinkDAO - selectByID : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });

        return (result == null) ? null : (TransactionLink) result;
    }

    public static TransactionLink insert(final TransactionLink link) {

        TransactionLink result;
        result = (TransactionLink) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                link.setLinkId(UUID.randomUUID().toString());
                String[] bindArgs = {
                        String.valueOf(link.getLinkId()),
                        String.valueOf(link.getTitle()),
                        String.valueOf(link.getMethod()),
                        String.valueOf(link.getAmount()),
                        String.valueOf(link.getNote()),
                        String.valueOf(link.getTargetAccountId()),
                        String.valueOf(link.getTargetAccountTitle()),
                        String.valueOf(link.getSourceAccountId()),
                        String.valueOf(link.getSourceAccountTitle()),
                        String.valueOf(link.getProjectId()),
                        String.valueOf(link.getProjectTitle()),
                        String.valueOf(link.isProjectLink() ? 1 : 0),
                        String.valueOf(link.getPersonId()),
                        String.valueOf(link.getPersonTitle()),
                        String.valueOf(link.getLinkOrder()),
                        "0",
                        String.valueOf(link.getCreatedAt()),
                        String.valueOf(link.getUpdatedAt()),
                        String.valueOf(link.getEditor()),
                        String.valueOf(link.getEditorDeviceId()),
                        "",
                        ""
                };
                database.execSQL(MainApplication.getAppContext().getString(R.string.sql_Link_Insert), bindArgs);
                return link;
            } catch (Exception e) {

                String msg = "LinkDAO - insert : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return result;
    }

    public static boolean update(final TransactionLink link) {
        Boolean result;
        result = (Boolean) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        String.valueOf(link.getTitle()),
                        String.valueOf(link.getMethod()),
                        String.valueOf(link.getAmount()),
                        String.valueOf(link.getNote()),
                        String.valueOf(link.getTargetAccountId()),
                        String.valueOf(link.getTargetAccountTitle()),
                        String.valueOf(link.getSourceAccountId()),
                        String.valueOf(link.getSourceAccountTitle()),
                        String.valueOf(link.getProjectId()),
                        String.valueOf(link.getProjectTitle()),
                        String.valueOf(link.isProjectLink() ? 1 : 0),
                        String.valueOf(link.getPersonId()),
                        String.valueOf(link.getPersonTitle()),
                        String.valueOf(link.getLinkOrder()),
                        "0",
                        String.valueOf(link.getCreatedAt()),
                        String.valueOf(link.getUpdatedAt()),
                        String.valueOf(link.getEditor()),
                        String.valueOf(link.getEditorDeviceId()),
                        "",
                        "",
                        String.valueOf(link.getLinkId()),
                };
                database.execSQL(MainApplication.getAppContext().getString(R.string.sql_Link_Update), bindArgs);
                return true;
            } catch (Exception e) {

                String msg = "LinkDAO - update : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return result;
    }

    private static void closeCursor(Cursor cursor) {
        try {


            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {

            String msg = "LinkDAO - closeCursor : " + e.getMessage();
            L.e(msg);
            throw new RuntimeException(msg);
        }
    }

    private static TransactionLink cursorToData(Cursor cursor) {
        try {

            int idIndex = cursor.getColumnIndex(Table.COLUMN_ID);
            int titleIndex = cursor.getColumnIndex(Table.COLUMN_TITLE);
            int methodIndex = cursor.getColumnIndex(Table.COLUMN_METHOD);
            int amountIndex = cursor.getColumnIndex(Table.COLUMN_AMOUNT);
            int noteIndex = cursor.getColumnIndex(Table.COLUMN_NOTE);

            int targetAccIndex = cursor.getColumnIndex(Table.COLUMN_TARGET_ACCOUNT_ID);
            int targetTitleIndex = cursor.getColumnIndex(Table.COLUMN_TARGET_ACCOUNT_TITLE);

            int sourceAccIndex = cursor.getColumnIndex(Table.COLUMN_SOURCE_ACCOUNT_ID);
            int sourceTitleIndex = cursor.getColumnIndex(Table.COLUMN_SOURCE_ACCOUNT_TITLE);

            int projectIndex = cursor.getColumnIndex(Table.COLUMN_PROJECT_ID);
            int projectNameIndex = cursor.getColumnIndex(Table.COLUMN_PROJECT_NAME);
            int isProjectIndex = cursor.getColumnIndex(Table.COLUMN_IS_PROJECT_LINK);

            int personIdIndex = cursor.getColumnIndex(Table.COLUMN_PERSON_ID);
            int personNameIndex = cursor.getColumnIndex(Table.COLUMN_PERSON_NAME);

            int linkOrderIndex = cursor.getColumnIndex(Table.COLUMN_ORDER);

            int createdAtIndex = cursor.getColumnIndex(Table.COLUMN_CREATEDAT);
            int updatedAtIndex = cursor.getColumnIndex(Table.COLUMN_UPDATEDAT);
            int editorIndex = cursor.getColumnIndex(Table.COLUMN_EDITOR);


            TransactionLink item = new TransactionLink();

            item.setLinkId(cursor.getString(idIndex));
            item.setTitle(cursor.getString(titleIndex));
            item.setMethod(cursor.getInt(methodIndex));
            item.setAmount(cursor.getDouble(amountIndex));
            item.setNote(cursor.getString(noteIndex));

            item.setTargetAccountId(cursor.getString(targetAccIndex));
            item.setTargetAccountTitle(cursor.getString(targetTitleIndex));
            item.setSourceAccountId(cursor.getString(sourceAccIndex));
            item.setSourceAccountTitle(cursor.getString(sourceTitleIndex));


            item.setProjectId(cursor.getString(projectIndex));
            item.setProjectTitle(cursor.getString(projectNameIndex));
            item.setIsProjectLink(cursor.getInt(isProjectIndex) == 1);

            item.setPersonId(cursor.getString(personIdIndex));
            item.setPersonTitle(cursor.getString(personNameIndex));

            item.setLinkOrder(cursor.getInt(linkOrderIndex));

            item.setCreatedAt(cursor.getLong(createdAtIndex));
            item.setUpdatedAt(cursor.getLong(updatedAtIndex));
            item.setEditor(cursor.getString(editorIndex));


            return item;
        } catch (Exception e) {

            String msg = "LinkDAO - cursorToMasterData : " + e.getMessage();
            L.e(msg);
            throw new RuntimeException(msg);
        }
    }

    private static List<TransactionLink> manageCursor(Cursor cursor) {
        try {
            List<TransactionLink> dataList = new ArrayList<>();

            if (cursor != null) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    TransactionLink link = cursorToData(cursor);
                    dataList.add(link);
                    cursor.moveToNext();
                }
            }
            return dataList;
        } catch (Exception e) {

            String msg = "LinkDAO - manageCursor : " + e.getMessage();
            L.e(msg);
            throw new RuntimeException(msg);
        }
    }

    public interface Table {

        String COLUMN_ID = "LinkId";
        String COLUMN_TITLE = "Title";
        String COLUMN_METHOD = "Method";
        String COLUMN_AMOUNT = "Amount";
        String COLUMN_NOTE = "Note";

        String COLUMN_TARGET_ACCOUNT_ID = "TargetAccountId";
        String COLUMN_TARGET_ACCOUNT_TITLE = "TargetAccountTitle";

        String COLUMN_SOURCE_ACCOUNT_ID = "SourceAccountId";
        String COLUMN_SOURCE_ACCOUNT_TITLE = "SourceAccountTitle";

        String COLUMN_PROJECT_ID = "ProjectId";
        String COLUMN_PROJECT_NAME = "ProjectTitle";
        String COLUMN_IS_PROJECT_LINK = "IsProjectLink";

        String COLUMN_PERSON_ID = "PersonId";
        String COLUMN_PERSON_NAME = "PersonName";

        String COLUMN_ORDER = "LinkOrder";

        String COLUMN_CREATEDAT = "CreatedAt";
        String COLUMN_UPDATEDAT = "UpdatedAt";
        String COLUMN_EDITOR = "Editor";


    }

}