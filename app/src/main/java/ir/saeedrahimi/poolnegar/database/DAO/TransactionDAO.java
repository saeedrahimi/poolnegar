package ir.saeedrahimi.poolnegar.database.DAO;

/**
 * Created by Saeed on 3/19/2017.
 */

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.MainApplication;
import ir.saeedrahimi.poolnegar.application.helper.L;
import ir.saeedrahimi.poolnegar.database.AppDatabaseManager;
import ir.saeedrahimi.poolnegar.database.model.TransactionItem;

public class TransactionDAO {

    public static String getCreateTable() {
        return MainApplication.getAppContext().getString(R.string.sql_Transaction_CreateTable);
    }

    public static String getDropTable() {
        return MainApplication.getAppContext().getString(R.string.sql_Transaction_DropTable);
    }

    public static void create() {
        AppDatabaseManager.getInstance().executeQuery(database -> {
            try {

                String createCommand = MainApplication.getAppContext().getString(R.string.sql_Transaction_CreateTable);
                database.execSQL(createCommand);
                return true;
            } catch (Exception e) {

                String msg = "TransactionItemDAO - create : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
    }

    public static void deleteById(final String id) {
        Boolean result;
        result = (Boolean) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        id
                };

                String deleteAllCommand = MainApplication.getAppContext().getString(R.string.sql_Transaction_DeleteById);
                database.execSQL(deleteAllCommand, bindArgs);
                return true;
            } catch (Exception e) {

                String msg = "TransactionDAO - deleteById : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
    }

    public static void deleteByProjectId(final String id) {
        Boolean result;
        result = (Boolean) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        id
                };

                String deleteAllCommand = MainApplication.getAppContext().getString(R.string.sql_Transaction_DeleteBy_ProjectId);
                database.execSQL(deleteAllCommand, bindArgs);
                return true;
            } catch (Exception e) {

                String msg = "TransactionDAO - deleteByProjectId : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
    }

    public static List<TransactionItem> getAll() {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String query = MainApplication.getAppContext().getString(R.string.sql_Transaction_SelectAll);

                Cursor cursor = database.rawQuery(query, null);

                List<TransactionItem> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "TransactionDAO - selectAll : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });

        return (List<TransactionItem>) result;
    }

    public static List<TransactionItem> getAll(final int method, final String startDate, final String endDate) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {

                String whereClause = "";
                String parsedStartDate = startDate;
                String parsedEndDate = endDate;
                if (parsedStartDate.equals("")) {
                    parsedStartDate = "0000/00/00";
                }
                if (parsedEndDate.equals("")) {
                    parsedEndDate = "9999/99/99";
                }
                String query = MainApplication.getAppContext().getString(R.string.sql_Transaction_SelectAll);
                if(method == 2){
                    whereClause += " AND (" + Table.COLUMN_METHOD + " = 1";
                    whereClause += " OR " + Table.COLUMN_METHOD + " = 0)";
                } else{

                    whereClause += " AND " + Table.COLUMN_METHOD + " = " + method;
                }
                whereClause += " AND (" + Table.COLUMN_REG_FA_DATE + " >= '" + parsedStartDate
                        + "' AND " + Table.COLUMN_REG_FA_DATE + " <= '" + parsedEndDate + "')";
                String orderPart = MainApplication.getAppContext().getString(R.string.sql_Transaction_Select_Order_Part);
                query += whereClause + " " + orderPart;

                Cursor cursor = database.rawQuery(query, null);

                List<TransactionItem> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "TransactionDAO - getAllByDate : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });

        return (List<TransactionItem>) result;
    }

    public static List<TransactionItem> getAllByAccount(final String accountId, final int method, final String startDate, final String endDate) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {

                String whereClause;
                String parsedStartDate = startDate;
                String parsedEndDate = endDate;
                if (parsedStartDate.equals("")) {
                    parsedStartDate = "0000/00/00";
                }
                if (parsedEndDate.equals("")) {
                    parsedEndDate = "9999/99/99";
                }
                String query = MainApplication.getAppContext().getString(R.string.sql_Transaction_SelectAll);

                query += " AND " + Table.COLUMN_METHOD + " = " + method;
                query += " AND (" + Table.COLUMN_REG_FA_DATE + " >= '" + parsedStartDate
                        + "' AND " + Table.COLUMN_REG_FA_DATE + " <= '" + parsedEndDate + "')";
                if (accountId != "") {
                    query += " AND ProjectId = '" + accountId + "' ";
                }

                query += MainApplication.getAppContext().getString(R.string.sql_Transaction_Select_Order_Part);

                Cursor cursor = database.rawQuery(query, null);

                List<TransactionItem> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "TransactionDAO - getAllByProject : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });

        return (List<TransactionItem>) result;
    }

    public static List<TransactionItem> getAllByProject(final String projectId, final int method, final String startDate, final String endDate) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {

                String whereClause;
                String parsedStartDate = startDate;
                String parsedEndDate = endDate;
                if (parsedStartDate.equals("")) {
                    parsedStartDate = "۰۰۰۰/۰۰/۰۰";
                }
                if (parsedEndDate.equals("")) {
                    parsedEndDate = "۹۹۹۹/۹۹/۹۹";
                }
                String query = MainApplication.getAppContext().getString(R.string.sql_Transaction_SelectAll);

                query += " AND " + Table.COLUMN_METHOD + " = " + method;
                query += " AND (" + Table.COLUMN_REG_FA_DATE + " >= '" + parsedStartDate
                        + "' AND " + Table.COLUMN_REG_FA_DATE + " <= '" + parsedEndDate + "')";
                if (projectId != "") {
                    query += " AND ProjectId = '" + projectId + "' ";
                }

                query += " " + MainApplication.getAppContext().getString(R.string.sql_Transaction_Select_Order_Part);

                Cursor cursor = database.rawQuery(query, null);

                List<TransactionItem> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "TransactionDAO - getAllByProject : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });

        return (List<TransactionItem>) result;
    }

    public static boolean HasAccountTransaction(final String accountId) {
        boolean result;
        String queruResult = (String) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                int existResult = 0;
                String[] bindArgs = {
                        accountId
                };

                String query = MainApplication.getAppContext().getString(R.string.sql_Transaction_ExistByAccountId);

                Cursor cursor = database.rawQuery(query, bindArgs);

                if (cursor != null && cursor.moveToFirst()) {
                    existResult = cursor.getInt(0);
                }
                closeCursor(cursor);

                return String.valueOf(existResult);
            } catch (Exception e) {

                String msg = "TransactionDAO - HasAccountTransaction : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });
        result = queruResult == "1";
        return result;
    }

    public static boolean HasSubAccountTransaction(final String accountId) {
        boolean result;
        String queruResult = (String) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                int existResult = 0;
                String[] bindArgs = {
                        accountId
                };

                String query = MainApplication.getAppContext().getString(R.string.sql_Transaction_ExistBySubAccountId);

                Cursor cursor = database.rawQuery(query, bindArgs);

                if (cursor != null && cursor.moveToFirst()) {
                    existResult = cursor.getInt(0);
                }
                closeCursor(cursor);

                return String.valueOf(existResult);
            } catch (Exception e) {

                String msg = "TransactionDAO - HasAccountTransaction : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });
        result = queruResult == "1";
        return result;
    }

    public static TransactionItem selectByID(final String Id) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        String.valueOf(Id)};

                String query = MainApplication.getAppContext().getString(R.string.sql_Transaction_SelectByTransactionID);

                Cursor cursor = database.rawQuery(query, bindArgs);

                List<TransactionItem> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return (dataList.size() > 0) ? dataList.get(0) : null;
            } catch (Exception e) {

                String msg = "TransactionDAO - selectByID : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });

        return (result == null) ? null : (TransactionItem) result;
    }

    public static double getAccountBalance(final String accountId) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        String.valueOf(accountId),
                        String.valueOf(accountId)
                };

                String query = MainApplication.getAppContext().getString(R.string.sql_Transaction_Balance);
                Cursor cursor = database.rawQuery(query, bindArgs);
                double total = 0;
                if (cursor != null && cursor.moveToFirst()) {
                    total = cursor.getDouble(cursor.getColumnIndex("Total"));
                }
                closeCursor(cursor);

                return total;
            } catch (Exception e) {

                String msg = "TransactionDAO - getAccountBalance : " + e.toString();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });

        return (result == null) ? 0 : (double) result;
    }

    public static double getProjectIncomeBalance(final String projectId) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        String.valueOf(projectId)
                };

                String query = MainApplication.getAppContext().getString(R.string.sql_Transaction_Project_Total_Income);
                Cursor cursor = database.rawQuery(query, bindArgs);
                double total = 0;
                if (cursor != null && cursor.moveToFirst()) {
                    total = cursor.getDouble(cursor.getColumnIndex("Total"));
                }
                closeCursor(cursor);

                return total;
            } catch (Exception e) {

                String msg = "TransactionDAO - getProjectIncomeBalance : " + e.toString();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });

        return (result == null) ? 0 : (double) result;
    }

    public static double getProjectPaymentBalance(final String projectId) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        String.valueOf(projectId)
                };

                String query = MainApplication.getAppContext().getString(R.string.sql_Transaction_Project_Total_Payment);
                Cursor cursor = database.rawQuery(query, bindArgs);
                double total = 0;
                if (cursor != null && cursor.moveToFirst()) {
                    total = cursor.getDouble(cursor.getColumnIndex("Total"));
                }
                closeCursor(cursor);

                return total;
            } catch (Exception e) {

                String msg = "TransactionDAO - getProjectPaymentBalance : " + e.toString();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });

        return (result == null) ? 0 : (double) result;
    }
    public static List<TransactionItem> getAccountPayments(final String accountId) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        String.valueOf(accountId)
                };
                String query = MainApplication.getAppContext().getString(R.string.sql_Transaction_Account_Payments);

                Cursor cursor = database.rawQuery(query, bindArgs);

                List<TransactionItem> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "TransactionDAO - getAccountPayments : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });

        return (List<TransactionItem>) result;
    }

    public static List<TransactionItem> getAccountReceives(final String accountId) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        String.valueOf(accountId)
                };
                String query = MainApplication.getAppContext().getString(R.string.sql_Transaction_Account_Receives);

                Cursor cursor = database.rawQuery(query, bindArgs);

                List<TransactionItem> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "TransactionDAO - getAccountPayments : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });

        return (List<TransactionItem>) result;
    }

    public static TransactionItem insert(final TransactionItem transaction) {

        TransactionItem result;
        result = (TransactionItem) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                transaction.setTransactionId(UUID.randomUUID().toString());
                String[] bindArgs = {
                        String.valueOf(transaction.getTransactionId()),
                        String.valueOf(transaction.getSerial()),
                        String.valueOf(transaction.getRegDateFa()),
                        String.valueOf(transaction.getRegDateGe()),
                        String.valueOf(transaction.getMethod()),
                        String.valueOf(transaction.getAmount()),
                        String.valueOf(transaction.getNote()),
                        String.valueOf(transaction.getIsCash()),
                        String.valueOf(transaction.getTargetAccountId()),
                        String.valueOf(transaction.getTargetAccountTitle()),
                        String.valueOf(transaction.getSourceAccountId()),
                        String.valueOf(transaction.getSourceAccountTitle()),
                        String.valueOf(transaction.getProjectId()),
                        String.valueOf(transaction.getProjectTitle()),
                        String.valueOf(transaction.getPersonId()),
                        String.valueOf(transaction.getPersonTitle()),
                        String.valueOf(transaction.getReminderDateFa()),
                        String.valueOf(transaction.getReminderDateGe()),
                        String.valueOf(transaction.getCheqId()),
                        String.valueOf(transaction.getIsCheqPassed()),
                        String.valueOf("0"),
                        String.valueOf(transaction.getCreatedAt()),
                        String.valueOf(transaction.getUpdatedAt()),
                        String.valueOf(transaction.getEditor()),
                        String.valueOf(transaction.getEditorDeviceId()),
                        String.valueOf(transaction.getTAG()),
                        "",
                        ""
                };
                database.execSQL(MainApplication.getAppContext().getString(R.string.sql_Transaction_Insert), bindArgs);
                return transaction;
            } catch (Exception e) {

                String msg = "TransactionDAO - insert : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return result;
    }

    public static boolean update(final TransactionItem transaction) {
        Boolean result;
        result = (Boolean) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        String.valueOf(transaction.getSerial()),
                        String.valueOf(transaction.getRegDateFa()),
                        String.valueOf(transaction.getRegDateGe()),
                        String.valueOf(transaction.getMethod()),
                        String.valueOf(transaction.getAmount()),
                        String.valueOf(transaction.getNote()),
                        String.valueOf(transaction.getIsCash()),
                        String.valueOf(transaction.getTargetAccountId()),
                        String.valueOf(transaction.getTargetAccountTitle()),
                        String.valueOf(transaction.getSourceAccountId()),
                        String.valueOf(transaction.getSourceAccountTitle()),
                        String.valueOf(transaction.getProjectId()),
                        String.valueOf(transaction.getProjectTitle()),
                        String.valueOf(transaction.getPersonId()),
                        String.valueOf(transaction.getPersonTitle()),
                        String.valueOf(transaction.getReminderDateFa()),
                        String.valueOf(transaction.getReminderDateGe()),
                        String.valueOf(transaction.getCheqId()),
                        String.valueOf(transaction.getIsCheqPassed()),
                        String.valueOf("0"),
                        String.valueOf(transaction.getCreatedAt()),
                        String.valueOf(transaction.getUpdatedAt()),
                        String.valueOf(transaction.getEditor()),
                        String.valueOf(transaction.getEditorDeviceId()),
                        String.valueOf(transaction.getTAG()),
                        "",
                        "",
                        String.valueOf(transaction.getTransactionId()),
                };
                database.execSQL(MainApplication.getAppContext().getString(R.string.sql_Transaction_Update), bindArgs);
                return true;
            } catch (Exception e) {

                String msg = "TransactionDAO - update : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return result;
    }

    private static void closeCursor(Cursor cursor) {
        try {


            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {

            String msg = "TransactionDAO - closeCursor : " + e.getMessage();
            L.e(msg);
            throw new RuntimeException(msg);
        }
    }

    private static TransactionItem cursorToData(Cursor cursor) {
        try {

            int idIndex = cursor.getColumnIndex(Table.COLUMN_ID);
            int serialIndex = cursor.getColumnIndex(Table.COLUMN_SERIAL);
            int dateFaIndex = cursor.getColumnIndex(Table.COLUMN_REG_FA_DATE);
            int dateGeIndex = cursor.getColumnIndex(Table.COLUMN_REG_GE_DATE);
            int methodIndex = cursor.getColumnIndex(Table.COLUMN_METHOD);
            int amountIndex = cursor.getColumnIndex(Table.COLUMN_AMOUNT);
            int infoIndex = cursor.getColumnIndex(Table.COLUMN_NOTE);
            int isCashIndex = cursor.getColumnIndex(Table.COLUMN_IS_CASH);

            int targetAccIndex = cursor.getColumnIndex(Table.COLUMN_TARGET_ACCOUNT_ID);
            int targetTitleIndex = cursor.getColumnIndex(Table.COLUMN_TARGET_ACCOUNT_TITLE);

            int sourceAccIndex = cursor.getColumnIndex(Table.COLUMN_SOURCE_ACCOUNT_ID);
            int sourceTitleIndex = cursor.getColumnIndex(Table.COLUMN_SOURCE_ACCOUNT_TITLE);

            int projectIndex = cursor.getColumnIndex(Table.COLUMN_PROJECT_ID);
            int projectNameIndex = cursor.getColumnIndex(Table.COLUMN_PROJECT_NAME);

            int personIdIndex = cursor.getColumnIndex(Table.COLUMN_PERSON_ID);
            int personNameIndex = cursor.getColumnIndex(Table.COLUMN_PERSON_NAME);

            int reminderFaIndex = cursor.getColumnIndex(Table.COLUMN_REMINDER_FA_DATE);
            int reminderGeIndex = cursor.getColumnIndex(Table.COLUMN_REMINDER_GE_DATE);

            int cheqIdIndex = cursor.getColumnIndex(Table.COLUMN_CHEQUE_ID);
            int isCheqPassedIndex = cursor.getColumnIndex(Table.COLUMN_IS_CHEQUE_PASSED);

            int createdAtIndex = cursor.getColumnIndex(Table.COLUMN_CREATEDAT);
            int updatedAtIndex = cursor.getColumnIndex(Table.COLUMN_UPDATEDAT);
            int tagIndex = cursor.getColumnIndex(Table.COLUMN_TAG);
            int editorIndex = cursor.getColumnIndex(Table.COLUMN_EDITOR);


            TransactionItem item = new TransactionItem();

            item.setTransactionId(cursor.getString(idIndex));
            item.setSerial(cursor.getInt(serialIndex));
            item.setRegDateFa(cursor.getString(dateFaIndex));
            item.setRegDateGe(cursor.getLong(dateGeIndex));
            item.setMethod(cursor.getInt(methodIndex));
            item.setAmount(cursor.getDouble(amountIndex));
            item.setNote(cursor.getString(infoIndex));
            item.setIsCash(cursor.getInt(isCashIndex));

            item.setTargetAccountId(cursor.getString(targetAccIndex));
            item.setTargetAccountTitle(cursor.getString(targetTitleIndex));
            item.setSourceAccountId(cursor.getString(sourceAccIndex));
            item.setSourceAccountTitle(cursor.getString(sourceTitleIndex));


            item.setProjectId(cursor.getString(projectIndex));
            item.setProjectTitle(cursor.getString(projectNameIndex));

            item.setPersonId(cursor.getString(personIdIndex));
            item.setPersonTitle(cursor.getString(personNameIndex));

            item.setReminderDateFa(cursor.getString(reminderFaIndex));
            item.setReminderDateGe(cursor.getDouble(reminderGeIndex));

            item.setCheqId(cursor.getString(cheqIdIndex));
            item.setIsCheqPassed(cursor.getInt(isCheqPassedIndex));
            item.setCreatedAt(cursor.getLong(createdAtIndex));
            item.setUpdatedAt(cursor.getLong(updatedAtIndex));
            item.setTAG(cursor.getString(tagIndex));
            item.setEditor(cursor.getString(editorIndex));


            return item;
        } catch (Exception e) {

            String msg = "TransactionItemDAO - cursorToMasterData : " + e.getMessage();
            L.e(msg);
            throw new RuntimeException(msg);
        }
    }

    private static List<TransactionItem> manageCursor(Cursor cursor) {
        try {
            List<TransactionItem> dataList = new ArrayList<>();

            if (cursor != null) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    TransactionItem account = cursorToData(cursor);
                    dataList.add(account);
                    cursor.moveToNext();
                }
            }
            return dataList;
        } catch (Exception e) {

            String msg = "TransactionDAO - manageCursor : " + e.getMessage();
            L.e(msg);
            throw new RuntimeException(msg);
        }
    }

    public interface Table {

        String COLUMN_ID = "TransactionId";
        String COLUMN_SERIAL = "Serial";
        String COLUMN_REG_FA_DATE = "RegFaDate";
        String COLUMN_REG_GE_DATE = "RegGeDate";
        String COLUMN_METHOD = "Method";
        String COLUMN_AMOUNT = "Amount";
        String COLUMN_NOTE = "Note";
        String COLUMN_IS_CASH = "IsCash";

        String COLUMN_TARGET_ACCOUNT_ID = "TargetAccountId";
        String COLUMN_TARGET_ACCOUNT_TITLE = "TargetAccountTitle";

        String COLUMN_SOURCE_ACCOUNT_ID = "SourceAccountId";
        String COLUMN_SOURCE_ACCOUNT_TITLE = "SourceAccountTitle";

        String COLUMN_PROJECT_ID = "ProjectId";
        String COLUMN_PROJECT_NAME = "ProjectTitle";

        String COLUMN_PERSON_ID = "PersonId";
        String COLUMN_PERSON_NAME = "PersonName";

        String COLUMN_REMINDER_FA_DATE = "ReminderFaDate";
        String COLUMN_REMINDER_GE_DATE = "ReminderGeDate";

        String COLUMN_CHEQUE_ID = "ChequeId";
        String COLUMN_IS_CHEQUE_PASSED = "IsChequePassed";

        String COLUMN_CREATEDAT = "CreatedAt";
        String COLUMN_UPDATEDAT = "UpdatedAt";
        String COLUMN_TAG = "Tag";
        String COLUMN_EDITOR = "Editor";


    }

}