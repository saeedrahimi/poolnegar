package ir.saeedrahimi.poolnegar.database.model;

import java.lang.annotation.Retention;
import java.util.UUID;

import androidx.annotation.IntDef;

import static java.lang.annotation.RetentionPolicy.SOURCE;

public class TransactionItem extends BaseModel
{
  private int isCash = 1;
  private int isCheqPassed;
  private String cheqId;
  private String projectId ;
  private String projectTitle;
  private String personId ;
  private String personTitle;
  private String note;
  private double amount;
  private String regDateFa;
  private long regDateGe;
  private String reminderDateFa;
  private double reminderDateGe;
  private int serial;
  private String transactionId;
  private String checqBankAccId;
  private String cheqSerial;
  private String cheqDate;
  private double m;
  private int method;

  private String targetAccountId;
  private String targetAccountTitle;
  private String sourceAccountId;
  private String sourceAccountTitle;
  private String Title;
  private String TAG;
  private String ImageId;
  private int Sort;

  public TransactionItem(){

    ImageId = UUID.randomUUID().toString();
  }
  public String getTitle() {
    return Title;
  }
  public void setTitle(String title) {
    Title = title;
  }

  public String getTransactionId()
  {
    return this.transactionId;
  }
  public void setTransactionId(String id)
  {
    this.transactionId = id;
  }

  public int getSerial() {return this.serial;}
  public void setSerial(int paramInt) {this.serial = paramInt;}

  @Retention(SOURCE)
  @IntDef({TRANSACTION_METHOD_INCOME, TRANSACTION_METHOD_PAYMENT, TRANSACTION_METHOD_ALL})
  public @interface TransactionMethod {}
  public static final int TRANSACTION_METHOD_INCOME = 0;
  public static final int TRANSACTION_METHOD_PAYMENT = 1;
  public static final int TRANSACTION_METHOD_ALL = 2;

  @TransactionMethod
  public int getMethod() {
    return this.method;
  }

  public void setMethod(@TransactionMethod int set) {
    this.method = set;
  }




  public String getNote()
  {
    return this.note;
  }
  public void setNote(String str)
  {
    this.note = str;
  }

  public double getAmount()
  {
    return this.amount;
  }
  public void setAmount(double amount)
  {
    this.amount = amount;
  }

  public int getIsCheqPassed()
  {
    return this.isCheqPassed;
  }
  public void setIsCheqPassed(int n)
  {
    this.isCheqPassed = n;
  }

  public int getIsCash()
  {
    return this.isCash;
  }
  public void setIsCash(int n)
  {
    this.isCash = n;
  }

  public void setRegDateFa(String regDateFa)
  {
    this.regDateFa = regDateFa;
  }

  public String getRegDateFa()
  {
    return this.regDateFa;
  }

  public String getProjectId()
  {
    return this.projectId;
  }
  public void setProjectId(String paramInt)
  {
    this.projectId = paramInt;
  }

  public String getProjectTitle() {
    return projectTitle;
  }
  public void setProjectTitle(String projectTitle) {
    this.projectTitle = projectTitle;
  }

  public String getCheqId() {return this.cheqId;}
  public void setCheqId(String n)
  {
    this.cheqId = n;
  }

  public String getCheqDate()
  {
    return this.cheqDate;
  }
  public void setCheqDate(String s)
  {
    this.cheqDate = s;
  }

  public String getCheqSerial()
  {
    return this.cheqSerial;
  }
  public void setCheqSerial(String s) { this.cheqSerial = s; }

  public String getChecqBankAccId()
  {
    return this.checqBankAccId;
  }
  public void setChecqBankAccId(String n)
  {
    this.checqBankAccId = n;
  }

  public int getSort() {
    return Sort;
  }
  public void setSort(int sort) {
    Sort = sort;
  }

  public String getTAG() {
    return TAG;
  }
  public void setTAG(String TAG) {
    this.TAG = TAG;
  }



  public double v()
  {
    return this.m;
  }
  public void b(double paramDouble)
  {
    this.m = paramDouble;
  }

  public long getRegDateGe() {
    return regDateGe;
  }

  public void setRegDateGe(long regDateGe) {
    this.regDateGe = regDateGe;
  }

  public String getPersonTitle() {
    return personTitle;
  }

  public void setPersonTitle(String personTitle) {
    this.personTitle = personTitle;
  }

  public String getPersonId() {
    return personId;
  }

  public void setPersonId(String personId) {
    this.personId = personId;
  }

  public double getReminderDateGe() {
    return reminderDateGe;
  }

  public void setReminderDateGe(double reminderDateGe) {
    this.reminderDateGe = reminderDateGe;
  }

  public String getReminderDateFa() {
    return reminderDateFa;
  }

  public void setReminderDateFa(String reminderDateFa) {
    this.reminderDateFa = reminderDateFa;
  }

  public String getTargetAccountId() {
    return targetAccountId;
  }

  public void setTargetAccountId(String targetAccountId) {
    this.targetAccountId = targetAccountId;
  }

  public String getTargetAccountTitle() {
    return targetAccountTitle;
  }

  public void setTargetAccountTitle(String targetAccountTitle) {
    this.targetAccountTitle = targetAccountTitle;
  }

  public String getSourceAccountId() {
    return sourceAccountId;
  }

  public void setSourceAccountId(String sourceAccountId) {
    this.sourceAccountId = sourceAccountId;
  }

  public String getSourceAccountTitle() {
    return sourceAccountTitle;
  }

  public void setSourceAccountTitle(String sourceAccountTitle) {
    this.sourceAccountTitle = sourceAccountTitle;
  }

  public String getImageId() {
    return ImageId;
  }

  public void setImageId(String imageId) {
    ImageId = imageId;
  }


  public void clearAccounts(){
    this.setTargetAccountId("");
    this.setSourceAccountId("");
    this.setTargetAccountTitle("");
    this.setSourceAccountTitle("");
  }
}



