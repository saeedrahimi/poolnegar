package ir.saeedrahimi.poolnegar.database.DAO;

/**
 * Created by Saeed on 3/19/2017.
 */

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import ir.saeedrahimi.poolnegar.Constants;
import ir.saeedrahimi.poolnegar.R;
import ir.saeedrahimi.poolnegar.application.MainApplication;
import ir.saeedrahimi.poolnegar.application.helper.L;
import ir.saeedrahimi.poolnegar.database.AppDatabaseManager;
import ir.saeedrahimi.poolnegar.database.model.Account;
import ir.saeedrahimi.poolnegar.models.AccountMultiLevel;

public class AccountDAO {
    private Context mContext;

    public AccountDAO(Context context) {
        this.mContext = context;
    }

    public static String getCreateTable() {
        return MainApplication.getAppContext().getString(R.string.sql_Account_CreateTable);
    }

    public static String getDropTable() {
        return MainApplication.getAppContext().getString(R.string.sql_Account_DropTable);
    }

    public static void create() {
        AppDatabaseManager.getInstance().executeQuery(database -> {
            try {

                String createCommand = MainApplication.getAppContext().getString(R.string.sql_Account_CreateTable);
                database.execSQL(createCommand);
                return true;
            } catch (Exception e) {

                String msg = "AccountDAO - create : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
    }

    public static boolean deleteById(final String id) {
        Boolean result;
        result = (Boolean) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        id
                };

                String deleteAllCommand = MainApplication.getAppContext().getString(R.string.sql_Account_DeleteById);
                database.execSQL(deleteAllCommand, bindArgs);
                return true;
            } catch (Exception e) {

                String msg = "AccountDAO - deleteById : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return result;

    }

    public static List<Account> getAll() {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String query = MainApplication.getAppContext().getString(R.string.sql_Account_SelectAll);

                Cursor cursor = database.rawQuery(query, null);

                List<Account> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "AccountDAO - selectAll : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });

        return (List<Account>) result;
    }


    public static List<Account> getAllWhere(String where) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String query = MainApplication.getAppContext().getString(R.string.sql_Account_SelectAll);
                String orderPart = MainApplication.getAppContext().getString(R.string.sql_Account_Order_Part);
                query += where + orderPart;
                Cursor cursor = database.rawQuery(query, null);

                List<Account> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "AccountDAO - getAllWhere : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return (List<Account>) result;
    }

    public static List<AccountMultiLevel> getRecursiveByParentId(String parentId)  {

        return getRecursiveByParentId(parentId, true, true);
    }
    public static List<AccountMultiLevel> getRecursiveByParentId(String parentId, boolean isPayable, boolean isReceivable)  {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        parentId,
                        String.valueOf(isPayable ? 1 : 0),
                        String.valueOf(isReceivable ? 1 : 0),
                        parentId,
                        String.valueOf(isPayable ? 1 : 0),
                        String.valueOf(isReceivable ? 1 : 0)
                };
                String query = MainApplication.getAppContext().getString(R.string.sql_Account_SelectRecursiveAllByParentId);
                Cursor cursor = database.rawQuery(query, bindArgs);

                List<AccountMultiLevel> multiLevels = new ArrayList<>();
                List<Account> dataList = manageCursor(cursor);
                Account parentAccount = new Account("temp Title","",0,"",true, true);
                parentAccount.setAccID(parentId);
                multiLevels.addAll(AccountMultiLevel.PopulateChildren(-1, new AccountMultiLevel(-1,parentAccount), dataList));

                closeCursor(cursor);

                return multiLevels.get(0).getChildren();

            } catch (Exception e) {

                String msg = "AccountDAO - getRecursiveByParentId : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });

        return (List<AccountMultiLevel>) result;
    }
    public static List<AccountMultiLevel> getRecursiveById(String accountId) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        accountId
                };
                String query = MainApplication.getAppContext().getString(R.string.sql_Account_SelectRecursiveByAccountId);
                Cursor cursor = database.rawQuery(query, bindArgs);

                List<AccountMultiLevel> multiLevels = new ArrayList<>();
                List<Account> dataList = manageCursor(cursor);
                Account parentAccount = new Account("temp Title","",0,"",true, true);
                parentAccount.setAccID(accountId);
                multiLevels.addAll(AccountMultiLevel.PopulateChildren(-1, new AccountMultiLevel(-1,parentAccount), dataList));

                closeCursor(cursor);

                return multiLevels.get(0).getChildren();

            } catch (Exception e) {

                String msg = "AccountDAO - getRecursiveById : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });

        return (List<AccountMultiLevel>) result;
    }
    public static List<Account> getPayableRootAccounts() {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String query = MainApplication.getAppContext().getString(R.string.sql_Account_SelectParentsWithChildCondition);
                String whereClause = "AND " + Table.COLUMN_PAYABLE + " = 1";
                query.replace(Constants.SQL_VARIABLE.WhereClause, whereClause);
                Cursor cursor = database.rawQuery(query, null);

                List<Account> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "AccountDAO - getPayableRootAccounts : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return (List<Account>) result;
    }

    public static List<Account> getReceivableRootAccounts() {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String query = MainApplication.getAppContext().getString(R.string.sql_Account_SelectParentsWithChildCondition);
                String whereClause = "AND " + Table.COLUMN_RECEIVABLE + " = 1";
                query = query.replace(Constants.SQL_VARIABLE.WhereClause, whereClause);
                Cursor cursor = database.rawQuery(query, null);

                List<Account> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "AccountDAO - getReceivableRootAccounts : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return (List<Account>) result;
    }

    public static List<Account> getProjects(final boolean hideClosed) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String query = MainApplication.getAppContext().getString(R.string.sql_Account_SelectParentsWithChildCondition);
                String whereClause = " AND " + Table.COLUMN_ISPORJECT + " = 1";
                if(hideClosed){
                    whereClause += " AND " + Table.COLUMN_IS_PROJECT_CLOSED + " != 1";
                }
                query = query.replace(Constants.SQL_VARIABLE.WhereClause, whereClause);
                Cursor cursor = database.rawQuery(query, null);

                List<Account> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;

            } catch (Exception e) {

                String msg = "AccountDAO - getReceivableRootAccounts : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return (List<Account>) result;
    }
    public static Account selectByID(final String Id) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        Id};

                String query = MainApplication.getAppContext().getString(R.string.sql_Account_SelectByAccountID);

                Cursor cursor = database.rawQuery(query, bindArgs);

                List<Account> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return (dataList.size() > 0) ? dataList.get(0) : null;
            } catch (Exception e) {

                String msg = "AccountDAO - selectByID : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });

        return (result == null) ? null : (Account) result;
    }


    public static String getTitleByIdRecursive(final String Id) {
        String result;
        result = (String) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String titleResult = "";
                String[] bindArgs = {
                        Id};

                String query = MainApplication.getAppContext().getString(R.string.sql_Account_SelectAccountTitleRecursive);

                Cursor cursor = database.rawQuery(query, bindArgs);

                if (cursor != null && cursor.moveToFirst()) {
                    titleResult = cursor.getString(0);
                }

                closeCursor(cursor);

                return titleResult;
            } catch (Exception e) {

                String msg = "AccountDAO - getTitleByIdRecursive : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });

        return result;
    }

   public static String getTitleById(final String Id) {
        String result;
        result = (String) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String titleResult = "";
                String[] bindArgs = {
                        Id};

                String query = MainApplication.getAppContext().getString(R.string.sql_Account_SelectTitleById);

                Cursor cursor = database.rawQuery(query, bindArgs);

                if (cursor != null && cursor.moveToFirst()) {
                    titleResult = cursor.getString(0);
                }

                closeCursor(cursor);

                return titleResult;
            } catch (Exception e) {

                String msg = "AccountDAO - getTitleById : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });

        return result;
    }
    public static List<Account> getChildren(final String parentId) {
        Object result;
        result = AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                String[] bindArgs = {
                        parentId
                };

                String query = MainApplication.getAppContext().getString(R.string.sql_Account_SelectChildren);
                Cursor cursor = database.rawQuery(query, bindArgs);
                List<Account> dataList = manageCursor(cursor);

                closeCursor(cursor);

                return dataList;
            } catch (Exception e) {

                String msg = "AccountDAO - getChildren : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });

        return (List<Account>) result;
    }

    public static int getChildrenCount(final String parentId) {
        int result;
        result = (int) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                int countResult = 0;
                String[] bindArgs = {
                        parentId
                };

                String query = MainApplication.getAppContext().getString(R.string.sql_Account_CountChildItems);

                Cursor cursor = database.rawQuery(query, bindArgs);

                if (cursor != null && cursor.moveToFirst()) {
                    countResult = cursor.getInt(0);
                }

                closeCursor(cursor);

                return countResult;
            } catch (Exception e) {

                String msg = "AccountDAO - getChildrenCount : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });

        return result;
    }
    public static boolean ExistsInParent(final String parentId, final String acTitle) {
        boolean result;
        String queruResult = (String) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                int existResult = 0;
                String[] bindArgs = {
                        acTitle,
                        parentId
                };

                String query = MainApplication.getAppContext().getString(R.string.sql_Account_Exists_In_Parent);

                Cursor cursor = database.rawQuery(query, bindArgs);

                if (cursor != null && cursor.moveToFirst()) {
                    existResult = cursor.getInt(0);
                }
                closeCursor(cursor);

                return String.valueOf(existResult);
            } catch (Exception e) {

                String msg = "AccountDAO - ExistsInParent : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });
        result = queruResult == "1";
        return result;
    }


    public static boolean HasChild(final String accountId) {
        boolean result;
        String queruResult = (String) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                int existResult = 0;
                String[] bindArgs = {
                        accountId
                };

                String query = MainApplication.getAppContext().getString(R.string.sql_Account_Has_Child);

                Cursor cursor = database.rawQuery(query, bindArgs);

                if (cursor != null && cursor.moveToFirst()) {
                    existResult = cursor.getInt(0);
                }
                closeCursor(cursor);

                return String.valueOf(existResult);
            } catch (Exception e) {

                String msg = "AccountDAO - hasChild : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }

        });
        result = queruResult == "1";
        return result;
    }


    public static Account insert(final Account item) {
        return insert(item, true);
    }
    public static Account insert(final Account item, boolean autoGenerateId) {

        Account result;
        result = (Account) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {
                if(autoGenerateId){
                    item.setAccID(UUID.randomUUID().toString());
                }
                String[] bindArgs = {

                        String.valueOf(item.getAccID()),
                        String.valueOf(item.getTitle()),
                        String.valueOf(item.getImageId()),
                        String.valueOf(item.getBalance()),
                        String.valueOf(item.getParentId()),
                        String.valueOf(item.getBankId()),
                        String.valueOf(item.getIsProject()),
                        String.valueOf(item.isClosed() ? 1 : 0),
                        String.valueOf(item.getProjectDate()),
                        String.valueOf(item.getInfo()),
                        String.valueOf(item.getIsPayable()),
                        String.valueOf(item.getIsReceivable()),
                        String.valueOf(item.getIsLocked()),
                        String.valueOf(item.getIsSystematic()),
                        String.valueOf(item.getTag()),
                        String.valueOf(""),
                        String.valueOf(item.getCreatedAt()),
                        String.valueOf(item.getUpdatedAt()),
                        String.valueOf(""),
                        String.valueOf(""),
                        String.valueOf(""),


                };
                database.execSQL(MainApplication.getAppContext().getString(R.string.sql_Account_Insert), bindArgs);
                return item;
            } catch (Exception e) {

                String msg = "AccountDAO - insert : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return result;
    }

    public static boolean update(final Account item) {
        Boolean result;
        result = (Boolean) AppDatabaseManager.getInstance().executeQuery(database -> {
            try {


                String[] bindArgs = {
                        String.valueOf(item.getTitle()),
                        String.valueOf(item.getImageId()),
                        String.valueOf(item.getBalance()),
                        String.valueOf(item.getParentId()),
                        String.valueOf(item.getBankId()),
                        String.valueOf(item.getIsProject()),
                        String.valueOf(item.isClosed() ? 1 : 0),
                        String.valueOf(item.getProjectDate()),
                        String.valueOf(item.getInfo()),
                        String.valueOf(item.getIsPayable()),
                        String.valueOf(item.getIsReceivable()),
                        String.valueOf(item.getIsLocked()),
                        String.valueOf(item.getIsSystematic()),
                        String.valueOf(item.getTag()),
                        String.valueOf(""),
                        String.valueOf(item.getCreatedAt()),
                        String.valueOf(item.getUpdatedAt()),
                        String.valueOf(""),
                        String.valueOf(""),
                        String.valueOf(""),

                        String.valueOf(item.getAccID())
                };
                database.execSQL(MainApplication.getAppContext().getString(R.string.sql_Account_Update), bindArgs);
                return true;
            } catch (Exception e) {

                String msg = "AccountDAO - update : " + e.getMessage();
                L.e(msg);
                throw new RuntimeException(msg);
            }
        });
        return result;
    }

    protected static void closeCursor(Cursor cursor) {
        try {


            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {

            String msg = "AccountDAO - closeCursor : " + e.getMessage();
            L.e(msg);
            throw new RuntimeException(msg);
        }
    }

    protected static Account cursorToData(Cursor cursor) {
        try {

            int idIndex = cursor.getColumnIndex(Table.COLUMN_ID);
            int titleIndex = cursor.getColumnIndex(Table.COLUMN_TITLE);
            int imageIdIndex = cursor.getColumnIndex(Table.COLUMN_IMAGEID);
            int parentIdIndex = cursor.getColumnIndex(Table.COLUMN_PARENT_ACCOUNT_ID);
            int bankIdIndex = cursor.getColumnIndex(Table.COLUMN_BANK_ID);
            int isProjectIndex = cursor.getColumnIndex(Table.COLUMN_ISPORJECT);
            int isProjectClosedIndex = cursor.getColumnIndex(Table.COLUMN_IS_PROJECT_CLOSED);
            int infoIndex = cursor.getColumnIndex(Table.COLUMN_INFO);
            int isLockedIndex = cursor.getColumnIndex(Table.COLUMN_ISLOCKED);
            int isSystematicIndex = cursor.getColumnIndex(Table.COLUMN_ISSYSTEMATIC);
            int payableIndex = cursor.getColumnIndex(Table.COLUMN_PAYABLE);
            int receivableIndex = cursor.getColumnIndex(Table.COLUMN_RECEIVABLE);
            int balanceIndex = cursor.getColumnIndex(Table.COLUMN_BALANCE);

            int projectDateIndex = cursor.getColumnIndex(Table.COLUMN_PROJECT_DATE);
            int createdIndex = cursor.getColumnIndex(Table.COLUMN_CREATEDAT);
            int updatedIndex = cursor.getColumnIndex(Table.COLUMN_UPDATEDAT);
            int pathIndex = cursor.getColumnIndex(Table.COLUMN_PATH);


            Account item = new Account();
            item.setAccID(cursor.getString(idIndex));
            item.setTitle(cursor.getString(titleIndex));
            item.setImageId(cursor.getString(imageIdIndex));
            item.setParentId(cursor.getString(parentIdIndex));
            item.setBankId(cursor.getString(bankIdIndex));
            item.setIsProject(cursor.getInt(isProjectIndex));
            item.setClosed(cursor.getInt(isProjectClosedIndex));
            item.setInfo(cursor.getString(infoIndex));
            item.setIsLocked(cursor.getInt(isLockedIndex));
            item.setIsSystematic(cursor.getInt(isSystematicIndex));
            item.setIsPayable(cursor.getInt(payableIndex));
            item.setIsReceivable(cursor.getInt(receivableIndex));
            item.setBalance(cursor.getDouble(balanceIndex));
            item.setProjectDate(cursor.getString(projectDateIndex));
            item.setCreatedAt(cursor.getLong(createdIndex));
            item.setUpdatedAt(cursor.getLong(updatedIndex));
            if(pathIndex != -1){
                item.setPath(cursor.getString(pathIndex));
            }


            return item;
        } catch (Exception e) {

            String msg = "AccountDAO - cursorToMasterData : " + e.getMessage();
            L.e(msg);
            throw new RuntimeException(msg);
        }
    }

    protected static List<Account> manageCursor(Cursor cursor) {
        try {
            List<Account> dataList = new ArrayList<>();

            if (cursor != null) {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    Account account = cursorToData(cursor);
                    dataList.add(account);
                    cursor.moveToNext();
                }
            }
            return dataList;
        } catch (Exception e) {

            String msg = "AccountDAO - manageCursor : " + e.getMessage();
            L.e(msg);
            throw new RuntimeException(msg);
        }
    }


    public interface Table {
        String COLUMN_ID = "AccountId";
        String COLUMN_TITLE = "AccountName";
        String COLUMN_IMAGEID = "ImageId";
        String COLUMN_PARENT_ACCOUNT_ID = "ParentAccountId";
        String COLUMN_BANK_ID = "BankId";
        String COLUMN_ISPORJECT = "IsProject";
        String COLUMN_IS_PROJECT_CLOSED = "IsProjectClosed";
        String COLUMN_PROJECT_DATE = "ProjectDate";
        String COLUMN_INFO = "Info";
        String COLUMN_ISLOCKED = "IsLocked";
        String COLUMN_ISSYSTEMATIC = "IsSystematic";
        String COLUMN_PAYABLE = "IsPayable";
        String COLUMN_RECEIVABLE = "IsReceivable";
        String COLUMN_BALANCE = "Balance";


        String COLUMN_CREATEDAT = "CreatedAt";
        String COLUMN_UPDATEDAT = "UpdatedAt";

        String COLUMN_PATH = "Path";



    }

}