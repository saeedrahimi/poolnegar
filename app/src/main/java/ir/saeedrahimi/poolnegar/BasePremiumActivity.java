package ir.saeedrahimi.poolnegar;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.viewbinding.ViewBinding;
import ir.devage.hamrahpay.HamrahPay;
import ir.devage.hamrahpay.LastPurchase;
import ir.devage.hamrahpay.SupportInfo;
import ir.saeedrahimi.poolnegar.ui.activities.Activity_GeneralBase;


public abstract class BasePremiumActivity<T extends ViewBinding> extends Activity_GeneralBase<T> {


  private void checkPremium(){

    if (!HamrahPay.isPremium(this,Constants.paySKU)) {        // Check If User Have Premium Key
      AlertDialog.Builder builder = new AlertDialog.Builder(this);
      // Add the buttons
      builder.setPositiveButton(R.string.activate, new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
          new HamrahPay(BasePremiumActivity.this)
                  .sku(Constants.paySKU)
                  .verificationType(HamrahPay.EMAIL_VERIFICATION)
                  .pageTopColor(Color.parseColor("#2ecc71"))     // ActionBar Color Of PayActivity
                  .pageTitleColor(Color.WHITE)        // ActionBar Title Color Of PayActivity
                  .listener(
                          new HamrahPay.Listener() {        // لیسنر برای آگاهی شما از موفق بودن یا نبودن پرداخت
                            @Override
                            public void onErrorOccurred(String status, String message) {
                              // مشکلی در پرداخت روی داده است یا کاربر پرداخت را انجام نداده است
                              Toast.makeText(BasePremiumActivity.this,message,Toast.LENGTH_SHORT).show();
                              Log.e("HamrahPay", status + ": " + message);
                            }

                            @Override
                            public void onPaymentSucceed(String payCode) {
                              // کاربر با موفقیت پرداخت را انجام داده است
                              Toast.makeText(BasePremiumActivity.this,"پرداخت موفقیت آمیز بوده است",Toast.LENGTH_SHORT).show();
                              Log.i("HamrahPay", "payCode: " + payCode);
                            }

                            @Override
                            public void onGetLastPurchaseInfo(LastPurchase lastPurchase) {

                            }

                            @Override
                            public void onGetSupportInfo(SupportInfo supportInfo) {

                            }

                            @Override
                            public void onGetDeviceID(String deviceID) {

                            }
                          }
                  )
                  .startPayment();    // Start Payment And Library Will Do The Rest ;)

        }
      });
      builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int id) {
          BasePremiumActivity.this.finish();
        }
      });
      builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialogInterface) {
          //	BasePremiumActivity.this.finish();
        }
      });
      builder.setCancelable(false);
      builder.setMessage("کابر گرامی، جهت دسترسی به تمامی امکانات، لطفا نرم‌افزار خود را فعال نمایید.");
      AlertDialog dialog = builder.create();
      dialog.show();
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    checkPremium();
  }

  @Override
  protected void onResume() {
    super.onResume();
    checkPremium();
  }


}