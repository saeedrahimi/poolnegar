package ir.saeedrahimi.support;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import ir.saeedrahimi.poolnegar.R;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Saeed on 12/29/2015.
 */
public class Numpad implements View.OnClickListener{
    private TreeMap<Integer,Character> mTreeMap;
    private LinearLayout mLinearlayout;
    private ViewGroup mViewgroup;
    private Context mContext;
    private LinkedList<NumberStruct> mLinkedList;
    private static double mDouble;
    private static DecimalFormat mDecimalFormat;
    public Numpad(Context context, LinearLayout linearLayout, ViewGroup viewGroup){
        mContext = context;
        mLinearlayout = linearLayout;
        mViewgroup = viewGroup;
        setNumber(0);
        updateText();
        fillTreemap();
        prepare();
        g();
    }
    public void setNumber(double number)
    {
        this.mLinkedList = new LinkedList();
        NumberStruct numberStruct = new NumberStruct();
        numberStruct.decimal = getDecimalPoint(number);
        numberStruct.operand = 0;
        numberStruct.number = number;
        this.mLinkedList.add(numberStruct);
    }
    public double getNumber()
    {
        //b();
        if (this.mLinkedList.isEmpty()) {
            return 0.0D;
        }
        return mLinkedList.getFirst().number;
    }
    private void fillTreemap()
    {
        this.mTreeMap = new TreeMap();
        this.mTreeMap.put(R.id.partial_number_input_n0, Character.valueOf('0'));
        this.mTreeMap.put(R.id.partial_number_input_n1, Character.valueOf('1'));
        this.mTreeMap.put(R.id.partial_number_input_n2, Character.valueOf('2'));
        this.mTreeMap.put(R.id.partial_number_input_n3, Character.valueOf('3'));
        this.mTreeMap.put(R.id.partial_number_input_n4, Character.valueOf('4'));
        this.mTreeMap.put(R.id.partial_number_input_n5, Character.valueOf('5'));
        this.mTreeMap.put(R.id.partial_number_input_n6, Character.valueOf('6'));
        this.mTreeMap.put(R.id.partial_number_input_n7, Character.valueOf('7'));
        this.mTreeMap.put(R.id.partial_number_input_n8, Character.valueOf('8'));
        this.mTreeMap.put(R.id.partial_number_input_n9, Character.valueOf('9'));
        this.mTreeMap.put(R.id.partial_number_input_add, Character.valueOf('+'));
        this.mTreeMap.put(R.id.partial_number_input_decimal, Character.valueOf('.'));
        this.mTreeMap.put(R.id.partial_number_input_minus, Character.valueOf('-'));
        this.mTreeMap.put(R.id.partial_number_input_multiply, Character.valueOf('x'));
        this.mTreeMap.put(R.id.partial_number_input_divide, Character.valueOf('/'));
    }
    public void updateText()
    {
        ((TextView)this.mViewgroup.findViewById(R.id.partial_number_input_viewer_text)).setText(getStyledNumber());
    }
    public Spanned getStyledNumber()
    {
        NumberStruct numStruct;
        Iterator<NumberStruct> numberIterator = mLinkedList.iterator();
        String str = "";
        while (numberIterator.hasNext()) {
            numStruct = numberIterator.next();
            str = str + getStructNumber(numStruct);
        }
        return Html.fromHtml(str);
    }
    private DecimalFormat formatNumber(int paramInt)
    {
        String str = "###,##0";
        if (paramInt >= 1) {
            str = str + ".";
        }
        for (;;)
        {
            if (paramInt <= 1)
            {
                DecimalFormatSymbols localDecimalFormatSymbols = DecimalFormatSymbols.getInstance(Locale.ENGLISH);
                localDecimalFormatSymbols.setDecimalSeparator('/');

                return new DecimalFormat(str, localDecimalFormatSymbols);
            }
            str = str + "0";
            paramInt--;
        }
    }

    public static DecimalFormat formatNumber()
    {
        DecimalFormatSymbols localDecimalFormatSymbols;
        String str;
        if (mDecimalFormat == null)
        {
            localDecimalFormatSymbols = new DecimalFormatSymbols(Locale.getDefault());
            localDecimalFormatSymbols.setDecimalSeparator('/');
            str = "###,###.";
            for (int j = 0;j <= 2; j++)
            {
                str = str + "#";
                mDecimalFormat = new DecimalFormat(str, localDecimalFormatSymbols);
            }
        }
        return mDecimalFormat;

    }
    private String getStructNumber(NumberStruct paramu)
    {
        String str;
        int textColorId=this.mContext.getResources().getColor(R.color.operand_text);
        int operandStringId=0;
        Locale locale = Locale.getDefault();
        if (paramu.operand == 0)
        {
            textColorId = this.mContext.getResources().getColor(R.color.default_text);
            str = formatNumber(paramu.decimal).format(paramu.number);
            return String.format(locale, "<font color=\"#%s\">%s</font>", new String[]{Integer.toHexString(textColorId).substring(2),str});
        }
        if (paramu.operand == 1) {
            operandStringId = R.string.input_add;
        }else if (paramu.operand == 2) {
            operandStringId = R.string.input_minus;
        } else if (paramu.operand == 3) {
            operandStringId = R.string.input_multiply;
        } else if (paramu.operand == 4) {
            operandStringId = R.string.input_divide;
        }
        str = this.mContext.getResources().getString(operandStringId);
        return String.format(locale, "<font color=\"#%s\">%s</font>", new String[]{Integer.toHexString(textColorId).substring(2),str});
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.partial_number_input_viewer_backspace:
                backPressed();
                break;
            case R.id.partial_number_input_equal:
                break;
            case R.id.partial_number_input_hide:
                break;

        }
    }
    public static double checkDouble()
    {
        if (mDouble == 0.0D) {
            mDouble = Math.pow(0.1D, 4.0D);
        }
        return mDouble;
    }
    private void backPressed() {
        if (this.mLinkedList.isEmpty()) {
            return;
        }
        NumberStruct localu = this.mLinkedList.getLast();
        if (localu.operand != 0)
        {
            this.mLinkedList.removeLast();
            updateText();
            return;
        }
        if ((localu.decimal == 0) && (localu.number <= 9.0D + checkDouble()))
        {
            this.mLinkedList.removeLast();
            updateText();
            return;
        }
        b(localu);

    }
    private void b(NumberStruct paramu)
    {
        if (paramu.decimal == 0)
        {
            paramu.number = Math.floor(0.1D * paramu.number);
            return;
        }
        if (paramu.decimal == 1)
        {
            paramu.number = 0;
            return;
        }
        double d1 = Math.pow(0.1D, -2 + paramu.decimal);
        paramu.number = (d1 * Math.floor(paramu.number / d1));
        paramu.decimal = (-1 + paramu.decimal);
    }

    private void prepare()
    {

        View btnBack = this.mViewgroup.findViewById(R.id.partial_number_input_viewer_backspace);
        View btnEqal = this.mLinearlayout.findViewById(R.id.partial_number_input_equal);
        btnBack.setOnClickListener(this);
        btnBack.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mLinkedList.clear();
                updateText();
                return false;
            }
        });
        btnEqal.setOnClickListener(this);
        fillNumpad();
    }

    private void g()
    {
        Iterator<Integer> localIterator1 = this.mTreeMap.keySet().iterator();

        while (localIterator1.hasNext()) {
            final TextView tv = (TextView)this.mLinearlayout.findViewById(localIterator1.next());
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Toast.makeText(mContext, "Button - " + ((TextView) v).getText(), Toast.LENGTH_SHORT);
                    Character currentChar =  mTreeMap.get(Integer.valueOf(v.getId())).charValue();
                    if ((!mLinkedList.isEmpty()) && (mLinkedList.getLast()).operand == 0) {
                        if (((currentChar >= '0') && (currentChar <= '9')) || (currentChar == '.')) {
                            NumberStruct lastNum = mLinkedList.getLast();
                            if ((currentChar >= '0') && (currentChar <= '9')) {
                                if (lastNum.decimal == 0) {
                                    lastNum.number = (10.0D * lastNum.number + (currentChar - '0'));
                                }
                            }
                            if ((currentChar != '.') || (lastNum.decimal != 0)) {
                                if (lastNum.decimal > 2) {
                                    lastNum.number += Math.pow(0.1D, lastNum.decimal) * (currentChar - '0');
                                    lastNum.decimal = (1 + lastNum.decimal);
                                }
                            }
                            else{
                                lastNum.decimal = 1;
                            }

                        }
                    }
                    NumberStruct num = new NumberStruct();
                    num.operand=0; num.decimal=0;
                    num.number = Integer.valueOf(currentChar);
                    mLinkedList.addLast(num);
                    updateText();
                    Log.d("Iterator==>>", "BTN - > " + ((TextView) v).getText());
                }
            });
            this.mLinearlayout.findViewById(R.id.partial_number_input_equal).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext, "Button - " + ((TextView) v).getText(), Toast.LENGTH_SHORT);
                    Log.d("Iterator==>>", "BTN - > " + ((TextView) v).getText());
                }
            });


        }
    }
    private void fillNumpad()
    {
        Iterator treeIterator = mTreeMap.entrySet().iterator();
        while (treeIterator.hasNext()) {
            Map.Entry<Integer,Character> item = (Map.Entry)treeIterator.next();
            int currentKey = (item.getKey()).intValue();
            char currentChar = (item.getValue()).charValue();
            if ((currentChar >= '0') && (currentChar <= '9')) {
                String text = Character.toString(currentChar);// formatNumber().format('?' + currentChar);
                ((TextView)this.mLinearlayout.findViewById(currentKey)).setText(text);
            }
        }
        ((TextView)this.mLinearlayout.findViewById(R.id.partial_number_input_decimal)).setText("/");
    }

    private int getDecimalPoint(double paramDouble)
    {
        double d1 = paramDouble - Math.floor(paramDouble);
        int i = 0;
        for (int j = 1;; j++)
        {
            if (j > 2) {
                return i;
            }
            int k = (int)Math.round(d1 * Math.pow(10.0D, j));
            if (k - 10 * (k / 10) != 0) {
                i = j + 1;
            }
        }
    }
    private NumberStruct getStructFromChar(char paramChar)
    {
        NumberStruct struct = new NumberStruct();
        if ((paramChar >= '0') && (paramChar <= '9')) {
            struct.operand = 0;
            struct.number = (paramChar - '0');
            struct.decimal = 0;
            return struct;
        }
        if (paramChar == '.')
            {
                struct.operand = 0;
                struct.number = 0.0D;
                struct.decimal = 1;
            }
            if (paramChar == '+')   struct.operand = 1;
            if (paramChar == '-')   struct.operand = 2;
            if (paramChar == 'x')   struct.operand = 3;
            if (paramChar == '/')   struct.operand = 4;
        return struct;
    }

    class NumberStruct
            implements Serializable
    {
        public int operand;//a
        public double number;//b
        public int decimal;//c
    }
}
