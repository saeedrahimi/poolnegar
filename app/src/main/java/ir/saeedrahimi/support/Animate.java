package ir.saeedrahimi.support;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;
import java.util.LinkedList;
/**
 * Created by Saeed on 12/29/2015.
 */
public class Animate {
    boolean isVisible;
    LinkedList b = new LinkedList();
    private int c;
    private LinearLayout d;

    public Animate(LinearLayout paramLinearLayout, int paramInt, boolean isVisible)
    {
        this.d = paramLinearLayout;
        this.isVisible = isVisible;
        this.c = paramInt;
        e();
    }

    private void e()
    {
        if (this.isVisible)
        {
            this.d.setTranslationY(0.0F);
            return;
        }
        this.d.setTranslationY(h());
    }

    private void f()
    {
        LinearLayout localLinearLayout = this.d;
        float[] arrayOfFloat = new float[2];
        arrayOfFloat[0] = h();
        arrayOfFloat[1] = 0.0F;
        ObjectAnimator localObjectAnimator = ObjectAnimator.ofFloat(localLinearLayout, "translationY", arrayOfFloat);
        localObjectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        localObjectAnimator.setDuration(200L);
        localObjectAnimator.start();
        AnimatorSet localAnimatorSet = new AnimatorSet();
        localAnimatorSet.playTogether(this.b);
        localAnimatorSet.start();
    }

    private void g()
    {
        LinearLayout localLinearLayout = this.d;
        float[] arrayOfFloat = new float[2];
        arrayOfFloat[0] = 0.0F;
        arrayOfFloat[1] = h();
        ObjectAnimator localObjectAnimator = ObjectAnimator.ofFloat(localLinearLayout, "translationY", arrayOfFloat);
        localObjectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        localObjectAnimator.setDuration(200L);
        localObjectAnimator.start();
    }

    private int h()
    {
        return this.c;
    }

    public void a()
    {
        if (d())
        {
            f();
            this.isVisible = true;
        }
    }

    public void a(Object paramObject, float paramFloat)
    {
        float f = paramFloat * (this.c / 3);
        ObjectAnimator localObjectAnimator1 = ObjectAnimator.ofFloat(paramObject, "translationY", new float[] { 0.0F, f });
        ObjectAnimator localObjectAnimator2 = ObjectAnimator.ofFloat(paramObject, "translationY", new float[] { f, 0.0F });
        localObjectAnimator2.setDuration(300L);
        localObjectAnimator1.setDuration(50L);
        AnimatorSet localAnimatorSet = new AnimatorSet();
        localAnimatorSet.play(localObjectAnimator1).before(localObjectAnimator2);
        this.b.add(localAnimatorSet);
    }

    public void b()
    {
        if (getIsVisible())
        {
            g();
            this.isVisible = false;
        }
    }

    public boolean getIsVisible()
    {
        return this.isVisible;
    }

    public boolean d()
    {
        return !this.isVisible;
    }
}
