﻿CREATE UNIQUE INDEX actindex ON Activity (
    Act_info,
    Act_date COLLATE NOCASE DESC,
    Act_ser DESC,
    Act_id DESC
);

CREATE INDEX acc_index ON accounts (
    Ac_regDate,
    Ac_title COLLATE NOCASE,
    Ac_id
);
CREATE INDEX deftrans_index ON DefaultTransactions (
    Act_id,
    Title,
    Act_date
);
CREATE INDEX [] ON cheq (
    ch_activity_id DESC,
    ch_id DESC,
    ch_serial DESC
);
alter table Activity add column image_path TEXT